using System;
using Xunit;
using SunEater.Support;

namespace UnitTests
{
    public class MultiSetTest
    {
        private class SetItem
        { }

        [Fact]
        public void DefaultTest()
        {
            MultiSet<SetItem> multiSet = new MultiSet<SetItem>();
            Assert.Equal(0, multiSet.UniqueCount);
        }
    }
}
