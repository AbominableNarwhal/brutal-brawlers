// GENERATED AUTOMATICALLY FROM 'Assets/Input System/InputMaster.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputMaster : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputMaster()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputMaster"",
    ""maps"": [
        {
            ""name"": ""Card Actions"",
            ""id"": ""993ebea9-ea30-47e5-891e-4b3f9565934c"",
            ""actions"": [
                {
                    ""name"": ""Click"",
                    ""type"": ""Button"",
                    ""id"": ""d1acd882-c0a7-467e-959c-0651a9130072"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""fbcc6749-e771-474b-a616-2dd6af523a0f"",
                    ""path"": ""<Mouse>/press"",
                    ""interactions"": ""Tap"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Click"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Card Actions
        m_CardActions = asset.FindActionMap("Card Actions", throwIfNotFound: true);
        m_CardActions_Click = m_CardActions.FindAction("Click", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Card Actions
    private readonly InputActionMap m_CardActions;
    private ICardActionsActions m_CardActionsActionsCallbackInterface;
    private readonly InputAction m_CardActions_Click;
    public struct CardActionsActions
    {
        private @InputMaster m_Wrapper;
        public CardActionsActions(@InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Click => m_Wrapper.m_CardActions_Click;
        public InputActionMap Get() { return m_Wrapper.m_CardActions; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CardActionsActions set) { return set.Get(); }
        public void SetCallbacks(ICardActionsActions instance)
        {
            if (m_Wrapper.m_CardActionsActionsCallbackInterface != null)
            {
                @Click.started -= m_Wrapper.m_CardActionsActionsCallbackInterface.OnClick;
                @Click.performed -= m_Wrapper.m_CardActionsActionsCallbackInterface.OnClick;
                @Click.canceled -= m_Wrapper.m_CardActionsActionsCallbackInterface.OnClick;
            }
            m_Wrapper.m_CardActionsActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Click.started += instance.OnClick;
                @Click.performed += instance.OnClick;
                @Click.canceled += instance.OnClick;
            }
        }
    }
    public CardActionsActions @CardActions => new CardActionsActions(this);
    public interface ICardActionsActions
    {
        void OnClick(InputAction.CallbackContext context);
    }
}
