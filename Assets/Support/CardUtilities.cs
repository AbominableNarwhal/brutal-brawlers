﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace SunEater.Support
{
    public static class CardUtilities
    {
        public static string DeckDirectory
        {
            get { return Application.persistentDataPath + "/Decks"; }
        }

        public static void SaveDeck(Deck deck)
        {
            if (!Directory.Exists(DeckDirectory))
            {
                Directory.CreateDirectory(DeckDirectory);
            }

            string path = ConvertDeckNameToPath(deck.name);
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Create);

            DeckData data = new DeckData(deck);
            formatter.Serialize(stream, data);
            stream.Close();

            Debug.Log("Saved Dack to " + path);
        }


        public static async Task<Deck> LoadDeckByName(string name)
        {
            var task = LoadDeck(ConvertDeckNameToPath(name));
            await task;
            return task.Result;
        }

        public static async Task<Deck> LoadDeck(string path)
        {
            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);

                DeckData deckData = formatter.Deserialize(stream) as DeckData;
                stream.Close();

                var task = deckData.CreateDeck();
                await task;

                return task.Result;
            }
            else
            {
                Debug.Log("Deck file not found!");
                return null;
            }
        }

        public static string[] GetSavedDeckNames()
        {
            if (!Directory.Exists(DeckDirectory))
            {
                Directory.CreateDirectory(DeckDirectory);
                return new string[0];
            }

            var nameList = new List<string>();
            foreach (var filePath in Directory.EnumerateFiles(DeckDirectory))
            {
                nameList.Add(Path.GetFileNameWithoutExtension(filePath));
            }

            return nameList.ToArray();
        }

        private static string ConvertDeckNameToPath(string deckName)
        {
            return DeckDirectory + "/" + deckName + ".dk";
        }
    }
}
