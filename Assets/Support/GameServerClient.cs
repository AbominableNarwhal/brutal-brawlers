﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using UnityEngine;

namespace SunEater.Support
{
    public static class GameServerClient
    {
        public static string HostName = "192.168.0.14";

        private readonly static int Port = 9000;
        private static string playerName;

        public static bool IsConnected
        { get; set; } = false;

        private static Thread networkThread;
        private volatile static Queue<string> writeList;
        private volatile static Queue<string> readList;

        public static void TryConnect(string name, string ipAddress = "192.168.0.14")
        {
            playerName = name;
            HostName = ipAddress;
            networkThread = new Thread(new ThreadStart(ProcessNetork))
            {
                IsBackground = true,
                Priority = System.Threading.ThreadPriority.Normal
            };

            networkThread.Start();
        }

        public static void TryDisconect()
        {
            IsConnected = false;
        }

        public static ServerMessage Read()
        {
            Monitor.Enter(readList);

            string message = null;
            if (readList.Count > 0)
            {
                message = readList.Dequeue();
            }
            Monitor.Exit(readList);

            return message != null ? new ServerMessage(message) : null;
        }

        public static ServerMessage Peek()
        {
            Monitor.Enter(readList);

            string message = null;
            if (readList.Count > 0)
            {
                message = readList.Peek();
            }
            Monitor.Exit(readList);

            return message != null ? new ServerMessage(message) : null;
        }

        public static void Write(string name, object message)
        {
            string jsonString = JsonUtility.ToJson(message);
            WriteString(name, jsonString);
        }

        public static void WriteString(string message)
        {
            WriteString(null, message);
        }

        public static void WriteString(string name, string message)
        {
            Monitor.Enter(writeList);
            if (name != null)
            {
                writeList.Enqueue(name + ":" + message);
            }
            else
            {
                writeList.Enqueue(message);
            }
            Monitor.Exit(writeList);
        }

        private static void ProcessNetork()
        {
            try
            {
                //First connect to the server
                var client = new TcpClient();
                client.Connect(HostName, Port);
                IsConnected = true;
                
                writeList = new Queue<string>();
                readList = new Queue<string>();

                StreamWriter writer = new StreamWriter(client.GetStream());
                StreamReader reader = new StreamReader(client.GetStream());

                writer.AutoFlush = true;
                writer.WriteLine(playerName);

                while (IsConnected)
                {
                    // Is there something to write?
                    Monitor.Enter(writeList);

                    if (writeList.Count > 0)
                    {
                        while (writeList.Count > 0)
                        {
                            string message = writeList.Dequeue();

                            try
                            {
                                writer.WriteLine(message);
                            }
                            catch (Exception e)
                            {
                                Debug.LogError(e);
                                IsConnected = false;
                            }
                        }
                    }

                    Monitor.Exit(writeList);

                    // Is there something to read?
                    if (client.Available > 0)
                    {
                        string message = reader.ReadLine();

                        if (message == "ping")
                        {
                            Monitor.Enter(writeList);
                            writer.WriteLine(message);
                            Monitor.Exit(writeList);
                        }
                        else if (message != null)
                        {
                            AddRead(message);
                        }
                    }
                }

                writer.Close();
                reader.Close();
                client.Close();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        private static void AddRead(string message)
        {
            Monitor.Enter(readList);
            readList.Enqueue(message);
            Monitor.Exit(readList);
        }
    }

    public class ServerMessage
    {

        public string Protocol
        {
            get; private set;
        }

        public string Content
        {
            get; private set;
        }

        private string message;

        public ServerMessage(string _message)
        {
            message = _message;
            Protocol = SubMessage(0, message.IndexOf(':'));
            Content = SubMessage(message.IndexOf(':'));
        }

        public T DeserializeJsonMessage<T>()
        {
            return JsonUtility.FromJson<T>(Content);
        }

        private string SubMessage(int start)
        {
            int start1 = start + 1;
            return SubMessage(start1, message.Length - start1);
        }

        private string SubMessage(int start, int end)
        {
            if (start >= message.Length)
            {
                return "";
            }

            if (end == -1)
            {
                return message;
            }

            return message.Substring(start, end);
        }
    }

    public class TimeKeep
    {
        private DateTime startTime;

        public TimeKeep()
        {
            Reset();
        }

        public void Reset()
        {
            startTime = DateTime.Now;
        }

        public bool IsTimeUp(double milliseconds)
        {
            return (DateTime.Now - startTime).TotalMilliseconds >= milliseconds;
        }
    }
}
