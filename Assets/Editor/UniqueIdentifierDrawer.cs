﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;

[CustomPropertyDrawer(typeof(UniqueIdentifierAttribute))]
public class UniqueIdentifierDrawer : PropertyDrawer
{
    public static readonly string AbilityLabel = "Ability";
    public static readonly string FighterLabel = "Fighter";
    public static readonly string PowerLabel = "Power";
    public static readonly string EquipmentLabel = "Equipment";
    public static readonly string AuraLabel = "Aura";
    public static readonly string CardLabel = "Card";

    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
    {
        string assetPath = AssetDatabase.GetAssetPath(prop.serializedObject.targetObject.GetInstanceID());
        string uniqueId = AssetDatabase.AssetPathToGUID(assetPath);

        prop.stringValue = uniqueId;

        Rect textFieldPosition = position;
        textFieldPosition.height = 16;
        DrawLabelField(textFieldPosition, prop, label);
    }

    void DrawLabelField(Rect position, SerializedProperty prop, GUIContent label)
    {
        EditorGUI.LabelField(position, label, new GUIContent(prop.stringValue));
    }

    [MenuItem("Tools/Refresh Addressables")]
    public static void RefreshAddressables()
    {
        var settings = AddressableAssetSettingsDefaultObject.Settings;
        var group = settings.FindGroup("Packed Assets");
        var guids = AssetDatabase.FindAssets("t:Card", new[] { "Assets/Card Data/Objects" });

        var entriesAdded = new List<AddressableAssetEntry>();
        for (int i = 0; i < guids.Length; i++)
        {
            var entry = settings.CreateOrMoveEntry(guids[i], group, readOnly: false, postEvent: false);
            var card = entry.TargetAsset as Card;
            entry.SetAddress(card.UniqueId, false);
            entry.labels.Add(CardLabel);

            switch (card.type)
            {
                case Card.Type.AbilityCard:
                    entry.labels.Add(AbilityLabel);
                    break;
                case Card.Type.FighterCard:
                    entry.labels.Add(FighterLabel);
                    break;
                case Card.Type.PowerCard:
                    entry.labels.Add(PowerLabel);
                    break;
                case Card.Type.EquipmentCard:
                    entry.labels.Add(EquipmentLabel);
                    break;
                case Card.Type.AuraCard:
                    entry.labels.Add(AuraLabel);
                    break;
            }

            entriesAdded.Add(entry);
        }
        
        settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryAdded, entriesAdded, true);
    }
}
