﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tests/Player Side Setup")]
public class PlayerSideSetup : ScriptableObject
{
    [SerializeField]
    private Deck deck = null;

    [SerializeField]
    private Fighter activeFighter = null;
    [SerializeField]
    private Ability[] activeFighterAbilities = null;
    [SerializeField]
    private Equipment[] activeFighterEquipment = null;

    public Deck PlayerDeck
    {
        get { return deck; }
    }

    public Fighter ActiveFighter
    {
        get { return activeFighter; }
    }

    public Ability[] ActiveFighterAbilities
    {
        get { return activeFighterAbilities; }
    }

    public Equipment[] ActiveFighterEquipment
    {
        get { return activeFighterEquipment; }
    }
}
