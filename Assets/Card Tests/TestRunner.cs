﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRunner : MonoBehaviour
{
    [SerializeField]
    private int randomSeed = 0;
    
    [SerializeField]
    private Transform gameBoardPrefab = null;

    [SerializeField]
    private GameController gameController = null;

    private bool first = true;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (first)
        {
            int i = -1;
            foreach (Transform testCaseTranform in transform)
            {
                i++;
                var testCase = testCaseTranform.GetComponent<GameTestCase>();

                if (!testCaseTranform.gameObject.activeSelf || testCase == null)
                {
                    continue;
                }

                //Player 1
                var deck = ScriptableObject.CreateInstance<Deck>();
                PlayerSideSetup playerSet1 = testCase.GameboardSetup.Player1Position;
                var player1 = ScriptableObject.CreateInstance<DummyPlayer>();
                player1.name = "player 1 " + i;

                SetupPlayer(deck, player1, playerSet1);

                //Player 2
                var deck2 = ScriptableObject.CreateInstance<Deck>();
                PlayerSideSetup playerSet2 = testCase.GameboardSetup.Player2Position;
                deck2.fighters[0] = playerSet2.ActiveFighter;

                var player2 = ScriptableObject.CreateInstance<DummyPlayer>();
                player2.name = "player 2 " + i;
                player2.playerDeck = deck2;


                //Set the game data
                var gameState = new GameObject().AddComponent<GameStateParameters>().GetComponent<GameStateParameters>();

                gameState.Player1 = player1;
                gameState.Player2 = player2;
                gameState.RandomSeed = randomSeed;

                //Create the game environment
                var gameBoard = Instantiate(gameBoardPrefab, null).gameObject;

                GameController testGameController = null;
                if (gameController == null)
                {
                    testGameController = new GameObject().AddComponent<GameController>().GetComponent<GameController>();
                }
                else
                {
                    testGameController = Instantiate(gameController);
                }

                testGameController.gameBoard = gameBoard;
                testGameController.gameObject.SetActive(false);
                testGameController.InitializeGameBoard();

                //Set the Game Board cards
                /*Transform player1Transform = gameBoard.transform.Find("Player 1");
                player1Transform.Find("Active Fighter").GetChild(1);*/

                try
                {
                    SetupCenario(testGameController, playerSet1, player1);
                    SetupCenario(testGameController, playerSet2, player2);
                    testCase.RunTest(testGameController);
                }
                catch (Exception ex)
                {
                    Debug.LogError(testCaseTranform.name + " Failed!");
                    Debug.LogError(ex.Message + "\n" + ex.StackTrace);
                    first = false;
                    return;
                }

                Destroy(testGameController.gameObject);
                Destroy(gameState.gameObject);
                Destroy(gameBoard.gameObject);
                Destroy(player1);
                Destroy(player2);
                Destroy(deck2);
                Destroy(deck);
            }
            Debug.Log(name + " Passed!");
            first = false;
        }
    }

    private void SetupPlayer(Deck deck, Player player, PlayerSideSetup playerSet)
    {
        deck.fighters[0] = playerSet.ActiveFighter;

        if (playerSet.PlayerDeck != null)
        {
            deck.cards = playerSet.PlayerDeck.cards;
        }

        player.playerDeck = deck;
    }

    private void SetupCenario(GameController gameController, PlayerSideSetup playerSet, Player player)
    {
        FighterCardComponent fighterCard = gameController.GetPlayerGameBoard(player).ActiveFighterCard;

        foreach (Ability ability in playerSet.ActiveFighterAbilities)
        {
            var abilityCard = AbilityCardComponent.CreateAbilityCard(ability);
            abilityCard.AbilityData.Owner = player;

            fighterCard.AttachAbility(abilityCard);
            gameController.PutAttachedCardIntoPlay(abilityCard, fighterCard.FighterData);
        }

        foreach(Equipment equipment in playerSet.ActiveFighterEquipment)
        {
            var equipmentCard = EquipmentCardComponent.CreateEquipmentCard(equipment);
            equipmentCard.EquipmentData.Owner = player;

            fighterCard.FighterData.AttachEquipment(equipmentCard.EquipmentData);

        }
    }
}
