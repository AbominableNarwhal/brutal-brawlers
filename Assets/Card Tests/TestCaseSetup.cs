﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tests/Test Case Setup")]
public class TestCaseSetup : ScriptableObject
{
    [SerializeField]
    private PlayerSideSetup player1Side = null;
    [SerializeField]
    private PlayerSideSetup player2Side = null;

    public PlayerSideSetup Player1Position
    {
        get { return player1Side; }
    }

    public PlayerSideSetup Player2Position
    {
        get { return player2Side; }
    }
}
