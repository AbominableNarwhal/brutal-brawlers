﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FlameWallTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var flameWallEffect = gameController.FindCardEffect(abilityCardTestSubject) as FlameWall;

        GameAction[] actions = flameWallEffect.Defend(gameController, null);

        Assert.AreEqual(3, actions.Length);
        Assert.IsTrue(actions[0] is SetBlockerAction);
        Assert.IsTrue(actions[1] is DamageAction);
        Assert.IsTrue(actions[2] is SetMarkerAction);

        var blockerAction = actions[0] as SetBlockerAction;

        Assert.AreEqual(20, blockerAction.BlockerStrength, "Blocker Strength is incorrect.");
        Assert.AreEqual(flameWallEffect.GetThisFighterCard(gameController), blockerAction.TargetFighter, "Blocker has wrong target.");

        var damageAction = actions[1] as DamageAction;

        Assert.AreEqual(20, damageAction.damageAmount, "Damage amount is incorrect.");
        Assert.IsFalse(damageAction.IsAttackDamage, "The damage is not supposed to be attack damage.");
        Assert.AreEqual(gameController.Player2Board.ActiveFighterCard, damageAction.targetFighterCard, "This abiliy is not targeting the opposing active fighter.");

        var markerAction = actions[2] as SetMarkerAction;

        Assert.AreEqual(gameController.Player2Board.ActiveFighterCard, markerAction.FighterCard, "this ability does not set a marker on the right firghter.");
        Assert.AreEqual(gameController.FireMark, markerAction.GameMarker, "This ability is setting the wrong marker.");
        Assert.AreEqual(GameAction.SetMode.Add, markerAction.SetAction);

    }
}
