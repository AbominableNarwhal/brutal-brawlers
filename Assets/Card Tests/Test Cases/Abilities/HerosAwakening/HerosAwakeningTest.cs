﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public class HerosAwakeningTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var herosAwakeningEffect = gameController.FindCardEffect(abilityCardTestSubject) as HerosAwakening;

        // Get the Enabled event actions
        GameAction[] actions = gameController.ActionStack.ToArray();

        Assert.AreEqual(1, actions.Length);
        Assert.IsTrue(actions[0] is SetOnHitTriggerAction);

        var onHitTriggerAction = actions[0] as SetOnHitTriggerAction;

        Assert.IsNotNull(onHitTriggerAction.OnHitTrigger);
        Assert.AreEqual(gameController.Player1Board.ActiveFighterCard.Abilities[2], onHitTriggerAction.TargetAbility);
        Assert.AreEqual(GameAction.SetMode.Add, onHitTriggerAction.Mode);

        DamageAction damageAction = new DamageAction { damageAmount = 10, IsAttackDamage = true, OriginEffect = herosAwakeningEffect };

        //Daze the enemy
        gameController.Player2Board.ActiveFighter.AddStatusCondition(StatusCondition.Dazed);
        damageAction.Execute(gameController);

        GameAction[] onHitActions = onHitTriggerAction.OnHitTrigger.TriggerOnHit(gameController, null, damageAction);

        Assert.AreEqual(1, onHitActions.Length);
        Assert.IsTrue(onHitActions[0] is DamageAction);

        var onHitDamageAction = onHitActions[0] as DamageAction;

        Assert.AreEqual(30, onHitDamageAction.damageAmount);
        Assert.IsFalse(onHitDamageAction.IsAttackDamage);
        Assert.AreEqual(gameController.Player2Board.ActiveFighterCard, onHitDamageAction.GetTarget(gameController));

        var endTurnAction = new EndTurnAction();
        GameAction[] onRemoveActions = null;
        for (int i = 0; i < 3; i++)
        {
            endTurnAction.Execute(gameController);
            onRemoveActions = herosAwakeningEffect.OnActionCompleteBroadcast(gameController, endTurnAction);
        }

        Assert.AreEqual(1, onRemoveActions.Length);
        Assert.IsTrue(onRemoveActions[0] is DiscardAction);

        onRemoveActions = herosAwakeningEffect.Disabled(gameController);

        Assert.AreEqual(1, onRemoveActions.Length);

        var onRemoveAction = onRemoveActions[0] as SetOnHitTriggerAction;

        Assert.IsNotNull(onRemoveAction.OnHitTrigger);
        Assert.AreEqual(onHitTriggerAction.OnHitTrigger, onRemoveAction.OnHitTrigger);
        Assert.AreEqual(onHitTriggerAction.TargetAbility, onRemoveAction.TargetAbility);
        Assert.AreEqual(GameAction.SetMode.Remove, onRemoveAction.Mode);

    }
}
