﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ArcaneReconingTest : AbilityEffectTestCase
{
    private static readonly int NumOfAura = 3;
    private static readonly int AuraToTap = 2;

    [SerializeField]
    private Aura auraCard = null;
    protected override void RunAbilityTest(GameController gameController)
    {
        var arkaneReconingEffect = gameController.FindCardEffect(abilityCardTestSubject) as ArcaneReckoning;

        //Add 3 aura to the pool
        for (int i = 0; i < NumOfAura; i++)
        {
            CardFactory.CreateCardObject(auraCard).transform.SetParent(gameController.Player1Board.AuraStash);
        }

        GameAction[] actions = arkaneReconingEffect.Attack(gameController);

        Assert.AreEqual<int>(1, actions.Length);
        Assert.IsTrue(actions[0] is QueryAction);

        var queryAction = actions[0] as QueryAction;

        Assert.IsTrue(queryAction.query is CardPickQuery);
        var cardPick = queryAction.query as CardPickQuery;

        Assert.AreEqual<int>(NumOfAura, cardPick.cards.Length);

        for (int i = 0; i < NumOfAura; i++)
        {
            Assert.AreEqual(Card.Type.AuraCard, cardPick.cards[i].CardData.CardType);
        }

        var cardPickAnswer = new CardPickAnswer();
        cardPickAnswer.cards = new CardComponent[2];

        for (int i = 0; i < AuraToTap; i++)
        {
            cardPickAnswer.cards[i] = cardPick.cards[i];
        }

        actions = arkaneReconingEffect.QueryAnswered(gameController, cardPick, cardPickAnswer);

        Assert.AreEqual<int>(AuraToTap, actions.Length);

        Assert.IsTrue(actions[0] is TapAuraAction);
        var tapAuraAction = actions[0] as TapAuraAction;

        Assert.AreEqual(AuraToTap, tapAuraAction.NumberOfAura);

        Assert.IsTrue(actions[1] is DamageAction);
        var damageAction = actions[1] as DamageAction;

        Assert.AreEqual(AuraToTap * ArcaneReckoning.Multiplier + abilityTestSubject.damage, damageAction.damageAmount);
    }
}
