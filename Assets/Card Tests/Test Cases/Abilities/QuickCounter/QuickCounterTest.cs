﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using static GameController;

public class QuickCounterTest : AbilityEffectTestCase
{
    [SerializeField]
    private Ability ability = null;
    protected override void RunAbilityTest(GameController gameController)
    {
        var quickCounterEffect = gameController.FindCardEffect(abilityCardTestSubject) as QuickCounter;

        AbilityCardComponent abilityCard = AbilityCardComponent.CreateAbilityCard(ability);
        abilityCard.CardData.Owner = gameController.player2;
        gameController.PutAttachedCardIntoPlay(abilityCard, gameController.Player2Board.ActiveFighter);

        var damageAction = new DamageAction { damageAmount = 20, IsAttackDamage = true };

        gameController.AddAction(damageAction, gameController.FindCardEffect(abilityCard));

        var combatQuery = new CombatQuery { CombatType = PlayType.Defend};
        var combatAnswer = new CombatQueryAnswer { card = quickCounterEffect.CardObject, type = CombatQueryAnswer.Type.ActivateAbility };
         
        ValidationResult validationResult = quickCounterEffect.ValidateQuery(gameController, combatQuery, combatAnswer);

        Assert.AreEqual(ValidationResult.Defer, validationResult);

        damageAction.damageAmount = 50;

        validationResult = quickCounterEffect.ValidateQuery(gameController, combatQuery, combatAnswer);

        Assert.AreEqual(ValidationResult.Fail, validationResult);

        damageAction.damageAmount = 30;

        GameAction[] actions = quickCounterEffect.Defend(gameController, damageAction);

        Assert.AreEqual(2, actions.Length);
        Assert.IsTrue(actions[0] is NegateAction);
        Assert.IsTrue(actions[1] is DamageAction);

        var negateAction = actions[0] as NegateAction;

        Assert.AreEqual(damageAction, negateAction.ActionToNegate);

        var defendDamage = actions[1] as DamageAction;

        Assert.AreEqual(damageAction.damageAmount, defendDamage.damageAmount);
        Assert.IsFalse(defendDamage.IsAttackDamage);

        Destroy(abilityCard.gameObject);
    }
}
