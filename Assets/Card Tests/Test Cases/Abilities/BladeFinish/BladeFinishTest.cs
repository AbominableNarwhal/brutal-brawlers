﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class BladeFinishTest : AbilityEffectTestCase
{
    private static readonly int SwordAttacks = 3;

    protected override void RunAbilityTest(GameController gameController)
    {
        var herosAwakeningEffect = gameController.FindCardEffect(abilityCardTestSubject) as BladeFinish;

        AbilityCardBaseComponent rapidSlashCard = gameController.Player1Board.ActiveFighterCard.Abilities[1];
        var rapidSlashEffect = gameController.FindCardEffect(rapidSlashCard) as RapidSlash;

        var activateAction = new ActivateAbilityAction() { abilityCard = rapidSlashCard, type = PlayType.Attack };

        for (int i = 0; i < SwordAttacks; i++)
        {
            gameController.AddAction(activateAction, rapidSlashEffect);
            gameController.ExecuteGameLoop();
        }

        GameAction[] attackActions = herosAwakeningEffect.OnAttack(gameController);

        Assert.AreEqual(1, attackActions.Length);
        Assert.IsTrue(attackActions[0] is DamageAction);

        var damageAction = attackActions[0] as DamageAction;

        Assert.IsTrue(damageAction.IsAttackDamage);
        Assert.AreEqual(abilityTestSubject.damage + 10 * SwordAttacks, damageAction.damageAmount);
    }
}
