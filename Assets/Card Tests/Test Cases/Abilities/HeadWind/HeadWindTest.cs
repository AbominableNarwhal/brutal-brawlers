﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class HeadWindTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var headWindEffect = gameController.FindCardEffect(abilityCardTestSubject) as HeadWind;

        GameAction[] actions = headWindEffect.Defend(gameController, null);

        Assert.AreEqual(1, actions.Length);
        Assert.IsTrue(actions[0] is SetBlockerAction);

        var blockerAction = actions[0] as SetBlockerAction;

        Assert.AreEqual(30, blockerAction.BlockerStrength);
        Assert.AreEqual(gameController.Player1Board.ActiveFighterCard, blockerAction.TargetFighter);
    }
}
