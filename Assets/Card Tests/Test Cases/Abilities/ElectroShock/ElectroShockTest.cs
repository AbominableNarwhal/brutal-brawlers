﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ElectroShockTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var electroShock = gameController.FindCardEffect(abilityCardTestSubject) as ElectroShock;

        GameAction[] attackActions = electroShock.Attack(gameController);

        Assert.AreEqual(1, attackActions.Length);

        var damageAction = attackActions[0] as DamageAction;

        Assert.AreEqual(abilityTestSubject.damage, damageAction.damageAmount);
        Assert.IsTrue(damageAction.IsAttackDamage);

        damageAction.Execute(gameController);

        GameAction[] triggerActions = electroShock.TriggerOnHit(gameController, damageAction);

        Assert.AreEqual(2, triggerActions.Length);
        Assert.IsTrue(triggerActions[0] is ChangeStatusConditionAction);
        Assert.IsTrue(triggerActions[1] is ChangeStatusConditionAction);

        var statusAction = triggerActions[0] as ChangeStatusConditionAction;
        ValidateStatusChange(statusAction, damageAction.targetFighterCard);

        statusAction = triggerActions[1] as ChangeStatusConditionAction;
        ValidateStatusChange(statusAction, damageAction.targetFighterCard);
    }

    private void ValidateStatusChange(ChangeStatusConditionAction action, FighterCardComponent fighterCard)
    {
        Assert.IsTrue(action.AddCondition);
        Assert.AreEqual(StatusCondition.Electrified, action.Condition);
        Assert.AreEqual(fighterCard, action.TargetFighter);
    }
}
