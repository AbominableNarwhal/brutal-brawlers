﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FlareBombTest : AbilityEffectTestCase
{
    private static readonly int FireMarks = 2;

    [SerializeField]
    private Marker fireMaker = null;

    protected override void RunAbilityTest(GameController gameController)
    {
        var flareBombEffect = gameController.FindCardEffect(abilityCardTestSubject) as FlareBomb;

        for (int i = 0; i < FireMarks; i++)
        {
            gameController.Player2Board.ActiveFighterCard.AddMarker(fireMaker);
        }

        GameAction[] attackActions = flareBombEffect.Attack(gameController);

        Assert.AreEqual(3, attackActions.Length);
        Assert.IsTrue(attackActions[0] is DamageAction);
        Assert.IsTrue(attackActions[1] is SetMarkerAction);
        Assert.IsTrue(attackActions[2] is SetMarkerAction);

        var damageAction = attackActions[0] as DamageAction;

        Assert.IsTrue(damageAction.IsAttackDamage);
        Assert.AreEqual(FlareBomb.BaseDamage * FireMarks, damageAction.damageAmount);

        var markerAction = attackActions[1] as SetMarkerAction;
        ValidateMarkerAction(markerAction, gameController);

        markerAction = attackActions[2] as SetMarkerAction;
        ValidateMarkerAction(markerAction, gameController);
    }

    private void ValidateMarkerAction(SetMarkerAction action, GameController gameController)
    {
        Assert.AreEqual(fireMaker, action.GameMarker);
        Assert.AreEqual(GameAction.SetMode.Remove, action.SetAction);
        Assert.AreEqual(gameController.Player2Board.ActiveFighterCard, action.FighterCard);
    }
}
