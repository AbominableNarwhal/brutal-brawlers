using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class RedHotCloakTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var redHotCloakEffect = gameController.FindCardEffect(abilityCardTestSubject) as RedHotCloak;
        Assert.IsNotNull(redHotCloakEffect);

        FighterCardComponent fighterCard = gameController.Player1Board.ActiveFighterCard;
        FighterCardComponent oppoFighterCard = gameController.Player2Board.ActiveFighterCard;
        GameAction[] defendActions = redHotCloakEffect.Defend(gameController, null);

        Assert.AreEqual(2, defendActions.Length);
        Assert.IsTrue(defendActions[0] is SetBlockerAction);
        Assert.IsTrue(defendActions[1] is DamageAction);

        var blockerAction = defendActions[0] as SetBlockerAction;

        Assert.AreEqual(redHotCloakEffect.ParentAbility.BlockDamage, blockerAction.BlockerStrength);
        Assert.AreEqual(fighterCard, blockerAction.TargetFighter);

        var damageAction = defendActions[1] as DamageAction;

        Assert.AreEqual(RedHotCloak.PassiveDamage, damageAction.damageAmount);
        Assert.IsFalse(damageAction.IsAttackDamage);
        Assert.AreEqual(oppoFighterCard, damageAction.targetFighterCard);
    }
}
