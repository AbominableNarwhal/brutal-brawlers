using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class BlazingStrikeTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var blazingStrikeEffect = gameController.FindCardEffect(abilityCardTestSubject) as BlazingStrike;

        FighterCardComponent fighterCard = gameController.Player1Board.ActiveFighterCard;

        for (int i = 0; i < 4; i++)
        {
            fighterCard.AddMarker(gameController.FireMark);
        }

        GameAction[] attackActions = blazingStrikeEffect.Attack(gameController);

        Assert.AreEqual(1, attackActions.Length);
        Assert.IsTrue(attackActions[0] is DamageAction);

        var damageAction = attackActions[0] as DamageAction;

        Assert.IsTrue(damageAction.IsAttackDamage);
        Assert.AreEqual(blazingStrikeEffect.ParentAbility.Damage, damageAction.damageAmount);

        GameAction[] onHitActions = blazingStrikeEffect.TriggerOnHit(gameController, damageAction);

        Assert.AreEqual(4, onHitActions.Length);

        for (int i = 0; i < 3; i++)
        {
            Assert.IsTrue(onHitActions[i] is SetMarkerAction);

            var markerAction = onHitActions[i] as SetMarkerAction;

            Assert.AreEqual(fighterCard, markerAction.FighterCard);
            Assert.AreEqual(GameAction.SetMode.Remove, markerAction.SetAction);
            Assert.AreEqual(gameController.FireMark, markerAction.GameMarker);
        }

        Assert.IsTrue(onHitActions[3] is DamageAction);

        var passiveDamageAction = onHitActions[3] as DamageAction;
        FighterCardComponent oppoFighterCard = gameController.Player2Board.ActiveFighterCard;

        Assert.IsFalse(passiveDamageAction.IsAttackDamage);
        Assert.AreEqual(BlazingStrike.BasePassiveDamage * BlazingStrike.MarkersToRemove, passiveDamageAction.damageAmount);
        Assert.AreEqual(oppoFighterCard, passiveDamageAction.targetFighterCard);
    }
}
