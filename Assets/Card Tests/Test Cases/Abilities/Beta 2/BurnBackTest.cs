using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class BurnBackTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var burnBackEffect = gameController.FindCardEffect(abilityCardTestSubject) as BurnBack;
        Assert.IsNotNull(burnBackEffect);

        // Test first activation
        GameAction[] defendActions = burnBackEffect.Defend(gameController, null);

        Assert.AreEqual(1 + BurnBack.BaseMarksToAdd, defendActions.Length);
        Assert.IsTrue(defendActions[0] is DamageAction);
        Assert.IsTrue(defendActions[1] is SetMarkerAction);

        var damageAction = defendActions[0] as DamageAction;
        FighterCardComponent oppoFighter = gameController.Player2Board.ActiveFighterCard;
        FighterCardComponent fighter = gameController.Player1Board.ActiveFighterCard;

        Assert.AreEqual(BurnBack.BasePassiveDamage, damageAction.damageAmount);
        Assert.IsFalse(damageAction.IsAttackDamage);
        Assert.AreEqual(oppoFighter, damageAction.targetFighterCard);

        var markerAction = defendActions[1] as SetMarkerAction;

        Assert.AreEqual(fighter, markerAction.FighterCard);
        Assert.AreEqual(gameController.FireMark, markerAction.GameMarker);
        Assert.AreEqual(GameAction.SetMode.Add, markerAction.SetAction);

        // Test second activation
        var activateAbilityAction = new ActivateAbilityAction() { abilityCard = burnBackEffect.AbilityCardBody, type = PlayType.Defend };
        gameController.AddAction(activateAbilityAction, burnBackEffect);
        gameController.ExecuteGameLoop();

        defendActions = burnBackEffect.Defend(gameController, null);

        Assert.AreEqual(1 + BurnBack.BaseMarksToAdd * 2, defendActions.Length);

        damageAction = defendActions[0] as DamageAction;

        Assert.AreEqual(BurnBack.BasePassiveDamage * 2, damageAction.damageAmount);
        Assert.IsFalse(damageAction.IsAttackDamage);
        Assert.AreEqual(oppoFighter, damageAction.targetFighterCard);

        for (int i = 1; i < defendActions.Length; i++)
        {
            markerAction = defendActions[i] as SetMarkerAction;

            Assert.AreEqual(fighter, markerAction.FighterCard);
            Assert.AreEqual(gameController.FireMark, markerAction.GameMarker);
            Assert.AreEqual(GameAction.SetMode.Add, markerAction.SetAction);
        }

        // Test third activation
        gameController.AddAction(activateAbilityAction, burnBackEffect);
        gameController.ExecuteGameLoop();

        defendActions = burnBackEffect.Defend(gameController, null);

        Assert.AreEqual(1 + BurnBack.BaseMarksToAdd * 3, defendActions.Length);

        damageAction = defendActions[0] as DamageAction;

        Assert.AreEqual(BurnBack.BasePassiveDamage * 3, damageAction.damageAmount);
        Assert.IsFalse(damageAction.IsAttackDamage);
        Assert.AreEqual(oppoFighter, damageAction.targetFighterCard);

        for (int i = 1; i < defendActions.Length; i++)
        {
            markerAction = defendActions[i] as SetMarkerAction;

            Assert.AreEqual(fighter, markerAction.FighterCard);
            Assert.AreEqual(gameController.FireMark, markerAction.GameMarker);
            Assert.AreEqual(GameAction.SetMode.Add, markerAction.SetAction);
        }

        //Test turn reset
        gameController.EndTurn();

        defendActions = burnBackEffect.Defend(gameController, null);

        Assert.AreEqual(1 + BurnBack.BaseMarksToAdd, defendActions.Length);
        Assert.IsTrue(defendActions[0] is DamageAction);
        Assert.IsTrue(defendActions[1] is SetMarkerAction);

        damageAction = defendActions[0] as DamageAction;

        Assert.AreEqual(BurnBack.BasePassiveDamage, damageAction.damageAmount);
        Assert.IsFalse(damageAction.IsAttackDamage);
        Assert.AreEqual(oppoFighter, damageAction.targetFighterCard);

        markerAction = defendActions[1] as SetMarkerAction;

        Assert.AreEqual(fighter, markerAction.FighterCard);
        Assert.AreEqual(gameController.FireMark, markerAction.GameMarker);
        Assert.AreEqual(GameAction.SetMode.Add, markerAction.SetAction);
    }
}
