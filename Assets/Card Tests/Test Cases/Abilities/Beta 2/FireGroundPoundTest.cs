using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FireGroundPoundTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var fireGroundPoundEffect = gameController.FindCardEffect(abilityCardTestSubject) as FireGroundPound;
        Assert.IsNotNull(fireGroundPoundEffect);

        FighterCardComponent fighterCard = gameController.Player1Board.ActiveFighterCard;

        for(int i = 0; i < 4; i++)
        {
            fighterCard.AddMarker(gameController.FireMark);
        }

        GameAction[] attackActions = fireGroundPoundEffect.Attack(gameController);
        Assert.AreEqual(1, attackActions.Length);
        Assert.IsTrue(attackActions[0] is DamageAction);

        var damageAction = attackActions[0] as DamageAction;

        Assert.IsFalse(damageAction.IsAttackDamage);
        Assert.AreEqual(FireGroundPound.BasePassiveDamage * fighterCard.MarkerCount(gameController.FireMark), damageAction.damageAmount);
    }
}
