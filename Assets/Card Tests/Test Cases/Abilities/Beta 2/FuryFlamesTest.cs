using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FuryFlamesTest : AbilityEffectTestCase
{
    [SerializeField]
    private Ability blazingStrike = null;
    protected override void RunAbilityTest(GameController gameController)
    {
        var furyFlamesEffect = gameController.FindCardEffect(abilityCardTestSubject) as FuryFlames;
        Assert.IsNotNull(furyFlamesEffect);

        FighterCardComponent fighterCard = gameController.Player1Board.ActiveFighterCard;

        AbilityCardComponent blazingStrikeCard = AbilityCardComponent.CreateAbilityCard(blazingStrike);
        blazingStrikeCard.CardData.Owner = gameController.player2;
        var incomingAbilityEffect = new BlazingStrike(blazingStrikeCard, fighterCard.FighterData);

        var damageAction = new DamageAction() { damageAmount = 50, IsAttackDamage = true, OriginEffect = incomingAbilityEffect };
        GameAction[] defendActions = furyFlamesEffect.Defend(gameController, damageAction);

        Assert.AreEqual(blazingStrikeCard.AbilityData.AuraCost + 1, defendActions.Length);
        Assert.IsTrue(defendActions[0] is SetBlockerAction);
        for (int i = 1; i < blazingStrikeCard.AbilityData.AuraCost + 1; i++)
        {
            Assert.IsTrue(defendActions[i] is SetMarkerAction);
        }

        var blockerAction = defendActions[0] as SetBlockerAction;

        Assert.AreEqual(furyFlamesEffect.ParentAbility.BlockDamage, blockerAction.BlockerStrength);
        Assert.AreEqual(fighterCard, blockerAction.TargetFighter);

        for (int i = 1; i < blazingStrikeCard.AbilityData.AuraCost + 1; i++)
        {
            var markerAction = defendActions[i] as SetMarkerAction;
            
            Assert.AreEqual(fighterCard, markerAction.FighterCard);
            Assert.AreEqual(gameController.FireMark, markerAction.GameMarker);
            Assert.AreEqual(GameAction.SetMode.Add, markerAction.SetAction);
        }
    }
}
