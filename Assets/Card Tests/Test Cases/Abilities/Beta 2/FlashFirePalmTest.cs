using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FlashFirePalmTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var flashFirePalmEffect = gameController.FindCardEffect(abilityCardTestSubject) as FlashFirePalm;

        GameAction[] attackActions = flashFirePalmEffect.Attack(gameController);

        Assert.AreEqual(1, attackActions.Length);
        Assert.IsTrue(attackActions[0] is DamageAction);

        var damageAction = attackActions[0] as DamageAction;

        Assert.IsTrue(damageAction.IsAttackDamage);
        Assert.AreEqual(flashFirePalmEffect.ParentAbility.Damage, damageAction.damageAmount);

        FighterCardComponent fighterCard = gameController.Player1Board.ActiveFighterCard;
        fighterCard.AddMarker(gameController.FireMark);
        fighterCard.AddMarker(gameController.FireMark);

        attackActions = flashFirePalmEffect.Attack(gameController);

        Assert.AreEqual(3, attackActions.Length);
        Assert.IsTrue(attackActions[0] is SetMarkerAction);
        Assert.IsTrue(attackActions[1] is SetMarkerAction);
        Assert.IsTrue(attackActions[2] is DamageAction);

        var setMarkerAction1 = attackActions[0] as SetMarkerAction;
        var setMarkerAction2 = attackActions[1] as SetMarkerAction;
        damageAction = attackActions[2] as DamageAction;

        Assert.AreEqual(fighterCard, setMarkerAction1.FighterCard);
        Assert.AreEqual(GameAction.SetMode.Remove, setMarkerAction1.SetAction);
        Assert.AreEqual(gameController.FireMark, setMarkerAction1.GameMarker);

        Assert.AreEqual(fighterCard, setMarkerAction2.FighterCard);
        Assert.AreEqual(GameAction.SetMode.Remove, setMarkerAction2.SetAction);
        Assert.AreEqual(gameController.FireMark, setMarkerAction2.GameMarker);

        Assert.IsTrue(damageAction.IsAttackDamage);
        Assert.AreEqual(flashFirePalmEffect.ParentAbility.Damage + FlashFirePalm.BaseDamage * 2, damageAction.damageAmount);

        GameAction[] onHitActions = flashFirePalmEffect.TriggerOnHit(gameController, damageAction);

        Assert.AreEqual(3, onHitActions.Length);
        Assert.IsTrue(onHitActions[0] is SetMarkerAction);
        Assert.IsTrue(onHitActions[1] is SetMarkerAction);
        Assert.IsTrue(onHitActions[2] is SetMarkerAction);

        setMarkerAction1 = onHitActions[0] as SetMarkerAction;
        setMarkerAction2 = onHitActions[1] as SetMarkerAction;
        var setMarkerAction3 = onHitActions[2] as SetMarkerAction;

        Assert.AreEqual(fighterCard, setMarkerAction1.FighterCard);
        Assert.AreEqual(GameAction.SetMode.Add, setMarkerAction1.SetAction);
        Assert.AreEqual(gameController.FireMark, setMarkerAction1.GameMarker);

        Assert.AreEqual(fighterCard, setMarkerAction2.FighterCard);
        Assert.AreEqual(GameAction.SetMode.Add, setMarkerAction2.SetAction);
        Assert.AreEqual(gameController.FireMark, setMarkerAction2.GameMarker);

        Assert.AreEqual(fighterCard, setMarkerAction3.FighterCard);
        Assert.AreEqual(GameAction.SetMode.Add, setMarkerAction3.SetAction);
        Assert.AreEqual(gameController.FireMark, setMarkerAction3.GameMarker);

    }
}
