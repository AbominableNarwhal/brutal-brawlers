using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class SearingStrikeTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var searingStrikeEffect = gameController.FindCardEffect(abilityCardTestSubject) as SearingStrike;

        FighterCardComponent fighterCard = gameController.Player1Board.ActiveFighterCard;
        fighterCard.AddMarker(gameController.FireMark);

        GameAction[] attackActions = searingStrikeEffect.Attack(gameController);

        Assert.AreEqual(1, attackActions.Length);
        Assert.IsTrue(attackActions[0] is DamageAction);

        var damageAction = attackActions[0] as DamageAction;

        Assert.IsTrue(damageAction.IsAttackDamage);
        Assert.AreEqual(searingStrikeEffect.ParentAbility.Damage, damageAction.damageAmount);

        GameAction[] onHitActions = searingStrikeEffect.TriggerOnHit(gameController, damageAction);

        Assert.IsNotNull(onHitActions);
        Assert.AreEqual(1, onHitActions.Length);
        Assert.IsTrue(onHitActions[0] is QueryAction);

        var queryAction = onHitActions[0] as QueryAction;

        Assert.IsNotNull(queryAction.query);

        var optionAnswer = new OptionAnswer() { selection = "Yes" };

        GameAction[] queryActions = searingStrikeEffect.QueryAnswered(gameController, queryAction.query, optionAnswer);

        Assert.IsNotNull(onHitActions);
        Assert.AreEqual(2, queryActions.Length);
        Assert.IsTrue(queryActions[0] is SetMarkerAction);
        Assert.IsTrue(queryActions[1] is DamageAction);

        var markerAction = queryActions[0] as SetMarkerAction;

        Assert.AreEqual(fighterCard, markerAction.FighterCard);
        Assert.AreEqual(GameAction.SetMode.Remove, markerAction.SetAction);
        Assert.AreEqual(gameController.FireMark, markerAction.GameMarker);

        var passiveDamageAction = queryActions[1] as DamageAction;
        FighterCardComponent oppoFighterCard = gameController.Player2Board.ActiveFighterCard;

        Assert.IsFalse(passiveDamageAction.IsAttackDamage);
        Assert.AreEqual(SearingStrike.PassiveDamage, passiveDamageAction.damageAmount);
        Assert.AreEqual(oppoFighterCard, passiveDamageAction.targetFighterCard);
    }
}
