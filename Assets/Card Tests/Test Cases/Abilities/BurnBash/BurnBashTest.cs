﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class BurnBashTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var burnBashEffect = gameController.FindCardEffect(abilityCardTestSubject) as BurnBash;

        GameAction[] actions = burnBashEffect.Attack(gameController);

        Assert.IsNotNull(actions);
        Assert.AreEqual<int>(1, actions.Length);
        Assert.IsTrue(actions[0] is DamageAction);

        var damageAction = actions[0] as DamageAction;

        Assert.AreEqual(abilityTestSubject.damage, damageAction.damageAmount);
        Assert.IsTrue(damageAction.IsAttackDamage);

        damageAction.Execute(gameController);

        GameAction[] onHitActions = burnBashEffect.TriggerOnHit(gameController, damageAction);

        Assert.IsNotNull(onHitActions);
        Assert.AreEqual<int>(1, onHitActions.Length);
        Assert.IsTrue(onHitActions[0] is DrawAction);

        var drawAction = onHitActions[0] as DrawAction;

        Assert.AreEqual(gameController.player1, drawAction.Player);

        drawAction.Execute(gameController);

        GameAction[] drawResultActions = burnBashEffect.OnActionCompleteBroadcast(gameController, drawAction);

        Assert.IsNotNull(drawResultActions);
        Assert.AreEqual<int>(1, drawResultActions.Length);
        Assert.IsTrue(drawResultActions[0] is DamageAction);

        var onHitDamageAction = drawResultActions[0] as DamageAction;

        Assert.AreEqual(30, onHitDamageAction.damageAmount);
        Assert.IsFalse(onHitDamageAction.IsAttackDamage);
        Assert.AreEqual(gameController.Player2Board.ActiveFighterCard, onHitDamageAction.targetFighterCard);

    }
}
