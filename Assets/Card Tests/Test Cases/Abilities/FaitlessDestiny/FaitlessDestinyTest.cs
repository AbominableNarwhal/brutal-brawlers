﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FaitlessDestinyTest : AbilityEffectTestCase
{
    protected override void RunAbilityTest(GameController gameController)
    {
        var fatelessDestinyEffect = gameController.FindCardEffect(gameController.Player1Board.ActiveFighter.Abilities[0]) as FatelessDestiny;

        GameAction[] attackActions = fatelessDestinyEffect.Attack(gameController);

        Assert.IsNotNull(attackActions);
        Assert.AreEqual(1, attackActions.Length);
        Assert.IsTrue(attackActions[0] is TapAuraAction);

        var tapAuraAction = attackActions[0] as TapAuraAction;

        Assert.AreEqual(-2, tapAuraAction.NumberOfAura);
        Assert.AreEqual(fatelessDestinyEffect.Parent.Owner, tapAuraAction.TargetPlayer);
    }
}
