﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityEffectTestCase : GameTestCase
{
    protected Ability abilityTestSubject = null;
    protected AbilityCardComponent abilityCardTestSubject = null;

    public override void RunTest(GameController gameController)
    {
        if (testSubject != null)
        {
            abilityTestSubject = testSubject as Ability;
            abilityCardTestSubject = AbilityCardComponent.CreateAbilityCard(abilityTestSubject);
            abilityCardTestSubject.CardData.Owner = gameController.player1;
            gameController.Player1Board.ActiveFighterCard.AttachAbility(abilityCardTestSubject);
            gameController.PutAttachedCardIntoPlay(abilityCardTestSubject, gameController.Player1Board.ActiveFighter);
        }
        gameController.offensivePlayer = gameController.player1;
        gameController.defensivePlayer = gameController.player2;
        RunAbilityTest(gameController);

        if (testSubject != null)
        {
            Destroy(abilityCardTestSubject.gameObject);
        }
    }

    protected abstract void RunAbilityTest(GameController gameController);
}
