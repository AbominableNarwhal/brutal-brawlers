﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameTestCase : MonoBehaviour
{
    [SerializeField]
    private TestCaseSetup gameboardSetup = null;
    [SerializeField]
    protected Card testSubject = null;

    public TestCaseSetup GameboardSetup
    {
        get { return gameboardSetup; }
    }

    public abstract void RunTest(GameController gameController);
}
