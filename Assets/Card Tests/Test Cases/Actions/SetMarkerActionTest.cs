using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class SetMarkerActionTest : GameTestCase
{
    public override void RunTest(GameController gameController)
    {
        FighterCardComponent fighterCard = gameController.Player1Board.ActiveFighterCard;
        var setMarkerAction = new SetMarkerAction(fighterCard, gameController.FireMark, GameAction.SetMode.Add);

        setMarkerAction.Execute(gameController);

        Assert.AreEqual(1, fighterCard.MarkerCount(gameController.FireMark), "Mark not added to fighter");

        setMarkerAction.Execute(gameController);

        Assert.AreEqual(2, fighterCard.MarkerCount(gameController.FireMark), "Mark not added to fighter");

        setMarkerAction = new SetMarkerAction(fighterCard, gameController.FireMark, GameAction.SetMode.Remove);
        setMarkerAction.Execute(gameController);

        Assert.AreEqual(1, fighterCard.MarkerCount(gameController.FireMark), "Mark not removed from fighter");

        setMarkerAction.Execute(gameController);

        Assert.AreEqual(0, fighterCard.MarkerCount(gameController.FireMark), "Mark not removed from fighter");
    }
}
