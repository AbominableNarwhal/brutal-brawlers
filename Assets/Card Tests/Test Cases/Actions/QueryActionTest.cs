using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class QueryActionTest : GameTestCase
{
    public override void RunTest(GameController gameController)
    {
        DummyPlayer player = gameController.player1 as DummyPlayer;

        var cardEffect = new DummyEffect();

        var query = new OptionPickQuery { from = cardEffect, to = player };
        var queryAction = new QueryAction { query = query };

        queryAction.Execute(gameController);

        Assert.IsNotNull(player.CurrentQuery);
        Assert.AreEqual(player.CurrentQuery, query);
    }

    private class DummyEffect : CardEffect
    {
        public DummyEffect() : base(null)
        { }
    }
}
