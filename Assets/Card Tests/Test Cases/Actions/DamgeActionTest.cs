﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class DamgeActionTest : GameTestCase
{
    public override void RunTest(GameController gameController)
    {
        FighterCardComponent fighterCard = gameController.Player1Board.ActiveFighterCard;

        int startingHp = fighterCard.Hp;
        var damageAction = new DamageAction
        {
            damageAmount = 120,
            targetFighterCard = fighterCard
        };

        damageAction.Execute(gameController);

        Assert.AreEqual(startingHp - 120, fighterCard.Hp);
        Assert.AreEqual(120, damageAction.ResultDamage.HpLost);
        Assert.AreEqual(0, damageAction.ResultDamage.BlockersBroken);
        //TODO do more testing with blockers
    }
}
