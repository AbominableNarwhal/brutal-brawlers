﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using SunEater.Support;

public enum StatusCondition { Dazed, Bleed, Electrified, None }

[Serializable]
public class FighterGameCard : GameCard
{
    [SerializeField]
    private Fighter fighterData;
    private List<AbilityGameCard> abilities;
    private List<EquipmentGameCard> equipment;
    private MultiSet<Marker> markers;
    private List<int> blockers;
    private MultiSet<StatusCondition> statusConditions;

    private int hp = 0;
    private int tagInCost = 0;

    public int Hp
    {
        get { return hp; }
        set
        {
            if (value < 0)
            {
                hp = 0;
            }
            else
            {
                hp = value;
            }
        }
    }

    public int MaxHp
    {
        get { return fighterData.Hp; }
    }

    public int Magic
    {
        get { return fighterData.magic; }
        set { fighterData.magic = value; }
    }

    public int Tech
    {
        get { return fighterData.tech; }
        set { fighterData.tech = value; }
    }
    public int Strength
    {
        get { return fighterData.strength; }
        set { fighterData.strength = value; }
    }

    public int AbilitySlots
    {
        get { return fighterData.abilitySlots; }
    }

    public int EquipmentSlots
    {
        get { return fighterData.equipmentSlots; }
    }

    public int TagInCost
    {
        get { return tagInCost; }
        set { tagInCost = value; }
    }

    public ReadOnlyDictionary<Marker, int> Markers { get; }

    public ReadOnlyCollection<AbilityGameCard> Abilities { get; }

    public ReadOnlyCollection<int> Blockers { get; }

    public FighterGameCard(Fighter fighter) : base(fighter)
    {
        fighterData = fighter;
        abilities = new List<AbilityGameCard>();
        equipment = new List<EquipmentGameCard>();
        markers = new MultiSet<Marker>();
        blockers = new List<int>();
        statusConditions = new MultiSet<StatusCondition>();

        Hp = fighter.Hp;
        TagInCost = fighter.TagInCost;

        foreach (var ability in fighterData.abilities)
        {
            AbilityGameCard abilityGameCard = new AbilityGameCard(ability);
            abilities.Add(abilityGameCard);
        }

        Abilities = abilities.AsReadOnly();
        Markers = markers.GetReadOnlyItems();
        Blockers = blockers.AsReadOnly();
    }

    public bool HasAbility(AbilityGameCard abilityGameCard)
    {
        return Abilities.Contains(abilityGameCard);
    }

    public void AddStatusCondition(StatusCondition condition)
    {
        statusConditions.Add(condition);
    }

    public void RemoveStatusCondition(StatusCondition condition)
    {
        statusConditions.Remove(condition);
    }

    public bool HasStatusCondition(StatusCondition condition)
    {
        return statusConditions.ItemCount(condition) > 0;
    }

    public void AttachAbility(AbilityGameCard ability)
    {
        abilities.Add(ability);
    }

    public void DetachAbility(AbilityGameCard ability)
    {
        abilities.Remove(ability);
    }

    public void AttachEquipment(EquipmentGameCard _equipment)
    {
        equipment.Add(_equipment);
    }

    public void DetachEquipment(EquipmentGameCard _equipment)
    {
        equipment.Remove(_equipment);
    }

    public void AddMarker(Marker marker)
    {
        markers.Add(marker);
    }

    public void RemoveMarker(Marker marker)
    {
        markers.Remove(marker);
    }

    public int MarkerCount(Marker marker)
    {
        return markers.ItemCount(marker);
    }

    public int MarkerCount()
    {
        return markers.Count;
    }

    public void AddBlocker(int blocker)
    {
        blockers.Insert(0, blocker);
    }

    public void RemoveBlocker()
    {
        blockers.RemoveAt(0);
    }

    public DamageReport DealDamage(int damage)
    {
        DamageReport damageReport = ReportDamage(damage);

        blockers.RemoveRange(0, damageReport.BlockersBroken);
        Hp = damageReport.RemainingHp;

        return damageReport;
    }

    public int GetHpAfterDamage(int damage)
    {
        DamageReport damageReport = ReportDamage(damage);

        return damageReport.RemainingHp;
    }

    public int[] GetBlockersAfterDamage(int damage)
    {
        DamageReport damageReport = ReportDamage(damage);

        if (damageReport.BlockersBroken == blockers.Count)
        {
            return new int[0];
        }

        List<int> futureBlockers = blockers.GetRange(damageReport.BlockersBroken, blockers.Count - damageReport.BlockersBroken);
        return futureBlockers.ToArray();
    }

    public DamageReport ReportDamage(int damage)
    {
        DamageReport damageReport = new DamageReport { BlockersBroken = 0, RemainingHp = Hp };

        int i = 0;
        while (damage > 0)
        {
            if (blockers.Count > i)
            {
                damage -= blockers[i];
                damageReport.BlockersBroken += 1;
                i += 1;
            }
            else
            {
                damageReport.RemainingHp -= damage;
                damageReport.DamageDealt += damage;

                if (damageReport.RemainingHp < 0)
                {
                    damageReport.RemainingHp = 0;
                }

                damage = 0;
            }
        }

        return damageReport;
    }

    public struct DamageReport
    {
        public int BlockersBroken { get; set; }
        public int RemainingHp { get; set; }
        public int DamageDealt { get; set; }
    }
}
