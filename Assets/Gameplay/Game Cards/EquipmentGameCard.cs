﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EquipmentGameCard : GameCard
{
    [SerializeField]
    private Equipment equipmentData;

    public string Description
    {
        get { return equipmentData.description; }
    }

    public Ability.Style FightStyle
    {
        get { return equipmentData.FightStyle; }
    }

    public EquipmentGameCard(Equipment equipment) : base(equipment)
    {
        equipmentData = equipment;
    }
}
