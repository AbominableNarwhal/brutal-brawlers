﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PowerGameCard : GameCard, IAuraConsumer
{
    [SerializeField]
    private Power powerData;
    public string Description
    {
        get { return powerData.description; }
    }

    public int AuraCost
    {
        get { return powerData.AuraCost; }
    }

    public PowerGameCard(Power power) : base(power)
    {
        powerData = power;
    }
}
