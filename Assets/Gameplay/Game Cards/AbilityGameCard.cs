﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

[Serializable]
public class AbilityGameCard : GameCard, IAuraConsumer
{
    [SerializeField]
    private Ability abilityData;
    private List<Status> statusChanges;
    private Status compiledStats;
    private List<IOnHitTriggerAddOn> onHitTriggers;
    private int magicRequirement;
    private int techRequirement;
    private int strengthRequirement;

    public int Damage
    {
        get
        {
            return abilityData.damage;
        }
    }

    public bool Enabled
    { get; set; } = true;

    public bool IsSupport
    {
        get { return abilityData.IsSupport; }
    }

    public Ability.Style Style
    {
        get { return abilityData.style; }
    }

    public Ability.AbilityType AbilityType
    {
        get { return abilityData.abilityType; }
    }

    public int MagicRequirement
    {
        get { return magicRequirement; }
        set { magicRequirement = value; }
    }

    public int TechRequirement
    {
        get { return techRequirement; }
        set { techRequirement = value; }
    }

    public int StrengthRequirement
    {
        get { return strengthRequirement; }
        set { strengthRequirement = value; }
    }

    public int AuraCost
    {
        get { return Math.Max(compiledStats.auraCost + BaseAuraCost, 0); }
    }

    public int TributeCost
    {
        get { return Math.Max(compiledStats.tributeCost + BaseTributeCost, 0); }
    }

    public int BaseTributeCost
    {
        get { return abilityData.TributeCost; }
    }

    public int BaseAuraCost
    {
        get { return abilityData.AuraCost; }
    }

    public int BlockDamage
    {
        get { return abilityData.blockDamage; }
    }

    public int Limit
    {
        get { return abilityData.limit; }
    }

    public bool HasAttack
    {
        get { return abilityData.hasAttack; }
    }

    public bool HasDefend
    {
        get { return abilityData.hasDefend; }
    }

    public string FullDescription
    {
        get { return abilityData.FullDescription; }
    }

    public ReadOnlyCollection<Status> StatusChanges { get; }
    public ReadOnlyCollection<IOnHitTriggerAddOn> OnHitTriggers { get; }

    public AbilityGameCard(Ability ability) : base(ability)
    {
        abilityData = ability;
        onHitTriggers = new List<IOnHitTriggerAddOn>();
        OnHitTriggers = onHitTriggers.AsReadOnly();
        statusChanges = new List<Status>();
        StatusChanges = statusChanges.AsReadOnly();

        magicRequirement = abilityData.MagicRequirement;
        techRequirement = abilityData.TechRequirement;
        strengthRequirement = abilityData.StrengthRequirement;
    }

    public void AddOnHitTrigger(IOnHitTriggerAddOn onHitTrigger)
    {
        onHitTriggers.Add(onHitTrigger);
    }

    public void RemoveOnHitTrigger(IOnHitTriggerAddOn onHitTrigger)
    {
        onHitTriggers.Remove(onHitTrigger);
    }

    public void AddStatusChange(Status statusChange)
    {
        statusChanges.Add(statusChange);
        compiledStats = CompileStatus();
    }

    public void RemoveStatusChange(Status statusChange)
    {
        for (int i = statusChanges.Count - 1; i >= 0; i--)
        {
            if (statusChanges[i].name == statusChange.name)
            {
                statusChanges.RemoveAt(i);
                break;
            }
        }

        compiledStats = CompileStatus();
    }

    public void RemoveAllStatusChanges()
    {
        statusChanges.Clear();
    }

    public Status CompileStatus()
    {
        Status sum = new Status();

        foreach (Status status in statusChanges)
        {
            sum += status;
        }

        return sum;
    }

    public struct Status
    {
        //public delegate bool AbilityCompatible(AbilityGameCard ability);

        public string name;
        public int tributeCost;
        public int auraCost;

        public static Status operator+ (Status lhs, Status rhs)
        {
            return new Status()
            {
                tributeCost = lhs.tributeCost + rhs.tributeCost,
                auraCost = lhs.auraCost + rhs.auraCost
            };
        }

        public static Status operator- (Status lhs, Status rhs)
        {
            return new Status()
            {
                tributeCost = lhs.tributeCost - rhs.tributeCost,
                auraCost = lhs.auraCost - rhs.auraCost
            };
        }
    }
}

public interface IOnHitTriggerAddOn
{
    GameAction[] TriggerOnHit(GameController gameController, AttachedEffect effect, DamageAction damageAction);
}
