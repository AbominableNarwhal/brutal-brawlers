﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class AuraGameCard : GameCard
{
    [SerializeField]
    private Aura auraData;

    public AuraGameCard(Aura aura) : base(aura)
    {
        auraData = aura;
    }
}
