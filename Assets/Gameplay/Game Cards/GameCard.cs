﻿using System.Collections;
using System.Collections.Generic;
using System.Media;
using UnityEngine;

[System.Serializable]
public abstract class GameCard
{
    [SerializeField]
    private Card cardData;
    public Player Owner { get; set; }
    public Card.Type CardType
    {
        get
        { return cardData.type; } 
    }

    public string CardName
    {
        get { return cardData.cardName; }
    }

    public Sprite Artwork
    {
        get { return cardData.artwork; }
    }

    public GameCard(Card card)
    {
        cardData = card;
    }

    public Card StaticData
    {
        get { return cardData; }
    }
}
