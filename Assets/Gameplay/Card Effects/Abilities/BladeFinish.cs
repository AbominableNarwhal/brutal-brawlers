using UnityEngine;

public class BladeFinish : AttachedEffect
{
    public BladeFinish(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        //count the number of sword attacks this fighter has used
        int count = 0;
        foreach(GameAction action in gameController.CompletedActions)
        {
            ActivateAbilityAction abilityAction = action as ActivateAbilityAction;
            if(abilityAction == null)
            {
                continue;
            }

            AbilityGameCard activatedAbility = abilityAction.abilityCard.CardData as AbilityGameCard;
            if(activatedAbility.Style == Ability.Style.Sword && User.HasAbility(activatedAbility))
            {
                count++;
            }
        }

        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage + 10 * count);
    }
}