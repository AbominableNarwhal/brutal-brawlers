﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SigilOfArms : AttachedEffect
{
    private static readonly string CardPickMessage = "Choose an equipment card from your deck to add to your hand.";
    public SigilOfArms(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        Transform deck = gameController.GetPlayerGameBoard(User.Owner).Deck;

        var validCards = new List<CardComponent>();
        foreach (Transform child in deck)
        {
            var card = child.GetComponent<CardComponent>();
            if (card.CardData.CardType == Card.Type.EquipmentCard)
            {
                validCards.Add(card);
            }
        }

        var cardpickQuery = new CardPickQuery() { cards = validCards.ToArray(), enableCancel = false, from = this, maxCards = 1, minCards = 0, to = User.Owner, message = CardPickMessage };
        var queryAction = new QueryAction() { query = cardpickQuery };

        return new[] { queryAction };
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var cardPickAnswer = answer as CardPickAnswer;

        if (cardPickAnswer != null && cardPickAnswer.cards.Length > 0)
        {
            var actions = new List<GameAction>();

            Transform hand = gameController.GetPlayerGameBoard(User.Owner).Hand;
            foreach (CardComponent card in cardPickAnswer.cards)
            {
                actions.Add(new RevealCardAction(card));
                actions.Add(new MoveCardAction(card, hand));

                var equipmentCard = card as EquipmentCardComponent;
                if (equipmentCard.EquipmentData.FightStyle == Ability.Style.Sword)
                {
                    actions.Add(GenerateAbilityDamageAction(gameController, ParentAbility.Damage));
                }
                else if (equipmentCard.EquipmentData.FightStyle == Ability.Style.Shield)
                {
                    actions.Add(new SetBlockerAction(ParentAbility.Damage, GetThisFighterCard(gameController)));
                }
            }

            return actions.ToArray();
        }

        return null;
    }
}
