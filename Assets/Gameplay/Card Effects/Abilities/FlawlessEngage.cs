using System.Collections.Generic;
using UnityEngine;
using static GameController;

public class FlawlessEngage : AttachedEffect
{
    public FlawlessEngage(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        if(!IsFirstUse(gameController))
        {
            return null;
        }

        List<GameAction> actions = new List<GameAction>();
        actions.Add(GenerateAbilityDamageAction(gameController, ParentAbility.Damage));

        FighterGameCard otherFighter = GetUserOpposingActiveFighter(gameController);
        if(User.Tech > otherFighter.Tech)
        {
            Transform playerDeck = gameController.GetPlayerOpponentGameObject(User.Owner).transform.Find("Deck");
            if(playerDeck.childCount > 0)
            {
                CardComponent card = playerDeck.GetChild(0).GetComponent<CardComponent>();
                actions.Add(new DiscardAction(card));
            }
        }

        return actions.ToArray();
    }

    public override ValidationResult ValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    { 
        return ActivateQueryDeferCondition(answer, IsFirstUse(gameController));
    }

    private bool IsFirstUse(GameController gameController)
    {
        bool firstUsed = true;
        foreach (GameAction action in gameController.CompletedActions)
        {
            ActivateAbilityAction abilityAction = action as ActivateAbilityAction;
            if (abilityAction == null)
            {
                continue;
            }

            AbilityGameCard activatedAbility = abilityAction.abilityCard.CardData as AbilityGameCard;
            if (activatedAbility.Owner = Parent.Owner)
            {
                firstUsed = false;
                break;
            }
        }

        return firstUsed;
    }
}