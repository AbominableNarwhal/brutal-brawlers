using System.Collections.Generic;
using UnityEngine;

public class ArcaneReckoning : AttachedEffect
{
    public static readonly int Multiplier = 20;
    private static readonly string QueryMessage = "Choose additional aura card to spend.";
    public ArcaneReckoning(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    {}

    public override GameAction[] Attack(GameController gameController)
    {
        Transform auraStash = gameController.GetPlayerGameBoard(User.Owner).AuraStash;
        List<CardComponent> untappedAura = new List<CardComponent>();
        foreach(Transform child in auraStash)
        {
            CardComponent card = child.GetComponent<CardComponent>();
            if(card == null)
            {
                return null;
            }

            if(card.IsFaceUp)
            {
                untappedAura.Add(card);
            }
        }

        CardPickQuery cardPickQuery = new CardPickQuery(){ cards = untappedAura.ToArray(), maxCards = 12, minCards = 0,
            enableCancel = true, message = QueryMessage, from = this, to = User.Owner};
        GameAction[] actions = new GameAction[1];
        actions[0] = new QueryAction() { query = cardPickQuery };
        return actions;
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        CardPickAnswer cardPickAnswer = answer as CardPickAnswer;
        if (cardPickAnswer == null)
        {
            return null;
        }
        
        int auraCount = cardPickAnswer.cards.Length;

        GameAction[] actions = new GameAction[2];
        actions[0] = new TapAuraAction() { NumberOfAura = auraCount, TargetPlayer = User.Owner};
        actions[1] = GenerateAbilityDamageAction(gameController, ParentAbility.Damage + auraCount * Multiplier);

        return actions;
    }
}