﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameWall : AttachedEffect
{
    public static readonly int WallDamage = 20;
    public FlameWall(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return new GameAction[] {
            new SetBlockerAction(ParentAbility.BlockDamage, GetThisFighterCard(gameController)),
            new DamageAction() { damageAmount = WallDamage, targetFighterCard = GetUserOpposingActiveFighterCard(gameController), IsAttackDamage = false },
            new SetMarkerAction(GetUserOpposingActiveFighterCard(gameController), gameController.FireMark, GameAction.SetMode.Add)
        };
    }
}
