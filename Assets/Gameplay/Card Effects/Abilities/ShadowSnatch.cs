﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using SunEater.Support;

public class ShadowSnatch : AttachedEffect
{
    private static readonly string QueryMessage = "Choose an ability on your opponents active fighter to disable.";
    private bool readyToUse = true;

    public ShadowSnatch(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    private GameAction[] Activate(GameController gameController)
    {
        if (!readyToUse)
        {
            return null;
        }

        readyToUse = false;
        TurnTimer = 0;
        List<GameObject> fighterAbilities = new List<GameObject>();

        foreach (var card in GetUserOpposingActiveFighterCard(gameController).Abilities)
        {
            fighterAbilities.Add(card.gameObject);
        }

        FieldPickQuery fieldPickQuery = new FieldPickQuery() { from = this, to = User.Owner, MaxItems = 1, MinItems = 1,
            ValidSelections = fighterAbilities.ToArray(), Message = QueryMessage };

        return new[] { new QueryAction() { query = fieldPickQuery } };
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var fieldPickAnswer = answer as FieldPickAnswer;
        if (fieldPickAnswer == null)
        {
            return null;
        }

        var card = fieldPickAnswer.Selections[0].GetComponent<AbilityCardComponent>();

        AbilityCardBody.AddTarget(card);
        return new[] { new EnableAttachedEffectAction() { CardObject = card, SetEnabled = false } };
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return Activate(gameController);
    }

    public override GameAction[] TurnEnd(GameController gameController)
    {
        if (!readyToUse)
        {
            if (TurnTimer == 1 && AbilityCardBody.Targets.Count > 0)
            {
                return new[] { new EnableAttachedEffectAction() { CardObject = AbilityCardBody.Targets[0] as AbilityCardBaseComponent, SetEnabled = false } };
            }
            else if (TurnTimer == 3)
            {
                readyToUse = true;
            }
        }

        return null;
    }
}
