using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UltimateBladeDance : AttachedEffect
{
    private bool isActive;

    public UltimateBladeDance(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        isActive = true;
        return null;
    }

    public override GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        if(!isActive)
        {
            return null;
        }

        DamageAction damageAction = action as DamageAction;
        AttachedEffect originEffect = action.OriginEffect as AttachedEffect;
        if(damageAction == null || !damageAction.IsAttackDamage || originEffect == null ||
        originEffect.Parent.CardType != Card.Type.AbilityCard ||  originEffect.User != User)
        {
            return null;
        }

        GameAction[] actions = new GameAction[1];
        actions[0] = new IncreaseDamageAction(50);

        return actions;
    }

    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        EndTurnAction endTurnAction = action as EndTurnAction;
        if(endTurnAction == null)
        {
            return null;
        }

        isActive = false;
        return null;
    }
}