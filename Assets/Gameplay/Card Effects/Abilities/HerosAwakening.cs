﻿
using System.Collections.Generic;

public class HerosAwakening : AttachedEffect
{
    private static readonly int DamageBonus = 30;

    public HerosAwakening(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    {}


    public override GameAction[] TriggerOnAttach(GameController gameController)
    {
        TurnTimer = 0;
        return null;
    }

    public override GameAction[] Enabled(GameController gameController)
    {
        FighterCardComponent fighterCard = GetThisFighterCard(gameController);

        AbilityCardBaseComponent thisAbilityCard = CardObject as AbilityCardBaseComponent;
        List<GameAction> actions = new List<GameAction>();

        foreach (AbilityCardBaseComponent abilityCard in fighterCard.Abilities)
        {
            if (abilityCard.AbilityData.Style == Ability.Style.Melee && abilityCard.AbilityData.HasAttack)
            {
                thisAbilityCard.AddTarget(abilityCard);
                actions.Add(new SetOnHitTriggerAction(abilityCard, this, GameAction.SetMode.Add));
            }
        }
        return actions.ToArray();
    }

    public override GameAction[] Disabled(GameController gameController)
    {
        AbilityCardBaseComponent thisAbilityCard = CardObject as AbilityCardBaseComponent;
        List<GameAction> actions = new List<GameAction>();

        foreach (CardComponent abilityCard in thisAbilityCard.Targets)
        {
            actions.Add(new SetOnHitTriggerAction(abilityCard as AbilityCardBaseComponent, this, GameAction.SetMode.Remove));
        }

        thisAbilityCard.RemoveAllTargets();

        return actions.ToArray();
    }

    public override GameAction[] TurnEnd(GameController gameController)
    {
        if (TurnTimer >= 3)
        {
            GameAction[] actions = new GameAction[1];
            actions[0] = new DiscardAction (CardObject);
            return actions;
        }
        return null;
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        FighterGameCard opposingFighter = GetUserOpposingActiveFighter(gameController);
        if (!opposingFighter.HasStatusCondition(StatusCondition.Dazed))
        {
            return null;
        }

        GameAction[] actions = { new DamageAction { damageAmount = DamageBonus, targetFighterCard = damageAction.targetFighterCard} };

        return actions;
    }
}
