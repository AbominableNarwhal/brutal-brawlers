﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IcicleShard : AttachedEffect
{
    public IcicleShard(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }
}
