﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class FatelessDestiny : AttachedEffect
{
    public FatelessDestiny(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        GameAction[] actions = new GameAction[1];
        actions[0] = new TapAuraAction() { NumberOfAura = -2, TargetPlayer = User.Owner };
        return actions;
    }

    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        var abilityAction = action as AttachAbilityCardAction;

        if (abilityAction != null && abilityAction.FighterCard == GetThisFighterCard(gameController))
        {
            return new[] { new DrawAction(User.Owner) };
        }

        return null;
    }
}
