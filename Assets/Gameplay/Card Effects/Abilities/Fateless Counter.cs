﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class FatelessCounter : AttachedEffect
{
    public FatelessCounter(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        foreach (GameAction action in gameController.ActionStack)
        {
            damageAction = action as DamageAction;
            if (damageAction == null || !damageAction.IsAttackDamage || damageAction.damageAmount > 30)
            {
                continue;
            }
            break;
        }

        if (damageAction == null)
        {
            return null;
        }

        GameAction[] actions = new GameAction[2];
        actions[0] = new NegateAction() { ActionToNegate = damageAction };
        actions[1] = new DamageAction()
        {
            damageAmount = ParentAbility.Damage,
            targetFighterCard = GetUserOpposingActiveFighterCard(gameController)
        };

        return actions;
    }
}
