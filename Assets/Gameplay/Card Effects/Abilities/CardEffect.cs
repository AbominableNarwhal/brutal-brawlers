using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using static GameController;

public abstract class CardEffect
{
    protected int TurnTimer { get; set; } = 0;
    protected int OffensiveTurnTimer { get; set; } = 0;
    protected int DefensiveTurnTimer { get; set; } = 0;
    public GameCard Parent
    {
        get;
    }

    public CardComponent CardObject
    {
        get;
    }

    public CardEffect(CardComponent cardComponent)
    {
        if(cardComponent == null)
        {
            return;
        }
        Parent = cardComponent.CardData;
        CardObject = cardComponent;
    }
    public virtual GameAction[] OnAttack(GameController gameController)
    {
        return null;
    }

    public virtual GameAction[] OnDefend(GameController gameController, DamageAction damageAction)
    {
        return null;
    }

    public virtual GameAction[] OnActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        return null;
    }

    public virtual GameAction[] OnActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        if (action is EndTurnAction)
        {
            TurnTimer++;
            if (gameController.offensivePlayer == Parent.Owner)
            {
                DefensiveTurnTimer++;
            }
            else
            {
                OffensiveTurnTimer++;
            }
            return OnTurnEnd(gameController);
        }
        return null;
    }

    public virtual GameAction[] OnQueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return null;
    }

    public virtual GameAction[] OnRemovedFromPlay(GameController gameController)
    {
        return null;
    }

    public virtual GameAction[] OnTurnEnd(GameController gameController)
    {
        return null;
    }

    public virtual ValidationResult OnValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return ValidationResult.Defer;
    }

    public virtual int OnAlterAuraCost(GameController gameController, IAuraConsumer powerOrAbility)
    {
        return 0;
    }

    #region Helper Functions

    protected void AddActionsToList(List<GameAction> list, GameAction[] actions)
    {
        if (actions == null)
        {
            return;
        }

        list.AddRange(actions);
    }

    /// <summary>Returns the active fighter
    /// </summary>
    public FighterGameCard GetActiveFighter(GameController gameController)
    {
        return gameController.GetPlayerGameBoard(Parent.Owner).ActiveFighter;
    }

    /// <summary>Returns the opposing active fighter
    /// </summary>
    public FighterGameCard GetOpposingActiveFighter(GameController gameController)
    {
        return gameController.GetPlayerGameBoard(gameController.GetPlayerOpponent(Parent.Owner)).ActiveFighter;
    }

    public FighterCardComponent GetOpposingActiveFighterCard(GameController gameController)
    {
        return gameController.GetPlayerGameBoard(gameController.GetPlayerOpponent(Parent.Owner)).ActiveFighterCard;
    }

    #endregion
}
