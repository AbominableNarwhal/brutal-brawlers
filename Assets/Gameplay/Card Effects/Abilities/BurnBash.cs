﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnBash : AttachedEffect
{
    DrawAction drawAction = null;
    FighterCardComponent targetFighterCard = null;

    public BurnBash(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    {}

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        targetFighterCard = damageAction.targetFighterCard;
        GameAction[] actions = new GameAction[1];
        drawAction = new DrawAction(User.Owner);
        actions[0] = drawAction;

        return actions;
    }

    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        if (drawAction == action && drawAction.ResultDraw.DrawnCards.Length == 1 && drawAction.ResultDraw.DrawnCards[0].CardData.CardType == Card.Type.AuraCard)
        {
            GameAction[] actions = new GameAction[1];
            actions[0] = new DamageAction { damageAmount = 30, IsAttackDamage = false, targetFighterCard = targetFighterCard };

            return actions;
        }

        return null;
    }
}
