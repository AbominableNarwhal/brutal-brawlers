public class ElectroShock : AttachedEffect
{
    public ElectroShock(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        return new[] { 
            new ChangeStatusConditionAction() { AddCondition = true, Condition = StatusCondition.Electrified, TargetFighter = damageAction.targetFighterCard },
            new ChangeStatusConditionAction() { AddCondition = true, Condition = StatusCondition.Electrified, TargetFighter = damageAction.targetFighterCard }
        };
    }
}