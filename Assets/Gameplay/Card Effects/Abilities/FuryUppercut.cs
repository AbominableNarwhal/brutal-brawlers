﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class FuryUppercut : AttachedEffect
{
    public FuryUppercut(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    {}

    public override GameAction[] Attack(GameController gameController)
    {
        GameAction[] actions = new GameAction[1];
        actions[0] = GenerateAbilityDamageAction(gameController, ParentAbility.Damage, new GameAction[1] {
            new ChangeStatusConditionAction() { AddCondition = true, Condition = StatusCondition.Dazed,
                TargetFighter = GetUserOpposingActiveFighterCard(gameController)
            }
        });
        return actions;
    }
}
