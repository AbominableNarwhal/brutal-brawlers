﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class SaygensWill : AttachedEffect
{
    AbilityGameCard.Status saygensWillBuff;
    public SaygensWill(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    {
        saygensWillBuff = new AbilityGameCard.Status { name = Parent.CardName, tributeCost = -1 };
    }

    public override GameAction[] TurnEnd(GameController gameController)
    {
        return Update(gameController);
    }

    public override GameAction[] Enabled(GameController gameController)
    {
        return Update(gameController);
    }

    public override GameAction[] Disabled(GameController gameController)
    {
        return Update(gameController);
    }

    public GameAction[] Update(GameController gameController)
    {
        if (gameController.defensivePlayer == ParentAbility.Owner)
        {
            return Activate(gameController);
        }
        else if (gameController.offensivePlayer == ParentAbility.Owner)
        {
            return Deactivate();
        }
        return null;
    }

    public GameAction[] Activate(GameController gameController)
    {
        List<GameAction> actions = new List<GameAction>();

        foreach(AbilityCardBaseComponent abilityCard in GetThisFighterCard(gameController).Abilities)
        {
            if (abilityCard.AbilityData.HasDefend)
            {
                AbilityCardBody.AddTarget(abilityCard);
                actions.Add(new SetAbilityStatsChangeAction(abilityCard, saygensWillBuff, GameAction.SetMode.Add));
            }
        }

        return actions.ToArray();
    }

    public GameAction[] Deactivate()
    {
        List<GameAction> actions = new List<GameAction>();

        foreach (AbilityCardBaseComponent cardComponent in AbilityCardBody.Targets)
        {
            actions.Add(new SetAbilityStatsChangeAction(cardComponent, saygensWillBuff, GameAction.SetMode.Remove));
        }

        AbilityCardBody.RemoveAllTargets();

        return actions.ToArray();
    }

}
