﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public class AuroraGemx3 : AttachedEffect
{
    private static readonly string CardPickMessage = "Pick 3 aura cards to add to your hand.";
    private bool firstOffensiveTurn = true;

    public AuroraGemx3(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        var phaseChangeAction = action as PhaseChangeAction;

        if (phaseChangeAction != null && phaseChangeAction.nextPhase == GameController.GamePhase.Attack && firstOffensiveTurn && ThisFighterIsActive(gameController) && gameController.offensivePlayer == User.Owner)
        {
            firstOffensiveTurn = false;
            Transform deck = gameController.GetPlayerGameBoard(User.Owner).Deck;

            var validCards = new List<CardComponent>();
            foreach (Transform child in deck)
            {
                var card = child.GetComponent<CardComponent>();
                if (card.CardData.CardType == Card.Type.AuraCard)
                {

                    validCards.Add(card);
                }
            }

            var cardPickQuery = new CardPickQuery() { cards = validCards.ToArray(), enableCancel = true, message = CardPickMessage, to = User.Owner, from = this, maxCards = 3, minCards = 0 };
            var queryAction = new QueryAction() { query = cardPickQuery };

            return new[] { queryAction };
        }

        return null;
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var cardPickAnswer = answer as CardPickAnswer;

        if (cardPickAnswer != null && cardPickAnswer.cards.Length > 0)
        {
            Transform hand = gameController.GetPlayerGameBoard(User.Owner).Hand;
            var actions = new List<GameAction>();
            foreach (var card in cardPickAnswer.cards)
            {
                actions.Add(new MoveCardAction(card, hand));
            }

            return actions.ToArray();
        }

        return null;
    }
}
