public class RapidSlash : AttachedEffect
{
    public RapidSlash(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }
}
