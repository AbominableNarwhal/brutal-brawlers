public class FuryPunch : AttachedEffect
{
    public FuryPunch(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage + User.Strength / 100 * 10);
    }
}