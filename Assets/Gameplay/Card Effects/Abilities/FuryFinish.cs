﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GameController;

public class FuryFinish : AttachedEffect
{
    public FuryFinish(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        if (!FinisherReady(gameController))
        {
            return null;
        }

        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }

    public override ValidationResult ValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return ActivateQueryDeferCondition(answer, FinisherReady(gameController));
    }

    private bool FinisherReady(GameController gameController)
    {
        bool usedFuryPunch = false;
        bool usedFuryUppercut = false;
        foreach (GameAction action in gameController.CompletedActions)
        {
            var abilityAction = action as ActivateAbilityAction;
            if (abilityAction == null)
            {
                continue;
            }

            if (abilityAction.abilityCard.CardData.CardName == "Fury Punch")
            {
                usedFuryPunch = true;
            }

            if (abilityAction.abilityCard.CardData.CardName == "Fury Uppercut")
            {
                usedFuryUppercut = true;
            }
        }

        return usedFuryPunch && usedFuryUppercut;
    }
}
