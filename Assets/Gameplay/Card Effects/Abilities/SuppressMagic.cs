using System.Collections.Generic;
using UnityEngine;

public class SuppressMagic : AttachedEffect
{
    public SuppressMagic(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        TurnTimer = 0;
        List<GameObject> validSelections = new List<GameObject>();
        CardComponent fighterCard = GetUserOpposingActiveFighterCard(gameController);

        //Find Attached Abilities
        Transform attachedAbilities = fighterCard.transform.GetChild(0).Find("Attached Abilities");
        foreach(Transform child in attachedAbilities)
        {
            validSelections.Add(child.gameObject);
        }

        //Find Native Abilities
        Transform nativeAbilities = fighterCard.transform.GetChild(0).Find("Ability Start");
        foreach(Transform child in nativeAbilities)
        {
            validSelections.Add(child.gameObject);
        }

        FieldPickQuery fieldPickQuery = new FieldPickQuery(){ ValidSelections = validSelections.ToArray(), Message = "Choose an ability on your opponents active fighter.",
        to = User.Owner, from = this };
        GameAction[] actions = new GameAction[1];
        actions[0] = new QueryAction() { query = fieldPickQuery };

        return actions;
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        FieldPickAnswer fieldPickAnswer = answer as FieldPickAnswer;

        if (fieldPickAnswer == null)
        {
            return null;
        }

        if (fieldPickAnswer.Selections.Length == 0)
        {
            return null;
        }
        var abiltyToDiable = fieldPickAnswer.Selections[0].GetComponent<AbilityCardComponent>();

        var action = new EnableAttachedEffectAction() { CardObject = abiltyToDiable, SetEnabled = false};

        AbilityCardBody.AddTarget(abiltyToDiable);

        return new[] { action };
    }

    public override GameAction[] TurnEnd(GameController gameController)
    {
        if (TurnTimer == 1 && AbilityCardBody.Targets.Count > 0)
        {
            return new[] { new EnableAttachedEffectAction() { CardObject = AbilityCardBody.Targets[0] as AbilityCardBaseComponent, SetEnabled = false } };
        }

        return null;
    }
}