﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class NomadsSight : AttachedEffect
{
    private static readonly string QueryMessage = "Choose a card to put on the top of your deck";
    public NomadsSight(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    {}

    public override GameAction[] Attack(GameController gameController)
    {
        Transform deck = gameController.GetPlayerGameBoard(User.Owner).Deck;
        if (deck.childCount == 0)
        {
            return null;
        }

        var cards = new List<CardComponent>();
        for (int i = 0; i < 3; i++)
        {
            if (deck.childCount <= i)
            {
                continue;
            }

            CardComponent card = deck.GetChild(i).GetComponent<CardComponent>();

            cards.Add(card);
        }

        CardPickQuery cardPick = new CardPickQuery() { cards = cards.ToArray(), enableCancel = false, from = this, to = User.Owner, maxCards = 1, minCards = 1, message = QueryMessage };
        GameAction[] actions = new GameAction[1];
        actions[0] = new QueryAction() { query = cardPick };
        return actions;
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var queryAnswer = answer as CardPickAnswer;

        DeckMoveAction action = new DeckMoveAction(queryAnswer.cards[0], 0);
        return new[] { action };
    }
}
