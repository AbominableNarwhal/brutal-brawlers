﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NomadsLesson : AttachedEffect
{
    private static readonly string QueryMessage = "You may choose one Ability card from your deck and add it to your hand.";

    public NomadsLesson(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        TagInAction tagInAction = action as TagInAction;
        if(tagInAction == null || tagInAction.targetFighter.CardData != User)
        {
            return null;
        }
        
        Transform deck = gameController.GetPlayerGameObject(User.Owner).transform.Find("Deck");
        List<CardComponent> cards = new List<CardComponent>();
        foreach(Transform child in deck)
        {
            CardComponent card = child.GetComponent<CardComponent>();
            if(card.CardData.CardType == Card.Type.AbilityCard)
            {
                cards.Add(child.GetComponent<CardComponent>());
            }
        }


        CardPickQuery query = new CardPickQuery() { from = this, to = User.Owner,  cards = cards.ToArray(), message = QueryMessage, enableCancel = true};
        GameAction[] actions = new GameAction[1];
        actions[0] = new QueryAction() { query = query };
        return actions;
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        CardPickAnswer cardPickAnswer = answer as CardPickAnswer;
        if(cardPickAnswer == null || cardPickAnswer.cards.Length == 0)
        {
            return null;
        }

        GameAction[] actions = new GameAction[1];
        actions[0] = new DrawAction(User.Owner, cardPickAnswer.cards);
        return actions;
    }
}
