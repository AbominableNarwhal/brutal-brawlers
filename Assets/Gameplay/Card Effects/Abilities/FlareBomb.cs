﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareBomb : AttachedEffect
{
    public static readonly int BaseDamage = 20;
    public FlareBomb(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        FighterCardComponent oppoFighter = GetUserOpposingActiveFighterCard(gameController);
        int fireMarkCount = oppoFighter.MarkerCount(gameController.FireMark);

        var actions = new List<GameAction>();
        actions.Add(GenerateAbilityDamageAction(gameController, fireMarkCount * BaseDamage));

        for (int i = 0; i < fireMarkCount; i++)
        {
            actions.Add(new SetMarkerAction(oppoFighter, gameController.FireMark, GameAction.SetMode.Remove));
        }

        return actions.ToArray();
    }
}
