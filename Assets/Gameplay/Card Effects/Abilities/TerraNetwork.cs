using UnityEngine;
using static GameController;

public class TerraNetwork : AttachedEffect
{
    public TerraNetwork(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    {}

    public override GameAction[] Attack(GameController gameController)
    {
        GameAction[] actions = new GameAction[1];

        Transform deck = gameController.GetPlayerGameBoard(User.Owner).Deck;
        foreach(Transform child in deck)
        {
            CardComponent card = child.GetComponent<CardComponent>();
            if(card == null)
            {
                return null;
            }

            if(card.CardData.CardType == Card.Type.AuraCard)
            {
                card.IsFaceUp = false;
                actions[0] = new PlayAuraAction() { auraCard = card };
                break;
            }
        }

        return actions;
    }

    public override ValidationResult ValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var combatAnswer = answer as CombatQueryAnswer;
        if (combatAnswer != null && combatAnswer.type == CombatQueryAnswer.Type.ActivateAbility && combatAnswer.card == CardObject)
        {
            if (gameController.GetPlayerTotalAuraAmount(User.Owner) >= CombatQuery.MaxAuraCardInPlay)
            {
                return ValidationResult.Fail;
            }
        }
        
        return ValidationResult.Defer;
    }
}