using static GameController;

public class QuickCounter : AttachedEffect
{
    public QuickCounter(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        int damageToReflect = damageAction.damageAmount;

        GameAction[] actions = new GameAction[2];
        actions[0] = new NegateAction() { ActionToNegate = damageAction };
        actions[1] = new DamageAction() { damageAmount = damageToReflect + ParentAbility.Damage,
        targetFighterCard = GetUserOpposingActiveFighterCard(gameController) };

        return actions;
    }

    public override ValidationResult ValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return ActivateQueryDeferCondition(answer, CounterReady(gameController));
    }

    private bool CounterReady(GameController gameController)
    {
        DamageAction incomingDamage = gameController.IncomingDamage;
        var ability = incomingDamage.OriginEffect.CardObject.CardData as AbilityGameCard;
        return incomingDamage != null && ability != null && ability.Style != Ability.Style.Spell && incomingDamage.damageAmount <= 30;
    }
}