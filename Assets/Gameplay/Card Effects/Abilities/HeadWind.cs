﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadWind : AttachedEffect
{
    public HeadWind(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return new GameAction[] { new SetBlockerAction(ParentAbility.BlockDamage, GetThisFighterCard(gameController)) };
    }
}
