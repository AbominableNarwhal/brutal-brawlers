public class BleedingSlash : AttachedEffect
{
    private GameAction damageAction;
    private bool hit = false;
    private FighterCardComponent targetFighterCard;

    public BleedingSlash(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        GameAction[] actions = GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
        damageAction = actions[0];
        return actions;
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        return new[] {
            new ChangeStatusConditionAction() { AddCondition = false, Condition = StatusCondition.Bleed, TargetFighter = damageAction.targetFighterCard },
            new ChangeStatusConditionAction() { AddCondition = false, Condition = StatusCondition.Bleed, TargetFighter = damageAction.targetFighterCard },
            new ChangeStatusConditionAction() { AddCondition = false, Condition = StatusCondition.Bleed, TargetFighter = damageAction.targetFighterCard } };
    }

    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        if(action == damageAction)
        {
            hit = true;
            targetFighterCard = GetUserOpposingActiveFighterCard(gameController);
        }

        EndTurnAction endTurnAction = action as EndTurnAction;
        if(endTurnAction == null || gameController.offensivePlayer != User.Owner || !hit)
        {
            return null;
        }

        hit = false;
        GameAction[] actions = new GameAction[1];
        actions[0] = new DamageAction() { damageAmount = 30, targetFighterCard = targetFighterCard };
        return actions;
    }
}