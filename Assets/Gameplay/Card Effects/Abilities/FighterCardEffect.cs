﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterCardEffect : CardEffect
{
    public readonly static int BleedDamage = 10;
    public readonly static int ElectrifiedDamage = 20;

    private FighterGameCard fighterParent = null;
    private FighterCardComponent fighterCardBody = null;

    public FighterCardEffect(FighterCardComponent cardComponent) : base(cardComponent)
    {
        fighterCardBody = cardComponent;
        fighterParent = fighterCardBody.FighterData;
    }

    public override GameAction[] OnActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        List<GameAction> actions = new List<GameAction>();
        AddActionsToList(actions, base.OnActionCompleteBroadcast(gameController, action));

        if (fighterParent.HasStatusCondition(StatusCondition.Electrified))
        {
            var activateAction = action as ActivateAbilityAction;
            if (activateAction != null && activateAction.abilityCard.AbilityData.AbilityType == Ability.AbilityType.Water && gameController.FindAttachedFighter(activateAction.abilityCard.AbilityData) == fighterParent)
            {
                actions.Add(new DamageAction() { damageAmount = ElectrifiedDamage, IsAttackDamage = false, targetFighterCard = fighterCardBody });
            }
        }
        return actions.ToArray();
    }

    public override GameAction[] OnTurnEnd(GameController gameController)
    {
        var actions = new List<GameAction>();
        if (fighterParent.HasStatusCondition(StatusCondition.Dazed))
        {
            actions.Add(new ChangeStatusConditionAction() { AddCondition = false, Condition = StatusCondition.Dazed, TargetFighter = fighterCardBody });
        }

        if (fighterParent.HasStatusCondition(StatusCondition.Electrified))
        {
            actions.Add(new ChangeStatusConditionAction() { AddCondition = false, Condition = StatusCondition.Electrified, TargetFighter = fighterCardBody });
        }

        if (fighterParent.HasStatusCondition(StatusCondition.Bleed))
        {
            actions.Add(new DamageAction() { damageAmount = BleedDamage, IsAttackDamage = false, targetFighterCard = fighterCardBody });
            actions.Add(new ChangeStatusConditionAction() { AddCondition = false, Condition = StatusCondition.Bleed, TargetFighter = fighterCardBody });
        }
        return actions.ToArray();
    }

    public override GameController.ValidationResult OnValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        if (fighterParent.HasStatusCondition(StatusCondition.Dazed))
        {
            var combatQuery = query as CombatQuery;
            var combatAnswer = answer as CombatQueryAnswer;

            if(combatAnswer != null && combatQuery != null && combatAnswer.type == CombatQueryAnswer.Type.ActivateAbility && combatQuery.CombatType == PlayType.Defend)
            {
                return GameController.ValidationResult.Fail;
            }
        }
        return GameController.ValidationResult.Defer;
    }
}
