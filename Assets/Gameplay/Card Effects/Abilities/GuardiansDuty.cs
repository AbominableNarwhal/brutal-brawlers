﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardiansDuty : AttachedEffect
{
    public static readonly int TagInReductionAmount = 4;
    public GuardiansDuty(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        var phaseChangeAction = action as PhaseChangeAction;

        if (phaseChangeAction != null)
        {
            if (phaseChangeAction.nextPhase == GameController.GamePhase.Defend)
            {
                return new GameAction[] { new FighterStatChangeAction(GetThisFighterCard(gameController), 0, 0, 0, -TagInReductionAmount) };
            }
            else if (phaseChangeAction.nextPhase == GameController.GamePhase.Resolve)
            {
                return new GameAction[] { new FighterStatChangeAction(GetThisFighterCard(gameController), 0, 0, 0, TagInReductionAmount) };
            }
        }

        return null;
    }
}
