using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearingStrike : AttachedEffect
{
    public readonly static int PassiveDamage = 20;
    private readonly static string MessageText = "Would you like to remove one fire-mark to deal 20 pasive damage?";

    public SearingStrike(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        var actions = new List<GameAction>();

        actions.Add(GenerateAbilityDamageAction(gameController, ParentAbility.Damage));
        
        return actions.ToArray();
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        int fireMarkCount = User.MarkerCount(gameController.FireMark);

        if (fireMarkCount > 0)
        {
            var optionPick = new OptionPickQuery() { from = this, to = User.Owner, message = MessageText, options = new List<string>() { "Yes", "No" } };
            var queryAction = new QueryAction() { query = optionPick };

            return new GameAction[] { queryAction };
        }
        return null;
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var optionCardAnswer = answer as OptionAnswer;

        if (optionCardAnswer != null && optionCardAnswer.selection == "Yes")
        {

            FighterCardComponent fighter = GetThisFighterCard(gameController);
            FighterCardComponent oppoFighter = GetUserOpposingActiveFighterCard(gameController);
            return new GameAction[] {
                new SetMarkerAction(fighter, gameController.FireMark, GameAction.SetMode.Remove),
                new DamageAction(){ damageAmount = PassiveDamage, targetFighterCard = oppoFighter, IsAttackDamage = false}
            };
        }

        return null;
    }
}
