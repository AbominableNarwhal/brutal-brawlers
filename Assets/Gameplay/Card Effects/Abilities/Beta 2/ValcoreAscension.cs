using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValcoreAscension : AttachedEffect
{
    public static readonly int MarkerThreshold = 5;
    public static readonly int DamageAmp = 30;
    public ValcoreAscension(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        DamageAction damageAction = action as DamageAction;

        if (damageAction != null && damageAction.IsAttackDamage)
        {
            AttachedEffect attachedEffect = damageAction.OriginEffect as AttachedEffect;
            int markerCount = GetThisFighterCard(gameController).MarkerCount();

            if (attachedEffect.User == User && markerCount >= MarkerThreshold)
            {
                return new GameAction[] { new IncreaseDamageAction(DamageAmp) };
            }
        }

        return null;
    }
}
