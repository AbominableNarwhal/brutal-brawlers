using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VanishingNomad : AttachedEffect
{
    public static readonly string Message = "Choose a fighter to tag in.";
    public VanishingNomad(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        var queryCards = new List<GameObject>();
        FighterCardComponent fighter = gameController.GetPlayerGameBoard(User.Owner).SupportFighter1Card;

        if (!fighter.IsKOed)
        {
            queryCards.Add(fighter.gameObject);
        }

        fighter = gameController.GetPlayerGameBoard(User.Owner).SupportFighter2Card;

        if (!fighter.IsKOed)
        {
            queryCards.Add(fighter.gameObject);
        }

        var fieldQuery = new FieldPickQuery() { from = this, to = User.Owner, ValidSelections = queryCards.ToArray(), Message = Message };

        return new GameAction[] { new QueryAction() { query = fieldQuery } };
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var fieldPickAnswer = answer as FieldPickAnswer;
        if (fieldPickAnswer != null && fieldPickAnswer.Selections.Length > 0)
        {
            var fighter = fieldPickAnswer.Selections[0].GetComponent<FighterCardComponent>();

            if (fighter != null)
            {
                return new GameAction[] { new TagInAction() { targetFighter = fighter } };
            }
        }

        return null;
    }
}
