using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalIceBarbs : AttachedEffect
{
    public CrystalIceBarbs(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        var actions = new List<GameAction>();

        if (damageAction.ResultDamage.BlockersBroken > 0)
        {
            FighterCardComponent oppoFighter = GetUserOpposingActiveFighterCard(gameController);

            for (int i = 0; i < damageAction.ResultDamage.BlockersBroken; i++)
            {
                actions.Add(new SetMarkerAction(oppoFighter, gameController.WaterMark, GameAction.SetMode.Add));
            }
        }

        return actions.ToArray();
    }
}
