using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveCrash : AttachedEffect
{
    public static readonly int BaseAttackDamage = 30;
    public WaveCrash(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        FighterCardComponent oppoFighter = GetUserOpposingActiveFighterCard(gameController);

        int waterMarks = oppoFighter.MarkerCount(gameController.WaterMark);
        int damage = waterMarks * BaseAttackDamage;

        return GenerateAbilityDamageActionArr(gameController, damage);
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        FighterCardComponent oppoFighter = damageAction.targetFighterCard;
        int waterMarks = oppoFighter.MarkerCount(gameController.WaterMark);

        var actions = new List<GameAction>();
        for (int i = 0; i < waterMarks; i++)
        {
            actions.Add(new SetMarkerAction(oppoFighter, gameController.WaterMark, GameAction.SetMode.Remove));
        }

        return actions.ToArray();
    }
}
