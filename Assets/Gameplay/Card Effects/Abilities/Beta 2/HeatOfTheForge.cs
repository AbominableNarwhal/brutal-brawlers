using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatOfTheForge : AttachedEffect
{
    private static readonly string AuraOnlyMessage = "Choose one Aura card to add to your hand";
    private static readonly string AuraAndEquipmentMessage = "Choose one Aura card or Equipment to add to your hand";
    private static readonly int AuraOnlyQuery = 0;
    private static readonly int AuraEquipmentQuery = 1;
    private int queriesAsked = 0;
    private CardComponent firstAuraCard = null;

    public HeatOfTheForge(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateUnaskedQuery(gameController);
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var actions = new List<GameAction>();
        CardPickAnswer cardPickAnswer = answer as CardPickAnswer;
        if (cardPickAnswer != null && cardPickAnswer.cards.Length > 0)
        {
            firstAuraCard = cardPickAnswer.cards[0];
            actions.Add(new DrawAction(User.Owner, cardPickAnswer.cards));
        }

        GameAction[] unaskedQuery = GenerateUnaskedQuery(gameController);

        if (unaskedQuery != null)
        {
            actions.AddRange(unaskedQuery);
        }

        return actions.ToArray();
    }

    public override GameAction[] TurnEnd(GameController gameController)
    {
        queriesAsked = 0;
        firstAuraCard = null;
        return null;
    }

    public GameAction[] GenerateUnaskedQuery(GameController gameController)
    {
        if (queriesAsked == 0)
        {
            var queryCards = new List<CardComponent>();
            Transform deck = gameController.GetPlayerGameBoard(User.Owner).Deck;

            foreach (Transform child in deck)
            {
                var card = child.GetComponent<CardComponent>();

                if (card.CardData.CardType == Card.Type.AuraCard)
                {
                    queryCards.Add(card);
                }
            }

            var cardQuery = new CardPickQuery() {
                cards = queryCards.ToArray(),
                from = this, to = User.Owner,
                enableCancel = false,
                message = AuraOnlyMessage,
                minCards = (uint)Math.Min(queryCards.Count, 1),
                Id = AuraOnlyQuery };

            queriesAsked += 1;
            return new GameAction[] { new QueryAction() { query = cardQuery } };
        }
        else if (queriesAsked == 1)
        {
            var queryCards = new List<CardComponent>();
            Transform deck = gameController.GetPlayerGameBoard(User.Owner).Deck;

            foreach (Transform child in deck)
            {
                var card = child.GetComponent<CardComponent>();

                if (card != firstAuraCard && (card.CardData.CardType == Card.Type.AuraCard || card.CardData.CardType == Card.Type.EquipmentCard))
                {
                    queryCards.Add(card);
                }
            }

            var cardQuery = new CardPickQuery() {
                cards = queryCards.ToArray(),
                from = this,
                to = User.Owner,
                enableCancel = false,
                message = AuraAndEquipmentMessage,
                minCards = (uint)Math.Min(queryCards.Count, 1),
                Id = AuraEquipmentQuery
            };

            queriesAsked += 1;
            return new GameAction[] { new QueryAction() { query = cardQuery } };
        }

        return null;
    }
}
