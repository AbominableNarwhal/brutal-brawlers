using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlacierFort : AttachedEffect
{
    public static readonly string GeyserFortName = "Geyser Fort";
    public GlacierFort(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        var attachAbilityCardAction = action as AttachAbilityCardAction;

        if (attachAbilityCardAction != null && attachAbilityCardAction.AbilityCard.CardData.CardName == GeyserFortName && attachAbilityCardAction.AbilityCardToReplace == CardObject)
        {
            return new GameAction[] { new RecallAction(AbilityCardBody, User.Owner) };
        }

        return null;
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        FighterCardComponent fighter = GetThisFighterCard(gameController);

        return new GameAction[] { new SetBlockerAction(ParentAbility.BlockDamage, fighter)};
    }
}
