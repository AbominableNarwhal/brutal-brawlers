using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoltThrust : AttachedEffect
{
    public JoltThrust(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        GameAction action = null;
        if (AuraCardWasPlayed(gameController))
        {
            action = new SetMarkerAction(GetThisFighterCard(gameController), gameController.ElectricMark, GameAction.SetMode.Add);
        }

        return new GameAction[] { action };
    }
}
