using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceCannon : AttachedEffect
{
    public static readonly int MaxWaterMarksToGive = 3;
    public static readonly string WaterBulletName = "WaterBullet";
    public IceCannon(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        var attachAbilityCardAction = action as AttachAbilityCardAction;

        if (attachAbilityCardAction != null && attachAbilityCardAction.AbilityCard.CardData.CardName == WaterBulletName && attachAbilityCardAction.AbilityCardToReplace == CardObject)
        {
            return new GameAction[] { new RecallAction(AbilityCardBody, User.Owner) };
        }

        return null;
    }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        FighterCardComponent targetFighter = damageAction.targetFighterCard;

        int watermarks = MaxWaterMarksToGive - targetFighter.MarkerCount(gameController.WaterMark);

        var actions = new List<GameAction>();
        for (int i = 0; i < watermarks; i++)
        {
            actions.Add(new SetMarkerAction(targetFighter, gameController.WaterMark, GameAction.SetMode.Add));
        }

        return actions.ToArray();
    }
}
