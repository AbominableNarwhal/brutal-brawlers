using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeInduction : AttachedEffect
{
    public BladeInduction(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        var phaseChangeAction = action as PhaseChangeAction;
        var playAuraAction = action as PlayAuraAction;
        GameAction newAction = null;

        if (phaseChangeAction != null && phaseChangeAction.nextPhase == GameController.GamePhase.Draw && gameController.offensivePlayer == User.Owner ||
            playAuraAction != null && playAuraAction.auraCard.CardData.Owner == User.Owner)
        {
            newAction = new SetMarkerAction(GetThisFighterCard(gameController), gameController.ElectricMark, GameAction.SetMode.Add);
        }

        return new GameAction[] { newAction };
    }

    public override GameAction[] TurnEnd(GameController gameController)
    {
        GameAction action = null;
        if (OffensiveTurnTimer == 3)
        {
            action = new DiscardAction(CardObject);
        }

        return new GameAction[] { action };
    }
}
