using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoltDrill : AttachedEffect
{
    public static readonly int MarksToAdd = 3;
    public JoltDrill(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        var actions = new List<GameAction>();
        if (AuraCardWasPlayed(gameController))
        {
            for (int i = 0; i < MarksToAdd; i++)
            {
                actions.Add(new SetMarkerAction(GetThisFighterCard(gameController), gameController.ElectricMark, GameAction.SetMode.Add));
            }
        }

        return actions.ToArray();
    }
}
