using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrozenBody : AttachedEffect
{
    public static readonly int MarkersToAdd = 2;

    public FrozenBody(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        FighterCardComponent fighter = GetThisFighterCard(gameController);
        FighterCardComponent oppoFighter = GetUserOpposingActiveFighterCard(gameController);
        var actions = new List<GameAction>();

        actions.Add(new SetBlockerAction(ParentAbility.BlockDamage, fighter));
        for (int i = 0; i < MarkersToAdd; i++)
        {
            actions.Add(new SetMarkerAction(oppoFighter, gameController.WaterMark, GameAction.SetMode.Add));
        }

        return actions.ToArray();
    }
}
