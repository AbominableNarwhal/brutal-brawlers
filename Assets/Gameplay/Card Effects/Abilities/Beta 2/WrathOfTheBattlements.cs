using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrathOfTheBattlements : AttachedEffect
{
    public static readonly int BaseDamage = 10;
    public static readonly int BaseBlock = 20;

    public WrathOfTheBattlements(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        FighterCardComponent fighterCard = GetThisFighterCard(gameController);

        int damage = 0;
        int blockers = 0;
        foreach (EquipmentCardComponent equipmentCard in fighterCard.FighterAttachedEquipment())
        {
            if (equipmentCard.EquipmentData.FightStyle == Ability.Style.Sword)
            {
                damage += BaseDamage;
            }
            else if (equipmentCard.EquipmentData.FightStyle == Ability.Style.Shield)
            {
                blockers += 1;
            }
        }

        var actions = new List<GameAction>();

        if (damage > 0)
        {
            actions.Add(GenerateAbilityDamageAction(gameController, damage));
        }
        if (blockers > 0)
        {
            for (int i = 0; i < blockers; i++)
            {
                actions.Add(new SetBlockerAction(BaseBlock, fighterCard));
            }
        }

        return actions.ToArray();
    }

    public override GameController.ValidationResult ValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var combatAnswer = answer as CombatQueryAnswer;

        if (combatAnswer.type == CombatQueryAnswer.Type.ActivateAbility && combatAnswer.card == CardObject)
        {
            FighterCardComponent fighterCard = GetThisFighterCard(gameController);

            foreach (EquipmentCardComponent equipmentCard in fighterCard.FighterAttachedEquipment())
            {
                if (equipmentCard.EquipmentData.FightStyle == Ability.Style.Sword || equipmentCard.EquipmentData.FightStyle == Ability.Style.Shield)
                {
                    return GameController.ValidationResult.Defer;
                }
            }
            return GameController.ValidationResult.Fail;
        }

        return GameController.ValidationResult.Defer;
    }
}
