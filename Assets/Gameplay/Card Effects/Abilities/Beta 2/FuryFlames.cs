using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuryFlames : AttachedEffect
{
    public FuryFlames(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        FighterCardComponent fighterCard = GetThisFighterCard(gameController);
        var actions = new List<GameAction>();
        actions.Add(new SetBlockerAction(ParentAbility.BlockDamage, fighterCard));

        var incomingAbilityEffect = damageAction.OriginEffect as AttachedEffect;

        if (incomingAbilityEffect != null)
        {
            for (int i = 0; i < incomingAbilityEffect.ParentAbility.AuraCost; i++)
            {
                actions.Add(new SetMarkerAction(fighterCard, gameController.FireMark, GameAction.SetMode.Add));
            }
        }

        return actions.ToArray();
    }
}
