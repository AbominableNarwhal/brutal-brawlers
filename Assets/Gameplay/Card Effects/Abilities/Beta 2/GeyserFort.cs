using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeyserFort : AttachedEffect
{
    public static readonly int BlockersToAdd = 3;
    public static readonly string GlacierFortName = "Glacier Fort";

    public GeyserFort(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        var attachAbilityCardAction = action as AttachAbilityCardAction;

        if (attachAbilityCardAction != null && attachAbilityCardAction.AbilityCard.CardData.CardName == GlacierFortName && attachAbilityCardAction.AbilityCardToReplace == CardObject)
        {
            return new GameAction[] { new RecallAction(AbilityCardBody, User.Owner) };
        }

        return null;
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        var actions = new List<GameAction>();
        FighterCardComponent fighter = GetThisFighterCard(gameController);

        for (int i = 0; i < BlockersToAdd; i++)
        {
            actions.Add(new SetBlockerAction(ParentAbility.BlockDamage, fighter));
        }
            
        return actions.ToArray();
    }
}
