using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;

public class BurnBack : AttachedEffect
{
    public static readonly int BasePassiveDamage = 10;
    public static readonly int BaseMarksToAdd = 1;

    public BurnBack(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        int abilityUsed = gameController.CountAbilityUsage(ParentAbility);

        int damage = BasePassiveDamage + BasePassiveDamage * abilityUsed;
        int newMarks = BaseMarksToAdd + BaseMarksToAdd * abilityUsed;

        var actions = new List<GameAction>();
        actions.Add(new DamageAction() { damageAmount = damage, IsAttackDamage = false, targetFighterCard = GetUserOpposingActiveFighterCard(gameController) });

        for (int i = 0; i < newMarks; i++)
        {
            actions.Add(new SetMarkerAction(GetThisFighterCard(gameController), gameController.FireMark, GameAction.SetMode.Add));
        }

        return actions.ToArray();
    }
}
