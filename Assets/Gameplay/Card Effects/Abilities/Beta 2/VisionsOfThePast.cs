using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionsOfThePast : AttachedEffect
{
    public VisionsOfThePast(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        var queryCards = new List<CardComponent>();
        Transform deck = gameController.GetPlayerGameBoard(User.Owner).Discard;

        foreach (Transform child in deck)
        {
            var card = child.GetComponent<CardComponent>();
            queryCards.Add(card);
        }

        var cardQuery = new CardPickQuery() { cards = queryCards.ToArray(), from = this, to = User.Owner, enableCancel = true };

        return new GameAction[] { new QueryAction() { query = cardQuery } };
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var actions = new List<GameAction>();
        CardPickAnswer cardPickAnswer = answer as CardPickAnswer;
        if (cardPickAnswer != null || cardPickAnswer.cards.Length > 0)
        {
            actions.Add(new DrawAction(User.Owner, cardPickAnswer.cards));
        }

        return actions.ToArray();
    }
}
