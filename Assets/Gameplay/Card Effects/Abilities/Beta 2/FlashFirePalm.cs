using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashFirePalm : AttachedEffect
{
    public static readonly int MarkersToRemove = 3;
    public static readonly int MarkersToAdd = 3;
    public static readonly int BaseDamage = 20;

    public FlashFirePalm(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        var actions = new List<GameAction>();

        int fireMarkCount = Mathf.Min(User.MarkerCount(gameController.FireMark), MarkersToRemove);
        int damage = ParentAbility.Damage + fireMarkCount * BaseDamage;

        for (int i = 0; i < fireMarkCount; i++)
        {
            actions.Add(new SetMarkerAction(GetThisFighterCard(gameController), gameController.FireMark, GameAction.SetMode.Remove));
        }

        actions.Add(new DamageAction() { damageAmount = damage, IsAttackDamage = true});

        return actions.ToArray();
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        var actions = new List<GameAction>();

        for (int i = 0; i < MarkersToAdd; i++)
        {
            actions.Add(new SetMarkerAction(GetThisFighterCard(gameController), gameController.FireMark, GameAction.SetMode.Add));
        }
        return actions.ToArray();
    }
}
