using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerBladeFlux : AttachedEffect
{
    public static readonly int AuraCardsToPick = 1;
    public static readonly string AuraPickMessage = "Pick one aura card to add to your hand";

    public PowerBladeFlux(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        var cards = new List<CardComponent>();
        foreach (Transform child in gameController.GetPlayerGameBoard(User.Owner).AuraStash)
        {
            var auraCard = child.GetComponent<AuraCardComponent>();
            if (auraCard && auraCard.IsFaceUp)
            {
                cards.Add(auraCard);
            }
        }

        var cardPickQuery = new CardPickQuery()
        {
            cards = cards.ToArray(), 
            enableCancel = false,
            from = this,
            to = User.Owner,
            minCards = (uint)Math.Min(cards.Count, AuraCardsToPick),
            maxCards = (uint)AuraCardsToPick,
            message = AuraPickMessage
        };
        return new GameAction[] { new QueryAction() { query = cardPickQuery} };
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var cardPickAnswer = answer as CardPickAnswer;

        if (cardPickAnswer != null && cardPickAnswer.cards.Length > 0)
        {
            var auraCard = cardPickAnswer.cards[0] as AuraCardComponent;
            return new GameAction[] { new RecallAction(auraCard, User.Owner) };
        }

        return null;
    }
}
