using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IonicGuard : AttachedEffect
{
    public static readonly int BlockersToAdd = 3;
    public static readonly int BonusBlockerDamage = 30;

    public IonicGuard(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        var actions = new List<GameAction>();
        FighterCardComponent fighter = GetThisFighterCard(gameController);

        for (int i = 0; i < BlockersToAdd; i++)
        {
            actions.Add(new SetBlockerAction(ParentAbility.BlockDamage, fighter));
        }

        if (fighter.MarkerCount(gameController.ElectricMark) > 0)
        {
            actions.Add(new SetBlockerAction(BonusBlockerDamage, fighter));
        }

        return actions.ToArray();
    }
}
