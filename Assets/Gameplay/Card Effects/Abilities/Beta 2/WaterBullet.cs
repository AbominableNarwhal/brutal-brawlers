using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBullet : AttachedEffect
{
    public static readonly string IceCannonName = "IceCannon";
    public WaterBullet(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        var attachAbilityCardAction = action as AttachAbilityCardAction;

        if (attachAbilityCardAction != null && attachAbilityCardAction.AbilityCard.CardData.CardName == IceCannonName && attachAbilityCardAction.AbilityCardToReplace == CardObject)
        {
            return new GameAction[] { new RecallAction(AbilityCardBody, User.Owner) };
        }

        return null;
    }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        return new GameAction[] { new SetMarkerAction(damageAction.targetFighterCard, gameController.WaterMark, GameAction.SetMode.Add) };
    }
}
