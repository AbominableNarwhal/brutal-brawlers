using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparkCut : AttachedEffect
{
    public SparkCut(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        var actions = new List<GameAction>();
        FighterCardComponent fighter = GetThisFighterCard(gameController);

        if (fighter.MarkerCount(gameController.ElectricMark) > 0)
        {
            actions.Add(new SetMarkerAction(fighter, gameController.ElectricMark, GameAction.SetMode.Remove));
        }

        actions.Add(new DamageAction() { damageAmount = ParentAbility.Damage, IsAttackDamage = true });

        return actions.ToArray();
    }

    public override GameController.ValidationResult ValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return ActivateQueryDeferCondition(answer, GetThisFighterCard(gameController).MarkerCount(gameController.ElectricMark) > 0);
    }
}
