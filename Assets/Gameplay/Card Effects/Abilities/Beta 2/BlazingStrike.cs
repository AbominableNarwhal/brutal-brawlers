using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlazingStrike : AttachedEffect
{
    public readonly static int MarkersToRemove = 3;
    public readonly static int BasePassiveDamage = 30;

    public BlazingStrike(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        var actions = new List<GameAction>();

        actions.Add(GenerateAbilityDamageAction(gameController, ParentAbility.Damage));

        return actions.ToArray();
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        int fireMarkCount = Math.Min(User.MarkerCount(gameController.FireMark), MarkersToRemove);

        if (fireMarkCount > 0)
        {
            var actions = new List<GameAction>();
            FighterCardComponent fighterCard = GetThisFighterCard(gameController);
            FighterCardComponent oppoFighterCard = GetUserOpposingActiveFighterCard(gameController);

            for (int i = 0; i < fireMarkCount; i++)
            {
                actions.Add(new SetMarkerAction(fighterCard, gameController.FireMark, GameAction.SetMode.Remove));
            }

            actions.Add(new DamageAction() { damageAmount = fireMarkCount * BasePassiveDamage, IsAttackDamage = false, targetFighterCard = oppoFighterCard });

            return actions.ToArray();
        }
        return null;
    }
}
