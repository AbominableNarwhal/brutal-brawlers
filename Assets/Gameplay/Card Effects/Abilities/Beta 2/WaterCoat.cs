using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterCoat : AttachedEffect
{
    public WaterCoat(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        FighterCardComponent fighter = GetThisFighterCard(gameController);
        FighterCardComponent oppoFighter = GetUserOpposingActiveFighterCard(gameController);

        var actions = new List<GameAction>();
        actions.Add(new SetBlockerAction(ParentAbility.BlockDamage, fighter));
        actions.Add(new SetMarkerAction(oppoFighter, gameController.WaterMark, GameAction.SetMode.Add));

        return actions.ToArray();
    }

    public override GameController.ValidationResult ValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        Transform playerHand = gameController.GetPlayerGameBoard(User.Owner).Hand;
        return ActivateQueryDeferCondition(answer, playerHand.childCount > 0);
    }
}
