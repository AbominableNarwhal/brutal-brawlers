using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolarityRepulse : AttachedEffect
{
    public static readonly int BlockersToAdd = 1;
    public static readonly int BonusBlockerDamage = 20;
    public PolarityRepulse(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        var actions = new List<GameAction>();
        FighterCardComponent fighter = GetThisFighterCard(gameController);

        actions.Add(new SetBlockerAction(ParentAbility.BlockDamage, fighter));


        if (fighter.MarkerCount(gameController.ElectricMark) > 0)
        {
            for (int i = 0; i < BlockersToAdd; i++)
            {
                actions.Add(new SetBlockerAction(BonusBlockerDamage, fighter));
            }
        }

        return actions.ToArray();
    }
}
