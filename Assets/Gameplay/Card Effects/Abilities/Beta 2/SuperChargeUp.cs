using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperChargeUp : AttachedEffect
{
    public static readonly int MarkersToRemove = 2;
    private static readonly string QueryMessage = "You may choose {0} Aura card{1} from your deck and add it to your hand.";

    public SuperChargeUp(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        var actions = new List<GameAction>();

        FighterCardComponent fighterCard = GetThisFighterCard(gameController);

        int electricMarkCount = Math.Min(fighterCard.MarkerCount(gameController.ElectricMark), MarkersToRemove);

        if (electricMarkCount > 0)
        {
            for (int i = 0; i < electricMarkCount; i++)
            {
                actions.Add(new SetMarkerAction(fighterCard, gameController.ElectricMark, GameAction.SetMode.Remove));
            }
        }

        var auraCards = new List<CardComponent>();
        Transform deck = gameController.GetPlayerGameBoard(User.Owner).Deck;

        foreach (Transform child in deck)
        {
            CardComponent card = child.GetComponent<CardComponent>();

            if (card.CardData.CardType == Card.Type.AuraCard)
            {
                auraCards.Add(card);
            }
        }

        int auraCardsToGet = electricMarkCount + 1;

        CardPickQuery query = new CardPickQuery() {
            from = this,
            to = User.Owner,
            cards = auraCards.ToArray(),
            message = string.Format(QueryMessage, auraCardsToGet, auraCardsToGet > 1 ? "s" : ""),
            enableCancel = false,
            minCards = (uint)(Math.Min(auraCards.Count, auraCardsToGet)),
            maxCards = (uint)(auraCardsToGet)
        };

        actions.Add(new QueryAction() { query = query });

        actions.Add(new SetMarkerAction(fighterCard, gameController.ElectricMark, GameAction.SetMode.Add));
        return actions.ToArray();
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        CardPickAnswer cardPickAnswer = answer as CardPickAnswer;
        if (cardPickAnswer == null || cardPickAnswer.cards.Length == 0)
        {
            return null;
        }

        GameAction[] actions = new GameAction[1];
        actions[0] = new DrawAction(User.Owner, cardPickAnswer.cards);
        return actions;
    }
}
