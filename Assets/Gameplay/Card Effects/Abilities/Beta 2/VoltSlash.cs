using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoltSlash : AttachedEffect
{
    public static readonly int MarkersToRemove = 3;
    public VoltSlash(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        var actions = new List<GameAction>();
        FighterCardComponent fighter = GetThisFighterCard(gameController);
        int electricMarks = fighter.MarkerCount(gameController.ElectricMark);

        for(int i = 0; i < Math.Min(electricMarks, MarkersToRemove); i++)
        {
            actions.Add(new SetMarkerAction(fighter, gameController.ElectricMark, GameAction.SetMode.Remove));
        }

        actions.Add(new DamageAction() { damageAmount = ParentAbility.Damage, IsAttackDamage = true });

        return actions.ToArray();
    }

    public override GameController.ValidationResult ValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return ActivateQueryDeferCondition(answer, GetThisFighterCard(gameController).MarkerCount(gameController.ElectricMark) >= 3);
    }
}
