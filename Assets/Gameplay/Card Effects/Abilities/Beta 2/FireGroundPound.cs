using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGroundPound : AttachedEffect
{
    public static readonly int BasePassiveDamage = 10;
    public FireGroundPound(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        FighterCardComponent oppoFighterCard = GetOpposingActiveFighterCard(gameController);
        int fireMarkCount = User.MarkerCount(gameController.FireMark);
        int damage = BasePassiveDamage * fireMarkCount;

        if (damage > 0)
        {
            var damageAction = new DamageAction() { damageAmount = damage, IsAttackDamage = false, targetFighterCard = oppoFighterCard };
            return new GameAction[] { damageAction };
        }

        return null;
    }
}
