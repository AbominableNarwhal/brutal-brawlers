using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClawOfTheAbyss : AttachedEffect
{
    public ClawOfTheAbyss(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return GenerateAbilityDamageActionArr(gameController, ParentAbility.Damage);
    }

    public override GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        int brokenBlockers = damageAction.ResultDamage.BlockersBroken;

        var actions = new List<GameAction>();
        if (brokenBlockers > 0)
        {
            for (int i = 0; i < brokenBlockers; i++)
            {
                actions.Add(new SetMarkerAction(damageAction.targetFighterCard, gameController.WaterMark, GameAction.SetMode.Add));
            }
        }

        return actions.ToArray();
    }
}
