using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedHotCloak : AttachedEffect
{
    public static readonly int PassiveDamage = 20;

    public RedHotCloak(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        GameAction[] actions = new GameAction[2];
        actions[0] = new SetBlockerAction(ParentAbility.BlockDamage, GetThisFighterCard(gameController));
        actions[1] = new DamageAction() { damageAmount = PassiveDamage, IsAttackDamage = false, targetFighterCard = GetUserOpposingActiveFighterCard(gameController) };

        return actions;
    }
}
