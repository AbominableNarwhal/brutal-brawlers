using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frostbite : AttachedEffect
{
    public static readonly int MarkersToAdd = 3;
    public static readonly string CrystalIceBarbsName = "Crystal Ice Barbs";
    
    private AbilityGameCard.Status frostbiteBuff;
    private AbilityCardBaseComponent targetAbility;

    public Frostbite(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    {
        frostbiteBuff = new AbilityGameCard.Status() { auraCost = ParentAbility.AuraCost, name = "Frostbite" };
    }

    public override GameAction[] Attack(GameController gameController)
    {
        var actions = new List<GameAction>();
        FighterCardComponent oppoFighter = GetUserOpposingActiveFighterCard(gameController);

        for (int i = 0; i < MarkersToAdd; i++)
        {
            actions.Add(new SetMarkerAction(oppoFighter, gameController.WaterMark, GameAction.SetMode.Add));
        }

        return actions.ToArray();
    }

    public override GameAction[] TriggerOnAttach(GameController gameController)
    {
        GameAction gameAction = null;
        foreach (GameAction action in gameController.CompletedActions)
        {
            gameAction = GetBuffAction(action);
            if (gameAction != null)
            {
                break;
            }
        }

        return new GameAction[] { gameAction };
    }

    public override GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        
        return new GameAction[] { GetBuffAction(action) };
    }

    public override GameAction[] TurnEnd(GameController gameController)
    {
        if (targetAbility)
        {
            targetAbility = null;
            return new GameAction[] { new SetAbilityStatsChangeAction(targetAbility, frostbiteBuff, GameAction.SetMode.Remove) };
        }

        return null;
    }

    private GameAction GetBuffAction(GameAction action)
    {
        DamageAction damageAction = action as DamageAction;
        GameAction gameAction = null;

        if (damageAction != null && damageAction.IsAttackDamage && damageAction.ResultDamage.HpLost > 0)
        {
            AttachedEffect abilityEffect = damageAction.OriginEffect as AttachedEffect;

            if (abilityEffect != null && abilityEffect.CardObject.CardData.CardName == CrystalIceBarbsName && targetAbility == null)
            {
                gameAction = new SetAbilityStatsChangeAction(targetAbility, frostbiteBuff, GameAction.SetMode.Add);
            }
        }

        return gameAction;
    }
}
