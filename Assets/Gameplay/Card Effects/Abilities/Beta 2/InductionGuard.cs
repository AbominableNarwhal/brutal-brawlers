using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InductionGuard : AttachedEffect
{
    public static readonly int BlockerStrength = 20;

    public InductionGuard(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        var auraAction = action as PlayAuraAction;
        if (auraAction != null && auraAction.auraCard.CardData.Owner == Parent.Owner)
        {
            return new GameAction[] { new SetBlockerAction(BlockerStrength, GetThisFighterCard(gameController))};
        }
        return null;
    }
}
