using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricWaterMagicParry : AttachedEffect
{
    public static readonly int CounterDamage = 20;
    public ElectricWaterMagicParry(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent, _user)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        if (damageAction.OriginEffect != null && damageAction.OriginEffect is AttachedEffect)
        {
            AbilityGameCard incomingAbility = (damageAction.OriginEffect as AttachedEffect).ParentAbility;

            if (incomingAbility.Style == Ability.Style.Spell && incomingAbility.AbilityType == Ability.AbilityType.Water)
            {
                return new GameAction[]
                {
                    new NegateAction()
                    {
                        ActionToNegate = damageAction
                    },
                    new DamageAction()
                    {
                        damageAmount = CounterDamage,
                        IsAttackDamage = false,
                        targetFighterCard = GetOpposingActiveFighterCard(gameController)
                    }
                };
            }
        }

        return null;
    }
}
