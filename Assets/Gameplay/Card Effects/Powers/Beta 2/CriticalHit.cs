using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CriticalHit : PowerEffect, IOnHitTriggerAddOn
{
    public static readonly int CritcalDamage = 50;
    public static readonly string Message = "Choose one ability on your active fighter to add an On Hit trigger onto.";

    private AbilityCardBaseComponent abilityCard = null;

    public CriticalHit(CardComponent card) : base(card)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        var abilityList = new List<GameObject>();

        FighterCardComponent fighterCard = gameController.GetPlayerGameBoard(Parent.Owner).ActiveFighterCard;

        foreach (AbilityCardBaseComponent fighterAbilityCard in fighterCard.Abilities)
        {
            abilityList.Add(fighterAbilityCard.gameObject);
        }

        FieldPickQuery fieldPickQuery = new FieldPickQuery(abilityList.ToArray(), Math.Min(abilityList.Count, 1), 1, Message);

        return new GameAction[] { new QueryAction(fieldPickQuery)};
    }

    public override GameAction[] OnQueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var fieldPickAnswer = answer as FieldPickAnswer;
        var actions = new List<GameAction>();

        if (fieldPickAnswer != null)
        {
            if(fieldPickAnswer.Selections.Length > 0)
            {
                GameObject gameObject = fieldPickAnswer.Selections[0];
                abilityCard = gameObject.GetComponent<AbilityCardBaseComponent>();

                if (abilityCard)
                {
                    actions.Add(new SetOnHitTriggerAction(abilityCard, this, GameAction.SetMode.Add));
                }
            }
        }

        return actions.ToArray();
    }

    public override GameAction[] OnTurnEnd(GameController gameController)
    {
        var actions = new List<GameAction>();
        if (abilityCard)
        {
            actions.Add(new SetOnHitTriggerAction(abilityCard, this, GameAction.SetMode.Remove));
            actions.Add(new DiscardAction(CardObject));
        }

        return actions.ToArray();
    }

    public GameAction[] TriggerOnHit(GameController gameController, AttachedEffect effect, DamageAction damageAction)
    {
        FighterCardComponent fighterCard = damageAction.targetFighterCard;

        return new GameAction[] { new DamageAction(fighterCard, CritcalDamage, false) };
    }
}
