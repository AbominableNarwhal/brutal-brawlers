using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MastersLesson : PowerEffect
{
    public static readonly string Message = "Choose one ability card to add to your hand.";

    public MastersLesson(CardComponent card) : base(card)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return Activate(gameController);
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return Activate(gameController);
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var cardPickAnswer = answer as CardPickAnswer;

        var actions = new List<GameAction>();
        Transform hand = gameController.GetPlayerGameBoard(Parent.Owner).Hand;

        foreach (CardComponent card in cardPickAnswer.cards)
        {
            actions.Add(new MoveCardAction(card, hand));
        }

        actions.Add(new ShuffleAction(Parent.Owner));
        actions.Add(new DiscardAction(CardObject));

        return actions.ToArray();
    }

    private GameAction[] Activate(GameController gameController)
    {
        var abilityCards = new List<CardComponent>();

        Transform deck = gameController.GetPlayerGameBoard(Parent.Owner).Deck;

        foreach (Transform child in deck)
        {
            var abilityCard = child.GetComponent<AbilityCardComponent>();

            if (abilityCard)
            {
                abilityCards.Add(abilityCard);
            }
        }

        var cardPickQuery = new CardPickQuery(abilityCards.ToArray(), Math.Min(abilityCards.Count, 1), 1, Message);

        return new GameAction[] { new QueryAction(cardPickQuery) };
    }
}
