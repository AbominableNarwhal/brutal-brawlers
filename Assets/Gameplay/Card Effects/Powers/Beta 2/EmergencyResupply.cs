using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmergencyResupply : PowerEffect
{
    public EmergencyResupply(CardComponent card) : base(card)
    { }

    public override GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        DrawAction drawAction = null;

        if (action is TributeAction)
        {
            drawAction = new DrawAction(Parent.Owner);
        }

        return new GameAction[] { drawAction };
    }

    public override GameAction[] OnTurnEnd(GameController gameController)
    {
        return new GameAction[] { new DiscardAction(CardObject) };
    }
}
