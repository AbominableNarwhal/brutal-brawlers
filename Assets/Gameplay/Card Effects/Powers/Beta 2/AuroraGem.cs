using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AuroraGem : PowerEffect
{
    public static readonly string MessageText = "Choose one aura card to add to your hand.";

    public AuroraGem(CardComponent card) : base(card)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return Activate(gameController);
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return Activate(gameController);
    }

    public GameAction[] Activate(GameController gameController)
    {
        PlayerGameBoardAccessor playerAccessor =  gameController.GetPlayerGameBoard(Parent.Owner);

        var validCards = new List<CardComponent>();
        foreach (Transform child in playerAccessor.Deck)
        {
            var auraCard = child.GetComponent<AuraCardComponent>();

            if (auraCard)
            {
                validCards.Add(auraCard);
            }
        }

        CardPickQuery cardPickQuery = new CardPickQuery() {
            cards = validCards.ToArray(),
            from = this,
            to = Parent.Owner,
            minCards = (uint)Math.Min(validCards.Count, 1),
            maxCards = 1,
            message = MessageText
        };

        return new GameAction[] { new QueryAction() { query = cardPickQuery } };
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var cardPickAnswer = answer as CardPickAnswer;

        PlayerGameBoardAccessor playerAccessor = gameController.GetPlayerGameBoard(Parent.Owner);

        var actions = new List<GameAction>();
        foreach (CardComponent card in cardPickAnswer.cards)
        {
            actions.Add(new DrawAction(Parent.Owner, new CardComponent[] { card }));
        }

        actions.Add(new DiscardAction(CardObject));

        return actions.ToArray();
    }
}
