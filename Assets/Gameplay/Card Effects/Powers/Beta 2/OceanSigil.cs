using System;
using System.Collections.Generic;
using UnityEngine;

public class OceanSigil : PowerEffect
{
    public static readonly string Message = "Choose a fighter the will gain 2 water-marks.";
    public static readonly int NumberOfMarksToGive = 2;

    public OceanSigil(CardComponent card) : base(card)
    { }

    public override GameAction[] OnAttack(GameController gameController)
    {
        return Activate(gameController);
    }

    public override GameAction[] OnDefend(GameController gameController, DamageAction damageAction)
    {
        return Activate(gameController);
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var fieldPickAnswer = answer as FieldPickAnswer;
        var actions = new List<GameAction>();

        if (fieldPickAnswer != null && fieldPickAnswer.Selections.Length > 0)
        {
            FighterCardComponent fighter = fieldPickAnswer.Selections[0].GetComponent<FighterCardComponent>();

            if (fighter)
            {
                for (int i = 0; i < NumberOfMarksToGive; i++)
                {
                    actions.Add(new SetMarkerAction(fighter, gameController.WaterMark, GameAction.SetMode.Add));
                }
            }
        }

        return actions.ToArray();
    }

    private GameAction[] Activate(GameController gameController)
    {
        var fighters = new List<GameObject>();

        AddfightersToList(gameController, fighters, gameController.player1);
        AddfightersToList(gameController, fighters, gameController.player2);

        FieldPickQuery fieldPickQuery = new FieldPickQuery(fighters.ToArray(), Math.Min(fighters.Count, 1), 1, Message);

        return new GameAction[] { new QueryAction(fieldPickQuery) };
    }

    private void AddfightersToList(GameController gameController, List<GameObject> fighters, Player player)
    {
        foreach (FighterCardComponent fighterCard in gameController.GetPlayerGameBoard(player).EnumerateFighterCards())
        {
            if (!fighterCard.IsKOed)
            {
                fighters.Add(fighterCard.gameObject);
            }
        }
    }
}
