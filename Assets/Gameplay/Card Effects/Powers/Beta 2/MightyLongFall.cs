using System;
using System.Collections.Generic;
using UnityEngine;

public class MightyLongFall : PowerEffect
{
    public static readonly string Message = "Choose one card to discard.";

    public MightyLongFall(CardComponent card) : base(card)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return Activate(gameController);
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return Activate(gameController);
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var fieldPickAnswer = answer as FieldPickAnswer;
        var actions = new List<GameAction>();

        if (fieldPickAnswer != null)
        {
            foreach (GameObject selection in fieldPickAnswer.Selections)
            {
                var card = selection.GetComponent<CardComponent>();
                if (card)
                {
                    actions.Add(new DiscardAction(card));
                }
            }
        }

        actions.Add(new DiscardAction(CardObject));
        return actions.ToArray();
    }

    private GameAction[] Activate(GameController gameController)
    {
        var handList = new List<GameObject>();

        Transform opponentHand = gameController.GetPlayerGameBoard(gameController.GetPlayerOpponent(Parent.Owner)).Hand;

        foreach (Transform child in opponentHand)
        {
            handList.Add(child.gameObject);
        }

        var fieldPickQuery = new FieldPickQuery(handList.ToArray(), Math.Min(handList.Count, 1), 1, Message);

        return new GameAction[] { new QueryAction(fieldPickQuery)};
    }
}
