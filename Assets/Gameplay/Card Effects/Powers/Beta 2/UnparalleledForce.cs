using System.Collections.ObjectModel;
using System.Collections.Generic;
using UnityEngine;

public class UnparalleledForce : PowerEffect
{
    public UnparalleledForce(CardComponent card) : base(card)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return Activate(gameController);
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return Activate(gameController);
    }

    private GameAction[] Activate(GameController gameController)
    {
        var actions = new List<GameAction>();

        FighterCardComponent oppoFighter = gameController.GetPlayerGameBoard(gameController.GetPlayerOpponent(Parent.Owner)).ActiveFighterCard;

        int blockerStrength = 0;
        foreach (int blocker in oppoFighter.Blockers)
        {
            blockerStrength += blocker;
            actions.Add(new SetBlockerAction(oppoFighter));
        }

        if (blockerStrength > 0)
        {
            actions.Add(new DamageAction(oppoFighter, blockerStrength));
        }

        return actions.ToArray();
    }
}
