using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteGuard : PowerEffect
{
    public CompleteGuard(CardComponent card) : base(card)
    { }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return new GameAction[] {
            new SetBlockerAction(damageAction.damageAmount, damageAction.GetTarget(gameController)),
            new DiscardAction(CardObject)
        };
    }
}
