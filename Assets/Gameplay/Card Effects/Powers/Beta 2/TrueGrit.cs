using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrueGrit : PowerEffect
{
    public TrueGrit(CardComponent card) : base(card)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return Activate(gameController);
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return Activate(gameController);
    }

    private GameAction[] Activate(GameController gameController)
    {
        var fighterList = new List<GameObject>();

        return new GameAction[] {
            new DrawAction(Parent.Owner),
            new DrawAction(Parent.Owner)
        };
    }
}
