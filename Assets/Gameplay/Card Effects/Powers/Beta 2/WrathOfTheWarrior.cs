using System;
using System.Collections.Generic;
using UnityEngine;

public class WrathTheWarrior : PowerEffect
{
    public static readonly string Message = "Choose a fighter whose strength will be increased by 50";
    public static readonly int StrengthBoost = 50;
    FighterCardComponent targetFighter = null;

    public WrathTheWarrior(CardComponent card) : base(card)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return Activate(gameController);
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return Activate(gameController);
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var fieldPickAnswer = answer as FieldPickAnswer;
        var actions = new List<GameAction>();

        if (fieldPickAnswer != null)
        {
            foreach (GameObject selection in fieldPickAnswer.Selections)
            {
                var card = selection.GetComponent<FighterCardComponent>();
                if (card)
                {
                    targetFighter = card;
                    actions.Add(new FighterStatChangeAction(card, 0, 0, StrengthBoost, 0));
                }
            }
        }

        actions.Add(new DiscardAction(CardObject));
        return actions.ToArray();
    }

    public override GameAction[] OnTurnEnd(GameController gameController)
    {
        if (!targetFighter)
        {
            return null;
        }

        var actions = new List<GameAction>();
        actions.Add(new FighterStatChangeAction(targetFighter, 0, 0, -StrengthBoost, 0));
        actions.Add(new DiscardAction(CardObject));

        return actions.ToArray();
    }

    private GameAction[] Activate(GameController gameController)
    {
        var fighterList = new List<GameObject>();

        PlayerGameBoardAccessor playerBoard = gameController.GetPlayerGameBoard(Parent.Owner);
        PlayerGameBoardAccessor opponentBoard = gameController.GetPlayerGameBoard(gameController.GetPlayerOpponent(Parent.Owner));

        foreach (FighterCardComponent fighterCard in playerBoard.EnumerateFighterCards())
        {
            fighterList.Add(fighterCard.gameObject);
        }
        foreach (FighterCardComponent fighterCard in opponentBoard.EnumerateFighterCards())
        {
            fighterList.Add(fighterCard.gameObject);
        }

        var fieldPickQuery = new FieldPickQuery(fighterList.ToArray(), Math.Min(fighterList.Count, 1), 1, Message);

        return new GameAction[] { new QueryAction(fieldPickQuery) };
    }
}
