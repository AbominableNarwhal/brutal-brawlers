using System;
using System.Collections.Generic;
using UnityEngine;

public class SuperArmor : PowerEffect
{
    public static readonly string Message = "Choose a fighter card that will gain a 50 damage blocker";
    public static readonly int BlockerStrength = 50;

    public SuperArmor(CardComponent card) : base(card)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        return Activate(gameController);
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return Activate(gameController);
    }

    public override GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        var fieldPickAnswer = answer as FieldPickAnswer;
        var actions = new List<GameAction>();

        if (fieldPickAnswer != null)
        {
            foreach (GameObject selection in fieldPickAnswer.Selections)
            {
                var card = selection.GetComponent<FighterCardComponent>();
                if (card)
                {
                    actions.Add(new SetBlockerAction(BlockerStrength, card));
                }
            }
        }

        actions.Add(new DiscardAction(CardObject));
        return actions.ToArray();
    }

    private GameAction[] Activate(GameController gameController)
    {
        var fighterList = new List<GameObject>();

        PlayerGameBoardAccessor playerBoard = gameController.GetPlayerGameBoard(Parent.Owner);
        PlayerGameBoardAccessor opponentBoard = gameController.GetPlayerGameBoard(gameController.GetPlayerOpponent(Parent.Owner));

        foreach (FighterCardComponent fighterCard in playerBoard.EnumerateFighterCards())
        {
            fighterList.Add(fighterCard.gameObject);
        }
        foreach (FighterCardComponent fighterCard in opponentBoard.EnumerateFighterCards())
        {
            fighterList.Add(fighterCard.gameObject);
        }

        var fieldPickQuery = new FieldPickQuery(fighterList.ToArray(), Math.Min(fighterList.Count, 1), 1, Message);

        return new GameAction[] { new QueryAction(fieldPickQuery) };
    }
}
