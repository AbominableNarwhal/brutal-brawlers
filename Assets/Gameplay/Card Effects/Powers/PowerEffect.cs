﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GameController;

public abstract class PowerEffect : CardEffect
{
    public PowerEffect(CardComponent cardComponent) : base(cardComponent)
    {

    }

    #region Card Effect Events
    public override GameAction[] OnAttack(GameController gameController)
    {
        return Attack(gameController);
    }

    public override GameAction[] OnDefend(GameController gameController, DamageAction damageAction)
    {
        return Defend(gameController, damageAction);
    }
    public override GameAction[] OnActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        return ActionPostSubmitBroadcast(gameController, action);
    }
    public override GameAction[] OnActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        return ActionCompleteBroadcast(gameController, action);
    }

    public override GameAction[] OnQueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return QueryAnswered(gameController, query, answer);
    }

    public override GameAction[] OnRemovedFromPlay(GameController gameController)
    {
        return RemovedFromPlay(gameController);
    }

    public override ValidationResult OnValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return ValidateQuery(gameController, query, answer);
    }

    public override int OnAlterAuraCost(GameController gameController, IAuraConsumer powerOrAbility)
    {
        return AlterAuraCost(gameController, powerOrAbility);
    }
    #endregion

    #region Child Power Effect Events
    public virtual GameAction[] Attack(GameController gameController)
    {
        return null;
    }
    public virtual GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return null;
    }
    public virtual GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        return null;
    }
    public virtual GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        return null;
    }

    public virtual GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return null;
    }

    public virtual GameAction[] RemovedFromPlay(GameController gameController)
    {
        return null;
    }

    public virtual ValidationResult ValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return ValidationResult.Defer;
    }

    public virtual int AlterAuraCost(GameController gameController, IAuraConsumer powerOrAbility)
    {
        return 0;
    }
    #endregion
}
