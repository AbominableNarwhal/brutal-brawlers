﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicalStrength : PowerEffect
{
    private int magicStrengthDiff;
    private FighterCardComponent activeFighter;

    public MagicalStrength(CardComponent cardComponent = null) : base(cardComponent)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        activeFighter = gameController.GetActiveFighterCard(Parent.Owner);
        magicStrengthDiff = activeFighter.Magic - activeFighter.Strength;

        GameAction[] actions = new GameAction[1];
        actions[0] = new FighterStatChangeAction(activeFighter, -magicStrengthDiff, 0, magicStrengthDiff, 0);
        return actions;
    }
    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        if(!(action is EndTurnAction))
        {
            return null;
        }

        GameObject playerGameObject = gameController.GetPlayerGameObject(Parent.Owner);
        activeFighter = gameController.GetActiveFighterCard(Parent.Owner);
        
        GameAction[] actions = new GameAction[2];
        actions[0] = new FighterStatChangeAction(activeFighter, magicStrengthDiff, 0, -magicStrengthDiff, 0);
        actions[1] = new DiscardAction(CardObject);
        return actions;
    }
    
}
