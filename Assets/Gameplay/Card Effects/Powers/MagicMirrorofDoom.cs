﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class MagicMirrorofDoom : PowerEffect
{
    private CardComponent copiedAbility = null;
    private CardEffect copiedAbilityEffect = null;
    private FighterGameCard activeFighter = null;

    public MagicMirrorofDoom(CardComponent card) : base(card)
    { }

    public override GameAction[] Attack(GameController gameController)
    {
        GameAction[] actions = new GameAction[1];
        actions[0] = new DiscardAction(CardObject);
        return actions;
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        AbilityGameCard incomingAbility = damageAction.OriginEffect.CardObject.CardData as AbilityGameCard;
        if (incomingAbility == null || incomingAbility.Style != Ability.Style.Spell)
        {
            return null;
        }

        //Trying to make it look like the card is cloned from the ability card and moves next to the power card
        copiedAbility = Object.Instantiate(damageAction.OriginEffect.CardObject, damageAction.OriginEffect.CardObject.transform);
        //Copying the ability data
        copiedAbility.SetCardData(copiedAbility.CardData);
        copiedAbility.CardData.Owner = Parent.Owner;
        copiedAbility.transform.SetParent(CardObject.transform.parent);

        return null;
    }

    public override GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        if (!(action is PhaseChangeAction) || gameController.CurrentGamePhase != GameController.GamePhase.Attack ||
            gameController.offensivePlayer != Parent.Owner || copiedAbility == null)
        {
            return null;
        }

        List<GameAction> actions = new List<GameAction>();
        activeFighter = GetActiveFighter(gameController);
        activeFighter.AttachAbility(copiedAbility.CardData as AbilityGameCard);
        copiedAbilityEffect = gameController.CreateAttachedCardEffectByClassName(copiedAbility, activeFighter);
        
        actions.AddRange(copiedAbilityEffect.OnAttack(gameController));
        actions.Add(new DiscardAction(CardObject));
        return actions.ToArray();
    }

    public override GameAction[] RemovedFromPlay(GameController gameController)
    {
        GameAction[] actions = null;
        if (copiedAbilityEffect != null)
        {
            activeFighter.DetachAbility(copiedAbility.CardData as AbilityGameCard);
            actions = copiedAbilityEffect.OnRemovedFromPlay(gameController);
            Object.Destroy(copiedAbility.gameObject);
            copiedAbility = null;
            copiedAbilityEffect = null;
        }
        return actions;
    }
}
