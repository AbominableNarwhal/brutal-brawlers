﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ComboBreaker : PowerEffect
{
    public ComboBreaker(CardComponent card) : base(card)
    {}

    private GameAction[] Activate()
    {
        return new GameAction[1] { new EndTurnAction() };
    }

    public override GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        if (!(action is EndTurnAction))
        {
            return null;
        }
        return new GameAction[1] { new DiscardAction(CardObject) };
    }

    public override GameAction[] Attack(GameController gameController)
    {
        return Activate();
    }

    public override GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return Activate();
    }
}
