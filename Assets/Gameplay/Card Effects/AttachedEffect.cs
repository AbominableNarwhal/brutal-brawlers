﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameController;

public abstract class AttachedEffect : CardEffect, IOnHitTriggerAddOn
{
    public AbilityCardBaseComponent AbilityCardBody { get; }
    public AbilityGameCard ParentAbility
    {
        get;
    }
    public FighterGameCard User
    {
        get;
    }

    public AttachedEffect(CardComponent cardComponent, FighterGameCard _user) : base(cardComponent)
    {
        AbilityCardBody = cardComponent as AbilityCardBaseComponent;
        if (AbilityCardBody == null)
        {
            Debug.LogError("A non Ability object passed to attached effect");
        }

        ParentAbility = AbilityCardBody.AbilityData;
        if (ParentAbility == null)
        {
            Debug.LogError("A non Ability object passed to attached effect");
        }

        User = _user;
    }

    #region Card Effect Events
    public override GameAction[] OnAttack(GameController gameController)
    {
        if (!IsUsable(gameController))
        {
            return null;
        }
        return Attack(gameController);
    }

    public override GameAction[] OnDefend(GameController gameController, DamageAction damageAction)
    {
        if (!IsUsable(gameController))
        {
            return null;
        }
        return Defend(gameController, damageAction);
    }
    public override GameAction[] OnActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        if (!IsUsable(gameController))
        {
            return null;
        }
        return ActionPostSubmitBroadcast(gameController, action);
    }
    public override GameAction[] OnActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        // Call back tot he parent
        List<GameAction> actions = new List<GameAction>();
        AddActionsToList(actions, base.OnActionCompleteBroadcast(gameController, action));

        var discardAction = action as DiscardAction;

        if (discardAction != null)
        {
            if(AbilityCardBody.IsTarget(discardAction.Card))
            {
                AbilityCardBody.RemoveTarget(discardAction.Card);
            }
        }

        if (!IsUsable(gameController))
        {
            return null;
        }

        var damageAction = action as DamageAction;
        var attachAction = action as AttachAbilityCardAction;

        if (damageAction != null)
        {
            if (damageAction.OriginEffect == this && damageAction.IsAttackDamage && damageAction.ResultDamage.HpLost > 0)
            {
                foreach (IOnHitTriggerAddOn onHitTrigger in ParentAbility.OnHitTriggers)
                {
                    AddActionsToList(actions, onHitTrigger.TriggerOnHit(gameController, this, damageAction));
                }
                AddActionsToList(actions, TriggerOnHit(gameController, null, damageAction));
            }
        }
        else if (attachAction != null && attachAction.AbilityCard == CardObject)
        {
            AddActionsToList(actions, TriggerOnAttach(gameController));
        }

        AddActionsToList(actions, ActionCompleteBroadcast(gameController, action));
        return actions.ToArray();
    }

    public override GameAction[] OnQueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        if (!IsUsable(gameController))
        {
            return null;
        }
        return QueryAnswered(gameController, query, answer);
    }

    public override GameAction[] OnRemovedFromPlay(GameController gameController)
    {
        AbilityCardBody.ResetGameplayData();

        if (!IsUsable(gameController))
        {
            return null;
        }
        return RemovedFromPlay(gameController);
    }

    public override GameAction[] OnTurnEnd(GameController gameController)
    {
        if (!IsUsable(gameController))
        {
            return null;
        }

        return TurnEnd(gameController);
    }

    public override ValidationResult OnValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        if (!IsUsable(gameController))
        {
            return ValidationResult.Defer;
        }
        return ValidateQuery(gameController, query, answer);
    }

    public override int OnAlterAuraCost(GameController gameController, IAuraConsumer powerOrAbility)
    {
        if (!IsUsable(gameController))
        {
            return 0;
        }
        return AlterAuraCost(gameController, powerOrAbility);
    }

    public GameAction[] TriggerOnHit(GameController gameController, AttachedEffect effect, DamageAction damageAction)
    {
        return TriggerOnHit(gameController, damageAction);
    }

    #endregion

    #region Child Attached Effect Events
    public virtual GameAction[] Attack(GameController gameController)
    {
        return null;
    }
    public virtual GameAction[] Defend(GameController gameController, DamageAction damageAction)
    {
        return null;
    }
    public virtual GameAction[] ActionPostSubmitBroadcast(GameController gameController, GameAction action)
    {
        return null;
    }
    public virtual GameAction[] ActionCompleteBroadcast(GameController gameController, GameAction action)
    {
        return null;
    }

    public virtual GameAction[] QueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return null;
    }

    public virtual GameAction[] RemovedFromPlay(GameController gameController)
    {
        return null;
    }

    public virtual ValidationResult ValidateQuery(GameController gameController, PlayerQuery query, PlayerAnswer answer)
    {
        return ValidationResult.Defer;
    }

    public virtual int AlterAuraCost(GameController gameController, IAuraConsumer powerOrAbility)
    {
        return 0;
    }

    public virtual GameAction[] TriggerOnHit(GameController gameController, DamageAction damageAction)
    {
        return null;
    }

    public virtual GameAction[] TriggerOnAttach(GameController gameController)
    {
        return null;
    }

    public virtual GameAction[] TurnEnd(GameController gameController)
    {
        return null;
    }

    public virtual GameAction[] Enabled(GameController gameController)
    {
        return null;
    }

    public virtual GameAction[] Disabled(GameController gameController)
    {
        return null;
    }

    #endregion

    #region Helper Functions

    public bool IsUsable(GameController gameController)
    {
        return gameController.IsAbilityUsable(ParentAbility);
    }

    public bool ThisFighterIsActive(GameController controller)
    {
        GameObject playerBoard = controller.GetPlayerGameObject(User.Owner);
        FighterGameCard activeFighter = playerBoard.transform.Find("Active Fighter").GetChild(0).GetComponent<CardComponent>().CardData as FighterGameCard;
        return User == activeFighter;
    }

    /// <summary>Returns this AbilityCardEffect's Fighter that it is attached to
    public FighterCardComponent GetThisFighterCard(GameController controller)
    {
        PlayerGameBoardAccessor playerBoard = controller.GetPlayerGameBoard(User.Owner);

        if(playerBoard.ActiveFighter.HasAbility(ParentAbility))
        {
            return playerBoard.ActiveFighterCard;
        }

        if(playerBoard.SupportFighter1.HasAbility(ParentAbility))
        {
            return playerBoard.SupportFighter1Card;
        }

        if(playerBoard.SupportFighter2.HasAbility(ParentAbility))
        {
            return playerBoard.SupportFighter2Card;
        }
        return null;
    }
    
    /// <summary>Returns the opposing active fighter to the fighter that this ability is attached to
    /// </summary>
    public FighterGameCard GetUserOpposingActiveFighter(GameController gameController)
    {
        return gameController.GetPlayerGameBoard(gameController.GetPlayerOpponent(User.Owner)).ActiveFighter;
    }

    /// <summary>Returns the opposing active fighter to the fighter that this ability is attached to
    /// </summary>
    public FighterCardComponent GetUserOpposingActiveFighterCard(GameController gameController)
    {
        return gameController.GetPlayerGameBoard(gameController.GetPlayerOpponent(User.Owner)).ActiveFighterCard;
    }

    public GameAction[] GenerateAbilityDamageActionArr(GameController gameController, int damage)
    {
        GameAction[] action = new GameAction[1];
        action[0] = new DamageAction(){damageAmount = damage, IsAttackDamage = true };
        return action;
    }

    public GameAction GenerateAbilityDamageAction(GameController gameController, int damage, GameAction[] onHitActions = null)
    {
        return new DamageAction() { damageAmount = damage, IsAttackDamage = true, OnHitActions = onHitActions};
    }

    public ValidationResult ActivateQueryDeferCondition(PlayerAnswer answer, bool cond)
    {
        var combatQuery = answer as CombatQueryAnswer;

        if (combatQuery != null)
        {
            if (combatQuery.type == CombatQueryAnswer.Type.ActivateAbility && combatQuery.card == CardObject && !cond)
            {
                return ValidationResult.Fail;
            }
        }

        return ValidationResult.Defer;
    }

    public bool AuraCardWasPlayed(GameController gameController)
    {
        foreach (GameAction action in gameController.CompletedActions)
        {
            PlayAuraAction playAuraAction = action as PlayAuraAction;
            if (playAuraAction != null && playAuraAction.auraCard.CardData.Owner == User.Owner)
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}
