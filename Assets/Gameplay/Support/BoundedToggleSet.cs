﻿using System.Collections;
using System.Collections.Generic;

namespace SunEater.Support
{
    public enum ToggleResult { Added, Removed, None }

    public class BoundedToggleSet<T> : IEnumerable<T>
    {
        private HashSet<T> items;
        private uint min = 0;
        private uint max = 0;

        public bool ItemCountInRange
        {
            get
            {
                return items.Count >= min && items.Count <= max;
            }
        }

        public BoundedToggleSet(uint _min, uint _max)
        {
            min = _min;
            max = _max;
            items = new HashSet<T>();
        }

        public bool Toggle(T item)
        {
            return Toggle(item, out ToggleResult result);
        }

        public bool Toggle(T item, out ToggleResult result)
        {
            result = ToggleResult.None;
            if(items.Contains(item))
            {
                if(Remove(item))
                {
                    result = ToggleResult.Removed;
                    return true;
                }
            }
            if(Add(item))
            {
                result = ToggleResult.Added;
                return true;
            }
            return false;
        }

        public bool Add(T item)
        {
            if(items.Count < max)
            {
                return items.Add(item);   
            }
            return false;
        }

        public bool Remove(T item)
        {
            return items.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public T[] ToArray()
        {
            T[] temp = new T[items.Count];
            items.CopyTo(temp);
            return temp;
        }
    }

    public class BoundedToggleSetEnumerator<T> : IEnumerator<T>
    {
        private BoundedToggleSet<T> boundedToggleSet;
        private IEnumerator<T> enumerator;

        BoundedToggleSetEnumerator(BoundedToggleSet<T> _boundedToggleSet)
        {
            boundedToggleSet = _boundedToggleSet;
            enumerator = boundedToggleSet.GetEnumerator();
        }

        public T Current
        {
            get
            {
                return enumerator.Current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public bool MoveNext()
        {
            return enumerator.MoveNext();
        }

        public void Reset()
        {
            enumerator.Reset();
        }

        public void Dispose()
        {
            enumerator.Dispose();
        }
    }
}
