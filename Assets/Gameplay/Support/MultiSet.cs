﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunEater.Support
{
    public class MultiSet<T>
    {
        private Dictionary<T, int> setDictionary;

        public int UniqueCount
        {
            get { return setDictionary.Count; }
        }

        public int Count
        { get; set; } = 0;

        public MultiSet()
        {
            setDictionary = new Dictionary<T, int>();
        }

        public void Add(T newItem)
        {
            if (setDictionary.ContainsKey(newItem))
            {
                setDictionary[newItem] += 1;
            }
            else
            {
                setDictionary.Add(newItem, 1);
            }

            Count += 1;
        }

        public void Remove(T item)
        {
            if (setDictionary.ContainsKey(item))
            {
                if (setDictionary[item] == 1)
                {
                    setDictionary.Remove(item);
                }
                else
                {
                    setDictionary[item] -= 1;
                }

                Count -= 1;
            } 
        }

        public int ItemCount(T item)
        {
            if (setDictionary.ContainsKey(item))
            {
                return setDictionary[item];
            }

            return 0;
        }

        public ReadOnlyDictionary<T, int> GetReadOnlyItems()
        {
            return new ReadOnlyDictionary<T, int>(setDictionary);
        }
    }
}
