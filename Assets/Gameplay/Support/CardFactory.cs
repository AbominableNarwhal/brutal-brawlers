﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CardFactory
{
    public static CardComponent CreateCardObject(Card card, Transform parentTransform = null)
    {
        switch (card.type)
        {
            case Card.Type.AbilityCard:
                return AbilityCardComponent.CreateAbilityCard(card as Ability, parentTransform);
            case Card.Type.FighterCard:
                return FighterCardComponent.CreateFighterCard(card as Fighter, parentTransform);
            case Card.Type.PowerCard:
                return PowerCardComponent.CreatePowerCard(card as Power, parentTransform);
            case Card.Type.EquipmentCard:
                return EquipmentCardComponent.CreateEquipmentCard(card as Equipment, parentTransform);
            case Card.Type.AuraCard:
                return AuraCardComponent.CreateAuraCard(card as Aura, parentTransform);

            default:
                return null;
        }
    }
}
