﻿using SunEater.Support;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStateParameters : MonoBehaviour
{
    private Stack<string> sceneStack;

    [SerializeField]
    private Player player1;
    [SerializeField]
    private Player player2;

    public bool IsHost { get; set; }
    public int RandomSeed { get; set; } = 2;
    public Deck DeckToEdit { get; set; } = null;
    public string DeckName { get; set; } = null;

    public Player Player1
    {
        get
        {
            return player1;
        }
        set { player1 = value; }
    }
    public Player Player2
    {
        get
        {
            return player2;
        }

        set { player2 = value; }
    }

    public string PlayerName
    { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void GoToLastScene()
    {
        if (sceneStack.Count > 1)
        {
            sceneStack.Pop();
            SceneManager.LoadScene(sceneStack.Pop());
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        sceneStack.Push(scene.name);
    }

    void OnEnable()
    {
        sceneStack = new Stack<string>();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnApplicationQuit()
    {
        GameServerClient.TryDisconect();
    }

}
