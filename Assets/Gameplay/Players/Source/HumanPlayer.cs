﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[CreateAssetMenu]
public class HumanPlayer : Player
{
    QueryHandler queryHandler;

    public override void PostQuery(PlayerQuery query, GameController gameController)
    {
        Debug.Log("Query Recieved by " + name + "!");
        CombatQuery combatQuery = query as CombatQuery;
        if(combatQuery != null)
        {
            queryHandler = new CombatQueryHandler(query, gameController);
        }
        else if(query is CardPickQuery || query is OptionPickQuery)
        {
            queryHandler = new UiQueryHandler(query, gameController);
        }
        else if(query is FieldPickQuery)
        {
            queryHandler = new FieldPickQueryHandler(query, gameController);
        }
    }
    public override PlayerAnswer GetQueryAnswer()
    {
        if(queryHandler == null)
        {
            return null;
        }
        PlayerAnswer answer = queryHandler.GetAnswerIfReady();
        if(answer == null)
        {
            return null;
        }
        queryHandler = null;
        return answer;
    }

    public override void OnGameInputEvent(object subject, GameInputEventArgs eventData)
    {
        var cardObject = subject as GameObject;

        if(queryHandler == null || cardObject == null || eventData == null)
        {
            return;
        }

        var card = cardObject.GetComponent<CardComponent>();

        if (card != null && eventData.EventType == GameInputEventArgs.PointerEvent.Click)
        {
            queryHandler.OnCardClicked(card, eventData.EventData);
        }
    }

    public override void OnEndTurnSignal()
    {
        if(queryHandler == null)
        {
            return;
        }

        queryHandler.OnEndTurnSignal();
    }
}
