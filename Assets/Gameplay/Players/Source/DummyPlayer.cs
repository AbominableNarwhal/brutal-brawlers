﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyPlayer : Player
{
    public PlayerQuery CurrentQuery { get; private set; } = null;
    public override PlayerAnswer GetQueryAnswer()
    {
        return null;
    }

    public override void PostQuery(PlayerQuery query, GameController gameController)
    {
        CurrentQuery = query;
    }
}
