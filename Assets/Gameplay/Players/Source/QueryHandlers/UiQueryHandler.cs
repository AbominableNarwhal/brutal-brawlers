public class UiQueryHandler : QueryHandler, PlayerUIListener
{
    private enum UiQueryType { CardPick, OptionPick}
    private UiQueryType uiQueryType;

    public UiQueryHandler(PlayerQuery query, GameController gameController) : base(query)
    {
        CardPickQuery cardPickQuery = query as CardPickQuery;
        if(cardPickQuery != null)
        {
            uiQueryType = UiQueryType.CardPick;
            gameController.ShowCardPickerUI(cardPickQuery.cards, cardPickQuery.minCards, cardPickQuery.maxCards, cardPickQuery.message, cardPickQuery.enableCancel, this);
        }

        OptionPickQuery optionPickQuery = query as OptionPickQuery;
        if(optionPickQuery != null)
        {
            uiQueryType = UiQueryType.OptionPick;
            gameController.ShowOptionPickerUI(optionPickQuery.options, optionPickQuery.message, this);
        }
    }

    public void OnUIReturned(UIReturnData returnData)
    {
        switch(uiQueryType)
        {
            case UiQueryType.CardPick:
            {
                CardPickerReturnData cardPickerData = returnData as CardPickerReturnData;
                if(cardPickerData != null)
                {
                    answer = new CardPickAnswer() { cards = cardPickerData.cards.ToArray() };
                }
                else
                {
                    answer = new CardPickAnswer() { cards = new CardComponent[0] };
                }
            } break;
            case UiQueryType.OptionPick:
            {
                OptionPickerReturnData optionPickerData = returnData as OptionPickerReturnData;
                answer = new OptionAnswer() { selection = optionPickerData.selection };
            } break;
        }
    }

}