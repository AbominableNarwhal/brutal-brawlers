﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using SunEater.Support;

public class CombatQueryHandler : QueryHandler
{
    private static readonly string EndTurnAttackStr = "End Attack";
    private static readonly string EndTurnDefenceStr = "End Defence";

    private CombatQueryAnswer tempAnswer;
    private PlayType combatType;
    private List<CombatQueryAnswer> answerCache;
    private GameController gameController;
    private CombatQueryAnswer[] validSubset;
    private CombatQueryAnswer[] oldValidSubset;

    public CombatQueryHandler(PlayerQuery query, GameController gameController) : base(query)
    {
        CombatQuery temp = query as CombatQuery;
        if (temp == null)
        {
            Debug.LogAssertion("CombatQueryHandler can only accept combat queries");
        }
        combatType = temp.CombatType;
        tempAnswer = new CombatQueryAnswer();
        answerCache = new List<CombatQueryAnswer>();
        this.gameController = gameController;

        if (gameController.offensivePlayer == query.to)
        {
            gameController.SetActionButton(true, EndTurnAttackStr);
        }
        else if (gameController.defensivePlayer == query.to)
        {
            gameController.SetActionButton(true, EndTurnDefenceStr);
        }

        //Start with the cards in hand
        Transform playerHand = gameController.GetPlayerGameBoard(query.to).Hand;

        foreach (Transform child in playerHand)
        {
            CardComponent card = child.GetComponent<CardComponent>();
            if (!card)
            {
                continue;
            }

            //Fill the cache of valid answers
            GetValidAnswers(card, query.to, true);
        }

        Transform playerBoard = gameController.GetPlayerGameObject(query.to).transform;
        var activeFighter = playerBoard.Find("Active Fighter").GetChild(0).GetComponent<FighterCardComponent>();
        var supportFighter1 = playerBoard.Find("Support Fighter 1").GetChild(0).GetComponent<FighterCardComponent>();
        var supportFighter2 = playerBoard.Find("Support Fighter 2").GetChild(0).GetComponent<FighterCardComponent>();

        //Validate ability activation
        ValidateFighterAbilities(activeFighter);
        ValidateFighterAbilities(supportFighter1);
        ValidateFighterAbilities(supportFighter2);

        //Validate Tag In
        GetValidAnswers(supportFighter1, query.to, true);
        GetValidAnswers(supportFighter2, query.to, true);

        HighlightCardsFromAnswers(answerCache.ToArray(), true, false);
    }

    private void ValidateFighterAbilities(FighterCardComponent figherCard)
    {
        foreach (CardComponent abilityCard in figherCard.Abilities)
        {
            GetValidAnswers(abilityCard, currentQuery.to, true);
        }
    }

    private void HighlightCardsFromAnswers(CombatQueryAnswer[] answers, bool enableHighlight, bool secondaryCard)
    {
        if (answers == null)
        {
            return;
        }
        foreach (CombatQueryAnswer possibleAnswer in answers)
        {
            if (!secondaryCard)
            {
                HightlightCard(possibleAnswer.card, enableHighlight, GameBoardGraphic.DefaultValidColor);
            }
            else
            {
                HightlightCard(possibleAnswer.targetFighter, enableHighlight, GameBoardGraphic.DefaultValidColor);
            }
        }
    }

    private void HightlightCard(CardComponent card, bool enableHighlight, Color highlightColor)
    {
        if (card != null)
        {
            var graphic = card.GetComponent<GameBoardGraphic>();
            if (graphic == null)
            {
                Debug.LogError("Missing Graphic");
            }
            graphic.EnableHighlight(enableHighlight);
            if (enableHighlight)
            {
                graphic.SetHighlightColor(highlightColor);
            }
        }
    }

    private CombatQueryAnswer[] GetValidAnswers(CardComponent card, Player player, bool queryGameController)
    {
        CombatQueryAnswer[] possibleAnswers = GetPossibleAnswers(card, player);
        List<CombatQueryAnswer> validAnswers = new List<CombatQueryAnswer>();

        foreach (CombatQueryAnswer _answer in possibleAnswers)
        {
            if (answerCache.Contains(_answer))
            {
                validAnswers.Add(_answer);
            }
            else if (queryGameController && gameController.ValidateWithCurrentCombatQuery(_answer))
            {
                validAnswers.Add(_answer);
                answerCache.Add(_answer);
            }
        }

        return validAnswers.ToArray();
    }

    private CombatQueryAnswer[] GetPossibleAnswers(CardComponent card, Player player)
    {
        List<CombatQueryAnswer> possibleAnswers = new List<CombatQueryAnswer>();
        // These actions are both valid if the player is attaking or defending
        if (card.CardData.CardType == Card.Type.PowerCard && card.transform.parent.name == "Hand")
        {
            possibleAnswers.Add(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.PlayPowerCard, card = card });
        }
        //Attached abilities and native abilities
        else if (card.transform.parent.name == FighterCardComponent.NativeAbilityZoneName || card.transform.parent.name == FighterCardComponent.AttachedAbilityZoneName)
        {
            if (card.CardData.CardType == Card.Type.AbilityCard)
            {
                possibleAnswers.Add(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.ActivateAbility, card = card });
            }
        }
        
        //Play aura card
        if (card.CardData.CardType == Card.Type.AuraCard && card.transform.parent.name == "Hand")
        {
            possibleAnswers.Add(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.PlayAura, card = card });
        }
        //Select an ability or equipment card to attach
        else if ((card.CardData.CardType == Card.Type.AbilityCard || card.CardData.CardType == Card.Type.EquipmentCard) && card.transform.parent.name == "Hand")
        {
            Transform playerBoard = gameController.GetPlayerGameObject(player).transform;
            if (card.CardData.CardType == Card.Type.AbilityCard)
            {
                possibleAnswers.Add(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.AttachAbility, card = card, targetFighter = playerBoard.Find("Active Fighter").GetChild(0).GetComponent<FighterCardComponent>() });
                possibleAnswers.Add(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.AttachAbility, card = card, targetFighter = playerBoard.Find("Support Fighter 1").GetChild(0).GetComponent<FighterCardComponent>() });
                possibleAnswers.Add(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.AttachAbility, card = card, targetFighter = playerBoard.Find("Support Fighter 2").GetChild(0).GetComponent<FighterCardComponent>() });
            }

            else if (card.CardData.CardType == Card.Type.EquipmentCard)
            {
                possibleAnswers.Add(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.AttachEquipment, card = card, targetFighter = playerBoard.Find("Active Fighter").GetChild(0).GetComponent<FighterCardComponent>() });
                possibleAnswers.Add(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.AttachEquipment, card = card, targetFighter = playerBoard.Find("Support Fighter 1").GetChild(0).GetComponent<FighterCardComponent>() });
                possibleAnswers.Add(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.AttachEquipment, card = card, targetFighter = playerBoard.Find("Support Fighter 2").GetChild(0).GetComponent<FighterCardComponent>() });
            }

        }
        else if (card.CardData.CardType == Card.Type.FighterCard && card.transform.parent.name.Contains("Support Fighter"))
        {
            possibleAnswers.Add(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.TagIn, card = card });
        }

            return possibleAnswers.ToArray();
    }

    public override void OnCardClicked(CardComponent card, PointerEventData eventData)
    {
        if(card.CardData.Owner != currentQuery.to)
        {
            return;
        }


        if (tempAnswer.card == null)
        {
            validSubset = GetValidAnswers(card, currentQuery.to, false);

            if (validSubset.Length == 0)
            {
                return;
            }

            if (validSubset[0].type == CombatQueryAnswer.Type.AttachAbility || validSubset[0].type == CombatQueryAnswer.Type.AttachEquipment)
            {
                tempAnswer.card = card;
                tempAnswer.type = validSubset[0].type;
                HighlightCardsFromAnswers(answerCache.ToArray(), false, false);
                HighlightCardsFromAnswers(validSubset, true, true);
            }
            else if(eventData.clickCount == 2)
            {
                AssignAnswer(validSubset[0]);
            }
        }
        else
        {
            tempAnswer.targetFighter = card as FighterCardComponent;
            foreach (var subAnswer in validSubset)
            {
                if (subAnswer == tempAnswer)
                {
                    AssignAnswer(tempAnswer);
                }
            }

            if (answer == null)
            {
                tempAnswer.card = null;
                tempAnswer.targetFighter = null;
                HighlightCardsFromAnswers(oldValidSubset, false, true);
                HighlightCardsFromAnswers(answerCache.ToArray(), true, false);
            }
        }

        oldValidSubset = validSubset;
    }

    private void AssignAnswer(PlayerAnswer _answer)
    {
        HighlightCardsFromAnswers(validSubset, false, true);
        HighlightCardsFromAnswers(oldValidSubset, false, true);
        HighlightCardsFromAnswers(answerCache.ToArray(), false, false);
        answer = _answer;
    }

    public override void OnAbilityActivateButton(AbilityGameCard ability)
    {
        CardEffect effect = gameController.FindCardEffect(ability);
        if (effect == null)
        {
            return;
        }
        AssignAnswer(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.ActivateAbility, card = effect.CardObject });
    }

    public override void OnEndTurnSignal()
    {
        AssignAnswer(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.EndTurn });
    }

    public override void OnFighterAbilityClicked(CardComponent ability, PointerEventData eventData)
    {
        if( eventData.clickCount == 2)
        {
            AssignAnswer(new CombatQueryAnswer() { type = CombatQueryAnswer.Type.ActivateAbility, card = ability });
        }
    }
}
