using UnityEngine;
using UnityEngine.EventSystems;
using SunEater.Support;

public class FieldPickQueryHandler : QueryHandler
{
    private BoundedToggleSet<GameObject> selections;
    private GameController gameController;
    private FieldPickQuery fieldPickQuery;

    public FieldPickQueryHandler(PlayerQuery query, GameController _gameController) : base(query)
    {
        fieldPickQuery = query as FieldPickQuery;
        if(fieldPickQuery == null)
        {
            Debug.LogAssertion("FieldPickQueryHandler can only accept FieldPickQuery querys");
        }

        selections = new BoundedToggleSet<GameObject>(fieldPickQuery.MinItems, fieldPickQuery.MaxItems);
        gameController = _gameController;
        gameController.ShowMessage(fieldPickQuery.Message);
        gameController.SetActionButton(true, "Done");

        EnableHighlightOnAllOptions(true);
    }

    private void EnableHighlightOnAllOptions(bool enable)
    {
        foreach (GameObject gameObject in fieldPickQuery.ValidSelections)
        {
            GameBoardGraphic graphic = gameObject.GetComponent<GameBoardGraphic>();
            if (graphic == null)
            {
                continue;
            }

            graphic.EnableHighlight(enable);
            if (enable)
            {
                graphic.SetHighlightColor(graphic.ValidColor);
            }
        }
    }

    public override void OnCardClicked(CardComponent card, PointerEventData eventData)
    {
        ProcessClick(card.gameObject);
    }

    public override void OnEndTurnSignal()
    {
        if (!selections.ItemCountInRange)
        {
            return;
        }
        foreach(GameObject obj in selections)
        {
            GameBoardGraphic graphic = obj.GetComponent<GameBoardGraphic>();
            graphic.EnableHighlight(false);
        }

        FieldPickAnswer fieldPickAnswer = new FieldPickAnswer();
        fieldPickAnswer.Selections = selections.ToArray();
        answer = fieldPickAnswer;
        gameController.HideMessageBox();
        EnableHighlightOnAllOptions(false);
    }

    public override void OnFighterAbilityClicked(CardComponent ability, PointerEventData eventData)
    {
        ProcessClick(ability.gameObject);
    }

    private void ProcessClick(GameObject gameObject)
    {
        if(!SelectionIsValid(gameObject))
        {
            return;
        }
        
        selections.Toggle(gameObject, out ToggleResult result);
        GameBoardGraphic graphic = gameObject.GetComponent<GameBoardGraphic>();
        
        switch(result)
        {
            case ToggleResult.Added:
                graphic.SetHighlightColor(graphic.SelectedColor);
                break;
            case ToggleResult.Removed:
                graphic.SetHighlightColor(graphic.ValidColor);
                break;
            default:
                break;
        }
    }

    private bool SelectionIsValid(GameObject gameObject)
    {
        foreach(GameObject obj in fieldPickQuery.ValidSelections)
        {
            if(obj == gameObject)
            {
                return true;
            }
        }

        return false;
    }
}