using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class QueryHandler
{
    protected PlayerQuery currentQuery;
    protected PlayerAnswer answer;
    public QueryHandler(PlayerQuery query)
    {
        currentQuery = query;
    }

    public virtual void OnCardClicked(CardComponent card, PointerEventData eventData)
    {}

    public virtual void OnAbilityActivateButton(AbilityGameCard ability)
    {}

    public virtual void OnEndTurnSignal()
    {}

    public virtual void OnFighterAbilityClicked(CardComponent ability, PointerEventData eventData)
    {}

    public PlayerAnswer GetAnswerIfReady()
    {
        return answer;
    }
}
