﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SunEater.Support;

[CreateAssetMenu]
public class NetworkPlayer : Player
{
    private PlayerQuery currentQuery;
    private PlayerGameBoardAccessor playerBoard;

    public override void PostQuery(PlayerQuery query, GameController gameController)
    {
        base.PostQuery(query, gameController);
        currentQuery = query;
        playerBoard = gameController.GetPlayerGameBoard(this);
    }

    public override void OnOpponentAnswer(PlayerQuery query, PlayerAnswer answer)
    {
        if (!GameServerClient.IsConnected)
        {
            return;
        }

        var answerData = answer.GetSerializableData(query);

        GameServerClient.Write(answerData.ToString(), answerData);
    }

    public override PlayerAnswer GetQueryAnswer()
    {
        if (!GameServerClient.IsConnected || GameServerClient.Peek() == null || currentQuery == null)
        {
            return null;
        }

        ServerMessage message = GameServerClient.Peek();

        if (message.Protocol.Contains("AnswerData"))
        {
            GameServerClient.Read(); // Must consume the current message
            string protocol = message.Protocol;
            PlayerAnswer finalAnswer = null;

            if (protocol == FieldPickAnswerData.ClassName())
            {
                var fieldPickQuery = currentQuery as FieldPickQuery;
                var answerData = message.DeserializeJsonMessage<FieldPickAnswerData>();

                var answer = new FieldPickAnswer();
                answer.Selections = new GameObject[answerData.selections.Length];

                for (int i = 0; i < answerData.selections.Length; i++)
                {
                    answer.Selections[i] = fieldPickQuery.ValidSelections[answerData.selections[i]];
                }

                finalAnswer = answer;
            }
            else if (protocol == CombatQueryAnswerData.ClassName())
            {
                var combatQuery = currentQuery as CombatQuery;
                var answerData = message.DeserializeJsonMessage<CombatQueryAnswerData>();

                var answer = new CombatQueryAnswer();
                answer.type = answerData.type;

                foreach (var card in playerBoard.EnumerateFieldCards())
                {
                    if (card.GameId == answerData.card)
                    {
                        answer.card = card;
                    }
                    else if (card.GameId == answerData.targetFighter)
                    {
                        answer.targetFighter = card as FighterCardComponent;
                    }
                }

                finalAnswer = answer;
            }
            else if (protocol == CardPickAnswerData.ClassName())
            {
                var cardPickQuery = currentQuery as CardPickQuery;
                var answerData = message.DeserializeJsonMessage<CardPickAnswerData>();

                var answer = new CardPickAnswer();
                answer.cards = new CardComponent[answerData.cards.Length];

                for (int i = 0; i < answerData.cards.Length; i++)
                {
                    answer.cards[i] = cardPickQuery.cards[answerData.cards[i]];
                }

                finalAnswer = answer;
            }
            else if (protocol == OptionAnswerData.ClassName())
            {
                var cardPickQuery = currentQuery as OptionPickQuery;
                var answerData = message.DeserializeJsonMessage<OptionAnswerData>();

                var answer = new OptionAnswer();
                answer.selection = answerData.selection;

                finalAnswer = answer;
            }

            currentQuery = null;
            return finalAnswer;
        }

        return null;
    }
   
}
