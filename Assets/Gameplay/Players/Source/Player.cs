﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class Player : ScriptableObject
{
    private static readonly string WaitForOpponentStr = "Waiting for Opponent...";

    public Deck playerDeck;
    [HideInInspector]
    public GameObject gameBoard;
    public virtual void PostQuery(PlayerQuery query, GameController gameController)
    {
        gameController.SetActionButton(false, WaitForOpponentStr);
    }
    public abstract PlayerAnswer GetQueryAnswer();
    public virtual void OnGameInputEvent(object subject, GameInputEventArgs eventData){}
    public virtual void OnEndTurnSignal(){}
    public virtual void OnOpponentAnswer(PlayerQuery query, PlayerAnswer answer) { }
}
