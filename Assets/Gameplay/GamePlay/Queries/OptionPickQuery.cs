using System;
using System.Collections.Generic;

public class OptionPickQuery : PlayerQuery
{
    public List<string> options;
    public string message;
    public override bool ValidateAnswer(PlayerAnswer answer, GameController gameController)
    {
        OptionAnswer optionAnswer = answer as OptionAnswer;
        if(optionAnswer == null)
        {
            return false;
        }

        return options.Contains(optionAnswer.selection);
    }
}

[Serializable]
public class OptionAnswer : PlayerAnswer
{
    public string selection;

    public override PlayerAnswerData GetSerializableData(PlayerQuery query)
    {
        return new OptionAnswerData(this);
    }
}

[Serializable]
public class OptionAnswerData : PlayerAnswerData
{
    public string selection;

    public OptionAnswerData(OptionAnswer answer)
    {
        selection = answer.selection;
    }

    public override string ToString()
    {
        return ClassName();
    }

    public static string ClassName()
    {
        return "OptionAnswerData";
    }
}