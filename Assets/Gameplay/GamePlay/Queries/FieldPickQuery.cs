using System;
using UnityEngine;

public class FieldPickQuery : PlayerQuery
{
    public GameObject[] ValidSelections
    { get; set; }

    public uint MinItems
    { get; set; } = 1;

    public uint MaxItems
    { get; set; } = 1;

    public string Message
    { get; set; } = "";

    [Obsolete("This constructor is deprecated use FieldPickQuery(GameObject[] validSelections, int minItems, int maxItems, string message) instead")]
    public FieldPickQuery()
    { }

    public FieldPickQuery(GameObject[] validSelections, int minItems, int maxItems, string message)
    {
        ValidSelections = validSelections;
        MinItems = (uint)minItems;
        MaxItems = (uint)maxItems;
        Message = message;
    }

    public override bool ValidateAnswer(PlayerAnswer answer, GameController gameController)
    {
        foreach(GameObject obj in (answer as FieldPickAnswer).Selections)
        {
            bool objectFound = false;
            foreach(GameObject validSelection in ValidSelections)
            {
                if(obj == validSelection)
                {
                    objectFound = true;
                }
            }
            if(!objectFound)
            {
                return false;
            }
        }
        return true;
    }
}

public class FieldPickAnswer : PlayerAnswer
{
    public GameObject[] Selections
    { get; set; }

    public override PlayerAnswerData GetSerializableData(PlayerQuery query)
    {
        return new FieldPickAnswerData(query as FieldPickQuery, this);
    }
}

[System.Serializable]
public class FieldPickAnswerData : PlayerAnswerData
{
    public int[] selections;

    public FieldPickAnswerData(FieldPickQuery query, FieldPickAnswer answer)
    {
        selections = new int[answer.Selections.Length];
        for(int c = 0; c < answer.Selections.Length; c++)
        {
            for (int i = 0; i < query.ValidSelections.Length; i++)
            {
                if (answer.Selections[c] == query.ValidSelections[i])
                {
                    selections[c] = i;
                }
            }
        }
    }

    public override string ToString()
    {
        return ClassName();
    }

    public static string ClassName()
    {
        return "FieldPickAnswerData";
    }
}