using System;
using System.Collections.Generic;

public class CardPickQuery : PlayerQuery
{
    [Obsolete]
    public CardComponent[] cards = null;
    [Obsolete]
    public uint minCards = 1;
    [Obsolete]
    public uint maxCards = 1;
    [Obsolete]
    public string message;
    [Obsolete]
    public bool enableCancel = false;

    [Obsolete("This constructor is deprecated use CardPickQuery(CardComponent[] _cards, int _minCards, int _maxCards, string _message, bool _enableCancel) instead")]
    public CardPickQuery()
    { }

    public CardPickQuery(CardComponent[] _cards, int _minCards, int _maxCards, string _message, bool _enableCancel = false)
    {
        cards = _cards;
        minCards = (uint)_minCards;
        maxCards = (uint)_maxCards;
        message = _message;
        enableCancel = _enableCancel;
}

    public override bool ValidateAnswer(PlayerAnswer answer, GameController gameController)
    {
        return true;
    }
}

public class CardPickAnswer : PlayerAnswer
{
    public CardComponent[] cards;

    public override PlayerAnswerData GetSerializableData(PlayerQuery query)
    {
        return new CardPickAnswerData(query as CardPickQuery, this);
    }
}

[Serializable]
public class CardPickAnswerData : PlayerAnswerData
{
    public int[] cards;

    public CardPickAnswerData(CardPickQuery query, CardPickAnswer answer)
    {
        cards = new int[answer.cards.Length];
        for (int i = 0; i < answer.cards.Length; i++)
        {
            for (int c = 0; c < query.cards.Length; c++)
            {
                if (answer.cards[i] == query.cards[c])
                {
                    cards[i] = c;
                }
            }
        }
    }

    public override string ToString()
    {
        return ClassName();
    }

    public static string ClassName()
    {
        return "CardPickAnswerData";
    }
}