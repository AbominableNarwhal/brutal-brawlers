﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerQuery
{
    public CardEffect from;
    public Player to;
    public int Id { get; set; } = 0;
    public abstract bool ValidateAnswer(PlayerAnswer answer, GameController gameController);
}

public abstract class PlayerAnswer
{
    public abstract PlayerAnswerData GetSerializableData(PlayerQuery query);
}

public abstract class PlayerAnswerData
{}