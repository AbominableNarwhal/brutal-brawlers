﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SunEater.Support;

public class CombatQuery : PlayerQuery
{
    public static readonly int MaxAuraCardInPlay = 10;

    public PlayType CombatType { get; set; }
    public override bool ValidateAnswer(PlayerAnswer answer, GameController gameController)
    {
        var combatQueryAnswer = answer as CombatQueryAnswer;
        if (combatQueryAnswer == null)
        {
            return false;
        }

        if (CombatType == PlayType.Attack)
        {
            switch (combatQueryAnswer.type)
            {
                case CombatQueryAnswer.Type.PlayAura:
                {
                    if (combatQueryAnswer.card == null || combatQueryAnswer.card.CardData.CardType != Card.Type.AuraCard)
                    {
                        return false;
                    }
                    return gameController.GetPlayerTotalAuraAmount(to) < MaxAuraCardInPlay;
                }
                case CombatQueryAnswer.Type.AttachAbility:
                {
                    if (combatQueryAnswer.card == null || combatQueryAnswer.targetFighter == null ||
                        combatQueryAnswer.card.CardData.CardType != Card.Type.AbilityCard || combatQueryAnswer.targetFighter.CardData.CardType != Card.Type.FighterCard)
                    {
                        return false;
                    }

                    var fighter = combatQueryAnswer.targetFighter.CardData as FighterGameCard;
                    var ability = combatQueryAnswer.card.CardData as AbilityGameCard;

                    if (ability.Style == Ability.Style.Sword)
                    {
                        bool hasSword = false;
                        foreach (CardComponent card in combatQueryAnswer.targetFighter.FighterAttachedEquipment())
                        {
                            var equipment = card.CardData as EquipmentGameCard;
                            if (equipment.FightStyle == Ability.Style.Sword)
                            {
                                hasSword = true;
                            }
                        }

                        if (!hasSword)
                        {
                            return false;
                        }
                    }

                    return fighter.Magic >= ability.MagicRequirement && fighter.Tech >= ability.TechRequirement && fighter.Strength >= ability.StrengthRequirement;
                }
                case CombatQueryAnswer.Type.ActivateAbility:
                {
                    return ValidateActivateAnswer(gameController, combatQueryAnswer);
                }
                case CombatQueryAnswer.Type.PlayPowerCard:
                {
                    return ValidatePlayPowerAnswer(gameController, combatQueryAnswer);
                }
                case CombatQueryAnswer.Type.AttachEquipment:
                {
                    if (combatQueryAnswer.card == null || combatQueryAnswer.targetFighter == null ||
                        combatQueryAnswer.card.CardData.CardType != Card.Type.EquipmentCard || combatQueryAnswer.targetFighter.CardData.CardType != Card.Type.FighterCard)
                    {
                        return false;
                    }

                    var fighter = combatQueryAnswer.targetFighter.CardData as FighterGameCard;

                    if (combatQueryAnswer.targetFighter.GetFighterAttachedEquipmentCount() >= fighter.EquipmentSlots)
                    {
                        return false;
                    }

                    return true;
                }
                case CombatQueryAnswer.Type.TagIn:
                {
                    return ValidateTagInAnswer(gameController, combatQueryAnswer);
                }
                default:
                    //End turn is always valid
                    return true;
            }
        }
        else if (CombatType == PlayType.Defend)
        {
            switch (combatQueryAnswer.type)
            {
                case CombatQueryAnswer.Type.ActivateAbility:
                {
                    return ValidateActivateAnswer(gameController, combatQueryAnswer);
                }
                case CombatQueryAnswer.Type.PlayPowerCard:
                {
                    return ValidatePlayPowerAnswer(gameController, combatQueryAnswer);
                }
                case CombatQueryAnswer.Type.AttachAbility:
                case CombatQueryAnswer.Type.AttachEquipment:
                case CombatQueryAnswer.Type.PlayAura:
                case CombatQueryAnswer.Type.TagIn:
                {
                    return ValidateTagInAnswer(gameController, combatQueryAnswer);
                }
                default:
                    //End turn is always valid
                    return true;
            }
        }
        return false;
    }

    private bool ValidateTagInAnswer(GameController gameController, CombatQueryAnswer combatQueryAnswer)
    {
        if (combatQueryAnswer.card == null)
        {
            return false;
        }

        var fighter = combatQueryAnswer.card.CardData as FighterGameCard;
        if (fighter == null)
        {
            return false;
        }

        return ValidateAuraRequirement(gameController, to, fighter.TagInCost);
    }

    private bool ValidateActivateAnswer(GameController gameController, CombatQueryAnswer combatQueryAnswer)
    {
        if (combatQueryAnswer.card == null || combatQueryAnswer.card.CardData.CardType != Card.Type.AbilityCard)
        {
            return false;
        }

        var ability = combatQueryAnswer.card.CardData as AbilityGameCard;

        if ((ability.HasAttack || ability.HasDefend) && gameController.IsAbilityFighterActive(ability) &&
            (ability.Limit == -1 || gameController.CountAbilityUsage(ability) < ability.Limit) &&
            ((gameController.offensivePlayer == to) == ability.HasAttack || (gameController.defensivePlayer == to) == ability.HasDefend))
        {
            //Check to see if the player has enough cards in hand for the tribute cost
            int cardsInHand = gameController.GetHandContainer(to).childCount;
            if (cardsInHand >= ability.TributeCost)
            {
                //Check to see if the player has enough aura
                return ValidateAuraRequirement(gameController, combatQueryAnswer.card.CardData as AbilityGameCard);
            }
        }

        return false;
    }

    private bool ValidatePlayPowerAnswer(GameController gameController, CombatQueryAnswer combatQueryAnswer)
    {
        if (combatQueryAnswer.card == null || combatQueryAnswer.card.CardData.CardType != Card.Type.PowerCard)
        {
            return false;
        }

        return ValidateAuraRequirement(gameController, combatQueryAnswer.card.CardData as PowerGameCard);
    }

    private bool ValidateAuraRequirement(GameController gameController, IAuraConsumer auraConsumer)
    {
        Player player = to;
        int availableAura = gameController.GetPlayerUntappedAuraAmount(player);
        int auraCost = gameController.GetEffectiveAuraCost(auraConsumer);

        return ValidateAuraRequirement(gameController, player, auraCost);
    }

    private bool ValidateAuraRequirement(GameController gameController, Player player, int auraCost)
    {
        int availableAura = gameController.GetPlayerUntappedAuraAmount(player);
        return availableAura >= auraCost;
    }
}

public class CombatQueryAnswer : PlayerAnswer, IEquatable<CombatQueryAnswer>
{
    public enum Type { PlayAura, AttachAbility, AttachEquipment, ActivateAbility, PlayPowerCard, EndTurn, TagIn }
    public Type type;
    public CardComponent card;
    public FighterCardComponent targetFighter;

    public bool Equals(CombatQueryAnswer other)
    {
        if (other == null)
        {
            return false;
        }

        return type == other.type && card == other.card && targetFighter == other.targetFighter;
    }

    public override bool Equals(object other)
    {
        if (other == null)
        {
            return false;
        }

        CombatQueryAnswer otherAnswer = other as CombatQueryAnswer;
        if (otherAnswer == null)
        {
            return false;
        }

        return Equals(otherAnswer);
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override PlayerAnswerData GetSerializableData(PlayerQuery query)
    {
        return new CombatQueryAnswerData(query as CombatQuery, this);
    }

    public static bool operator ==(CombatQueryAnswer left, CombatQueryAnswer right)
    {
        return object.Equals(left, right);
    }

    public static bool operator !=(CombatQueryAnswer left, CombatQueryAnswer right)
    {
        return !(left == right);
    }
}

[Serializable]
public class CombatQueryAnswerData : PlayerAnswerData
{
    public CombatQueryAnswer.Type type;
    public int card;
    public int targetFighter;

    public CombatQueryAnswerData(CombatQuery query, CombatQueryAnswer answer)
    {
        type = answer.type;
        card = answer.card != null ? answer.card.GameId : -1;
        targetFighter = answer.targetFighter != null ? answer.targetFighter.GameId : -1;
    }

    public override string ToString()
    {
        return ClassName();
    }

    public static string ClassName()
    {
        return "CombatQueryAnswerData";
    }
}