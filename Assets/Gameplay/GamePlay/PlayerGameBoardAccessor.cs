﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SunEater.Support;

public class PlayerGameBoardAccessor
{
    private Transform playerBoard = null;

    public Transform AuraStash { get; } = null;

    public Transform ActiveFighterPosition { get; } = null;

    public Transform SupportFighter1Pos { get; } = null;

    public Transform SupportFighter2Pos { get; } = null;

    public Transform Deck { get; } = null;

    public Transform Discard { get; } = null;

    public Transform Hand { get; } = null;

    public AuraCounter AuraStashObj { get; } = null;

    public FighterCardComponent ActiveFighterCard
    {
        get
        {
            return GetCardWithinContainer(ActiveFighterPosition);
        }
    }

    public FighterGameCard ActiveFighter
    {
        get
        {
            return GetCardDataWithinContainer(ActiveFighterPosition) as FighterGameCard;
        }
    }

    public FighterCardComponent SupportFighter1Card
    {
        get
        {
            return GetCardWithinContainer(SupportFighter1Pos);
        }
    }

    public FighterGameCard SupportFighter1
    {
        get
        {
            return GetCardDataWithinContainer(SupportFighter1Pos) as FighterGameCard;
        }
    }

    public FighterCardComponent SupportFighter2Card
    {
        get
        {
            return GetCardWithinContainer(SupportFighter2Pos);
        }
    }

    public FighterGameCard SupportFighter2
    {
        get
        {
            return GetCardDataWithinContainer(SupportFighter2Pos) as FighterGameCard;
        }
    }

    public IEnumerable<CardComponent> EnumerateFieldCards()
    {
        yield return ActiveFighterCard;
        foreach (CardComponent card in ActiveFighterCard.Abilities)
        {
            yield return card;
        }

        yield return SupportFighter1Card;
        foreach (CardComponent card in SupportFighter1Card.Abilities)
        {
            yield return card;
        }

        yield return SupportFighter2Card;
        foreach (CardComponent card in SupportFighter2Card.Abilities)
        {
            yield return card;
        }

        foreach (Transform child in Hand)
        {
            yield return child.GetComponent<CardComponent>();
        }
    }

    public IEnumerable<FighterCardComponent> EnumerateFighterCards()
    {
        yield return ActiveFighterCard;
        yield return SupportFighter1Card;
        yield return SupportFighter2Card;
    }

    public PlayerGameBoardAccessor(Transform _playerBoard)
    {
        playerBoard = _playerBoard;
        ActiveFighterPosition = playerBoard.Find("Active Fighter");
        SupportFighter1Pos = playerBoard.Find("Support Fighter 1");
        SupportFighter2Pos = playerBoard.Find("Support Fighter 2");
        Deck = playerBoard.Find("Deck");
        Discard = playerBoard.Find("Discard");
        Hand = playerBoard.Find("Hand");

        AuraStash = playerBoard.Find("Aura Stash");
        AuraStashObj = AuraStash.GetComponent<AuraCounter>();
    }

    private FighterCardComponent GetCardWithinContainer(Transform container)
    {
        if (container.childCount < 1)
        {
            return null;
        }

        return container.GetChild(0).GetComponent<FighterCardComponent>();
    }

    private GameCard GetCardDataWithinContainer(Transform container)
    {
        var cardObject = GetCardWithinContainer(container);
        if (cardObject == null)
        {
            return null;
        }

        return cardObject.CardData;
    }

}
