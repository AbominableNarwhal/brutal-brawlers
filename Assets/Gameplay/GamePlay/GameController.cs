﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.Linq;
using System;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

using SunEater.Support;

#region GameController

public class GameController : MonoBehaviour
{
    public static readonly Color OffenceColor = new Color(0.9725f, 0.1921f, 0.0235f);
    public static readonly Color DefenceColor = new Color(0.0235f, 0.1921f, 0.9725f);
    public static readonly Color NoColor = new Color(1f, 1f, 1f);
    public static readonly string FireMarkName = "Fire";
    public static readonly string ElectricMarkName = "Electric";
    public static readonly string WaterMarkName = "Water";

    public enum ValidationResult { Defer, Fail, Pass }
    public enum GamePhase { Setup, Draw, Attack, Defend, Resolve }

    private List<GameAction> actionStack;
    private ReadOnlyCollection<GameAction> readOnlyActionStack;
    private List<CardEffect> cardEffects;
    [SerializeField]
    private GamePhase gamePhase;
    private CardAnimator animator;
    [SerializeField]
    private PlayerQuery currentQuery;
    [SerializeField]
    private Canvas uiLayer = null;
    [SerializeField]
    private new ParticleSystem particleSystem = null;
    [SerializeField]
    private GameObject damageStatusPrefab = null;
    [SerializeField]
    private Marker[] gameMarkers = null;
    private TextMeshProUGUI victoryText = null;
    private TextMeshProUGUI defeatText = null;
    private Tooltip tooltip = null;
    private IncomingAbilityPanel incomingAbilityPanel = null;
    private List<GameAction> completedActions;
    private ReadOnlyCollection<GameAction> readOnlyCompletedActions;
    private List<RoundRecord> gameRecord;
    private ReadOnlyCollection<RoundRecord> readOnlyGameRecord;
    private AbilityCardBaseComponent activeOffensiveAbility = null;
    private AbilityCardBaseComponent activeDefensiveAbility = null;
    private DamageAction incomingDamage = null;
    private GameEffect gameEffect = null;
    private bool gameOver = false;
    private bool actionExecuting = false;
    /////////// Player UI Prefabs////////////////
    [SerializeField]
    private GameObject messageBox = null;
    [SerializeField]
    private GameObject cardPickerUiPrefab = null;
    [SerializeField]
    private GameObject optionPickerPrefab = null;
    [SerializeField]
    private GameObject offensiveTurnSign = null;
    [SerializeField]
    private GameObject defensiveTurnSign = null;
    private Image turnIndicator = null;

    #region Game Logic Structures
    public ReadOnlyCollection<GameAction> ActionStack
    {
        get
        {
            return readOnlyActionStack;
        }
    }

    public ReadOnlyCollection<GameAction> CompletedActions
    {
        get
        {
            return readOnlyCompletedActions;
        }
    }

    public ReadOnlyCollection<RoundRecord> GameRecord
    {
        get
        {
            return readOnlyGameRecord;
        }
    }

    public PlayerQuery CurrentQuery
    {
        get
        {
            return currentQuery;
        }
    }

    #endregion

    #region UILayer Properties
    private TextMeshProUGUI VictoryText
    {
        get
        {
            if (victoryText == null)
            {
                victoryText = uiLayer.transform.Find("Victory Text").GetComponent<TextMeshProUGUI>();
            }

            return victoryText;
        }
    }

    private TextMeshProUGUI DefeatText
    {
        get
        {
            if (defeatText == null)
            {
                defeatText = uiLayer.transform.Find("Defeat Text").GetComponent<TextMeshProUGUI>();
            }

            return defeatText;
        }
    }

    private Image TurnIndicator
    {
        get
        {
            if (turnIndicator == null && uiLayer != null)
            {
                turnIndicator = uiLayer.transform.Find("Turn Indicator").GetComponent<Image>();
            }
            return turnIndicator;
        }
    }

    private Button endTurnButton = null;
    private Button EndTurnButton
    {
        get
        {
            if (endTurnButton == null)
            {
                endTurnButton = uiLayer.transform.Find("End Turn Button").GetComponent<Button>();
            }
            return endTurnButton;
        }
    }

    private TextMeshProUGUI endTurnButtonText = null;
    public TextMeshProUGUI EndTurnButtonText
    {
        get
        {
            if (endTurnButtonText == null)
            {
                endTurnButtonText = EndTurnButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            }
            return endTurnButtonText;
        }
    }

    #endregion

    public PlayerGameBoardAccessor Player1Board
    { get; private set; }
    public PlayerGameBoardAccessor Player2Board
    { get; private set; }
    public Transform RevealArea
    { get; private set; }
    public Player HostPlayer { get; private set; }
    public Player ClientPlayer { get; private set; }

    public RandomModule RandomGenerator
    { get; set; }

    //[HideInInspector]
    public Player player1;
    //[HideInInspector]
    public Player player2;
    
    [HideInInspector]
    public Player offensivePlayer;
    [HideInInspector]
    public Player defensivePlayer;
    public GameObject gameBoard;

    #region Events
    private event EventHandler<GameInputEventArgs> GlobalGameInputEvent;
    #endregion

    public GameController()
    {
        actionStack = new List<GameAction>();
        readOnlyActionStack = actionStack.AsReadOnly();
        cardEffects = new List<CardEffect>();
        completedActions = new List<GameAction>();
        readOnlyCompletedActions = completedActions.AsReadOnly();
        gameRecord = new List<RoundRecord>();
        readOnlyGameRecord = gameRecord.AsReadOnly();
        RandomGenerator = new LocalRandomModule();
    }

    #region Player Events

    public void EndTurnSignal()
    {
        if (gameOver)
        {
            return;
        }

        player1.OnEndTurnSignal();
        player2.OnEndTurnSignal();
    }

    #endregion

    #region Game action behaviors

    public GamePhase CurrentGamePhase
    {
        get
        {
            return gamePhase;
        }
        set
        {
            if (gamePhase == GamePhase.Resolve)
            {
                activeOffensiveAbility = null;
                activeDefensiveAbility = null;
            }

            gamePhase = value;
            UpdateTurnIndicator();
            UpdateIncomingAbilityState();
            switch (gamePhase)
            {
                case GamePhase.Attack:
                    incomingDamage = null;
                    if (player1 == offensivePlayer)
                    {
                        Instantiate(offensiveTurnSign, uiLayer.transform);
                    }
                    break;
                case GamePhase.Defend:
                    if (player1 == defensivePlayer)
                    {
                        Instantiate(defensiveTurnSign, uiLayer.transform);
                    }
                    break;
            }
        }
    }

    public DamageAction IncomingDamage
    {
        get { return incomingDamage; }
    }

    public void DiscardCard(CardComponent card)
    {
        if (card == null)
        {
            return;
        }

        RemoveCardFromPlay(card);

        if (card.CardData.CardType == Card.Type.AbilityCard)
        {
            var abilityCard = card as AbilityCardComponent;
            FighterCardComponent fighterCard = FindAttachedFighterCard(abilityCard);

            if (fighterCard != null)
            {
                fighterCard.DetachAbility(abilityCard);
            }
        }
        else if (card.CardData.CardType == Card.Type.EquipmentCard)
        {
            var equipmentCard = card as EquipmentCardComponent;
            FighterCardComponent fighterCard = FindAttachedFighterCard(equipmentCard);

            if (fighterCard != null)
            {
                fighterCard.DetachEquipment(equipmentCard);
            }
        }

        card.transform.SetParent(GetPlayerGameBoard(card.CardData.Owner).Discard);
    }

    public void ModifyIncomingDamage(int amount)
    {
        if (IncomingDamage != null)
        {
            IncomingDamage.damageAmount += amount;
            incomingAbilityPanel.SetIncomingDamage(IncomingDamage.damageAmount);
        }
    }

    public void InterruptEffect(GameCard card)
    {
        for (int i = actionStack.Count - 1; i >= 0; i--)
        {
            if (actionStack[i].OriginEffect != null)
            {
                if (actionStack[i].OriginEffect.CardObject.CardData == card)
                {
                    actionStack.RemoveAt(i);
                }
            }
        }
    }

    public void DamageAnimation(Transform transform, int amount)
    {
        if (particleSystem == null)
        {
            return;
        }

        particleSystem.transform.position = transform.position;
        particleSystem.Emit(50);
        GameObject obj = Instantiate(damageStatusPrefab, gameBoard.transform);
        obj.transform.position = transform.position;
        obj.transform.GetChild(0).GetComponent<TextMeshPro>().text = amount.ToString();
    }

    /// <summary>
    /// Adds a CardEffect from Card to the CardEffects manager
    /// </summary>
    public void PutAttachedCardIntoPlay(AbilityCardBaseComponent card, FighterGameCard user)
    {
        CardEffect effect = CreateAttachedCardEffectByClassName(card, user);

        if (effect != null)
        {
            AddCardEffect(effect);
        }
    }

    public void PutFighterCardIntoPlay(FighterCardComponent fighterCard, FighterPosition position, Player player)
    {
        if (fighterCard == null)
        {
            return;
        }

        FighterCardEffect fighterCardEffect = new FighterCardEffect(fighterCard);

        switch(position)
        {
            case FighterPosition.Active:
                fighterCard.transform.SetParent(GetPlayerGameBoard(player).ActiveFighterPosition);
                break;
            case FighterPosition.Support1:
                fighterCard.transform.SetParent(GetPlayerGameBoard(player).SupportFighter1Pos);
                break;
            case FighterPosition.Support2:
                fighterCard.transform.SetParent(GetPlayerGameBoard(player).SupportFighter2Pos);
                break;
        }

        AddCardEffect(fighterCardEffect);
    }

    /// <summary>
    /// Adds a CardEffect from CardComponent to the CardEffects manager
    /// </summary>
    public void PutCardIntoPlay(CardComponent card)
    {
        CardEffect effect = CreateCardEffectByClassName(card);
        AddCardEffect(effect);
    }

    private void AddCardEffect(CardEffect effect)
    {
        if (effect == null)
        {
            return;
        }

        /*if (cardEffects.Count == 0)
        {*/
            cardEffects.Add(effect);
        /*}
        else
        {
            cardEffects.Insert(cardEffects.Count - 1, effect);
        }*/

        var attachedEffect = effect as AttachedEffect;
        if (attachedEffect != null)
        {
            AddActions(attachedEffect.Enabled(this), attachedEffect);
        }
    }

    /// <summary>
    /// Removes a CardEffect from Card from the CardEffects manager
    /// </summary>
    public void RemoveCardFromPlay(CardComponent card)
    {
        CardEffect effect = FindCardEffect(card.CardData, out int index);
        if(effect == null)
        {
            return;
        }


        if (card.CardData.CardType == Card.Type.AbilityCard)
        {
            var abilityCard = card as AbilityCardComponent;
            FighterCardComponent fighterCard = FindAttachedFighterCard(abilityCard);

            if (fighterCard != null)
            {
                fighterCard.DetachAbility(abilityCard);
            }
        }

        cardEffects.RemoveAt(index);
        InterruptEffect(card.CardData);

        var attachedEffect = effect as AttachedEffect;
        if (attachedEffect != null)
        {
            AddActions(attachedEffect.Disabled(this), attachedEffect);
        }
        effect.OnRemovedFromPlay(this);
    }

    public CardEffect CreateCardEffectByClassName(CardComponent card)
    {
        if (card == null)
        {
            return null;
        }

        return InstanciateCardEffectClass(card.CardData.CardName, card);
    }

    public CardEffect CreateAttachedCardEffectByClassName(CardComponent card, FighterGameCard user)
    {
        if (card == null)
        {
            return null;
        }

        return InstanciateCardEffectClass(card.CardData.CardName, card, user);
    }

    private CardEffect InstanciateCardEffectClass(string cardName, params object[] args)
    {
        System.Type t = System.Type.GetType(new string(cardName.Where(char.IsLetterOrDigit).ToArray()));

        if (t == null)
        {
            return null;
        }

        return (CardEffect)System.Activator.CreateInstance(t, args);
    }

    /// <summary>
    /// Simply switched the offensive and defensive player refrences
    /// </summary>
    public void EndTurn()
    {
        //Log the current round's completed actions in the game record
        RoundRecord roundRecord = new RoundRecord(offensivePlayer, defensivePlayer, completedActions.ToArray());
        gameRecord.Add(roundRecord);

        //Log the completed actions list for the new round
        completedActions.Clear();

        //empty the actionStack
        ClearActionStack();

        Player temp = offensivePlayer;
        offensivePlayer = defensivePlayer;
        defensivePlayer = temp;

        UpdateTurnIndicator();
    }

    public void ClearActionStack()
    {
        actionStack.Clear();
    }

    public void ActivatePowerDefend(PowerGameCard power)
    {
        CardEffect cardEffect = FindCardEffect(power);
        if (cardEffect == null)
        {
            Debug.LogError("No card effect for this power");
            return;
        }
        AddActions(cardEffect.OnDefend(this, incomingDamage), cardEffect);
    }

    public void ActivatePowerAttack(PowerGameCard power)
    {
        CardEffect cardEffect = FindCardEffect(power);
        if(cardEffect == null)
        {
            Debug.LogError("No card effect for this power");
            return;
        }
        AddActions(cardEffect.OnAttack(this), cardEffect);
    }

    public void ActivateAbilityDefend(AbilityGameCard ability)
    {
        CardEffect cardEffect = FindCardEffect(ability);
        if(cardEffect == null)
        {
            Debug.LogError("No card effect for this ability");
            return;
        }
        activeDefensiveAbility = cardEffect.CardObject as AbilityCardBaseComponent;
        AddActions(cardEffect.OnDefend(this, incomingDamage), cardEffect);
    }

    public void ActivateAbilityAttack(AbilityGameCard ability)
    {
        CardEffect cardEffect = FindCardEffect(ability);
        if(cardEffect == null)
        {
            Debug.LogError("No card effect for this ability");
            return;
        }
        activeOffensiveAbility = cardEffect.CardObject as AbilityCardBaseComponent;
        AddActions(cardEffect.OnAttack(this), cardEffect);
    }

    public void AddActions(GameAction[] actions, CardEffect originEffect)
    {
        if (actions == null)
        {
            return;
        }
        for (int i = actions.Length - 1; i >= 0; i--)
        {
            AddAction(actions[i], originEffect);
        }
    }
    public void AddAction(GameAction action, CardEffect originEffect)
    {
        if (action == null)
        {
            return;
        }

        DamageAction damageAction = action as DamageAction;
        if (damageAction != null && damageAction.IsAttackDamage)
        {
            incomingDamage = damageAction;
        }

        action.OriginEffect = originEffect;
        actionStack.Add(action);
        foreach (CardEffect effect in cardEffects)
        {
            AddActions(effect.OnActionPostSubmitBroadcast(this, action), effect);
        }
    }

    public void RemoveAction(GameAction action)
    {
        actionStack.Remove(action);
    }

    public void SubmitQuery(PlayerQuery query)
    {
        if (query == null)
        {
            Debug.LogError("Query is null");
            return;
        }
        if (query.to == null)
        {
            Debug.LogError("Query is not going to anyone");
            return;
        }
        if (query.from == null)
        {
            Debug.LogError("Query is not from anyone");
            return;
        }
        currentQuery = query;
        currentQuery.to.PostQuery(currentQuery, this);
    }

    /// <summary>
    /// Taps or untaps a player's aura
    /// </summary>
    /// <param name="player">The player who's aura to tap or untap</param>
    /// <param name="auraToTap">The number of aura to tap or untap</param>
    /// <param name="isTapped">If true, aura will be tapped. If false aura will be untapped</param>
    public void TapPlayerAura(Player player, int auraToTap, bool isTapped)
    {
        var playerBoard = GetPlayerGameBoard(player);
        Transform auraStash = playerBoard.AuraStash;
        int auraTapped = 0;

        foreach (Transform child in auraStash)
        {
            if (auraTapped == auraToTap)
            {
                break;
            }
            CardComponent aura = child.GetComponent<CardComponent>();
            if (aura.IsFaceUp == isTapped)
            {
                aura.IsFaceUp = !isTapped;
                auraTapped++;
                playerBoard.AuraStashObj.UpdateAuraCount();
            }
        }
    }

    public int GetEffectiveAuraCost(IAuraConsumer powerOrAbility)
    {
        if (powerOrAbility == null)
        {
            return -1;
        }

        int acumulator = 0;
        foreach (CardEffect cardEffect in cardEffects)
        {
            acumulator += cardEffect.OnAlterAuraCost(this, powerOrAbility);
        }

        int total = powerOrAbility.AuraCost + acumulator;

        if (total < 0)
        {
            total = 0;
        }

        return total;
    }

    public void EndGame(bool player1IsWinner)
    {
        if (player1IsWinner)
        {
            VictoryText.gameObject.SetActive(true);
        }
        else
        {
            DefeatText.gameObject.SetActive(true);
        }

        gameOver = true;
    }

    public bool ValidateWithCurrentCombatQuery(CombatQueryAnswer answer)
    {
        if (currentQuery == null)
        {
            return false;
        }

        foreach (var cardEffect in cardEffects)
        {
            var result = cardEffect.OnValidateQuery(this, currentQuery, answer);
            switch (result)
            {
                case ValidationResult.Pass:
                    return true;
                case ValidationResult.Fail:
                    return false;
                case ValidationResult.Defer:
                default:
                    break;
            }
        }

        return currentQuery.ValidateAnswer(answer, this);
    }

    #endregion

    #region Game object finding functions

    public Marker FireMark
    {
        get { return GetMarker(FireMarkName); }
    }

    public Marker ElectricMark
    {
        get { return GetMarker(ElectricMarkName); }
    }

    public Marker WaterMark
    {
        get { return GetMarker(WaterMarkName); }
    }

    public Marker GetMarker(string markerName)
    {
        foreach (Marker marker in gameMarkers)
        {
            if (marker.Name == markerName)
            {
                return marker;
            }
        }

        return null;
    }

    /// <summary> Returns the player's game object
    /// </summary>
    public GameObject GetPlayerGameObject(Player player)
    {
        if(player == player1)
        {
            return gameBoard.transform.Find("Player 1").gameObject;
        }
        else if(player == player2)
        {
            return gameBoard.transform.Find("Player 2").gameObject;
        }
        return null;
    }

    /// <summary> Returns the player's game object
    /// </summary>
    public PlayerGameBoardAccessor GetPlayerGameBoard(Player player)
    {
        if (player == player1)
        {
            return Player1Board;
        }
        else if (player == player2)
        {
            return Player2Board;
        }
        return null;
    }

    public bool IsAbilityCardFighterActive(CardComponent abilityCard)
    {
        AbilityGameCard ability = abilityCard.CardData as AbilityGameCard;
        return IsAbilityFighterActive(ability);
    }

    public bool IsAbilityFighterActive(AbilityGameCard ability)
    {
        if (ability == null)
        {
            return false;
        }

        FighterGameCard fighter = GetPlayerGameBoard(ability.Owner).ActiveFighter;

        return fighter != null && fighter.HasAbility(ability);
    }

    public bool IsAbilityUsable(AbilityGameCard ability)
    {
        if (ability == null || !ability.Enabled)
        {
            return false;
        }

        var fighter = FindAttachedFighter(ability);
        if (fighter == null || fighter.Hp <= 0)
        {
            return false;
        }

        return true;
    }

    public FighterGameCard FindAttachedFighter(AbilityGameCard ability)
    {
        var playerBoard = GetPlayerGameBoard(ability.Owner);

        var fighter = playerBoard.ActiveFighter;
        if (fighter != null && fighter.HasAbility(ability))
        {
            return fighter;
        }

        fighter = playerBoard.SupportFighter1;
        if (fighter != null && fighter.HasAbility(ability))
        {
            return fighter;
        }

        fighter = playerBoard.SupportFighter2;
        if (fighter != null && fighter.HasAbility(ability))
        {
            return fighter;
        }

        return null;
    }

    public FighterCardComponent FindAttachedFighterCard(AbilityCardBaseComponent ability)
    {
        var playerBoard = GetPlayerGameBoard(ability.AbilityData.Owner);

        var fighter = playerBoard.ActiveFighterCard;
        if (fighter != null && fighter.HasAbility(ability))
        {
            return fighter;
        }

        fighter = playerBoard.SupportFighter1Card;
        if (fighter != null && fighter.HasAbility(ability))
        {
            return fighter;
        }

        fighter = playerBoard.SupportFighter2Card;
        if (fighter != null && fighter.HasAbility(ability))
        {
            return fighter;
        }

        return null;
    }

    public FighterCardComponent FindAttachedFighterCard(EquipmentCardComponent equipmentCard)
    {
        var playerBoard = GetPlayerGameBoard(equipmentCard.EquipmentData.Owner);

        var fighter = playerBoard.ActiveFighterCard;
        if (fighter != null && fighter.HasEquipment(equipmentCard))
        {
            return fighter;
        }

        fighter = playerBoard.SupportFighter1Card;
        if (fighter != null && fighter.HasEquipment(equipmentCard))
        {
            return fighter;
        }

        fighter = playerBoard.SupportFighter2Card;
        if (fighter != null && fighter.HasEquipment(equipmentCard))
        {
            return fighter;
        }

        return null;
    }

    public int CountAbilityUsage(AbilityGameCard ability)
    {
        int count = 0;
        foreach (var completedAction in completedActions)
        {
            var action = completedAction as ActivateAbilityAction;
            if (action != null && action.abilityCard.CardData == ability)
            {
                count++;
            }
        }
        return count;
    }

    /// <summary> Returns the player's opponents's game object
    /// </summary>
    public GameObject GetPlayerOpponentGameObject(Player player)
    {
        return GetPlayerGameObject(GetPlayerOpponent(player));
    }

    public Player GetPlayerOpponent(Player player)
    {
        if(player == player1)
        {
            return player2;
        }
        else if(player == player2)
        {
            return player1;
        }
        return null;
    }

    public CardEffect FindCardEffect(GameCard card)
    {
        return FindCardEffect(card, out int i);
    }

    public CardEffect FindCardEffect(CardComponent card)
    {
        return FindCardEffect(card.CardData, out int i);
    }

    public CardEffect FindCardEffect(GameCard card, out int index)
    {
        index = 0;
        foreach(CardEffect cardEffect in cardEffects)
        {
            if(cardEffect.Parent == card)
            {
                return cardEffect;
            }
            index++;
        }
        return null;
    }

    public FighterGameCard GetActiveFighter(Player player)
    {
        return GetPlayerGameBoard(player).ActiveFighter;
    }

    public FighterCardComponent GetActiveFighterCard(Player player)
    {
        return GetPlayerGameBoard(player).ActiveFighterCard;
    }

    public CardComponent GetSupportFighterCard(Player player, int supportSlot)
    {
        if (supportSlot == 1)
        {
            return GetPlayerGameBoard(player).SupportFighter1Card;
        }
        else if (supportSlot == 2)
        {
            return GetPlayerGameBoard(player).SupportFighter2Card;
        }

        return null;
    }

    public Transform GetHandContainer(Player player)
    {
        return GetPlayerGameBoard(player).Hand;
    }

    public IEnumerable<CardComponent> CardsInHand(Player player)
    {
        Transform playerHand = GetHandContainer(player);

        foreach (Transform child in playerHand)
        {
            yield return child.GetComponent<CardComponent>();
        }
    }

    #endregion

    #region Helper Functions
    public int GetPlayerTotalAuraAmount(Player player)
    {
        Transform auraStash = GetPlayerGameBoard(player).AuraStash;
        return auraStash.childCount;
    }

    public int GetPlayerUntappedAuraAmount(Player player)
    {
        Transform auraStash = GetPlayerGameObject(player).transform.Find("Aura Stash");

        int count = 0;
        foreach (Transform child in auraStash)
        {
            CardComponent aura = child.GetComponent<CardComponent>();
            if (aura.IsFaceUp)
            {
                count++;
            }
        }
        return count;
    }
    #endregion

    #region Player UI Spawn Functions

    public void SetActionButton(bool enabled, string text)
    {
        EndTurnButton.interactable = enabled;
        EndTurnButtonText.text = text;
    }

    public void SetEndTurnButtonText(string text)
    {
        EndTurnButtonText.text = text;
    }

    public void ShowCardPickerUI(CardComponent[] cards, uint minCards, uint maxCards, string title, bool enableCancel, PlayerUIListener listener)
    {
        CardPickerUI cardPickerUI = Instantiate(cardPickerUiPrefab, uiLayer.transform).GetComponent<CardPickerUI>();
        cardPickerUI.cards = cards;
        cardPickerUI.minCards = minCards;
        cardPickerUI.maxCards = maxCards;
        cardPickerUI.titleText = title;
        cardPickerUI.EnableCancel(enableCancel);
        cardPickerUI.listener = listener;
    }

    public void ShowOptionPickerUI(List<string> options, string title, PlayerUIListener listener)
    {
        OptionPickerUI optionPickerUI = Instantiate(optionPickerPrefab, uiLayer.transform).GetComponent<OptionPickerUI>();
        optionPickerUI.options = options;
        optionPickerUI.titleText = title;
        optionPickerUI.listener = listener;
    }

    public void ShowMessage(string message)
    {
        messageBox.SetActive(true);
        messageBox.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = message;
        LayoutRebuilder.ForceRebuildLayoutImmediate(messageBox.GetComponent<RectTransform>());
    }

    public void HideMessageBox()
    {
        messageBox.SetActive(false);
    }

    private void UpdateTurnIndicator()
    {
        if (TurnIndicator == null)
        {
            return;
        }

        if (CurrentGamePhase == GamePhase.Setup)
        {
            TurnIndicator.color = NoColor;
            TurnIndicator.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Set Up";
        }
        else if (player1 == offensivePlayer)
        {
            TurnIndicator.color = OffenceColor;
            TurnIndicator.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Offense";
        }
        else
        {
            TurnIndicator.color = DefenceColor;
            TurnIndicator.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Defence";
        }
    }

    private CardComponent GetFighterCardIfNativeAbility(AbilityCardBaseComponent abilityCard)
    {
        if (abilityCard.IsFighterAbility)
        {
            FighterCardComponent fighterCard = FindAttachedFighterCard(abilityCard);
            if (fighterCard != null)
            {
                FighterCardComponent newFighterCard = FighterCardComponent.CreateFighterCard(fighterCard.FighterData.StaticData as Fighter);
                foreach (var ability in newFighterCard.Abilities)
                {
                    if (ability.CardData.CardName == abilityCard.CardData.CardName)
                    {
                        var graphic = ability.GetComponent<GameBoardGraphic>();
                        graphic.SetHighlightColor(graphic.SelectedColor);
                    }
                }

                return newFighterCard;
            }
        }

        return Instantiate(abilityCard);
    }

    private void UpdateIncomingAbilityState()
    {
        if (incomingAbilityPanel == null)
        {
            return;
        }

        switch (CurrentGamePhase)
        {
            case GamePhase.Attack:
                incomingAbilityPanel.HideIncomingAbilityState();
                break;
            case GamePhase.Defend:
            {
                if (activeOffensiveAbility)
                {
                    CardComponent card = GetFighterCardIfNativeAbility(activeOffensiveAbility);

                    SubcribeToGlobalListener(card);

                    IncomingAbilityParams incomingAbilityParams = new IncomingAbilityParams
                    {
                        incomingDamage = incomingDamage.damageAmount,
                        IsPlayer1Attacker = offensivePlayer == player1,
                        offensiveCard = card,
                        defensiveCard = null
                    };
                    incomingAbilityPanel.SetIncomingAbilityState(incomingAbilityParams);
                }
                break;
            }
            case GamePhase.Resolve:
            {
                if (activeOffensiveAbility && activeDefensiveAbility)
                {
                    CardComponent offensiveCard = GetFighterCardIfNativeAbility(activeOffensiveAbility);
                    CardComponent defensiveCard = GetFighterCardIfNativeAbility(activeDefensiveAbility);

                    SubcribeToGlobalListener(offensiveCard);
                    SubcribeToGlobalListener(defensiveCard);

                    IncomingAbilityParams incomingAbilityParams = new IncomingAbilityParams
                    {
                        incomingDamage = incomingDamage.damageAmount,
                        IsPlayer1Attacker = offensivePlayer == player1,
                        offensiveCard = offensiveCard,
                        defensiveCard = defensiveCard
                    };
                    incomingAbilityPanel.SetIncomingAbilityState(incomingAbilityParams);
                }
                break;
            }
        }
    }

    #endregion

    #region Unity Events
    public void InitializeGameBoard()
    {
        if (gameBoard == null)
        {
            Debug.LogError("Game board not set on controller!");
            return;
        }

        animator = gameBoard.GetComponent<CardAnimator>();
        var gameState = FindObjectOfType<GameStateParameters>();
        if (gameState == null)
        {
            Debug.LogError("Game State not inicialized!");
            return;
        }

        RandomGenerator.SetSeed(gameState.RandomSeed);

        player1 = gameState.Player1;
        player2 = gameState.Player2;
        HostPlayer = gameState.IsHost ? player1 : player2;
        ClientPlayer = gameState.IsHost ? player2 : player1;
        player1.gameBoard = gameBoard;
        player2.gameBoard = gameBoard;
        Player1Board = new PlayerGameBoardAccessor(gameBoard.transform.Find("Player 1"));
        Player2Board = new PlayerGameBoardAccessor(gameBoard.transform.Find("Player 2"));
        RevealArea = gameBoard.transform.Find("Reveal Area");
        if (uiLayer)
        {
            tooltip = uiLayer.transform.Find("Tooltip Container").GetChild(0).GetComponent<Tooltip>();
            incomingAbilityPanel = uiLayer.transform.Find("Incoming Ability Panel").GetComponent<IncomingAbilityPanel>();
        }

        gameEffect = new GameEffect();
        AddCardEffect(gameEffect);

        //Player 1
        LoadPlayerData(player1, Player1Board, gameState.IsHost ? 0 : 100);

        //Player 2
        LoadPlayerData(player2, Player2Board, !gameState.IsHost ? 0 : 100);

        //Hook Up input handlers
        GlobalGameInputEvent += player1.OnGameInputEvent;
        GlobalGameInputEvent += player2.OnGameInputEvent;
    }

    public void StartGame()
    {
        AddActions(gameEffect.OnAttack(this), gameEffect);
         
        UpdateTurnIndicator();
    }

    private IEnumerator WaitForAction(float waitTime)
    {
        actionExecuting = true;
        yield return new WaitForSeconds(waitTime);
        actionExecuting = false;
    }

    public void ExecuteGameLoop()
    {
        if (currentQuery != null)
        {
            PlayerAnswer answer = currentQuery.to.GetQueryAnswer();
            if (answer == null)
            {
                return;
            }
            GetPlayerOpponent(currentQuery.to).OnOpponentAnswer(currentQuery, answer);
            AddActions(currentQuery.from.OnQueryAnswered(this, currentQuery, answer), currentQuery.from);
            currentQuery.from = null;
            currentQuery.to = null;
            currentQuery = null;
        }
        else if (actionStack.Count > 0)
        {
            int index = actionStack.Count - 1;
            GameAction executingAction = actionStack[index];
            actionStack.RemoveAt(index);

            //Execute the action at the top of the stack
            //Debug.Log("Executing " + executingAction.GetType().Name);
            executingAction.Execute(this);

            if (executingAction.ExecutionDuration > 0)
            {
                IEnumerator coroutine = WaitForAction(executingAction.ExecutionDuration);
                StartCoroutine(coroutine);
            }

            //Log the action that just executed
            completedActions.Add(executingAction);
            foreach (CardEffect effect in cardEffects)
            {
                AddActions(effect.OnActionCompleteBroadcast(this, executingAction), effect);
            }
        }
        else
        {
            if (gameEffect != null)
            {
                AddActions(gameEffect.OnGameIdle(this), gameEffect);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        InitializeGameBoard();
        StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (animator.IsAnimating || gameOver || actionExecuting)
        {
            return;
        }

        ExecuteGameLoop();
    }

    #endregion

    #region Deck Loading Functions

    private void LoadPlayerData(Player player, PlayerGameBoardAccessor playerBoard, int startId)
    {
        Transform activeFighterSlot = playerBoard.ActiveFighterPosition;
        Transform deck = playerBoard.Deck;

        CreateFighter(player.playerDeck.fighters[0], activeFighterSlot, player, ref startId);
        CreateFighter(player.playerDeck.fighters[1], activeFighterSlot, player, ref startId);
        CreateFighter(player.playerDeck.fighters[2], activeFighterSlot, player, ref startId);

        LoadPlayerDeck(player, deck, ref startId);
    }

    private void LoadPlayerDeck(Player player, Transform deck, ref int startId)
    {
        if (player.playerDeck.cards != null)
        {
            foreach (Card card in player.playerDeck.cards)
            {
                CreateCard(card, deck, player, startId++);
            }
        }
    }

    private void CreateFighter(Fighter fighter, Transform fieldPosition, Player owner, ref int id)
    {
        if (fighter == null)
        {
            return;
        }
        FighterCardComponent fighterCardObject = FighterCardComponent.CreateFighterCard(fighter, fieldPosition);
        fighterCardObject.FighterData.Owner = owner;
        fighterCardObject.GameId = id++;
        int i = 0;
        foreach (CardComponent AbilityCard in fighterCardObject.Abilities)
        {
            FighterAbilityComponent nativeAbility = AbilityCard.GetComponent<FighterAbilityComponent>();
            nativeAbility.CardData.Owner = owner;
            nativeAbility.GameId = id++;
            PutAttachedCardIntoPlay(nativeAbility, fighterCardObject.FighterData);
            SubcribeToGlobalListener(nativeAbility);
            i++;
        }
        SubcribeToGlobalListener(fighterCardObject);
    }
    private void CreateCard(Card card, Transform fieldPosition, Player owner, int id)
    {
        if (card == null)
        {
            return;
        }
        CardComponent cardComponent = CardFactory.CreateCardObject(card, fieldPosition);
        cardComponent.GameId = id;
        cardComponent.CardData.Owner = owner;
        SubcribeToGlobalListener(cardComponent);
    }

    private void SubcribeToGlobalListener(CardComponent card)
    {
        var inputReceiver = card.GetComponent<InputSystemReceiver>();
        if (inputReceiver == null)
        {
            return;
        }

        inputReceiver.GameInputEvent += OnGamePointerEvent;
    }
    #endregion

    private void OnGamePointerEvent(object subject, GameInputEventArgs args)
    {
        if (args.EventType == GameInputEventArgs.PointerEvent.LongHover)
        {
            var gameObject = subject as GameObject;
            if (gameObject != null)
            {
                var tooltipGen = gameObject.GetComponent<TooltipGenerator>();
                if (tooltipGen != null)
                {
                    tooltip.SetTooltipUI(tooltipGen.GenerateTooltip());
                }
            }
        }
        else if (args.EventType == GameInputEventArgs.PointerEvent.Exit)
        {
            tooltip.ClearTooltip();
        }

        GlobalGameInputEvent?.Invoke(subject, args);
    }

    public void GoBackToMainMenu()
    {
        GameServerClient.TryDisconect();
        SceneManager.LoadScene("DeckSelection");
    }

#endregion

#region GameEffect

    private class GameEffect : CardEffect
    {
        private static readonly string PickNewFighterMessage = "Pick a new fighter!";
        private static readonly string ReplaceAbilityMessage = "Choose an ability attached to this fighter to remove.";
        private static readonly string ReplaceEquipmentMessage = "Choose an equipment attached to this fighter to remove.";
        private static readonly string TributeCardMessage = "Choose cards to tribute for this ability.";
        private static readonly string PickStartingFighterStr = "Choose your starting Fighter.";
        private static readonly string WaitForOpponentStr = "Waiting for Opponent...";
        private static readonly string DrawString = "Drawing";
        private static readonly int FigherKOFieldPick = 0;
        private static readonly int AbilityReplaceFieldPick = 1;
        private static readonly int TributeCardFieldPick = 2;
        private static readonly int StartingFighterFieldPick = 3;
        private static readonly int EquipmentReplaceFieldPick = 4;

        private CombatQueryAnswer abilityReplaceAnswer = null;

        public GameEffect() : base(null)
        {
        }

        public override GameAction[] OnAttack(GameController gameController)
        {
            //The game will start in setup Phase
            GameAction[] actions = new GameAction[1];
            actions[0] = new PhaseChangeAction(GamePhase.Setup);
            return actions;
        }
        public override GameAction[] OnActionPostSubmitBroadcast(GameController gameController, GameAction action)
        {
            GameAction[] actions = null;
            var damageAction = action as DamageAction;
            if (damageAction == null)
            {
                return null;
            }
            switch (gameController.gamePhase)
            {
                case GamePhase.Attack:
                {

                    if (damageAction.IsAttackDamage)
                    {
                        actions = new GameAction[1];
                        actions[0] = new PhaseChangeAction(GamePhase.Defend);
                    }
                }
                break;
            }
            return actions;
        }
        public override GameAction[] OnActionCompleteBroadcast(GameController gameController, GameAction action)
        {
            GameAction[] actions = null;
            if (action is PhaseChangeAction)
            {
                GameController.GamePhase phase = gameController.gamePhase;
                switch (phase)
                {
                    case GameController.GamePhase.Draw:
                    {
                        PlayEndTurnButtonUpdate(gameController);
                        actions = new GameAction[1];
                        actions[0] = new DrawAction(gameController.offensivePlayer);
                    }
                    break;
                    case GameController.GamePhase.Defend:
                    {
                        CombatQuery combatQuery = new CombatQuery() { from = this, to = gameController.defensivePlayer, CombatType = PlayType.Defend };
                        actions = new GameAction[1];
                        actions[0] = new QueryAction() { query = combatQuery };
                    }
                    break;
                    case GameController.GamePhase.Setup:
                    {
                        actions = new GameAction[2];
                        actions[0] = CreatePickFighterQuery(gameController, gameController.player1);
                        actions[1] = CreatePickFighterQuery(gameController, gameController.player2);
                    }
                    break;
                }
            }
            else if (action is EndTurnAction)
            {
                actions = new GameAction[1];
                actions[0] = new PhaseChangeAction(GamePhase.Draw);
            }
            else if (action is DrawAction && gameController.gamePhase == GamePhase.Draw)
            {
                var drawAction = action as DrawAction;
                if (drawAction.ResultDraw.DrawnCards.Length > 0 && drawAction.ResultDraw.DrawnCards[0].CardData.CardType == Card.Type.AuraCard)
                {
                    actions = new GameAction[1];
                    actions[0] = new DrawAction(gameController.offensivePlayer);
                }
                else
                {
                    actions = new GameAction[2];
                    actions[0] = new PhaseChangeAction(GamePhase.Attack);
                    actions[1] = new TapAuraAction()
                    {
                        NumberOfAura = -gameController.GetPlayerTotalAuraAmount(gameController.offensivePlayer),
                        TargetPlayer = gameController.offensivePlayer
                    };
                }
            }
            else if (gameController.gamePhase == GamePhase.Defend)
            {
                QueryAction queryAction = gameController.ActionStack.First() as QueryAction;

                if (action is ActivateAbilityAction)
                {
                    actions = new GameAction[1];
                    actions[0] = new PhaseChangeAction(GamePhase.Resolve);
                }
                else if (queryAction != null && queryAction.query is CombatQuery)
                {
                    if (GetFighterCardIfAlive(gameController, gameController.player1) == null || GetFighterCardIfAlive(gameController, gameController.player2) == null)
                    {
                        gameController.ClearActionStack();
                        actions = new GameAction[1];
                        actions[0] = new PhaseChangeAction(GamePhase.Attack);
                    }
                }
            }

            return actions;
        }
        public override GameAction[] OnQueryAnswered(GameController gameController, PlayerQuery query, PlayerAnswer answer)
        {
            GameAction[] actions = null;
            CombatQueryAnswer combatQueryAnswer = answer as CombatQueryAnswer;

            if(combatQueryAnswer != null)
            {
                GamePhase currentPhase = gameController.gamePhase;

                if (currentPhase == GamePhase.Attack)
                {
                    switch (combatQueryAnswer.type)
                    {
                        case CombatQueryAnswer.Type.PlayAura:
                        {
                            actions = new GameAction[1];
                            var card = combatQueryAnswer.card.GetComponent<CardComponent>();
                            card.IsFaceUp = true;
                            actions[0] = new PlayAuraAction() { auraCard = card };
                        }
                        break;

                        case CombatQueryAnswer.Type.AttachAbility:
                        {
                            FighterCardComponent fighterCard = combatQueryAnswer.targetFighter as FighterCardComponent;
                            FighterGameCard fighter = fighterCard.CardData as FighterGameCard;
                            actions = new GameAction[1];
                            if (fighterCard.GetFighterAttachedAbilitiesCount() >= fighter.AbilitySlots)
                            {
                                abilityReplaceAnswer = combatQueryAnswer;
                                List<GameObject> validAbilities = new List<GameObject>();

                                foreach (CardComponent abilityCard in fighterCard.FighterAttachedAbilities())
                                {
                                    validAbilities.Add(abilityCard.gameObject);
                                }

                                FieldPickQuery fieldPickQuery = new FieldPickQuery()
                                {
                                    from = this,
                                    to = gameController.offensivePlayer,
                                    MinItems = 0,
                                    MaxItems = 1,
                                    Message = ReplaceAbilityMessage,
                                    Id = AbilityReplaceFieldPick,
                                    ValidSelections = validAbilities.ToArray()
                                };

                                actions[0] = new QueryAction() { query = fieldPickQuery };
                            }
                            else
                            {
                                actions[0] = new AttachAbilityCardAction(combatQueryAnswer.targetFighter.GetComponent<FighterCardComponent>(), combatQueryAnswer.card.GetComponent<AbilityCardComponent>(), null);
                            }
                        }
                        break;
                        case CombatQueryAnswer.Type.AttachEquipment:
                        {
                            FighterCardComponent fighterCard = combatQueryAnswer.targetFighter as FighterCardComponent;
                            FighterGameCard fighter = fighterCard.CardData as FighterGameCard;

                            if (fighterCard.GetFighterAttachedEquipmentCount() >= fighter.EquipmentSlots)
                            {
                                abilityReplaceAnswer = combatQueryAnswer;
                                List<GameObject> validEquipment = new List<GameObject>();

                                foreach (CardComponent equipmentCard in fighterCard.FighterAttachedEquipment())
                                {
                                    validEquipment.Add(equipmentCard.gameObject);
                                }

                                FieldPickQuery fieldPickQuery = new FieldPickQuery()
                                {
                                    from = this,
                                    to = gameController.offensivePlayer,
                                    MinItems = 0,
                                    MaxItems = 1,
                                    Message = ReplaceEquipmentMessage,
                                    Id = EquipmentReplaceFieldPick,
                                    ValidSelections = validEquipment.ToArray()
                                };

                                actions[0] = new QueryAction() { query = fieldPickQuery };
                            }
                            else
                            {
                                actions = new GameAction[1];
                                actions[0] = new AttachEquipmentCardAction() { fighterCard = combatQueryAnswer.targetFighter, equipmentCard = combatQueryAnswer.card.GetComponent<EquipmentCardComponent>() };
                            }
                            
                        }
                        break;
                        case CombatQueryAnswer.Type.ActivateAbility:
                        {
                            var abilityCard = combatQueryAnswer.card as AbilityCardBaseComponent;
                            int auraCost = gameController.GetEffectiveAuraCost(abilityCard.AbilityData);
                            actions = ActivateAbilityActions(gameController, abilityCard, auraCost, abilityCard.AbilityData.TributeCost,
                            gameController.offensivePlayer, PlayType.Attack);
                        }
                        break;
                        case CombatQueryAnswer.Type.PlayPowerCard:
                        {
                            actions = PlayPowerActions(gameController, combatQueryAnswer.card as PowerCardComponent, gameController.offensivePlayer,PlayType.Attack);
                        }
                        break;

                        case CombatQueryAnswer.Type.TagIn:
                        {
                            var fighter = combatQueryAnswer.card.GetComponent<CardComponent>().CardData as FighterGameCard;
                            actions = new GameAction[2];
                            actions[0] = new TagInAction() { targetFighter = combatQueryAnswer.card.GetComponent<CardComponent>() };
                            actions[1] = new TapAuraAction() { NumberOfAura = fighter.TagInCost, TargetPlayer = query.to };
                        }
                        break;

                        case CombatQueryAnswer.Type.EndTurn:
                        {
                            actions = new GameAction[1];
                            actions[0] = new EndTurnAction();

                        }
                        break;
                    }
                }
                else if (currentPhase == GamePhase.Defend)
                {
                    switch (combatQueryAnswer.type)
                    {
                        case CombatQueryAnswer.Type.PlayPowerCard:
                        {
                            PlayEndTurnButtonUpdate(gameController);
                            List<GameAction> actionList = new List<GameAction>();
                            CombatQuery combatQuery = new CombatQuery() { from = this, to = gameController.defensivePlayer, CombatType = PlayType.Defend };

                            actionList.AddRange(PlayPowerActions(gameController, combatQueryAnswer.card as PowerCardComponent, gameController.defensivePlayer, PlayType.Defend));
                            actionList.Add(new QueryAction() { query = combatQuery });
                            actions = actionList.ToArray();
                        }
                        break;
                        case CombatQueryAnswer.Type.TagIn:
                        {
                            var fighter = combatQueryAnswer.card.GetComponent<CardComponent>().CardData as FighterGameCard;
                            actions = new GameAction[2];
                            actions[0] = new TagInAction() { targetFighter = combatQueryAnswer.card.GetComponent<CardComponent>() };
                            actions[1] = new TapAuraAction() { NumberOfAura = fighter.TagInCost, TargetPlayer = query.to };
                        }
                        break;
                        case CombatQueryAnswer.Type.EndTurn:
                        {
                            actions = new GameAction[1];
                            actions[0] = new PhaseChangeAction(GamePhase.Resolve);
                        }
                        break;
                        case CombatQueryAnswer.Type.ActivateAbility:
                        {
                            var ability = combatQueryAnswer.card as AbilityCardBaseComponent;
                            int auraCost = gameController.GetEffectiveAuraCost(ability.AbilityData);

                            actions = ActivateAbilityActions(gameController, ability, auraCost, ability.AbilityData.TributeCost,
                                gameController.defensivePlayer, PlayType.Defend);
                        }
                        break;
                    }
                }
                return actions;
            }

            FieldPickAnswer fieldPickAnswer = answer as FieldPickAnswer;
            if (fieldPickAnswer != null)
            {
                if (query.Id == FigherKOFieldPick)
                {
                    actions = new GameAction[1];
                    actions[0] = new TagInAction() { targetFighter = fieldPickAnswer.Selections[0].GetComponent<CardComponent>() };
                }
                else if (query.Id == AbilityReplaceFieldPick)
                {
                    if (fieldPickAnswer.Selections.Length >= 1)
                    {
                        actions = new GameAction[1];
                        actions[0] = new AttachAbilityCardAction(abilityReplaceAnswer.targetFighter, abilityReplaceAnswer.card as AbilityCardComponent, fieldPickAnswer.Selections[0].GetComponent<AbilityCardComponent>());
                    }
                }
                else if (query.Id == EquipmentReplaceFieldPick)
                {
                    if (fieldPickAnswer.Selections.Length >= 1)
                    {
                        actions = new GameAction[2];
                        actions[0] = new DiscardAction(fieldPickAnswer.Selections[0].GetComponent<CardComponent>());
                        actions[1] = new AttachEquipmentCardAction()
                        {
                            equipmentCard = abilityReplaceAnswer.card as EquipmentCardComponent,
                            fighterCard = abilityReplaceAnswer.targetFighter
                        };
                    }
                }
                else if (query.Id == TributeCardFieldPick)
                {
                    actions = new GameAction[fieldPickAnswer.Selections.Length];

                    for (int i = 0; i < fieldPickAnswer.Selections.Length; i++)
                    {
                        actions[i] = new TributeAction(fieldPickAnswer.Selections[i].GetComponent<CardComponent>());
                    }
                }
                else if (query.Id == StartingFighterFieldPick)
                {
                    List<GameAction> actionList = new List<GameAction>();
                    var selectedFighter = fieldPickAnswer.Selections[0].GetComponent<CardComponent>();
                    var fieldPickQuery = query as FieldPickQuery;
                    bool support1Assigned = false;
                    foreach (var fighterCardObject in fieldPickQuery.ValidSelections)
                    {
                        var fighterCard = fighterCardObject.GetComponent<FighterCardComponent>();
                        if (fighterCard != selectedFighter)
                        {
                            if (!support1Assigned)
                            {
                                actionList.Add(new SetFighterAction(fighterCard, FighterPosition.Support1, query.to));
                                support1Assigned = true;
                            }
                            else
                            {
                                actionList.Add(new SetFighterAction(fighterCard, FighterPosition.Support2, query.to));
                            }
                        }
                        else
                        {
                            actionList.Add(new SetFighterAction(fighterCard, FighterPosition.Active, query.to));
                        }
                    }

                    // If the last player has picked their starting fighter then begin the game
                    if (query.to == gameController.player2)
                    {
                        int hostFirst = gameController.RandomGenerator.RandomInt(0, 2);

                        if (hostFirst == 0)
                        {
                            gameController.offensivePlayer = gameController.HostPlayer;
                            gameController.defensivePlayer = gameController.ClientPlayer;
                        }
                        else
                        {
                            gameController.offensivePlayer = gameController.ClientPlayer;
                            gameController.defensivePlayer = gameController.HostPlayer;
                        }

                        actionList.Add(new ShuffleAction() { TargetPlayer = gameController.HostPlayer });
                        actionList.Add(new ShuffleAction() { TargetPlayer = gameController.ClientPlayer });
                        actionList.Add(new DrawAction(gameController.player1, 6));
                        actionList.Add(new DrawAction(gameController.player2, 6));
                        actionList.Add(new PhaseChangeAction(GamePhase.Draw));
                    }
                    actions = actionList.ToArray();
                }

                return actions;
            }

            return null;
        }
        public GameAction[] OnGameIdle(GameController gameController)
        {
            GameAction[] actions = null;
            if (gameController.gamePhase == GameController.GamePhase.Resolve)
            {
                actions = new GameAction[1];
                actions[0] = new PhaseChangeAction(GamePhase.Attack);
            }
            else if (gameController.gamePhase == GameController.GamePhase.Attack)
            {
                PlayEndTurnButtonUpdate(gameController);
                actions = CheckForFighterKOActions(gameController, gameController.offensivePlayer);
                if (actions != null)
                {
                    return actions;
                }

                actions = CheckForFighterKOActions(gameController, gameController.defensivePlayer);
                if (actions != null)
                {
                    return actions;
                }

                CombatQuery combatQuery = new CombatQuery() { from = this, to = gameController.offensivePlayer, CombatType = PlayType.Attack };
                actions = new GameAction[1];
                actions[0] = new QueryAction() { query = combatQuery };
            }

            return actions;
        }

        private GameAction[] PlayPowerActions(GameController gameController, PowerCardComponent card, Player player, PlayType playType)
        {
            int auraCost = gameController.GetEffectiveAuraCost(card.PowerData);

            List<GameAction> actions = new List<GameAction>();
            if (auraCost > 0)
            {
                actions.Add(new TapAuraAction() { NumberOfAura = auraCost, TargetPlayer = player });
            }

            actions.Add(new PlayPowerAction() { powerCard = card, PowerCardType = playType });
            return actions.ToArray();
        }

        private GameAction[] ActivateAbilityActions(GameController gameController, AbilityCardBaseComponent card, int auraCost, int tributeCost, Player player, PlayType playType)
        {
            List<GameAction> actions = new List<GameAction>();
            if (auraCost > 0)
            {
                actions.Add(new TapAuraAction() { NumberOfAura = auraCost, TargetPlayer = player });
            }
            if (tributeCost > 0)
            {
                List<GameObject> validCards = new List<GameObject>();
                foreach (CardComponent cardInHand in gameController.CardsInHand(player))
                {
                    validCards.Add(cardInHand.gameObject);
                }

                FieldPickQuery fieldPickQuery = new FieldPickQuery()
                {
                    from = this,
                    to = player,
                    MinItems = (uint)tributeCost,
                    MaxItems = (uint)tributeCost,
                    Message = TributeCardMessage,
                    Id = TributeCardFieldPick,
                    ValidSelections = validCards.ToArray()
                };
                
                actions.Add(new QueryAction() { query = fieldPickQuery });
            }

            actions.Add(new ActivateAbilityAction() { abilityCard = card, type = playType });
            return actions.ToArray();
        }

        private GameAction[] CheckForFighterKOActions(GameController gameController, Player player)
        {
            CardComponent fighterCard = GetFighterCardIfAlive(gameController, player);
            if (fighterCard != null)
            {
                return null;
            }

            List<GameObject> validFighters = new List<GameObject>();
            for (int i = 1; i < 3; i++)
            {
                CardComponent supportFighterCard = GetFighterCardIfAlive(gameController, player, i);
                if (supportFighterCard != null)
                {
                    validFighters.Add(supportFighterCard.gameObject);
                }
            }

            GameAction[] actions = new GameAction[1];

            if (validFighters.Count == 0)
            {
                actions[0] = new DeclareWinnerAction() { Winner = gameController.GetPlayerOpponent(player) };
            }
            else
            {
                FieldPickQuery fieldPickQuery = new FieldPickQuery()
                {
                    from = this,
                    to = player,
                    MinItems = 1,
                    MaxItems = 1,
                    Message = PickNewFighterMessage,
                    Id = FigherKOFieldPick,
                    ValidSelections = validFighters.ToArray()
                };

                actions[0] = new QueryAction() { query = fieldPickQuery };
            }

            return actions;
        }

        private CardComponent GetFighterCardIfAlive(GameController gameController, Player player, int supportSlot = 0)
        {
            FighterGameCard fighter = null;
            CardComponent fighterCard = null;
            if (supportSlot == 0)
            {
                fighterCard = gameController.GetActiveFighterCard(player);
            }
            else if (supportSlot == 1 || supportSlot == 2)
            {
                fighterCard = gameController.GetSupportFighterCard(player, supportSlot);
            }

            if (fighterCard == null)
            {
                return null;
            }

            fighter = fighterCard.CardData as FighterGameCard;
            if (fighter.Hp > 0)
            {
                return fighterCard;
            }

            return null;
        }

        private void PlayEndTurnButtonUpdate(GameController gameController)
        {
            if (gameController.player1 == gameController.offensivePlayer)
            {
                if (gameController.CurrentGamePhase == GamePhase.Defend)
                {
                    gameController.SetActionButton(false, WaitForOpponentStr);
                }
                else if (gameController.CurrentGamePhase == GamePhase.Draw)
                {
                    gameController.SetActionButton(false, DrawString);
                }
            }
            else if (gameController.player1 == gameController.defensivePlayer)
            {
                if (gameController.CurrentGamePhase == GamePhase.Attack || gameController.CurrentGamePhase == GamePhase.Draw)
                {
                    gameController.SetActionButton(false, WaitForOpponentStr);
                }
            }
        }

        private GameAction CreatePickFighterQuery(GameController gameController, Player player)
        {
            var playerboard = gameController.GetPlayerGameBoard(player);

            List<GameObject> validAbilities = new List<GameObject>();
            foreach (Transform fighterCard in playerboard.ActiveFighterPosition)
            {
                validAbilities.Add(fighterCard.gameObject);
            }

            FieldPickQuery fieldPickQuery = new FieldPickQuery()
            {
                from = this,
                to = player,
                MinItems = 1,
                MaxItems = 1,
                Message = PickStartingFighterStr,
                Id = StartingFighterFieldPick,
                ValidSelections = validAbilities.ToArray()
            };

            return new QueryAction() { query = fieldPickQuery };
        }
    }
}

#endregion

#region RoundRecord
    
public class RoundRecord
{
    private GameAction[] actions = null;

    public RoundRecord(Player offensivePlayer, Player defensivePlayer, GameAction[] _actions)
    {
        OffensivePlayer = offensivePlayer;
        DefensivePlayer = defensivePlayer;
        actions = _actions;
    }
    
    public int Length
    {
        get
        {
            if(actions == null)
            {
                return 0;
            }
            return actions.Length;
        }
    }

    public Player OffensivePlayer
    {
        get;
        private set;
    }

    public Player DefensivePlayer
    {
        get;
        private set;
    }

    public ref readonly GameAction this[int i]
    {
        get
        {
            return ref actions[i];
        }
    }
}

#endregion

#region RandomModule

public class LocalRandomModule : RandomModule
{
    private System.Random rand;

    public LocalRandomModule()
    {
        rand = new System.Random();
    }

    public byte[] RandomBytes(int numOfBytes, int low, int high)
    {
        byte[] bytes = new byte[numOfBytes];

        rand.NextBytes(bytes);

        int range = high - low;
        for (int i = 0; i < bytes.Length; i++)
        {
            bytes[i] = (byte)(bytes[i] % (range) + low);
        }

        return bytes;
    }

    public int RandomInt(int low, int high)
    {
        return rand.Next(low, high);
    }

    public void SetSeed(int seed)
    {
        rand = new System.Random(seed);
    }
}

public interface RandomModule
{
    int RandomInt(int low, int high);
    byte[] RandomBytes(int numOfBytes, int low, int high);
    void SetSeed(int seed);
}

#endregion
