﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameAction
{
    public enum SetMode
    {
        Add, Remove
    }

    public CardEffect OriginEffect
    {
        get;
        set;
    }

    public virtual float ExecutionDuration
    {
        get { return 0; }
    }

    public abstract void Execute(GameController gameController);
}
