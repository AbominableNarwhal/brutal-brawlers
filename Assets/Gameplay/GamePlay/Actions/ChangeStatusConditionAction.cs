﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ChangeStatusConditionAction : GameAction
{
    public FighterCardComponent TargetFighter { get; set; }
    public StatusCondition Condition { get; set; }
    public bool AddCondition { get; set; }

    public override void Execute(GameController gameController)
    {
        if (AddCondition)
        {
            TargetFighter.AddStatusCondition(Condition);
        }
        else
        {
            TargetFighter.RemoveStatusCondition(Condition);
        }
    }
}
