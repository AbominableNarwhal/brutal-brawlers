﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCardAction : GameAction
{
    public CardComponent Card { get; }
    public Transform Destination { get; }

    public MoveCardAction(CardComponent card, Transform destination)
    {
        Card = card;
        Destination = destination;
    }

    public override void Execute(GameController gameController)
    {
        Card.transform.SetParent(Destination);
    }
}
