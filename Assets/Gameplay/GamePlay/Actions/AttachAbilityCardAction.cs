﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachAbilityCardAction : GameAction
{
    public FighterCardComponent FighterCard { get; }
    public AbilityCardComponent AbilityCard { get; }
    public AbilityCardComponent AbilityCardToReplace { get; }

    public AttachAbilityCardAction(FighterCardComponent fighterCard, AbilityCardComponent abilityCard, AbilityCardComponent abilityCardToReplace)
    {
        FighterCard = fighterCard;
        AbilityCard = abilityCard;
        AbilityCardToReplace = abilityCardToReplace;
    }

    public override void Execute(GameController gameController)
    {
        if(!FighterCard || !AbilityCard)
        {
            return;
        }

        if (AbilityCardToReplace && FighterCard.HasAbility(AbilityCardToReplace))
        {
            gameController.DiscardCard(AbilityCardToReplace);
        }

        //Put it logically into play
        FighterCard.AttachAbility(AbilityCard);
        gameController.PutAttachedCardIntoPlay(AbilityCard, FighterCard.FighterData);
    }
}
