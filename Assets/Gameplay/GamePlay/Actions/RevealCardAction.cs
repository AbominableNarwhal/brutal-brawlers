﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RevealCardAction : GameAction
{
    public CardComponent TargetCard { get; }
    /*public override float ExecutionDuration
    {
        get { return 1; }
    }*/

    public RevealCardAction(CardComponent targetCard)
    {
        TargetCard = targetCard;
    }

    public override void Execute(GameController gameController)
    {
        TargetCard.transform.SetParent(gameController.RevealArea);
    }
}
