﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FighterPosition { Active, Support1, Support2 }

public class SetFighterAction : GameAction
{
    public FighterPosition SetPosition { get; }
    public FighterCardComponent FighterCard { get; }
    public Player PlayerSide { get; }

    public SetFighterAction(FighterCardComponent fighterCard, FighterPosition position, Player player)
    {
        SetPosition = position;
        FighterCard = fighterCard;
        PlayerSide = player;
    }
    public override void Execute(GameController gameController)
    {
        gameController.PutFighterCardIntoPlay(FighterCard, SetPosition, PlayerSide);
    }
}
