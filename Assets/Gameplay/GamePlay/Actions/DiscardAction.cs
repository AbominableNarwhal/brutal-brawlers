﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscardAction : GameAction
{
    public CardComponent Card { get; }

    public DiscardAction(CardComponent card)
    {
        Card = card;
    }

    public override void Execute(GameController gameController)
    {
        gameController.DiscardCard(Card);

    }
}
