﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetOnHitTriggerAction : GameAction
{
    public SetMode Mode { get; }
    public AbilityCardBaseComponent TargetAbility { get; } = null;
    public IOnHitTriggerAddOn OnHitTrigger { get; } = null;

    public SetOnHitTriggerAction(AbilityCardBaseComponent targetAbility, IOnHitTriggerAddOn onHitTrigger, SetMode mode)
    {
        TargetAbility = targetAbility;
        OnHitTrigger = onHitTrigger;
        Mode = mode;
    }

    public override void Execute(GameController gameController)
    {
        if (Mode == SetMode.Add)
        {
            TargetAbility.AddOnHitTrigger(OnHitTrigger);
        }
        else if (Mode == SetMode.Remove)
        {
            TargetAbility.RemoveOnHitTrigger(OnHitTrigger);
        }
    }
}
