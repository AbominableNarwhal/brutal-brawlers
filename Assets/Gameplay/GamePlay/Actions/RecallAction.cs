using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecallAction : GameAction
{
    public CardComponent Card { get; }
    public Player TargetPlayer { get; }

    public RecallAction(CardComponent card, Player player)
    {
        Card = card;
        TargetPlayer = player;
    }

    public override void Execute(GameController gameController)
    {
        Card.transform.SetParent(gameController.GetPlayerGameBoard(TargetPlayer).Hand);
    }
}
