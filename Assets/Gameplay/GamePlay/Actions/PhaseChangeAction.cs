﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseChangeAction : GameAction
{
    public GameController.GamePhase nextPhase;
    public PhaseChangeAction(GameController.GamePhase _nextPhase)
    {
        nextPhase = _nextPhase;
    }
    public override void Execute(GameController gameController)
    {
        gameController.CurrentGamePhase = nextPhase;
    }
}
