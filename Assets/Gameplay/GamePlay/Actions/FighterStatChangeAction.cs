﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterStatChangeAction : GameAction
{
    public FighterCardComponent TargetFighter { get; }
    public int Magic { get; }
    public int Tech { get; }
    public int Strength { get; }
    public int TagInCost { get; }

    public FighterStatChangeAction(FighterCardComponent targetFighter, int magic, int tech, int strength, int tagInCost)
    {
        TargetFighter = targetFighter;
        Magic = magic;
        Tech = tech;
        Strength = strength;
        TagInCost = tagInCost;
    }

    public override void Execute(GameController gameController)
    {
        if(TargetFighter == null)
        {
            Debug.LogError("No target fighter set!");
            return;
        }

        TargetFighter.Magic += Magic;
        TargetFighter.Tech += Tech;
        TargetFighter.Strength += Strength;
        TargetFighter.TagInCost += TagInCost;
    }
}
