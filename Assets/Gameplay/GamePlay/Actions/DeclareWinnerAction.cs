﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class DeclareWinnerAction : GameAction
{
    public Player Winner { get; set; }
    public override void Execute(GameController gameController)
    {
        gameController.EndGame(gameController.player1 == Winner);
    }
}
