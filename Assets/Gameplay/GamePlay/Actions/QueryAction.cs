﻿using System;

public class QueryAction : GameAction
{
    [Obsolete("This member is deprecated use Query instead")]
    public PlayerQuery query;
    public PlayerQuery Query
    {
        get { return query; }
        set { query = value; }
    }

    [Obsolete("This constructor is deprecated use QueryAction(PlayerQuery _query) instead")]
    public QueryAction()
    { }

    public QueryAction(PlayerQuery _query)
    {
        query = _query;
    }

    public override void Execute(GameController gameController)
    {
        gameController.SubmitQuery(query);
    }
}
