﻿using UnityEngine;
using UnityEditor;

public class EnableAttachedEffectAction : GameAction
{
    public AbilityCardBaseComponent CardObject { get; set; }
    public bool SetEnabled { get; set; }

    public override void Execute(GameController gameController)
    {
        AttachedEffect abilityEffect = gameController.FindCardEffect(CardObject.AbilityData) as AttachedEffect;
        if (abilityEffect == null)
        {
            return;
        }

        CardObject.AbilityData.Enabled = SetEnabled;
        if (SetEnabled)
        {
            gameController.AddActions(abilityEffect.Enabled(gameController), abilityEffect);
        }
        else
        {
            gameController.InterruptEffect(CardObject.CardData);
            gameController.AddActions(abilityEffect.Disabled(gameController), abilityEffect);
        }
    }
}