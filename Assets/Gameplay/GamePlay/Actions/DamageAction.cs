﻿using System;

public class DamageAction : GameAction
{
    public int damageAmount;
    public FighterCardComponent targetFighterCard = null;
    public GameAction[] OnHitActions { get; set; } = null;

    public DamageResult ResultDamage { get; private set;}

    public bool IsAttackDamage
    {
        get;
        set;
    }

    public override float ExecutionDuration
    {
        get { return 0.75f; }
    }

    [Obsolete("This constructor is deprecated use DamageAction(FighterCardComponent _targetFighterCard, int _damageAmount, bool isAttackDamge) instead")]
    public DamageAction()
    {
        IsAttackDamage = false;
    }

    public DamageAction(FighterCardComponent _targetFighterCard, int _damageAmount, bool isAttackDamge = false)
    {
        IsAttackDamage = isAttackDamge;
        damageAmount = _damageAmount;
        targetFighterCard = _targetFighterCard;
    }

    public FighterCardComponent GetTarget(GameController gameController)
    {
        if (IsAttackDamage)
        {
            targetFighterCard = gameController.GetPlayerGameBoard(gameController.defensivePlayer).ActiveFighterCard;
        }

        return targetFighterCard;
    }

    public override void Execute(GameController gameController)
    {
        GetTarget(gameController);
        if (damageAmount > 0)
        {
            FighterGameCard.DamageReport damageReport = targetFighterCard.FighterData.ReportDamage(damageAmount);
            targetFighterCard.DealDamage(damageAmount);
            ResultDamage = new DamageResult(damageReport, targetFighterCard);

            gameController.DamageAnimation(targetFighterCard.transform, damageAmount); //TODO remove
            gameController.AddActions(OnHitActions, OriginEffect);
        }
    }

    public struct DamageResult
    {
        public int BlockersBroken { get; set; }
        public int HpLost { get; set; }
        public FighterCardComponent DamagedFighter { get; set; }

        public DamageResult(FighterGameCard.DamageReport damageReport, FighterCardComponent _damagedFighter)
        {
            BlockersBroken = damageReport.BlockersBroken;
            HpLost = damageReport.DamageDealt;
            DamagedFighter = _damagedFighter;
        }
    }
}
