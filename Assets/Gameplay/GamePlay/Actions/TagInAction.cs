﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagInAction : GameAction
{
    public CardComponent targetFighter;
    public override void Execute(GameController gameController)
    {
        if(targetFighter == null || !targetFighter.transform.parent.name.Contains("Support Fighter"))
        {
            return;
        }
        GameObject playerBoard = gameController.GetPlayerGameObject(targetFighter.CardData.Owner);
        Transform activeFighterZone = playerBoard.transform.Find("Active Fighter"); 
        CardComponent activeFighter = activeFighterZone.GetChild(0).GetComponent<CardComponent>();
        Transform supportFighterZone = targetFighter.transform.parent;

        activeFighter.transform.SetParent(supportFighterZone);
        targetFighter.transform.SetParent(activeFighterZone);

    }
}
