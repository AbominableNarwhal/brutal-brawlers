using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TributeAction : GameAction
{
    public CardComponent Card { get; }

    public TributeAction(CardComponent card)
    {
        Card = card;
    }

    public override void Execute(GameController gameController)
    {
        gameController.DiscardCard(Card);

    }
}
