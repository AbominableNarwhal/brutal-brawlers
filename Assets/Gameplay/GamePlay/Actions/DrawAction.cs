﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawAction : GameAction
{
    private PlayerGameBoardAccessor playerBoard;

    public Player Player { get; }
    public int NumOfCards { get; } = 0;

    public DrawResult ResultDraw
    { get; private set; }

    public CardComponent[] Cards { get; } = null;

    public DrawAction(Player _player, int _numOfCards = 1)
    {
        Player = _player;
        NumOfCards = _numOfCards;
    }

    public DrawAction(Player _player, CardComponent[] _cards)
    {
        Player = _player;
        Cards = _cards;
    }

    public override void Execute(GameController gameController)
    {
        if(Player == null)
        {
            return;
        }

        playerBoard = gameController.GetPlayerGameBoard(Player);
        
        if (Cards == null)
        {
            DrawCards(NumOfCards);
        }
        else
        {
            DrawCards(Cards);
        }
        
    }

    /// <summary>
    /// Draws Cards from the deck
    /// </summary>
    /// <param name="numOfCards">The number of cards to draw from the deck</param>
    private void DrawCards(int numOfCards)
    {
        Transform deck = playerBoard.Deck;
        if (deck.childCount < numOfCards)
        {
            numOfCards = deck.childCount;
        }

        var cards = new Transform[numOfCards];

        for (int i = 0; i < numOfCards; i++)
        {

            Transform topCard = deck.GetChild(i);
            cards[i] = topCard;
        }

        DrawCards(cards);
    }

    private void DrawCards(CardComponent[] cards)
    {
        Transform deck = playerBoard.Deck;
        var cardTranforms = new Transform[cards.Length];

        for (int i = 0; i < cards.Length; i++)
        {
            cardTranforms[i] = cards[i].transform;
        }

        DrawCards(cardTranforms);
    }

    /// <summary>
    /// Transfers cards to the player's hand
    /// </summary>
    /// <param name="cards">The cards to put in the player's hand</param>
    private void DrawCards(Transform[] cards)
    {
        var cardsDrawn = new List<CardComponent>();
        foreach (var card in cards)
        {
            card.SetParent(playerBoard.Hand);
            cardsDrawn.Add(card.GetComponent<CardComponent>());
        }
        DrawResult drawResult = new DrawResult { DrawnCards = cardsDrawn.ToArray() };

        ResultDraw = drawResult;
    }

    public struct DrawResult
    {
        public CardComponent[] DrawnCards { get; set; }
    }
}
