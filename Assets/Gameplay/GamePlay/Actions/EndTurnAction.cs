﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTurnAction : GameAction
{
    public override void Execute(GameController gameController)
    {
        gameController.EndTurn();
    }
}
