﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMarkerAction : GameAction
{
    public FighterCardComponent FighterCard { get; }
    public Marker GameMarker { get; }
    public SetMode SetAction { get; }

    public SetMarkerAction(FighterCardComponent fighterCard, Marker gameMarker, SetMode setAction)
    {
        FighterCard = fighterCard;
        GameMarker = gameMarker;
        SetAction = setAction;
    }

    public override void Execute(GameController gameController)
    {
        if (SetAction == SetMode.Add)
        {
            FighterCard.AddMarker(GameMarker);
        }
        else if(SetAction == SetMode.Remove)
        {
            FighterCard.RemoveMarker(GameMarker);
        }
    }
}
