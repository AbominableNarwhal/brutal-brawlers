﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBlockerAction : GameAction
{
    public FighterCardComponent TargetFighter { get; }
    public int BlockerStrength { get; } = 0;
    public SetMode Mode { get; }

    public SetBlockerAction(int blockerStrength, FighterCardComponent fighterCardComponent)
    {
        BlockerStrength = blockerStrength;
        TargetFighter = fighterCardComponent;
        Mode = SetMode.Add;
    }

    public SetBlockerAction(FighterCardComponent fighterCardComponent)
    {
        TargetFighter = fighterCardComponent;
        Mode = SetMode.Remove;
    }

    public override void Execute(GameController gameController)
    {
        if (Mode == SetMode.Add)
        {
            TargetFighter.AddBlocker(BlockerStrength);
        }
        else if (Mode == SetMode.Remove)
        {
            TargetFighter.RemoveBlocker();
        }
    }
}
