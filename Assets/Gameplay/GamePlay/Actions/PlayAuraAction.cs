using UnityEngine;

public class PlayAuraAction : GameAction
{
    public CardComponent auraCard;
    public override void Execute(GameController gameController)
    {
        if(auraCard == null || auraCard.CardData.CardType != Card.Type.AuraCard)
        {
            return;
        }

        auraCard.transform.SetParent(gameController.GetPlayerGameObject(auraCard.CardData.Owner).transform.Find("Aura Stash"));
    }
}