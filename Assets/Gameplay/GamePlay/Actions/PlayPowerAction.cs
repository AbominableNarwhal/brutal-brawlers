﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPowerAction : GameAction
{
    public PowerCardComponent powerCard;
    public PlayType PowerCardType
    {
        get;
        set;
    }
    public override void Execute(GameController gameController)
    {
        if (powerCard == null)
        {
            return;
        }

        powerCard.transform.SetParent(gameController.GetPlayerGameObject(powerCard.PowerData.Owner).transform.Find("Play Zone"));
        gameController.PutCardIntoPlay(powerCard);

        if (PowerCardType == PlayType.Attack)
        {
            gameController.ActivatePowerAttack(powerCard.PowerData);
        }
        else if (PowerCardType == PlayType.Defend)
        {
            gameController.ActivatePowerDefend(powerCard.PowerData);
        }
    }
}
