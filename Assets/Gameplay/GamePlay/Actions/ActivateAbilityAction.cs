﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayType { Attack, Defend }

public class ActivateAbilityAction : GameAction
{
    public AbilityCardBaseComponent abilityCard;
    public PlayType type;
    public override void Execute(GameController gameController)
    {
        if (abilityCard == null)
        {
            return;
        }

        AbilityGameCard ability = abilityCard.AbilityData;
        if (ability == null)
        {
            return;
        }

        if(type == PlayType.Attack)
        {
            gameController.ActivateAbilityAttack(ability);
        }
        else if(type == PlayType.Defend)
        {
            gameController.ActivateAbilityDefend(ability);
        }
    }
}
