﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ShuffleAction : GameAction
{
    public Player TargetPlayer { get; set; }

    [Obsolete("This constructor is deprecated use ShuffleAction(Player targetPlayer) instead")]
    public ShuffleAction()
    { }

    public ShuffleAction(Player targetPlayer)
    {
        TargetPlayer = targetPlayer;
    }

    public override void Execute(GameController gameController)
    {
        var playerDeck = gameController.GetPlayerGameBoard(TargetPlayer).Deck;

        // Here we are using the first half of the array to pick which card we will move and the
        // second half of the array to pick where it will go in the deck
        var randBytes = gameController.RandomGenerator.RandomBytes(playerDeck.childCount * 2, 0, playerDeck.childCount);

        for (int i = 0; i < randBytes.Length/2; i++)
        {
            playerDeck.GetChild(randBytes[i]).SetSiblingIndex(randBytes[i + playerDeck.childCount]);
        }
    }
}
