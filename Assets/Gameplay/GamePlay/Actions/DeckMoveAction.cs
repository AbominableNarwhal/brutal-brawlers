﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckMoveAction : GameAction
{
    public CardComponent Card { get; }
    public int Position { get; }

    public DeckMoveAction(CardComponent card, int position)
    {
        Card = card;
        Position = position;
    }

    public override void Execute(GameController gameController)
    {

        Card.transform.SetSiblingIndex(Position);
    }
}
