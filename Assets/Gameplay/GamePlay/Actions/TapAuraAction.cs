﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class TapAuraAction : GameAction
{
    public int NumberOfAura { get; set; }
    public Player TargetPlayer { get; set; }

    public override void Execute(GameController gameController)
    {
        bool tapAura = NumberOfAura > 0;
        int AbsNumberOfAura = Math.Abs(NumberOfAura);
        gameController.TapPlayerAura(TargetPlayer, AbsNumberOfAura, tapAura);
    }
}
