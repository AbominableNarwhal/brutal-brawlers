using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseDamageAction : GameAction
{
    public DamageAction TargetAction
    { get; }
    public int IncreaseAmount
    { get; }

    public IncreaseDamageAction(int increaseAmount, DamageAction targetAction = null)
    {
        TargetAction = targetAction;
        IncreaseAmount = increaseAmount;
    }

    public override void Execute(GameController gameController)
    {
        if (TargetAction == null)
        {
            gameController.ModifyIncomingDamage(IncreaseAmount);
        }
        else
        {
            TargetAction.damageAmount += IncreaseAmount;
        }
    }
}
