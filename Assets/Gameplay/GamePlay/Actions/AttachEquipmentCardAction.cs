public class AttachEquipmentCardAction : GameAction
{
    public FighterCardComponent fighterCard;
    public EquipmentCardComponent equipmentCard;
    public override void Execute(GameController gameController)
    {
        if(!fighterCard || ! equipmentCard)
        {
            return;
        }

        fighterCard.AttachEquipment(equipmentCard);
    }
}