﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAbilityStatsChangeAction : GameAction
{
    public AbilityCardBaseComponent AbilityCard { get; }
    public AbilityGameCard.Status StatusChange { get; }
    public SetMode SetAction { get; }

    public SetAbilityStatsChangeAction(AbilityCardBaseComponent abilityCard, AbilityGameCard.Status statusChange, SetMode setAction)
    {
        AbilityCard = abilityCard;
        StatusChange = statusChange;
        SetAction = setAction;
    }

    public override void Execute(GameController gameController)
    {
        if (SetAction == SetMode.Add)
        {
            AbilityCard.AddStatusChange(StatusChange);
        }
        else if (SetAction == SetMode.Remove)
        {
            AbilityCard.RemoveStatusChange(StatusChange);
        }
    }
}
