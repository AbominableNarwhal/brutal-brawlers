public class NegateAction : GameAction
{
    public GameAction ActionToNegate
    {
        get;
        set;
    }
    public override void Execute(GameController gameController)
    {
        if(ActionToNegate == null)
        {
            return;
        }

        gameController.RemoveAction(ActionToNegate);
    }
}