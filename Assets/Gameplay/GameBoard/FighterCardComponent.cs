﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FighterCardComponent : CardComponent
{
    public static readonly string NativeAbilityZoneName = "Native Ability Zone";
    public static readonly string AttachedAbilityZoneName = "Attached Abilities Zone";
    public static readonly string AttachedEquipmentZoneName = "Attached Equipment Zone";

    [SerializeField]
    private Texture2D cardFrame = null;
    [SerializeField]
    private FighterAbilityComponent abilityPrefab = null;
    [SerializeField]
    private Texture2D bleedIcon = null;
    [SerializeField]
    private Texture2D dazedIcon = null;
    [SerializeField]
    private Texture2D electrifiedIcon = null;
    [SerializeField]
    private MarkerCardLayout_new markerCardLayout = null;
    [SerializeField]
    private BlockerLayout_new blockerLayout = null;
    [SerializeField]
    private GameObject statusPrefab = null;

    private TextMeshPro hpText = null;
    private TextMeshPro magicText = null;
    private TextMeshPro techText = null;
    private TextMeshPro strengthText = null;
    private TextMeshPro abilitySlots = null;
    private TextMeshPro equipmentSlots = null;
    private Transform markerZone = null;
    private Transform blockerZone = null;
    private Transform statusZone = null;
    private Transform nativeAbilityZone = null;
    private Transform attachedAbilitiesZone = null;
    private Transform attachedEquipmentZone = null;

    private FighterGameCard fighterData = null;
    private List<AbilityCardBaseComponent> abilities = null;
    private List<EquipmentCardComponent> equipment = null;
    private Stack<BlockerLayout_new> blockers = null;

    public FighterGameCard FighterData
    {
        get { return fighterData; }
        private set { fighterData = value; }
    }

    public int Hp
    {
        get { return FighterData.Hp; }

        set
        {
            FighterData.Hp = value;
            hpText.text = "HP " + FighterData.Hp.ToString();
        }
    }

    public bool IsKOed
    {
        get { return FighterData.Hp <= 0; }
    }

    public int Magic
    {
        get { return FighterData.Magic; }
        set
        {
            FighterData.Magic = value;
            magicText.text = "M\n" + FighterData.Magic.ToString();
        }
    }

    public int Tech
    {
        get { return FighterData.Tech; }
        set
        {
            FighterData.Tech = value;
            techText.text = "T\n" + FighterData.Tech.ToString();
        }
    }

    public int Strength
    {
        get { return FighterData.Strength; }
        set
        {
            FighterData.Strength = value;
            strengthText.text = "S\n" + FighterData.Strength.ToString();
        }
    }

    public int TagInCost
    {
        get { return FighterData.TagInCost; }
        set { FighterData.TagInCost = value; }
    }

    public ReadOnlyCollection<AbilityCardBaseComponent> Abilities { get; private set; }

    public FighterAbilityComponent AbilityPrefab
    {
        get { return abilityPrefab; }
    }

    public ReadOnlyCollection<int> Blockers
    {
        get { return fighterData.Blockers; }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    public FighterCardComponent()
    {
        abilities = new List<AbilityCardBaseComponent>();
        equipment = new List<EquipmentCardComponent>();
        blockers = new Stack<BlockerLayout_new>();
        Abilities = abilities.AsReadOnly();
    }

    public static FighterCardComponent CreateFighterCard(Fighter fighterData, Transform parentTransform = null)
    {
        GameObject fighterObject = Instantiate(fighterData.cardPrefab, parentTransform, false);
        var cardComponent = fighterObject.GetComponent<FighterCardComponent>();
        var fighterGameCard = new FighterGameCard(fighterData);
        cardComponent.SetCardData(fighterGameCard);
        return cardComponent;
    }

    public void SetCardData(FighterGameCard _gameCard)
    {
        base.SetCardData(_gameCard);
        FighterData = _gameCard;

        AttachElements();
        magicText = FindCardElement<TextMeshPro>("Magic", Body);
        techText = FindCardElement<TextMeshPro>("Tech", Body);
        strengthText = FindCardElement<TextMeshPro>("Strength", Body);
        hpText = FindCardElement<TextMeshPro>("HP Label", Body);
        abilitySlots = FindCardElement<TextMeshPro>("Ability Slots", Body);
        equipmentSlots = FindCardElement<TextMeshPro>("Equipment Slots", Body);
        markerZone = FindCardElement("Marker Zone", Body);
        blockerZone = FindCardElement("Blocker Zone", Body);
        statusZone = FindCardElement("Status Zone", Body);
        nativeAbilityZone = FindCardElement(NativeAbilityZoneName, Body);
        attachedAbilitiesZone = FindCardElement(AttachedAbilityZoneName, Body);
        attachedEquipmentZone = FindCardElement(AttachedEquipmentZoneName, Body);

        Hp = FighterData.Hp;
        Magic = FighterData.Magic;
        Tech = FighterData.Tech;
        Strength = FighterData.Strength;
        abilitySlots.text = fighterData.AbilitySlots.ToString();
        equipmentSlots.text = fighterData.EquipmentSlots.ToString();
        cardMaterial.SetTexture("_MainTex", cardFrame);
        cardMaterial.SetInt("_ShowMagic", 1);
        cardMaterial.SetInt("_ShowTech", 1);
        cardMaterial.SetInt("_ShowStrength", 1);

        float abilityHeight = 0;
        foreach (AbilityGameCard ability in FighterData.Abilities)
        {
            FighterAbilityComponent abilityObject = Instantiate(AbilityPrefab, nativeAbilityZone);
            abilityObject.transform.localPosition += new Vector3(0, abilityHeight, 0);
            abilityObject.SetCardData(ability);
            abilities.Add(abilityObject);

            float displayHieght = abilityObject.DisplayHieght;
            abilityHeight += -displayHieght;
        }
    }
    public void AddStatusCondition(StatusCondition condition)
    {
        FighterData.AddStatusCondition(condition);

        bool duplicatefound = false;
        for (int i = 0; i < statusZone.childCount; i++)
        {
            var iconImage = statusZone.GetChild(i).GetComponent<TextureImage>();
            if (IconToStatus(iconImage.Image) == condition)
            {
                AddStatusIconToDisplay(iconImage.Image, i);
                duplicatefound = true;
                i++;
            }
        }

        if (!duplicatefound)
        {
            AddStatusIconToDisplay(StatusToIcon(condition));
        }
    }

    public void RemoveStatusCondition(StatusCondition condition)
    {
        FighterData.RemoveStatusCondition(condition);

        for (int i = 0; i < statusZone.childCount; i++)
        {
            Transform iconItem = statusZone.GetChild(i);
            var iconImage = iconItem.GetComponent<TextureImage>();
            if (IconToStatus(iconImage.Image) == condition)
            {
                Destroy(iconItem.gameObject);
                return;
            }
        }
    }

    public void AddMarker(Marker marker)
    {
        fighterData.AddMarker(marker);
        var markerIconObject = markerZone.Find(marker.name);

        MarkerCardLayout_new markerLayout;
        if (!markerIconObject)
        {
            markerLayout = Instantiate(markerCardLayout, markerZone);
            markerLayout.name = marker.name;
            markerLayout.MarkIcon = marker.Icon.texture;
        }
        else
        {
            markerLayout = markerIconObject.GetComponent<MarkerCardLayout_new>();
        }

        markerLayout.SetMarkerCount(fighterData.MarkerCount(marker));
    }

    public void RemoveMarker(Marker marker)
    {
        fighterData.RemoveMarker(marker);
        var markerIconObject = markerZone.Find(marker.name);

        if (markerIconObject)
        {
            if (fighterData.MarkerCount(marker) <= 0)
            {
                Destroy(markerIconObject.gameObject);
            }
            else
            {
                var markerLayout = markerIconObject.GetComponent<MarkerCardLayout_new>();
                markerLayout.SetMarkerCount(fighterData.MarkerCount(marker));
            }
        }
    }

    public int MarkerCount(Marker marker)
    {
        return FighterData.MarkerCount(marker);
    }

    public int MarkerCount()
    {
        return FighterData.MarkerCount();
    }

    public void AddBlocker(int blocker)
    {
        fighterData.AddBlocker(blocker);
        BlockerLayout_new blockerItem = Instantiate(blockerLayout, blockerZone);
        blockerItem.BlockerStrength = blocker;
        blockers.Push(blockerItem);
    }

    public void RemoveBlocker()
    {
        fighterData.RemoveBlocker();
        RemoveBlockers(1);
    }

    public void DealDamage(int damage)
    {
        FighterGameCard.DamageReport damageReport = fighterData.DealDamage(damage);
        Hp = fighterData.Hp;
        RemoveBlockers(damageReport.BlockersBroken);
    }

    private void RemoveBlockers(int blockersRemoved)
    {
        if (blockerZone != null)
        {
            for (int i = 0; i < blockersRemoved; i++)
            {
                BlockerLayout_new blocker = blockers.Pop();
                Destroy(blocker.gameObject);
            }
        }
    }

    public void AttachAbility(AbilityCardComponent abilityCard)
    {
        abilities.Add(abilityCard);
        fighterData.AttachAbility(abilityCard.AbilityData);
        abilityCard.transform.SetParent(attachedAbilitiesZone, true);
    }

    public void DetachAbility(AbilityCardComponent abilityCard)
    {
       abilities.Remove(abilityCard);
        fighterData.DetachAbility(abilityCard.AbilityData);
    }

    public void AttachEquipment(EquipmentCardComponent equipmentCard)
    {
        equipment.Add(equipmentCard);
        fighterData.AttachEquipment(equipmentCard.EquipmentData);
        equipmentCard.transform.SetParent(attachedEquipmentZone);
    }

    public void DetachEquipment(EquipmentCardComponent equipmentCard)
    {
        equipment.Remove(equipmentCard);
        fighterData.DetachEquipment(equipmentCard.EquipmentData);
    }

    public bool HasAbility(AbilityCardBaseComponent abilityCard)
    {
        return abilities.Contains(abilityCard);
    }

    public bool HasEquipment(EquipmentCardComponent equipmentCard)
    {
        return equipment.Contains(equipmentCard);
    }

    private void UpdateDisplay()
    {
        hpText.text = Hp.ToString();
        magicText.text = "M\n" + Magic.ToString();
        techText.text = "T\n" + Tech.ToString();
        strengthText.text = "S\n" + Strength.ToString();
    }

    private StatusCondition IconToStatus(Texture2D icon)
    {
        if (icon == bleedIcon)
        {
            return StatusCondition.Bleed;
        }
        else if (icon == dazedIcon)
        {
            return StatusCondition.Dazed;
        }
        else if (icon == electrifiedIcon)
        {
            return StatusCondition.Electrified;
        }

        return StatusCondition.None;
    }

    private Texture2D StatusToIcon(StatusCondition statusCondition)
    {
        switch (statusCondition)
        {
            case StatusCondition.Bleed:
                return bleedIcon;
            case StatusCondition.Dazed:
                return dazedIcon;
            case StatusCondition.Electrified:
                return electrifiedIcon;
            default:
                return null;
        }
    }

    private void AddStatusIconToDisplay(Texture2D icon, int index = -1)
    {
        var statusIcon = Instantiate(statusPrefab, statusZone).GetComponent<TextureImage>();
        statusIcon.Image = icon;

        if (index > -1)
        {
            statusIcon.transform.SetSiblingIndex(index);
        }
    }


    public IEnumerable<CardComponent> FighterAttachedAbilities()
    {
        foreach (Transform child in attachedAbilitiesZone)
        {
            yield return child.GetComponent<CardComponent>();
        }
    }

    public IEnumerable<CardComponent> FighterAttachedEquipment()
    {
        foreach (Transform child in attachedEquipmentZone)
        {
            yield return child.GetComponent<CardComponent>();
        }
    }

    public int GetFighterAttachedAbilitiesCount()
    {
        return attachedAbilitiesZone.childCount;
    }

    public int GetFighterAttachedEquipmentCount()
    {
        return attachedEquipmentZone.childCount;
    }
}
