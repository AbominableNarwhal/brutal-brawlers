﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FighterAbilityComponent : AbilityCardBaseComponent
{
    private TextMeshPro description;
    private Transform quadBody;
    private BoxCollider boxCollider;
    private RectTransform rectBody;
    public RectTransform BoundingRect
    {
        get
        {
            return rectBody;
        }
    }

    public float DisplayHieght
    {
        get { return BoundingRect.rect.height; }
    }

    public void SetCardData(AbilityGameCard _gameCard)
    {
        base.SetCardData(_gameCard);
        abilityData = _gameCard;
        rectBody = transform.Find("Body").GetComponent<RectTransform>();

        quadBody = FindCardElement("Quad Body", rectBody);
        boxCollider = GetComponent<BoxCollider>();

        cardName = FindCardElement<TextMeshPro>("Ability Name", rectBody);
        description = FindCardElement<TextMeshPro>("Ability Description", rectBody);

        cardName.text = abilityData.CardName;
        description.text = abilityData.FullDescription;

        var infoText = FindCardElement<TextMeshPro>("Ability Info", cardName.transform);
        infoText.text = Ability.StyleToInlineText(abilityData.Style);
        if (abilityData.AuraCost > 0)
        {
            infoText.text += Ability.InlineAura + "x" + abilityData.AuraCost.ToString();
        }

        if (abilityData.TributeCost > 0)
        {
            infoText.text += Ability.InlineTribute + "x" + abilityData.TributeCost.ToString();
        }

        //Need to trigger two passes here in order to force the child layout elements to be fully updated
        LayoutRebuilder.ForceRebuildLayoutImmediate(BoundingRect);
        LayoutRebuilder.ForceRebuildLayoutImmediate(BoundingRect);

        quadBody.localScale = new Vector3(rectBody.rect.width, rectBody.rect.height);
        quadBody.localPosition = new Vector3(rectBody.rect.center.x, rectBody.rect.center.y);

        boxCollider.center = new Vector3(rectBody.rect.center.x, rectBody.rect.center.y);
        boxCollider.size = new Vector3(rectBody.rect.width, rectBody.rect.height, 0.01f);

        GetComponent<GameBoardGraphic>().NoColor = new Color(0, 0, 0, 0);
    }
}
