﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EquipmentCardComponent : CardComponent
{
    [SerializeField]
    private Texture2D cardFrame = null;

    private EquipmentGameCard equipmentData;

    public EquipmentGameCard EquipmentData
    {
        get { return equipmentData; }
        private set { equipmentData = value; }
    }

    public static EquipmentCardComponent CreateEquipmentCard(Equipment eqipmentData, Transform parentTransform = null)
    {
        GameObject EquipmentObject = Instantiate(eqipmentData.cardPrefab, parentTransform, false);
        var cardComponent = EquipmentObject.GetComponent<EquipmentCardComponent>();
        var EquipmentGameCard = new EquipmentGameCard(eqipmentData);
        cardComponent.SetCardData(EquipmentGameCard);
        return cardComponent;
    }

    public void SetCardData(EquipmentGameCard _gameCard)
    {
        base.SetCardData(_gameCard);
        EquipmentData = _gameCard;

        AttachElements();

        FindCardElement<TextMeshPro>("Type").text = EquipmentData.FightStyle.ToString();
        FindCardElement<TextMeshPro>("Description").text = EquipmentData.Description;

        cardMaterial.SetTexture("_MainTex", cardFrame);
    }
}
