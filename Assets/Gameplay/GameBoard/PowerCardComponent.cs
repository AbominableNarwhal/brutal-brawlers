﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PowerCardComponent : CardComponent
{
    [SerializeField]
    private Texture2D cardFrame = null;
    [SerializeField]
    private Texture2D aruaCostIcon = null;
    /*[SerializeField]
    private Texture2D tributeCostIcon = null;*/
    [SerializeField]
    private GameObject imagePrefab = null;

    private Transform costZone = null;
    private TextMeshPro description = null;
    private PowerGameCard powerData = null;

    public PowerGameCard PowerData
    {
        get { return powerData; }
        private set { powerData = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public static PowerCardComponent CreatePowerCard(Power powerData, Transform parentTransform = null)
    {
        GameObject powerObject = Instantiate(powerData.cardPrefab, parentTransform, false);
        var cardComponent = powerObject.GetComponent<PowerCardComponent>();
        var powerGameCard = new PowerGameCard(powerData);
        cardComponent.SetCardData(powerGameCard);
        return cardComponent;
    }

    public void SetCardData(PowerGameCard _gameCard)
    {
        base.SetCardData(_gameCard);
        PowerData = _gameCard;

        AttachElements();

        costZone = FindCardElement("Cost Zone");
        description = FindCardElement<TextMeshPro>("Description");

        description.text = PowerData.Description;

        cardMaterial.SetTexture("_MainTex", cardFrame);
        UpdateAuraAndTributeCost();
    }

    public void UpdateAuraAndTributeCost()
    {
        var textureImage = imagePrefab.GetComponent<TextureImage>();

        if (PowerData.AuraCost > 0)
        {
            TextureImage auraIcon = Instantiate(textureImage, costZone);
            auraIcon.Image = aruaCostIcon;

            PopulateParent(auraIcon.gameObject, costZone, PowerData.AuraCost - 1);
        }
        /*imagePrfab.Image = tributeCostIcon;
        PopulateParent(imagePrfab.gameObject, costZone, PowerData.);*/
    }

}
