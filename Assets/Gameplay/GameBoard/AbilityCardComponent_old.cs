﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AbilityCardComponent_old : AbilityCardBaseComponent_old
{
    [SerializeField]
    private GameObject auraIcon = null;
    [SerializeField]
    private GameObject tributeIcon = null;
    private Transform auraCostGroup = null;
    private TextMeshProUGUI magicText = null;
    private TextMeshProUGUI techText = null;
    private TextMeshProUGUI strengthText = null;

    public TextMeshProUGUI styleLable;
    public TextMeshProUGUI typeLable;
    public TextMeshProUGUI description;

    public TextMeshProUGUI MagicText
    {
        get
        {
            if (magicText == null)
            {
                magicText = BodyTransform.Find("Magic Text").GetComponent<TextMeshProUGUI>();
            }
            return magicText;
        }
    }

    public TextMeshProUGUI TechText
    {
        get
        {
            if (techText == null)
            {
                techText = BodyTransform.Find("Tech Text").GetComponent<TextMeshProUGUI>();
            }
            return techText;
        }
    }

    public TextMeshProUGUI StrengthText
    {
        get
        {
            if (strengthText == null)
            {
                strengthText = BodyTransform.Find("Strength Text").GetComponent<TextMeshProUGUI>();
            }
            return strengthText;
        }
    }

    public Transform AuraCostGroup
    {
        get
        {
            if (auraCostGroup == null)
            {
                auraCostGroup = BodyTransform.Find("Aura Cost");
            }
            return auraCostGroup;
        }
    }

    public override AbilityGameCard AbilityData
    {
        get { return abilityData; }
    }

    public void SetCardData(AbilityGameCard _gameCard)
    {
        base.SetCardData(_gameCard);
        abilityData = _gameCard;
        UpdateAuraAndTributeCost();
    }

    // Start is called before the first frame update
    protected void Start()
    {
        gameObject.name = AbilityData.CardName;
        styleLable.text = AbilityData.Style.ToString();
        typeLable.text = AbilityData.AbilityType.ToString();
        description.text = AbilityData.FullDescription;


        SetBubbleTextFieldIfGreaterZero(MagicText, BodyTransform.Find("Magic Bubble").GetComponent<Image>().gameObject, AbilityData.MagicRequirement, "M\n");
        SetBubbleTextFieldIfGreaterZero(TechText, BodyTransform.Find("Tech Bubble").GetComponent<Image>().gameObject, AbilityData.TechRequirement, "T\n");
        SetBubbleTextFieldIfGreaterZero(StrengthText, BodyTransform.Find("Strength Bubble").GetComponent<Image>().gameObject, AbilityData.StrengthRequirement, "S\n");
    }

    public static AbilityCardComponent_old CreateAbilityCard(Ability abilityData, Transform parentTransform = null)
    {
        GameObject abilityObject = Instantiate(abilityData.cardPrefab, parentTransform, false);
        var cardComponent = abilityObject.GetComponent<AbilityCardComponent_old>();
        var abilityGameCard = new AbilityGameCard(abilityData);
        cardComponent.SetCardData(abilityGameCard);
        return cardComponent;
    }

    private void SetBubbleTextFieldIfGreaterZero(TextMeshProUGUI textObj, GameObject bubble, int value, string prepend)
    {
        if (value > 0)
        {
            textObj.gameObject.SetActive(true);
            bubble.gameObject.SetActive(true);
            textObj.text = prepend + value.ToString();
        }
        else
        {
            textObj.gameObject.SetActive(false);
            bubble.gameObject.SetActive(false);
        }
    }

    public override void AddStatusChange(AbilityGameCard.Status statusChange)
    {
        base.AddStatusChange(statusChange);
        UpdateAuraAndTributeCost();
    }

    public override void RemoveStatusChange(AbilityGameCard.Status statusChange)
    {
        base.RemoveStatusChange(statusChange);
        UpdateAuraAndTributeCost();

    }

    public void UpdateAuraAndTributeCost()
    {
        foreach (Transform child in AuraCostGroup)
        {
            Destroy(child.gameObject);
        }

        PopulateParent(auraIcon, AuraCostGroup, AbilityData.AuraCost);
        PopulateParent(tributeIcon, AuraCostGroup, AbilityData.TributeCost);
    }
}
