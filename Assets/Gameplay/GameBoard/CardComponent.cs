﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.EventSystems;

public class CardComponent : MonoBehaviour
{
    private static readonly int UIContentScale = 150;
    private static readonly float FaceDownRotation = 180f;

    private float progress;
    private bool started = false;
    [SerializeField]
    private BoxCollider cardBoxCollider;
    private AnimFields initial;
    [SerializeField]
    private GameCard cardData;

    [SerializeField]
    private bool isFighterAbility = false;

    protected Texture2D cardArt;
    protected TextMeshPro cardName;
    protected Material cardMaterial;
    private Transform body;
    public GameCard CardData
    {
        get { return cardData; }
        private set { cardData = value; }
    }

    [HideInInspector]
    public Color highlightColor;
    [HideInInspector]
    public float duration;
    [HideInInspector]
    public Vector3 targetPosition;
    [HideInInspector]
    public AnimFields target;

    public int GameId { get; set; }

    public bool IsPlaying
    {
        get { return started; }
    }

    public bool IsFighterAbility
    {
        get { return isFighterAbility; }
        set { isFighterAbility = value; }
    }

    public bool IsFaceUp
    {
        get
        {
            return Body.localRotation.y == 0;
        }

        set
        {
            if (value)
            {
                Body.localRotation = Quaternion.identity;
            }
            else
            {
                Body.localRotation = Quaternion.Euler(0, FaceDownRotation, 0);
            }
        }
    }
    
    public float Width
    {
        get
        {
            var rectTransform = GetComponent<RectTransform>();
            return rectTransform.sizeDelta.x;
        }
    }

    public float Hieght
    {
        get
        {
            var rectTransform = GetComponent<RectTransform>();
            return rectTransform.sizeDelta.y;
        }
    }

    public Transform Body
    {
        get
        {
            if (body == null)
            {
                body = transform.Find("Body");
            }
            return body;
        }
    }

    // Start is called before the first frame update
    protected void AttachElements()
    {
        cardName = FindCardElement<TextMeshPro>("Card Name", Body);
        cardMaterial = FindCardElement<MeshRenderer>("Quad Body", Body).material;
        cardBoxCollider = GetComponent<BoxCollider>();

        if (cardMaterial != null)
        {
            cardMaterial.SetTexture("_CardArt", cardArt);
        }

        //Artwork is opional
        if (CardData.Artwork != null)
        {
            cardArt = CardData.Artwork.texture;
        }

        cardMaterial.SetTexture("_CardArt", cardArt);
        cardMaterial.SetInt("_ShowArt", 1);
        cardName.text = CardData.CardName;
    }

    public void EnableUIRenderMode()
    {
        MoveToLayer(9);
        SetCardScale(UIContentScale);
    }

    public void SetCardScale(float scaleFactor)
    {
        transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
        var rectTransform = GetComponent<RectTransform>();
        rectTransform.sizeDelta = rectTransform.sizeDelta * scaleFactor;
    }

    public void SetCardData(GameCard _gameCard)
    {
        CardData = _gameCard;
        name = CardData.CardName;
    }

    //Starts an animation
    public void Begin()
    {
        progress = 0;
        started = true;
        initial = new AnimFields { position = transform.localPosition, rotation = transform.localRotation, scale = transform.localScale };
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!started)
        {
            return;
        }

        progress += Time.deltaTime;
        float normTime = progress / duration;
        if (normTime > 1)
        {
            normTime = 1;
            started = false;
        }

        AnimFields fields = AnimFields.Lerp(initial, target, normTime);

        transform.localPosition = fields.position;
        transform.localRotation = fields.rotation;
        transform.localScale = fields.scale;
    }

    protected static void PopulateParent(GameObject obj, Transform parent, int numOfCopies)
    {
        for (int i = 0; i < numOfCopies; i++)
        {
            Instantiate(obj, parent);
        }
    }

    protected Transform FindCardElement(string name)
    {
        return FindCardElement<Transform>(name, Body);
    }

    protected Transform FindCardElement(string name, Transform body)
    {
        return FindCardElement<Transform>(name, Body);
    }

    protected T FindCardElement<T>(string name)
    {
        return FindCardElement<T>(name, Body);
    }

    protected T FindCardElement<T>(string name, Transform body)
    {
        var elementTransform = body.Find(name);

        if (elementTransform == null)
        {
            Debug.LogError("Could not find " + name + " in " + this.name + " hierarchy!");
        }

        var element = elementTransform.GetComponent<T>();

        if (element == null)
        {
            Debug.LogError("Could not find " + name + " in " + this.name + " hierarchy!");
        }
        return element;
    }

    public void MoveToLayer(int layer)
    {
        Stack<Transform> moveTargets = new Stack<Transform>();
        moveTargets.Push(transform);
        Transform currentTarget;
        while (moveTargets.Count != 0)
        {
            currentTarget = moveTargets.Pop();
            currentTarget.gameObject.layer = layer;
            foreach (Transform child in currentTarget)
                moveTargets.Push(child);
        }
    }
}
