﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public abstract class AbilityCardBaseComponent : CardComponent
{
    private List<CardComponent> targets;

    protected AbilityGameCard abilityData = null;

    public AbilityGameCard AbilityData
    {
        get { return abilityData; }
    }

    public ReadOnlyCollection<CardComponent> Targets;

    public AbilityCardBaseComponent()
    {
        targets = new List<CardComponent>();
        Targets = targets.AsReadOnly();
    }

    public virtual void AddStatusChange(AbilityGameCard.Status statusChange)
    {
        AbilityData.AddStatusChange(statusChange);
    }

    public virtual void RemoveStatusChange(AbilityGameCard.Status statusChange)
    {
        AbilityData.RemoveStatusChange(statusChange);
    }

    public void AddOnHitTrigger(IOnHitTriggerAddOn onHitTrigger)
    {
        AbilityData.AddOnHitTrigger(onHitTrigger);
    }

    public void RemoveOnHitTrigger(IOnHitTriggerAddOn onHitTrigger)
    {
        AbilityData.RemoveOnHitTrigger(onHitTrigger);
    }

    public void AddTarget(CardComponent card)
    {
        targets.Add(card);
    }

    public void RemoveTarget(CardComponent card)
    {
        targets.Remove(card);
    }

    public bool IsTarget(CardComponent card)
    {
        return targets.Contains(card);
    }

    public void RemoveAllTargets()
    {
        targets.Clear();
    }

    public void ResetGameplayData()
    {
        AbilityData.RemoveAllStatusChanges();
        RemoveAllTargets();
    }
}
