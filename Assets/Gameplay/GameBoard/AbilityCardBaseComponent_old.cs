﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public abstract class AbilityCardBaseComponent_old : CardComponent_old
{
    private List<CardComponent_old> targets;

    [SerializeField]
    protected AbilityGameCard abilityData = null;

    public abstract AbilityGameCard AbilityData { get; }

    public ReadOnlyCollection<CardComponent_old> Targets;

    public AbilityCardBaseComponent_old()
    {
        targets = new List<CardComponent_old>();
        Targets = targets.AsReadOnly();
    }

    public virtual void AddStatusChange(AbilityGameCard.Status statusChange)
    {
        AbilityData.AddStatusChange(statusChange);
    }

    public virtual void RemoveStatusChange(AbilityGameCard.Status statusChange)
    {
        AbilityData.RemoveStatusChange(statusChange);
    }

    public void AddOnHitTrigger(IOnHitTriggerAddOn onHitTrigger)
    {
        AbilityData.AddOnHitTrigger(onHitTrigger);
    }

    public void RemoveOnHitTrigger(IOnHitTriggerAddOn onHitTrigger)
    {
        AbilityData.RemoveOnHitTrigger(onHitTrigger);
    }

    public void AddTarget(CardComponent_old card)
    {
        targets.Add(card);
    }

    public void RemoveTarget(CardComponent_old card)
    {
        targets.Remove(card);
    }

    public bool IsTarget(CardComponent_old card)
    {
        return targets.Contains(card);
    }

    public void RemoveAllTargets()
    {
        targets.Clear();
    }

    public void ResetGameplayData()
    {
        AbilityData.RemoveAllStatusChanges();
        RemoveAllTargets();
    }
}
