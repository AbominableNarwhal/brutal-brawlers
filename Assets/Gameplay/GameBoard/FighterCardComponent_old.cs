﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.ObjectModel;

public class FighterCardComponent_old : CardComponent_old
{
    [SerializeField]
    private Text abilitySlotText = null;
    [SerializeField]
    private Text equipmentSlotText = null;
    [SerializeField]
    private FighterAbilityComponent_old abilityPrefab = null;
    [SerializeField]
    private TextMeshProUGUI hp = null;
    [SerializeField]
    private TextMeshProUGUI magicText = null;
    [SerializeField]
    private TextMeshProUGUI techText = null;
    [SerializeField]
    private TextMeshProUGUI strengthText = null;
    [SerializeField]
    private Transform markerZone = null;
    [SerializeField]
    private MarkerCardLayout markerCardLayout = null;
    [SerializeField]
    private Transform blockerZone = null;
    [SerializeField]
    private BlockerLayout blockerLayout = null;
    [SerializeField]
    private Sprite bleedIcon = null;
    [SerializeField]
    private Sprite dazedIcon = null;
    [SerializeField]
    private Sprite electrifiedIcon = null;
    [SerializeField]
    private GameObject statusPrefab = null;
    [SerializeField]
    private Transform statusZone = null;
    [SerializeField]
    private FighterGameCard fighterData = null;
    private List<AbilityCardBaseComponent_old> abilities = null;
    private List<EquipmentCardComponent_old> equipment = null;
    private Transform attachedAbilitiesZone = null;
    private Transform attachedEquipmentZone = null;

    public Text AbilitySlotText
    {
        get
        {
            return abilitySlotText;
        }
    }

    public Text EquipmentSlotText
    {
        get
        {
            return equipmentSlotText;
        }
    }

    public FighterGameCard FighterData
    {
        get { return fighterData; }
        private set { fighterData = value; }
    }

    public int Hp
    {
        get { return FighterData.Hp; }

        set
        {
            hp.text = FighterData.Hp.ToString();
        }
    }

    public int Magic
    {
        get { return FighterData.Magic; }
    }

    public int Tech
    {
        get { return FighterData.Tech; }
    }

    public int Strength
    {
        get { return FighterData.Strength; }
    }

    public ReadOnlyCollection<AbilityCardBaseComponent_old> Abilities { get; private set; }

    public FighterAbilityComponent_old AbilityPrefab
    {
        get { return abilityPrefab; }
    }

    public FighterCardComponent_old()
    {
        abilities = new List<AbilityCardBaseComponent_old>();
        equipment = new List<EquipmentCardComponent_old>();
        Abilities = abilities.AsReadOnly();
    }

    public static FighterCardComponent_old CreateFighterCard(Fighter fighterData, Transform parentTransform = null)
    {
        GameObject fighterObject = Instantiate(fighterData.cardPrefab, parentTransform, false);
        var cardComponent = fighterObject.GetComponent<FighterCardComponent_old>();
        var fighterGameCard = new FighterGameCard(fighterData);
        cardComponent.SetCardData(fighterGameCard);
        return cardComponent;
    }

    public void SetCardData(FighterGameCard _gameCard)
    {
        base.SetCardData(_gameCard);
        FighterData = _gameCard;

        Transform nativeAbilities = BodyTransform.Find("Ability Start");
        float abilityHeight = 0;
        foreach (AbilityGameCard ability in FighterData.Abilities)
        {
            FighterAbilityComponent_old abilityObject = Instantiate(AbilityPrefab, nativeAbilities);
            abilityObject.transform.localPosition += new Vector3(0, abilityHeight, 0);
            abilityObject.SetCardData(ability);
            abilities.Add(abilityObject);

            float displayHieght = abilityObject.DisplayHieght;
            abilityHeight += -displayHieght;
        }
    }
    public void AddStatusCondition(StatusCondition condition)
    {
        FighterData.AddStatusCondition(condition);

        bool duplicatefound = false;
        for (int i = 0; i < statusZone.childCount; i++)
        {
            var iconSprite = statusZone.GetChild(i).GetComponent<Image>().sprite;
            if (IconToStatus(iconSprite) == condition)
            {
                AddStatusIconToDisplay(iconSprite, i);
                duplicatefound = true;
                i++;
            }
        }

        if (!duplicatefound)
        {
            AddStatusIconToDisplay(StatusToIcon(condition));
        }
    }

    public void RemoveStatusCondition(StatusCondition condition)
    {
        FighterData.RemoveStatusCondition(condition);

        for (int i = 0; i < statusZone.childCount; i++)
        {
            Transform iconItem = statusZone.GetChild(i);
            var iconSprite = iconItem.GetComponent<Image>().sprite;
            if (IconToStatus(iconSprite) == condition)
            {
                Destroy(iconItem.gameObject);
                return;
            }
        }
    }

    public void AddMarker(Marker marker)
    {
        fighterData.AddMarker(marker);
        var markerIcon = Instantiate(markerCardLayout, markerZone);
        markerIcon.name = marker.name;
        markerIcon.MarkIcon.sprite = marker.Icon;
        markerIcon.SetMarkerCount(fighterData.MarkerCount(marker));
    }

    public void RemoveMarker(Marker marker)
    {
        fighterData.RemoveMarker(marker);
        Destroy(markerZone.Find(marker.name).gameObject);
    }

    public int MarkerCount(Marker marker)
    {
        return FighterData.MarkerCount(marker);
    }

    public void AddBlocker(int blocker)
    {
        fighterData.AddBlocker(blocker);
        BlockerLayout blockerItem = Instantiate(blockerLayout, blockerZone);
        blockerItem.BlockerStrength = blocker;
    }

    public void DealDamage(int damage)
    {
        fighterData.DealDamage(damage);

        if (blockerZone != null)
        {
            foreach (Transform blocker in blockerZone)
            {
                Destroy(blocker.gameObject);
            }

            for (int i = fighterData.Blockers.Count - 1; i >= 0; i--)
            {
                AddBlocker(fighterData.Blockers[i]);
            }
        }
    }

    public void AttachAbility(AbilityCardComponent_old abilityCard)
    {
        abilities.Add(abilityCard);
        fighterData.AttachAbility(abilityCard.AbilityData);
        abilityCard.transform.SetParent(attachedAbilitiesZone, true);
    }

    public void DetachAbility(AbilityCardComponent_old abilityCard)
    {
        abilities.Remove(abilityCard);
        fighterData.DetachAbility(abilityCard.AbilityData);
    }

    public void AttachEquipment(EquipmentCardComponent_old equipmentCard)
    {
        equipment.Add(equipmentCard);
        fighterData.AttachEquipment(equipmentCard.EquipmentData);
        equipmentCard.transform.SetParent(attachedEquipmentZone);
    }

    public void DetachEquipment(EquipmentCardComponent_old equipmentCard)
    {
        equipment.Remove(equipmentCard);
        fighterData.DetachEquipment(equipmentCard.EquipmentData);
    }

    public bool HasAbility(AbilityCardBaseComponent_old abilityCard)
    {
        return abilities.Contains(abilityCard);
    }

    public bool HasEquipment(EquipmentCardComponent_old equipmentCard)
    {
        return equipment.Contains(equipmentCard);
    }

    private void UpdateDisplay()
    {
        hp.text = Hp.ToString();
        magicText.text = "M\n" + Magic.ToString();
        techText.text = "T\n" + Tech.ToString();
        strengthText.text = "S\n" + Strength.ToString();
    }

    private StatusCondition IconToStatus(Sprite icon)
    {
        if (icon == bleedIcon)
        {
            return StatusCondition.Bleed;
        }
        else if (icon == dazedIcon)
        {
            return StatusCondition.Dazed;
        }
        else if (icon == electrifiedIcon)
        {
            return StatusCondition.Electrified;
        }

        return StatusCondition.None;
    }

    private Sprite StatusToIcon(StatusCondition statusCondition)
    {
        switch (statusCondition)
        {
            case StatusCondition.Bleed:
                return bleedIcon;
            case StatusCondition.Dazed:
                return dazedIcon;
            case StatusCondition.Electrified:
                return electrifiedIcon;
            default:
                return null;
        }
    }

    private void AddStatusIconToDisplay(Sprite sprite, int index = -1)
    {
        var icon = Instantiate(statusPrefab, statusZone).GetComponent<Image>();
        icon.sprite = sprite;

        if (index > -1)
        {
            icon.transform.SetSiblingIndex(index);
        }
    }

    // Start is called before the first frame update
    protected void Start()
    {
        UpdateDisplay();
        attachedAbilitiesZone = transform.GetChild(0).Find("Attached Abilities");
        attachedEquipmentZone = transform.GetChild(0).Find("Attached Equipment");
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDisplay();
    }
}
