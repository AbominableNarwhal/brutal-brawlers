﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardAnimator : MonoBehaviour
{
    public List<CardComponent> animationList;
    public bool EnableAnimations { get; set; } = true;

    public bool IsAnimating
    {
        get { return animationList.Count > 0; }
    }
    // Start is called before the first frame update
    void Start()
    {
        animationList = new List<CardComponent>();
    }

    public void AddAnimation(CardComponent animation)
    {
        if (EnableAnimations)
        {
            AnimFields newAnimTarget = new AnimFields { position = animation.transform.localPosition, rotation = animation.transform.localRotation, scale = animation.transform.localScale };
            if (animation.target == newAnimTarget)
            {
                return;
            }
            animation.Begin();
            animationList.Add(animation);
        }
        else
        {
            animation.transform.localPosition = Vector3.zero;
            animation.transform.localRotation = Quaternion.identity;
            animation.transform.localScale = Vector3.zero;
        }
    }
    // Update is called once per frame
    void Update()
    {
        for(int i = animationList.Count - 1; i >= 0; i--)
        {
            if(!animationList[i].IsPlaying || animationList[i] == null)
            {
                animationList.RemoveAt(i);
            }
        }
    }
}
