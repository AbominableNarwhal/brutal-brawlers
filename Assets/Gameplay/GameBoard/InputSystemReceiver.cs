﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputSystemReceiver : MonoBehaviour, IPointerExitHandler, IPointerEnterHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IDragHandler, IDropHandler
{
    private InputMaster controls;
    private GameInputEventArgs.PointerEvent currentEventType;
    private PointerEventData currentEventData;
    private Coroutine longHoverCoroutine = null;

    public event EventHandler<GameInputEventArgs> GameInputEvent;

    private void Awake()
    {
        controls = new InputMaster();
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
        if (currentEventType == GameInputEventArgs.PointerEvent.Down)
        {
            TriggerEvent(GameInputEventArgs.PointerEvent.Up, currentEventData);
        }
    }

    private void OnDestroy()
    {
        if (GameInputEvent != null)
        {
            foreach (Delegate d in GameInputEvent.GetInvocationList())
            {
                GameInputEvent -= (EventHandler<GameInputEventArgs>)d;
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        /*if (eventData.pointerEnter.gameObject != gameObject)
        {
            return;
        }*/

        if (longHoverCoroutine != null)
        {
            StopCoroutine(longHoverCoroutine);
        }
        TriggerEvent(GameInputEventArgs.PointerEvent.Exit, eventData);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerCurrentRaycast.gameObject != gameObject)
        {
            return;
        }

        longHoverCoroutine = StartCoroutine(WaitAndTriggerLongHover());
        TriggerEvent(GameInputEventArgs.PointerEvent.Enter, eventData);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.pointerCurrentRaycast.gameObject != gameObject)
        {
            return;
        }

        TriggerEvent(GameInputEventArgs.PointerEvent.Click, eventData);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.pointerCurrentRaycast.gameObject != gameObject)
        {
            return;
        }

        TriggerEvent(GameInputEventArgs.PointerEvent.Down, eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.pointerCurrentRaycast.gameObject != gameObject)
        {
            return;
        }

        TriggerEvent(GameInputEventArgs.PointerEvent.Up, eventData);
    }

    private void TriggerEvent(GameInputEventArgs.PointerEvent pointerEvent, PointerEventData eventData)
    {
        currentEventType = pointerEvent;
        currentEventData = eventData;

        GameInputEventArgs args = new GameInputEventArgs();

        args.EventType = pointerEvent;
        args.EventData = eventData;
        GameInputEvent?.Invoke(gameObject, args);
    }

    IEnumerator WaitAndTriggerLongHover()
    {
        yield return new WaitForSeconds(0.5f);
        TriggerEvent(GameInputEventArgs.PointerEvent.LongHover, currentEventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
    }

    public void OnDrop(PointerEventData eventData)
    {
    }
}

public class GameInputEventArgs : EventArgs
{
    public enum PointerEvent { Enter, Exit, Click, Down, Up, LongHover }

    public PointerEvent EventType { get; set; }
    public PointerEventData EventData { get; set; }
}
