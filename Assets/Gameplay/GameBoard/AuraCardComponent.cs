﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AuraCardComponent : CardComponent
{
    [SerializeField]
    private Texture2D cardFrame = null;
    // Start is called before the first frame update
    void Start()
    {
    }

    public void SetCardData(AuraGameCard _gameCard)
    {
        base.SetCardData(_gameCard);

        AttachElements();
        cardMaterial.SetTexture("_MainTex", cardFrame);
        cardMaterial.SetInt("_ShowMagic", 0);
        cardMaterial.SetInt("_ShowTech", 0);
        cardMaterial.SetInt("_ShowStrength", 0);
        cardMaterial.SetInt("_ShowArt", 0);
    }

    public static AuraCardComponent CreateAuraCard(Aura auraData, Transform parentTransform = null)
    {
        GameObject auraObject = Object.Instantiate(auraData.cardPrefab, parentTransform, false);
        var cardComponent = auraObject.GetComponent<AuraCardComponent>();
        var auraGameCard = new AuraGameCard(auraData);
        cardComponent.SetCardData(auraGameCard);
        return cardComponent;
    }
}
