using UnityEngine;

public struct AnimFields
{
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;

    public static AnimFields Lerp(AnimFields initial, AnimFields target, float step)
    {
        AnimFields properties = new AnimFields();
        properties.position = Vector3.Lerp(initial.position, target.position, step);
        properties.rotation = Quaternion.Lerp(initial.rotation, target.rotation, step);
        properties.scale = Vector3.Lerp(initial.scale, target.scale, step);
        return properties;
    }
    public static bool operator ==(AnimFields lhs, AnimFields rhs)
    {
        if(lhs.position == rhs.position && lhs.rotation == rhs.rotation && lhs.scale == rhs.scale)
        {
            return true;
        }
        return false;
    }
    public static bool operator !=(AnimFields lhs, AnimFields rhs)
    {
        return !(lhs == rhs);
    }
    public override bool Equals(object obj)
    {
        return this == (AnimFields)obj;
    }

    public override int GetHashCode()
    {
        return position.GetHashCode() * rotation.GetHashCode() * scale.GetHashCode();
    }
}