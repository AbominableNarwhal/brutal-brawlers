﻿using UnityEngine;
using UnityEngine.UI;

public class PowerCardComponent_old : CardComponent_old
{
    [SerializeField]
    private GameObject auraIcon = null;
    private Transform auraCostGroup = null;
    [SerializeField]
    private PowerGameCard powerData;
    public Text description;

    public PowerGameCard PowerData
    {
        get { return powerData; }
        private set { powerData = value; }
    }

    public Transform AuraCostGroup
    {
        get
        {
            if (auraCostGroup == null)
            {
                auraCostGroup = BodyTransform.Find("Aura Cost");
            }
            return auraCostGroup;
        }
    }

    public static PowerCardComponent_old CreatePowerCard(Power powerData, Transform parentTransform = null)
    {
        GameObject powerObject = Instantiate(powerData.cardPrefab, parentTransform, false);
        var cardComponent = powerObject.GetComponent<PowerCardComponent_old>();
        var powerGameCard = new PowerGameCard(powerData);
        cardComponent.SetCardData(powerGameCard);
        return cardComponent;
    }

    // Start is called before the first frame update
    protected void Start()
    {
        description.text = PowerData.Description;
    }

    public void SetCardData(PowerGameCard _gameCard)
    {
        base.SetCardData(_gameCard);
        PowerData = _gameCard;
        UpdateAuraCost();
    }

    public void UpdateAuraCost()
    {
        PopulateParent(auraIcon, AuraCostGroup, PowerData.AuraCost);
    }
}
