﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AbilityCardComponent : AbilityCardBaseComponent
{
    [SerializeField]
    private Texture2D cardFrame = null;
    [SerializeField]
    private Texture2D aruaCostIcon = null;
    [SerializeField]
    private Texture2D tributeCostIcon = null;
    [SerializeField]
    private GameObject imagePrefab = null;

    private Transform costZone = null;
    private TextMeshPro magicText = null;
    private TextMeshPro techText = null;
    private TextMeshPro strengthText = null;
    private TextMeshPro styleText = null;
    private TextMeshPro typeText = null;
    private TextMeshPro description = null;

    public int MagicRequirement
    {
        get { return abilityData.MagicRequirement; }
        set
        {
            abilityData.MagicRequirement = value;
            SetBubbleTextFieldIfGreaterZero("_ShowMagic", magicText, value, "M\n");
        }
    }

    public int TechRequirement
    {
        get { return abilityData.TechRequirement; }
        set
        {
            abilityData.TechRequirement = value;
            SetBubbleTextFieldIfGreaterZero("_ShowTech", techText, value, "T\n");
        }
    }

    public int StrengthRequirement
    {
        get { return abilityData.StrengthRequirement; }
        set
        {
            abilityData.StrengthRequirement = value;
            SetBubbleTextFieldIfGreaterZero("_ShowStrength", strengthText, value, "S\n");
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    public static AbilityCardComponent CreateAbilityCard(Ability abilityData, Transform parentTransform = null)
    {
        GameObject abilityObject = Instantiate(abilityData.cardPrefab, parentTransform, false);
        var cardComponent = abilityObject.GetComponent<AbilityCardComponent>();
        var abilityGameCard = new AbilityGameCard(abilityData);
        cardComponent.SetCardData(abilityGameCard);
        return cardComponent;
    }

    public void SetCardData(AbilityGameCard _gameCard)
    {
        base.SetCardData(_gameCard);
        abilityData = _gameCard;

        AttachElements();
        costZone = FindCardElement("Cost Zone");
        magicText = FindCardElement<TextMeshPro>("Magic");
        techText = FindCardElement<TextMeshPro>("Tech");
        strengthText = FindCardElement<TextMeshPro>("Strength");
        styleText = FindCardElement<TextMeshPro>("Style");
        typeText = FindCardElement<TextMeshPro>("Type");
        description = FindCardElement<TextMeshPro>("Description");

        MagicRequirement = abilityData.MagicRequirement;
        StrengthRequirement = abilityData.StrengthRequirement;
        TechRequirement = abilityData.TechRequirement;

        styleText.text = abilityData.Style.ToString();
        typeText.text = abilityData.AbilityType.ToString();
        description.text = abilityData.FullDescription;

        cardMaterial.SetTexture("_MainTex", cardFrame);
        UpdateAuraAndTributeCost();
    }

    public override void AddStatusChange(AbilityGameCard.Status statusChange)
    {
        base.AddStatusChange(statusChange);
        UpdateAuraAndTributeCost();
    }

    public override void RemoveStatusChange(AbilityGameCard.Status statusChange)
    {
        base.RemoveStatusChange(statusChange);
        UpdateAuraAndTributeCost();

    }

    private void SetBubbleTextFieldIfGreaterZero(string shaderName, TextMeshPro text, int value, string prefix)
    {
        if (value > 0)
        {
            text.text = prefix + value.ToString();
            text.gameObject.SetActive(true);
            cardMaterial.SetInt(shaderName, 1);
        }
        else
        {
            text.gameObject.SetActive(false);
            cardMaterial.SetInt(shaderName, 0);
        }
    }

    public void UpdateAuraAndTributeCost()
    {
        foreach (Transform child in costZone)
        {
            Destroy(child.gameObject);
        }

        var textureImage = imagePrefab.GetComponent<TextureImage>();

        if (AbilityData.AuraCost > 0)
        {
            TextureImage auraIcon = Instantiate(textureImage, costZone);
            auraIcon.Image = aruaCostIcon;

            PopulateParent(auraIcon.gameObject, costZone, AbilityData.AuraCost - 1);
        }

        if (AbilityData.TributeCost > 0)
        {
            TextureImage tributeIcon = Instantiate(textureImage, costZone);
            tributeIcon.Image = tributeCostIcon;

            PopulateParent(imagePrefab, costZone, AbilityData.TributeCost - 1);
        }
    }
}
