﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CardComponent_old : MonoBehaviour
{
    private static readonly float FaceDownRotation = 180f;

    private float progress;
    private bool started = false;
    private AnimFields initial;

    [SerializeField]
    private TextMeshProUGUI cardName = null;
    [SerializeField]
    private Image artWork = null;
    [SerializeField]
    private bool isFighterAbility = false;
    [SerializeField]
    private Transform bodyTransform = null;

    public Transform BodyTransform
    {
        get
        {
            return bodyTransform;
        }
    }

    [HideInInspector]
    public Color highlightColor;
    [HideInInspector]
    public float duration;
    [HideInInspector]
    public Vector3 targetPosition;
    [HideInInspector]
    public AnimFields target;


    public GameCard CardData { get; private set; }

    public int GameId { get; set; }

    public bool IsPlaying
    {
        get { return started; }
    }

    public bool IsFighterAbility
    {
        get { return isFighterAbility; }
        set { isFighterAbility = value; }
    }

    public bool IsFaceUp
    {
        get
        {
            return BodyTransform.localRotation.y == 0;
        }

        set
        {
            if(value)
            {
                BodyTransform.localRotation = Quaternion.identity;
            }
            else
            {
                BodyTransform.localRotation = Quaternion.Euler(0, FaceDownRotation, 0);
            }
        }
    }

    public void SetCardData(GameCard _gameCard)
    {
        CardData = _gameCard;

        //Artwork is opional
        if (artWork != null)
        {
            artWork.sprite = _gameCard.Artwork;
        }
        cardName.text = _gameCard.CardName;
    }

    //Starts an animation
    public void Begin()
    {
        progress = 0;
        started = true;
        initial = new AnimFields { position = transform.localPosition, rotation = transform.localRotation, scale = transform.localScale };
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!started)
        {
            return;
        }

        progress += Time.deltaTime;
        float normTime = progress/duration;
        if(normTime > 1)
        {
            normTime = 1;
            started = false;
        }

        AnimFields fields = AnimFields.Lerp(initial, target, normTime);
        
        transform.localPosition = fields.position;
        transform.localRotation = fields.rotation;
        transform.localScale = fields.scale;
    }

    public static IEnumerable<AbilityCardBaseComponent_old> FighterAbilities(FighterCardComponent_old fighterCard)
    {
        //Native Abilities
        Transform nativeAbilities = fighterCard.transform.GetChild(0).Find("Ability Start");
        foreach (Transform child in nativeAbilities)
        {
            yield return child.GetComponent<AbilityCardBaseComponent_old>();
        }

        //Attached Abilities
        Transform attachedAbilities = fighterCard.transform.GetChild(0).Find("Attached Abilities");
        foreach (Transform child in attachedAbilities)
        {
            yield return child.GetComponent<AbilityCardBaseComponent_old>();
        }
    }

    public static IEnumerable<CardComponent_old> FighterAttachedAbilities(CardComponent_old fighterCard)
    {
        Transform attachedAbilities = fighterCard.transform.GetChild(0).Find("Attached Abilities");
        foreach (Transform child in attachedAbilities)
        {
            yield return child.GetComponent<CardComponent_old>();
        }
    }

    public static IEnumerable<CardComponent_old> FighterAttachedEquipment(CardComponent_old fighterCard)
    {
        Transform attachedEquipment = fighterCard.transform.GetChild(0).Find("Attached Equipment");
        foreach (Transform child in attachedEquipment)
        {
            yield return child.GetComponent<CardComponent_old>();
        }
    }

    public static IEnumerable<CardComponent_old> FighterEquipment(CardComponent_old fighterCard)
    {
        Transform equipment = fighterCard.transform.GetChild(0).Find("Attached Equipment");
        foreach (Transform child in equipment)
        {
            yield return child.GetComponent<CardComponent_old>();
        }
    }

    public static int GetFighterAttachedAbilitiesCount(CardComponent_old fighterCard)
    {
        return fighterCard.transform.GetChild(0).Find("Attached Abilities").childCount;
    }

    public static int GetFighterAttachedEquipmentCount(CardComponent_old fighterCard)
    {
        return fighterCard.transform.GetChild(0).Find("Attached Equipment").childCount;
    }

    protected static void PopulateParent(GameObject obj, Transform parent, int numOfCopies)
    {
        for (int i = 0; i < numOfCopies; i++)
        {
            Instantiate(obj, parent);
        }
    }
}
