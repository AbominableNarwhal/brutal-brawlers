﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class FighterAbilityComponent_old : AbilityCardBaseComponent_old
{
    private RectTransform boundingRect = null;
    public override AbilityGameCard AbilityData
    {
        get { return abilityData; }
    }

    public RectTransform BoundingRect
    {
        get
        {
            if (boundingRect == null)
            {
                boundingRect = BodyTransform.GetComponent<RectTransform>();
            }
            return boundingRect;
        }
    }

    public float DisplayHieght
    {
        get { return BoundingRect.rect.height; }
    }

    public void SetCardData(AbilityGameCard _gameCard)
    {
        base.SetCardData(_gameCard);
        abilityData = _gameCard as AbilityGameCard;

        TextMeshProUGUI abilityNameText = BodyTransform.GetChild(0).GetComponent<TextMeshProUGUI>();
        abilityNameText.text = abilityData.CardName;

        var infoText = abilityNameText.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        infoText.text = Ability.StyleToInlineText(abilityData.Style);
        if (abilityData.AuraCost > 0)
        {
            infoText.text += Ability.InlineAura + "x" + abilityData.AuraCost.ToString();
        }

        if (abilityData.TributeCost > 0)
        {
            infoText.text += Ability.InlineTribute + "x" + abilityData.TributeCost.ToString();
        }

        TextMeshProUGUI abilityDescriptionText = BodyTransform.GetChild(1).GetComponent<TextMeshProUGUI>();
        abilityDescriptionText.text = abilityData.FullDescription;

        //Need to trigger two passes here in order to force the child layout elements to be fully updated
        LayoutRebuilder.ForceRebuildLayoutImmediate(BoundingRect);
        LayoutRebuilder.ForceRebuildLayoutImmediate(BoundingRect);

        GetComponent<GameBoardGraphic>().UpdateHighlightSizeWithBody(BoundingRect);
    }

}
