﻿using UnityEngine;
using TMPro;

public class EquipmentCardComponent_old : CardComponent_old
{
    [SerializeField]
    private EquipmentGameCard equipmentData;

    public EquipmentGameCard EquipmentData
    {
        get { return equipmentData; }
        private set { equipmentData = value; }
    }

    // Start is called before the first frame update
    protected void Start()
    {
        BodyTransform.Find("Description").GetComponent<TextMeshProUGUI>().text = EquipmentData.Description;
        BodyTransform.Find("Type").GetComponent<TextMeshProUGUI>().text = EquipmentData.FightStyle.ToString();
    }

    public void SetCardData(EquipmentGameCard _gameCard)
    {
        base.SetCardData(_gameCard);
        EquipmentData = _gameCard;
    }

    public static EquipmentCardComponent_old CreateEquipmentCard(Equipment eqipmentData, Transform parentTransform = null)
    {
        GameObject EquipmentObject = Instantiate(eqipmentData.cardPrefab, parentTransform, false);
        var cardComponent = EquipmentObject.GetComponent<EquipmentCardComponent_old>();
        var EquipmentGameCard = new EquipmentGameCard(eqipmentData);
        cardComponent.SetCardData(EquipmentGameCard);
        return cardComponent;
    }
}
