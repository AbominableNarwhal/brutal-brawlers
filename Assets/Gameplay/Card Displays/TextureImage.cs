﻿using System.Collections;
using System.Collections.Generic;
using Unity.Profiling.LowLevel;
using UnityEngine;

public class TextureImage : MonoBehaviour
{
    [SerializeField]
    private Texture2D image = null;

    private Material material = null;

    public Texture2D Image
    {
        get { return image; }
        set
        {
            image = value;
            if (material == null)
            {
                material = GetComponent<Renderer>().material;
            }
            material.SetTexture("_MainTex", image);
        }
    }

    void Start()
    {
        if (image)
        {
            Image = image;
        }
    }

    void OnDestroy()
    {
        if (material != null)
        {
            DestroyImmediate(material);
        }
    }
}
