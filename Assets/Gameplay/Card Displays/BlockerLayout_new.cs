﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextureImage))]
public class BlockerLayout_new : MonoBehaviour
{
    private TextureImage blockerImage = null;
    private TextMeshPro blockerText = null;
    private int blockerStrength = 0;

    // Start is called before the first frame update
    void Awake()
    {
        blockerImage = GetComponent<TextureImage>();
        blockerText = transform.Find("Blocker Text").GetComponent<TextMeshPro>();
    }

    public int BlockerStrength
    {
        get { return blockerStrength; }
        set
        {
            blockerStrength = value;
            blockerText.text = blockerStrength.ToString();
        }
    }
}
