﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AuraCounter : MonoBehaviour
{
    private int auraCount;

    public Text counterText;

    public int AuraCount
    {
        get
        {
            int num = 0;
            foreach (Transform child in transform)
            {
                if (child.GetComponent<CardComponent>().IsFaceUp)
                {
                    num++;
                }
            }

            return num;
        }
    }

    void OnTransformChildrenChanged()
    {
        UpdateAuraCount();
    }

    public void UpdateAuraCount()
    {
        if (counterText == null)
        {
            return;
        }
        counterText.text = AuraCount.ToString();
    }
}
