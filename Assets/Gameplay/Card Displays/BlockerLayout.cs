﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BlockerLayout : MonoBehaviour
{
    /*[SerializeField]
    private Image blockerImage = null;*/
    [SerializeField]
    private TextMeshProUGUI blockerText = null;
    private int blockerStrength = 0;

    public int BlockerStrength
    {
        get { return blockerStrength; }
        set
        {
            blockerStrength = value;
            blockerText.text = blockerStrength.ToString();
        }
    }
}
