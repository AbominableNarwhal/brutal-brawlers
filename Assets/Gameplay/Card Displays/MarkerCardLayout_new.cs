﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextureImage))]
public class MarkerCardLayout_new : MonoBehaviour
{
    private TextureImage markIcon = null;
    private TextMeshPro duplicateText = null;

    public Texture2D MarkIcon
    {
        get { return markIcon.Image; }
        set { markIcon.Image = value; }
    }

    void Awake()
    {
        markIcon = GetComponent<TextureImage>();
        duplicateText = transform.Find("Duplicate Text").GetComponent<TextMeshPro>();
    }

    public void SetMarkerCount(int count)
    {
        gameObject.SetActive(true);

        if (count > 1)
        {
            duplicateText.gameObject.SetActive(true);
            duplicateText.text = "x" + count;
        }
        else if (count == 1)
        {
            duplicateText.gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
