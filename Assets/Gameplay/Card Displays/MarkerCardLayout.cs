﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MarkerCardLayout : MonoBehaviour
{
    [SerializeField]
    private Image markIcon = null;
    [SerializeField]
    private TextMeshProUGUI duplicateText = null;

    public Image MarkIcon
    {
        get { return markIcon; }
    }

    public void SetMarkerCount(int count)
    {
        if (count > 1)
        {
            duplicateText.text = "x" + count;
        }
    }
}
