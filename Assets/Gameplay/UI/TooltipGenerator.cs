﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipGenerator : MonoBehaviour
{
    public virtual TooltipUiParams GenerateTooltip()
    {
        return new TooltipUiParams();
    }
}
