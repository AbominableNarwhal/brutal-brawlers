﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerUI : MonoBehaviour
{
    public PlayerUIListener listener = null;
    protected UIReturnData returnData = null;

    public void Cancel()
    {
        if(listener != null)
        {
            listener.OnUIReturned(null);
        }
        Destroy(gameObject);
    }

    public void Done()
    {
        if(listener == null)
        {
            return;
        }
        listener.OnUIReturned(returnData);
        Destroy(gameObject);
    }

    public virtual void OnCardClicked(CardComponent card) {}
}

public abstract class UIReturnData
{}

public interface PlayerUIListener
{
    void OnUIReturned(UIReturnData returnData);
}