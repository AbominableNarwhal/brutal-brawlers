﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameBoardGraphic : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private static readonly string BorderProperty = "_borderColor";
    public static readonly Color ClearColor = new Color(0f, 0f, 0f, 0f);
    public static readonly Color DefaultNoColor = new Color(0f, 0f, 0f, 1f);
    public static readonly Color DefaultHoverColor = new Color(0.0901960784313725f, 0.4431372549019608f, 0.9450980392156863f, 0.5f);
    public static readonly Color DefaultSelectedColor = new Color(0.1351015f, 0.8679245f, 0.1931287f);
    public static readonly Color DefaultValidColor = new Color(0.94117647058f, 0.6862745098f, 0.04705882352f);

    private enum MouseState
    {
        None,
        Hover
    }

    private MouseState mouseEventState;
    [SerializeField]
    private Renderer graphicRenderer = null;

    public bool hightlightOnMouseHover = false;

    private Material graphicMaterial = null;
    private Material GraphicMaterial
    {
        get
        {
            if (graphicMaterial == null)
            {
                graphicMaterial = graphicRenderer.material;
            }
            return graphicMaterial;
        }
    }

    private MouseState MouseEventState
    {
        get
        {
            return mouseEventState;
        }

        set
        {
            mouseEventState = value;
            if (!hightlightOnMouseHover)
            {
                return;
            }
            switch(value)
            {
                case MouseState.None:
                    SetHighlightColor(CurrentColor);
                    GraphicMaterial.SetColor(BorderProperty, CurrentColor);
                    break;
                case MouseState.Hover:
                    GraphicMaterial.SetColor(BorderProperty, HoverColor);
                    break;
            }
        }
    }

    public Color NoColor
    {
        get; set;
    } = DefaultNoColor;

    public Color HoverColor
    {
        get; set;
    } = DefaultHoverColor;

    public Color SelectedColor
    {
        get; set;
    } = DefaultSelectedColor;

    public Color ValidColor
    {
        get; set;
    } = DefaultValidColor;

    public Color CurrentColor
    {
        get; private set;
    }

    public void UpdateHighlightSizeWithBody(RectTransform body)
    {
        //REMOVE THIS FUNCTION WHEN OLD GAME CARDS ARE GONE
        /*float bodyHieght = body.GetComponent<RectTransform>().sizeDelta.y;

        RectTransform rectTransform  = GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, bodyHieght);*/
    }

    protected void Start()
    {
        MouseEventState = MouseState.None;
    }

    public void EnableHighlight(bool enabled)
    {
        if (!enabled)
        {
            CurrentColor = ClearColor;
            GraphicMaterial.SetColor(BorderProperty, NoColor);
        }
    }

    public void SetHighlightColor(Color color)
    {
        CurrentColor = color;
        GraphicMaterial.SetColor(BorderProperty, CurrentColor);
    }

    //Detect if the Cursor starts to pass over the GameObject
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        MouseEventState = MouseState.Hover;
    }

    //Detect when Cursor leaves the GameObject
    public void OnPointerExit(PointerEventData pointerEventData)
    {
        MouseEventState = MouseState.None;
    }

}
