﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class CardPickerUI : PlayerUI
{
    [SerializeField]
    private Transform content = null;
    private CardPickerReturnData data;
    [SerializeField] 
    private Button doneButton = null;
    [SerializeField]
    private TextMeshProUGUI title = null;
    [SerializeField]
    private Button cancelButton = null;
    public CardComponent[] cards;
    [HideInInspector]
    public string titleText;
    [HideInInspector]
    public uint minCards = 1;
    [HideInInspector]
    public uint maxCards = 1;

    public CardPickerUI()
    {
        returnData = new CardPickerReturnData();
        data = returnData as CardPickerReturnData;
    }

    // Start is called before the first frame update
    void Start()
    {
        title.text = titleText;
        foreach(CardComponent card in cards)
        {
            CardComponent newCard = Instantiate(card);
            newCard.EnableUIRenderMode();
            newCard.transform.SetParent(content);
            newCard.GetComponent<InputSystemReceiver>().GameInputEvent += OnGamePointerEvent;
        }
        CheckMin();
    }

    public override void OnCardClicked(CardComponent card)
    {
        if (card.transform.parent != content) // Make sure seleced cards are from the content area
        {
            return;
        }

        int index = card.transform.GetSiblingIndex();
        GameBoardGraphic graphic = card.GetComponent<GameBoardGraphic>();

        if(!data.cards.Contains(cards[index]))
        {
            if(data.cards.Count < maxCards)
            {
                graphic.EnableHighlight(true);
                graphic.SetHighlightColor(graphic.SelectedColor);
                data.cards.Add(cards[index]);
            }
        }
        else
        {
            data.cards.Remove(cards[index]);
            graphic.EnableHighlight(false);
        }
        CheckMin();
    }
    public void EnableCancel(bool enable)
    {
        cancelButton.gameObject.SetActive(enable);
    }

    void OnEnable()
    {
        doneButton.onClick.AddListener(Done);
        cancelButton.onClick.AddListener(Cancel);
    }
    void OnDisable()
    {
        doneButton.onClick.RemoveAllListeners();
        cancelButton.onClick.RemoveAllListeners();
    }
    private void CheckMin()
    {
        if(data.cards.Count >= minCards)
        {
            doneButton.gameObject.SetActive(true);
        }
        else
        {
            doneButton.gameObject.SetActive(false);
        }
    }
    private void OnGamePointerEvent(object subject, GameInputEventArgs args)
    {
        if (args.EventType == GameInputEventArgs.PointerEvent.Click)
        {
            var cardObject = subject as GameObject;
            if (cardObject == null)
            {
                return;
            }

            var card = cardObject.GetComponent<CardComponent>();
            if (card == null)
            {
                return;
            }

            OnCardClicked(card);
        }
    }
}

public class CardPickerReturnData : UIReturnData
{
    public List<CardComponent> cards;
    public CardPickerReturnData()
    {
        cards = new List<CardComponent>();
    }
}
