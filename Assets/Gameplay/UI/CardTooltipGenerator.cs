﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardTooltipGenerator : TooltipGenerator
{
    public override TooltipUiParams GenerateTooltip()
    {
        var card = Instantiate(gameObject).GetComponent<CardComponent>();
        card.transform.localPosition = Vector3.zero;
        card.transform.localRotation = Quaternion.identity;
        card.SetCardScale(400);
        card.MoveToLayer(11);

        return new TooltipUiParams { uiObject = card.gameObject, uiWidth = card.Width, uiHieght = card.Hieght };
    }
}
