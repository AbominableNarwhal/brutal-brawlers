﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Tooltip : MonoBehaviour
{
    [SerializeField]
    Canvas canvas = null;
    [SerializeField]
    private GameObject tooltipObject = null;
    private RectTransform rectTransform = null;

    public void SetTooltipUI(TooltipUiParams uiParams)
    {
        ClearTooltip();

        if (uiParams.uiObject)
        {
            uiParams.uiObject.transform.SetParent(transform, false);
            rectTransform.sizeDelta = new Vector2(uiParams.uiWidth, uiParams.uiHieght);
        }
    }

    public void ClearTooltip()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (tooltipObject)
        {
            Instantiate(tooltipObject, transform);
        }

        rectTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        Vector2 mousePos = Mouse.current.position.ReadValue() / canvas.scaleFactor;
        transform.localPosition = mousePos;

        float xPivot = FillterPivot(mousePos.x / Screen.width);
        float yPivot = FillterPivot(mousePos.y / Screen.height);

        if (xPivot == 0.5f && yPivot == 0.5f)
        {
            yPivot = 0.75f;
        }
        rectTransform.pivot = new Vector2(xPivot, yPivot);
    }

    private float FillterPivot(float value)
    {
        if (value > 0.66f)
        {
            return 0.75f;
        }
        else if (value > 0.33f)
        {
            return 0.5f;
        }

        return 0.25f;
    }
}

public struct TooltipUiParams
{
    public GameObject uiObject;
    public float uiWidth;
    public float uiHieght;
}
