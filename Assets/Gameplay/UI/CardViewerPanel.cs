﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardViewerPanel : MonoBehaviour
{
    private CardComponent currentCard = null;

    public void DisplayCard(CardComponent card)
    {
        if (card == null || card.IsFighterAbility)
        {
            return;
        }
        Hide();
        currentCard = Instantiate(card.gameObject, transform).GetComponent<CardComponent>();
        currentCard.transform.localPosition = Vector3.one;
        currentCard.transform.localRotation = Quaternion.identity;
        currentCard.transform.localScale = Vector3.one;
        currentCard.SetCardData(card.CardData);
        currentCard.GetComponent<GameBoardGraphic>().EnableHighlight(false);
    }

    public void Hide()
    {
        if (currentCard == null)
        {
            return;
        }
        Destroy(currentCard.gameObject);
        currentCard = null;
    }
}
