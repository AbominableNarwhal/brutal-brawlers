﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OptionPickerUI : PlayerUI
{
    private OptionPickerReturnData data;
    [SerializeField]
    private Button buttonPrefab = null;
    [SerializeField]
    private TextMeshProUGUI title = null;
    [SerializeField]
    private Transform buttonArea = null;
    [HideInInspector]
    public List<string> options;
    [HideInInspector]
    public string titleText;

    public OptionPickerUI()
    {
        returnData = new OptionPickerReturnData();
        data = returnData as OptionPickerReturnData;
    }

    // Start is called before the first frame update
    void Start()
    {
        title.text = titleText;
        foreach(string option in options)
        {
            Button button = Instantiate(buttonPrefab, buttonArea);
            button.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = option;
            button.onClick.AddListener(delegate { ButtonClick(button); });
        }
    }
    
    private void ButtonClick(Button button)
    {
        data.selection = options[button.transform.GetSiblingIndex()];
        Done();
    }
}

public class OptionPickerReturnData : UIReturnData
{
    public string selection;
}