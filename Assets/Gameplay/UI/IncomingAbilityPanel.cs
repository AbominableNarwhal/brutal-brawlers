﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class IncomingAbilityPanel : MonoBehaviour
{
    private static readonly string DamageTextPrefix = "Incoming Damage ";
    [SerializeField]
    private Color offensiveColor;
    [SerializeField]
    private Color defensiveColor;
    private PlayerPanel player1 = null;
    private PlayerPanel player2 = null;
    private PlayerPanel attackerPanel = null;
    private PlayerPanel defenderPanel = null;

    // Start is called before the first frame update
    void Start()
    {
        player1 = new PlayerPanel();
        player1.AttachElements(transform.GetChild(1));

        player2 = new PlayerPanel();
        player2.AttachElements(transform.GetChild(0));

        HideIncomingAbilityState();
    }

    public void SetIncomingAbilityState(IncomingAbilityParams incomingAbilityParams)
    {
        if (incomingAbilityParams.IsPlayer1Attacker)
        {
            attackerPanel = player1;
            defenderPanel = player2;
            SetAttacker(player1, incomingAbilityParams);
            SetDefender(player2, incomingAbilityParams);
        }
        else
        {
            attackerPanel = player2;
            defenderPanel = player1;
            SetAttacker(player2, incomingAbilityParams);
            SetDefender(player1, incomingAbilityParams);
        }
    }

    public void HideIncomingAbilityState()
    {
        player1.Hide();
        player2.Hide();
    }

    public void SetIncomingDamage(int damage)
    {
        attackerPanel.SetDamageLabelText(DamageTextPrefix + damage.ToString());
    }

    private void SetAttacker(PlayerPanel player, IncomingAbilityParams incomingAbilityParams)
    {
        player.Show();
        if (player == player1)
        {
            player.SetAttackLabelText("You attacked with");
        }
        else
        {
            player.SetAttackLabelText("Player 2 attacked with");
        }
        player.SetColor(offensiveColor);
        SetIncomingDamage(incomingAbilityParams.incomingDamage);
        player.SetCardToDisplay(incomingAbilityParams.offensiveCard);
    }

    private void SetDefender(PlayerPanel player, IncomingAbilityParams incomingAbilityParams)
    {
        if (incomingAbilityParams.defensiveCard)
        {
            player.Show();
            if (player == player1)
            {
                player.SetAttackLabelText("You defended with");
            }
            else
            {
                player.SetAttackLabelText("Player 2 defended with");
            }
            player.SetColor(defensiveColor);
            player.SetCardToDisplay(incomingAbilityParams.defensiveCard);
            player.HideDamageLable();
        }
    }

    private class PlayerPanel
    {
        TextMeshProUGUI actionLable = null;
        TextMeshProUGUI damageLable = null;
        Transform displayCardLocation = null;

        public void AttachElements(Transform transform)
        {
            actionLable = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            damageLable = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
            displayCardLocation = transform.GetChild(2);
        }

        public void SetAttackLabelText(string text)
        {
            if (actionLable)
            {
                actionLable.text = text;
            }
        }

        public void SetColor(Color color)
        {
            if (actionLable)
            {
                actionLable.color = color;
            }
            if (damageLable)
            {
                damageLable.color = color;
            }
        }

        public void SetDamageLabelText(string text)
        {
            if (damageLable)
            {
                damageLable.text = text;
            }
        }

        public void HideDamageLable()
        {
            if (damageLable)
            {
                damageLable.gameObject.SetActive(false);
            }
        }

        public void SetCardToDisplay(CardComponent card)
        {
            if (displayCardLocation)
            {
                foreach (Transform child in displayCardLocation)
                {
                    Destroy(child.gameObject);
                }

                card.transform.position = Vector3.zero;
                card.transform.localScale = new Vector3(190, 190, 190);
                card.transform.localRotation = Quaternion.identity;
                card.transform.SetParent(displayCardLocation, false);
            }
        }

        public void Hide()
        {
            if (actionLable && damageLable && displayCardLocation)
            {
                actionLable.gameObject.SetActive(false);
                damageLable.gameObject.SetActive(false);
                displayCardLocation.gameObject.SetActive(false);
            }
        }

        public void Show()
        {
            if (actionLable && damageLable && displayCardLocation)
            {
                actionLable.gameObject.SetActive(true);
                damageLable.gameObject.SetActive(true);
                displayCardLocation.gameObject.SetActive(true);
            }
        }

    }
}

public struct IncomingAbilityParams
{
    public bool IsPlayer1Attacker;
    public int incomingDamage;
    public CardComponent offensiveCard;
    public CardComponent defensiveCard;
}