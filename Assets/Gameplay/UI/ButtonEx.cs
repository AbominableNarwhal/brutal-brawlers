﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace SunEater
{
    public enum ButtonExEvent { ButtonUp, ButtonDown }

    public class ButtonEx : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField]
        private EventHander eventHander = null;

        public void OnPointerDown(PointerEventData eventData)
        {
            eventHander.Invoke(ButtonExEvent.ButtonDown, gameObject);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            eventHander.Invoke(ButtonExEvent.ButtonUp, gameObject);
        }

        public void OnDisable()
        {
            eventHander.Invoke(ButtonExEvent.ButtonUp, gameObject);
        }
    }

    [Serializable]
    public class EventHander : UnityEvent<ButtonExEvent, GameObject>
    { }
}
