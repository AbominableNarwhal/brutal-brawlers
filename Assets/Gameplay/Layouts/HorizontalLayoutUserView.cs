﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SunEater;

[ExecuteInEditMode]
public class HorizontalLayoutUserView : MonoBehaviour
{
    [SerializeField]
    private float slideSpeed = 0;
    [SerializeField]
    private Camera screenCamera = null;
    [SerializeField]
    private GameObject leftNavButton = null;
    [SerializeField]
    private GameObject rightNavButton = null;
    private float horizontalVelocity = 0;

    void Start()
    {
        if (leftNavButton == null || rightNavButton == null)
        {
            return;
        }

        leftNavButton.GetComponent<InputSystemReceiver>().GameInputEvent += NavButtonExEvent;
        rightNavButton.GetComponent<InputSystemReceiver>().GameInputEvent += NavButtonExEvent;
    }

    void FixedUpdate()
    {
        if (transform.childCount == 0 || leftNavButton == null || rightNavButton == null)
        {
            return;
        }
        RectTransform leftChild = transform.GetChild(0).GetComponent<RectTransform>();
        RectTransform rightChild = transform.GetChild(transform.childCount - 1).GetComponent<RectTransform>();

        bool leftActive = false;
        bool rightActive = false;
        if (leftChild != null && IsOffscreen(leftChild))
        {
            leftActive = true;
        }

        if (rightChild != null && IsOffscreen(rightChild))
        {
            rightActive = true;
        }

        leftNavButton.SetActive(leftActive);
        rightNavButton.SetActive(rightActive);

        transform.position += new Vector3(horizontalVelocity, 0, 0);
    }

    private bool IsOffscreen(RectTransform pos)
    {
        if (screenCamera == null)
        {
            return false;
        }

        Vector3[] v = new Vector3[4];
        pos.GetWorldCorners(v);
        var leftScreenPos = screenCamera.WorldToScreenPoint(v[1]);
        var rightScreenPos = screenCamera.WorldToScreenPoint(v[2]);

        if (leftScreenPos.x < 0 || leftScreenPos.x > Screen.width || rightScreenPos.x < 0 || rightScreenPos.x > Screen.width)
        {
            return true;
        }

        return false;
    }

    private void NavButtonExEvent(object subject, GameInputEventArgs args)
    {
        switch (args.EventType)
        {
            case GameInputEventArgs.PointerEvent.Down:
#pragma warning disable CS0252 // Possible unintended reference comparison; left hand side needs cast
                if (subject == leftNavButton)
#pragma warning restore CS0252 // Possible unintended reference comparison; left hand side needs cast
                {
                    horizontalVelocity = slideSpeed;
                }
#pragma warning disable CS0252 // Possible unintended reference comparison; left hand side needs cast
                else if (subject == rightNavButton)
#pragma warning restore CS0252 // Possible unintended reference comparison; left hand side needs cast
                {
                    horizontalVelocity = -slideSpeed;
                }
                break;
            case GameInputEventArgs.PointerEvent.Up:
                horizontalVelocity = 0;
                break;
        }
    }

}
