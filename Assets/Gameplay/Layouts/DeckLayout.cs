﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckLayout : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTransformChildrenChanged()
    {
        if(transform.childCount < 1)
        {
            return;
        }

        foreach (Transform child in transform)
        {
            if(child.gameObject.activeSelf)
            {
                child.gameObject.SetActive(false);
            }
            child.localPosition = Vector3.zero;
        }
    }
}
