﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalLayout : MonoBehaviour
{
    public enum CardFace { Up, Down, UnChanged }
    [SerializeField]
    private CardFace cardFacePosition;
    [SerializeField]
    private Vector3 childScale = Vector3.one;

    public bool centerLayout = true;
    public bool animateTransition = true;
    public int spacing = 0;

    public CardFace CardFacePosition
    {
        get { return cardFacePosition; }
        set { cardFacePosition = value; }
    }

    void OnTransformChildrenChanged()
    {
        if(transform.childCount < 1)
        {
            return;
        }
        
        float width = transform.GetChild(0).gameObject.GetComponent<RectTransform>().rect.width + spacing;
        float totalWidth = width * (transform.childCount - 1);
        int i = 0;

        RectTransform rectTransform = GetComponent<RectTransform>();
        if(rectTransform != null)
        {
            Vector2 extent = rectTransform.sizeDelta;
            extent.x = totalWidth + width;
            rectTransform.sizeDelta = extent;
        }

        foreach (Transform child in transform)
        {
            CardComponent anim = child.GetComponent<CardComponent>();
            if (anim == null)
            {
                continue;
            }

            if (!child.gameObject.activeSelf)
            {
                child.gameObject.SetActive(true);
            }


            switch (CardFacePosition)
            {
                case CardFace.Up:
                    anim.IsFaceUp = true;
                    break;
                case CardFace.Down:
                    anim.IsFaceUp = false;
                    break;
            }

            if (animateTransition)
            {
                anim.duration = 1;
            
                anim.target.position = GetPositionForChildAt(width, totalWidth, i);
                anim.target.rotation = Quaternion.identity;
                anim.target.scale = childScale;
                transform.GetComponentInParent<CardAnimator>().AddAnimation(anim);
            }
            else
            {
                child.localPosition = GetPositionForChildAt(width, totalWidth, i);
                child.localScale = childScale;
                child.localRotation = Quaternion.identity;
            }
            i++;
        }
        
    }
    private Vector3 GetPositionForChildAt(float width, float totalWidth, int index)
    {
        if(centerLayout)
        {
            return new Vector3(width * index - totalWidth / 2, 0, 0);
        }
        return new Vector3(width * index + width/2, 0, 0);
    }
}
