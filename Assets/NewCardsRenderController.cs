﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewCardsRenderController : MonoBehaviour
{
    [SerializeField]
    Card[] cards = null;

    // Start is called before the first frame update
    void Start()
    {
        foreach(Card card in cards)
        {
            CardFactory.CreateCardObject(card, transform);
        }
    }
}
