﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class FighterGameCardTests
    {
        Fighter fighterData;

        [SetUp]
        public void InitializeData()
        {
            fighterData = ScriptableObject.CreateInstance<Fighter>();
            fighterData.Hp = 500;
        }

        // A Test behaves as an ordinary method
        [Test]
        public void FighterConstructorTest()
        {
            // Use the Assert class to test conditions
            Assert.DoesNotThrow(delegate { new FighterGameCard(fighterData); });
        }

        [Test]
        public void BlockerTest()
        {
            var fighterGameCard = new FighterGameCard(fighterData);
            fighterGameCard.AddBlocker(50);
            fighterGameCard.AddBlocker(100);

            ReadOnlyCollection<int> blockers = fighterGameCard.Blockers;

            Assert.AreEqual(2, blockers.Count);
            Assert.AreEqual(100, blockers[0]);
            Assert.AreEqual(50, blockers[1]);

            int[] unbrokenBlockers = fighterGameCard.GetBlockersAfterDamage(0);

            Assert.AreEqual(2, unbrokenBlockers.Length);
            Assert.AreEqual(100, blockers[0]);
            Assert.AreEqual(50, blockers[1]);

            unbrokenBlockers = fighterGameCard.GetBlockersAfterDamage(50);

            Assert.AreEqual(1, unbrokenBlockers.Length);
            Assert.AreEqual(50, unbrokenBlockers[0]);

            unbrokenBlockers = fighterGameCard.GetBlockersAfterDamage(250);
            int hp = fighterGameCard.GetHpAfterDamage(250);

            Assert.AreEqual(0, unbrokenBlockers.Length);
            Assert.AreEqual(400, hp);

        }

        [Test]
        public void DealDamageTest()
        {
            var fighterGameCard = new FighterGameCard(fighterData);
            fighterGameCard.AddBlocker(50);
            fighterGameCard.AddBlocker(100);

            fighterGameCard.DealDamage(0);

            Assert.AreEqual(2, fighterGameCard.Blockers.Count);
            Assert.AreEqual(100, fighterGameCard.Blockers[0]);
            Assert.AreEqual(50, fighterGameCard.Blockers[1]);

            fighterGameCard.DealDamage(10);

            Assert.AreEqual(1, fighterGameCard.Blockers.Count);
            Assert.AreEqual(50, fighterGameCard.Blockers[0]);

            fighterGameCard.AddBlocker(100);
            fighterGameCard.DealDamage(250);

            Assert.AreEqual(0, fighterGameCard.Blockers.Count);
            Assert.AreEqual(400, fighterGameCard.Hp);
        }
    }
}
