﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using SunEater.Support;
using System.Collections.ObjectModel;

namespace Tests
{
    public class MultiSetTest
    {
        private class SetItem { }

        // A Test behaves as an ordinary method
        [Test]
        public void DefaultBehaviorTest()
        {
            var multiSet = new MultiSet<SetItem>();

            Assert.AreEqual(0, multiSet.UniqueCount, "Unique item count default value is wrong.");
            Assert.AreEqual(0, multiSet.Count, "Item count default value is wrong.");
        }

        [Test]
        public void AddTest()
        {
            var item1 = new SetItem();
            var multiSet = new MultiSet<SetItem>();

            multiSet.Add(item1);

            Assert.AreEqual(1, multiSet.UniqueCount, "UniqueCount was not incremented by Add.");
            Assert.AreEqual(1, multiSet.Count, "Count was not incremented by Add.");

            var item2 = new SetItem();

            multiSet.Add(item2);

            Assert.AreEqual(2, multiSet.UniqueCount, "UniqueCount was not incremented by Add.");
            Assert.AreEqual(2, multiSet.Count, "Count was not incremented by Add.");

            multiSet.Add(item1);

            Assert.AreEqual(2, multiSet.UniqueCount, "UniqueCount was incremented by non unique Add.");
            Assert.AreEqual(3, multiSet.Count, "Count was not incremented by Add.");
        }

        [Test]
        public void RemoveTest()
        {
            var item1 = new SetItem();
            var item2 = new SetItem();
            var multiSet = new MultiSet<SetItem>();

            Assert.DoesNotThrow(delegate { multiSet.Remove(item1); }, "Remove thows an exception.");

            // Add 3 items. 2 are unique
            multiSet.Add(item1);
            multiSet.Add(item1);
            multiSet.Add(item2);

            multiSet.Remove(item1);

            Assert.AreEqual(2, multiSet.UniqueCount, "UniqueCount changed after removing duplicate.");
            Assert.AreEqual(2, multiSet.Count, "Count not decremented after remove.");

            multiSet.Remove(item2);

            Assert.AreEqual(1, multiSet.UniqueCount, "UniqueCount not decrimented after removing non duplicate.");
            Assert.AreEqual(1, multiSet.Count, "Count not decremented after remove.");

            multiSet.Remove(item2);

            Assert.AreEqual(1, multiSet.UniqueCount, "UniqueCount changed after invalid Remove.");
            Assert.AreEqual(1, multiSet.Count, "Count changed after invalid Remove.");

            multiSet.Remove(item1);

            Assert.AreEqual(0, multiSet.UniqueCount, "UniqueCount not decrimented after removing non duplicate.");
            Assert.AreEqual(0, multiSet.Count, "Count not decremented after remove.");
        }

        [Test]
        public void ItemCountTest()
        {
            var item1 = new SetItem();
            var multiSet = new MultiSet<SetItem>();

            Assert.AreEqual(0, multiSet.ItemCount(item1), "An item was found in the set before it was added.");

            multiSet.Add(item1);

            Assert.AreEqual(1, multiSet.ItemCount(item1), "ItemCount was not incremented by Add.");

            multiSet.Add(item1);

            Assert.AreEqual(2, multiSet.ItemCount(item1), "ItemCount was not incremented by Add.");
        }

        [Test]
        public void ItemsTest()
        {
            var item1 = new SetItem();
            var item2 = new SetItem();
            var multiSet = new MultiSet<SetItem>();

            // Add 3 items. 2 are unique
            multiSet.Add(item1);
            multiSet.Add(item1);
            multiSet.Add(item2);

            ReadOnlyDictionary<SetItem, int> items = multiSet.GetReadOnlyItems();

            Assert.AreEqual(2, items[item1]);
            Assert.AreEqual(1, items[item2]);

        }
    }
}
