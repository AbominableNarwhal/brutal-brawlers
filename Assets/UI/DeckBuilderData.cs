﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

[CreateAssetMenu]
public class DeckBuilderData : ScriptableObject
{
    private bool deckDirty = true;
    private bool libraryDirty = true;

    [SerializeField]
    private List<CardComponent> library;
    [SerializeField]
    private Deck deck;

    public ReadOnlyCollection<CardComponent> Library
    {
        get;
        private set;
    }

    public Deck Deck
    {
        get
        {
            return deck;
        }
        set
        {
            deck = value;
        }
    }

    public bool DeckDirty
    {
        get
        {
            bool temp = deckDirty;
            deckDirty = false;
            return temp;
        }
    }
    public bool LibraryDirty
    {
        get
        {
            bool temp = libraryDirty;
            libraryDirty = false;
            return temp;
        }
    }

    public DeckBuilderData()
    {
        library = new List<CardComponent>();
        Library = library.AsReadOnly();
    }

    public void AddCardToLibrary(CardComponent card)
    {
        library.Add(card);
        libraryDirty = true;
    }

    public void AddCardToDeck(Card card)
    {
        deckDirty = true;
        if (card.type == Card.Type.FighterCard)
        {
            var fighter = card as Fighter;
            FillNextElement(deck.fighters, fighter);
        }
        else
        {
            Card cardData = card;
            FillNextElement(deck.cards, cardData);
        }
    }

    public void RemoveCardFromDeck(Card card)
    {
        deckDirty = true;

        if (card.type == Card.Type.FighterCard)
        {
            RemoveCard(deck.fighters, card);
        }
        else
        {
            RemoveCard(deck.cards, card);
        }
    }

    public void RemoveCardAt(int index, Card[] cards)
    {
        deckDirty = true;
        cards[index] = null;
    }

    public void InitializeDeck(int numOfCards, Deck _deck)
    {
        if (_deck == null)
        {
            InitializeDeck(numOfCards);
            return;
        }

        List<Card> cardList = new List<Card>(numOfCards);
        cardList.AddRange(_deck.cards);
        for (int i = 0; i < numOfCards - cardList.Count; i++)
        {
            cardList.Add(null);
        }

        deck = CreateInstance<Deck>();
        deck.fighters = _deck.fighters;
        deck.cards = cardList.ToArray();
        deckDirty = true;
    }

    public void InitializeDeck(int numOfCards)
    {
        deck = CreateInstance<Deck>();
        deck.fighters = new Fighter[3];
        deck.cards = new Card[numOfCards];
        deckDirty = true;
    }

    private void FillNextElement(Card[] arr, Card ele)
    {
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i] == null)
            {
                arr[i] = ele;
                break;
            }
        }
    }

    private void RemoveCard(Card[] cards, Card card)
    {
        for (int i = 0; i < cards.Length; i++)
        {
            if (card == cards[i])
            {
                cards[i] = null;
                break;
            }
        }
    }
}
