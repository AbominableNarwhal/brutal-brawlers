﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AbilityItemViewer : MonoBehaviour
{
    public delegate void AbilityButtonEvent(Ability ability);
    private Ability ability;
    [SerializeField]
    private TextMeshProUGUI abilityDesc = null;
    [SerializeField]
    private Image background = null;
    [SerializeField]
    private TextMeshProUGUI abilityName = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetAbilityData(Ability _ability)
    {
        if(_ability == null)
        {
            return;
        }
        abilityName.text = _ability.cardName;
        abilityDesc.text = _ability.FullDescription;
        // Resize the background to fit the ability description
        float totalHieght = background.rectTransform.rect.height + abilityDesc.preferredHeight;
        Vector2 extent = background.rectTransform.sizeDelta;
        extent.y = totalHieght;
        background.rectTransform.sizeDelta = extent;

        // Tell the layout group about the change
        LayoutElement layout = GetComponent<LayoutElement>();
        layout.preferredHeight = totalHieght;
    }
}
