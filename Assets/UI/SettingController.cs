﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SettingController : MonoBehaviour
{
    public static readonly string ServerIpPref = "Server Ip";
    public static readonly string ServerIpDefaultPref = "75.164.201.44";

    [SerializeField]
    private TMP_InputField IpAddressField = null;

    // Start is called before the first frame update
    void Start()
    {
        IpAddressField.text = PlayerPrefs.GetString(ServerIpPref, ServerIpDefaultPref);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SaveSettings()
    {
        PlayerPrefs.SetString(ServerIpPref, IpAddressField.text);
    }
}
