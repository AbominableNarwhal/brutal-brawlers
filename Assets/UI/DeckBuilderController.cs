﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System.Threading.Tasks;
using SunEater.Support;
using TMPro;
using UnityEngine.SceneManagement;

public class DeckBuilderController : MonoBehaviour
{
    public static readonly int MaxCardsInDeck = 40;

    [SerializeField]
    private DeckBuilderData deckBuilderData = null;
    [SerializeField]
    private TMP_InputField deckNameField = null;

    // Start is called before the first frame update
    void Start()
    {
        var gameState = FindObjectOfType<GameStateParameters>();
        deckBuilderData.InitializeDeck(MaxCardsInDeck, gameState.DeckToEdit);

        if (gameState.DeckName != null)
        {
            deckNameField.text = gameState.DeckName;
        }

        Addressables.LoadAssetsAsync<Card>("Card", AssetLoaded);
    }

    private void AssetLoaded(Card card)
    {
        if (card.cardPrefab != null)
        {
            deckBuilderData.AddCardToLibrary(CardFactory.CreateCardObject(card));
        }
    }

    public void AddCardToDeck(Card card)
    {
        deckBuilderData.AddCardToDeck(card);
    }

    public void RemoveCard(int index, Card[] cards)
    {
        deckBuilderData.RemoveCardAt(index, cards);
    }

    public void SaveDeck()
    {
        deckBuilderData.Deck.name = deckNameField.text;
        CardUtilities.SaveDeck(deckBuilderData.Deck);
    }

    public void Finish()
    {
        SceneManager.LoadScene("DeckSelection");
    }
}
