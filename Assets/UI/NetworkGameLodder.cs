﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SunEater.Support;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.AddressableAssets;
using System;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;

public class NetworkGameLodder : MonoBehaviour
{
    [SerializeField]
    private DeckData opponentDeckData = null;
    private Task<Deck> opponentDeckTask;
    private GameStateParameters gameState;

    private void Start()
    {
        if (GameServerClient.IsConnected)
        {
            gameState = FindObjectOfType<GameStateParameters>();
            
            GameServerClient.Write("deck", new DeckData(gameState.Player1.playerDeck));

            if (gameState.IsHost)
            {
                gameState.RandomSeed = (int)DateTime.Now.Ticks & 0x0000FFFF;
                var seedStr = gameState.RandomSeed.ToString();
                GameServerClient.WriteString("random-seed", seedStr);
            }
        }
    }

    // Update is called once per frame
    public void Update()
    {
        if (GameServerClient.IsConnected)
        {
            ServerMessage message = GameServerClient.Read();
            if (message != null)
            {
                if (message.Protocol == "random-seed")
                {
                    gameState.RandomSeed = int.Parse(message.Content);
                }
                else if (message.Protocol == "deck")
                {
                    opponentDeckData = message.DeserializeJsonMessage<DeckData>();
                    opponentDeckTask = opponentDeckData.CreateDeck();
                }
            }

            if (opponentDeckTask != null && opponentDeckTask.IsCompleted && gameState.RandomSeed > 1)
            {
                var player2 = ScriptableObject.CreateInstance<NetworkPlayer>();
                player2.playerDeck = opponentDeckTask.Result;
                gameState.Player2 = player2;

                SceneManager.LoadScene("CardGame");
            }
        }
    }
}
