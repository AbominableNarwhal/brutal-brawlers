﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LibraryViewController : MonoBehaviour
{
    private Transform contentArea;

    [SerializeField]
    private DeckBuilderData deckBuilderData = null;
    [SerializeField]
    private GameObject cardClickerPrefab = null;
    [SerializeField]
    private DeckBuilderController controller = null;

    // Start is called before the first frame update
    void Start()
    {
        contentArea = transform.GetChild(0).GetChild(0);
    }

    private void Update()
    {
        if (deckBuilderData.LibraryDirty)
        {
            foreach (Transform child in contentArea.transform)
            {
                Destroy(child.gameObject);
            }

            AddCardType(Card.Type.FighterCard);
            AddCardType(Card.Type.AbilityCard);
            AddCardType(Card.Type.EquipmentCard);
            AddCardType(Card.Type.PowerCard);
            AddCardType(Card.Type.AuraCard);
        }
    }

    private void AddCardType(Card.Type type)
    {
        foreach (var card in deckBuilderData.Library)
        {
            if (card != null && card.CardData.CardType == type)
            {
                var cardClicker = Instantiate(cardClickerPrefab, contentArea);
                var button = cardClicker.transform.GetChild(1).GetComponent<Button>();
                button.onClick.AddListener(delegate
                {
                    controller.AddCardToDeck(card.CardData.StaticData);
                });

                card.transform.SetParent(cardClicker.transform, false);
                card.transform.SetSiblingIndex(1);
            }
        }
    }
}
