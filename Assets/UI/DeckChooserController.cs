﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SunEater.Support;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class DeckChooserController : MonoBehaviour
{
    public static readonly string PlayerNamePref = "Player Name";

    private string selectedDeck;
    private GameStateParameters gameState;

    [SerializeField]
    private GameObject deckItemPrefab = null;
    [SerializeField]
    private Transform contentArea = null;
    [SerializeField]
    private TMP_InputField playerNameField = null;
    [SerializeField]
    private TextMeshProUGUI errorText = null;

    // Start is called before the first frame update
    void Start()
    {
        gameState = FindObjectOfType<GameStateParameters>();
        string[] deckNames = CardUtilities.GetSavedDeckNames();

        playerNameField.text = PlayerPrefs.GetString(PlayerNamePref, "");
        foreach (var deckName in deckNames)
        {
            var button = Instantiate(deckItemPrefab, contentArea).GetComponent<Button>();
            button.onClick.AddListener(delegate { SetSelectedDeck(button); });

            var buttonText = button.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            buttonText.text = deckName;
        }
    }

    public void SetSelectedDeck(Button button)
    {
        selectedDeck = button.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text;
        button.Select();
    }

    public void GoToSettings()
    {
        SceneManager.LoadScene("Settings");
    }

    public async void GoToDeckBuilder()
    {
        if (selectedDeck != null)
        {
            gameState.DeckToEdit = await CardUtilities.LoadDeckByName(selectedDeck);
        }
        else
        {
            gameState.DeckToEdit = null;
        }

        gameState.DeckName = selectedDeck;
        SceneManager.LoadScene("DeckBuilder");
    }

    public async void GoToLobby()
    {
        if (selectedDeck == null)
        {
            errorText.text = "Please choose a deck before starting a game";
            return;
        }
        if (playerNameField.text == "")
        {
            errorText.text = "Please choose a Name before starting a game";
            return;
        }

        PlayerPrefs.SetString("Player Name", playerNameField.text);
        Deck deck = await CardUtilities.LoadDeckByName(selectedDeck);

        gameState.Player1 = ScriptableObject.CreateInstance<HumanPlayer>();
        gameState.Player1.playerDeck = deck;
        gameState.PlayerName = playerNameField.text;

        SceneManager.LoadScene("Lobby");

    }

    public void CloseGame()
    {
        Application.Quit();
    }
}
