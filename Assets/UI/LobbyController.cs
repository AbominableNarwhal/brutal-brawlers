﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SunEater.Support;
using UnityEngine.SceneManagement;

public class LobbyController : MonoBehaviour
{
    private readonly double RefreshTime = 5000;

    [SerializeField]
    private GameObject lobbyPlayerItemPrefab = null;
    [SerializeField]
    private Transform contentManager = null;

    private TimeKeep timer;
    
    // Start is called before the first frame update
    void Start()
    {
        var gameState = FindObjectOfType<GameStateParameters>();
        GameServerClient.TryConnect(gameState.PlayerName, PlayerPrefs.GetString(SettingController.ServerIpPref, SettingController.ServerIpDefaultPref));
        timer = new TimeKeep();
    }

    // Update is called once per frame
    public void Update()
    {
        if (GameServerClient.IsConnected)
        {

            if (timer.IsTimeUp(RefreshTime))
            {
                GameServerClient.WriteString("get-lobby");
                timer.Reset();
            }

            ServerMessage message = GameServerClient.Read();
            if (message != null)
            {
                if (message.Protocol == "lobby")
                {
                    foreach (Transform child in contentManager)
                    {
                        Destroy(child.gameObject);
                    }

                    foreach (var playerName in message.Content.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        CreateLobbyPlayerItem(playerName);
                    }
                }
                else if (message.Protocol == "game-start")
                {
                    var gameState = FindObjectOfType<GameStateParameters>();
                    gameState.IsHost = message.Content == "host";
                    SceneManager.LoadScene("LoadNetworkGame");
                }
            }
        }
        
    }

    public void ChallengePlayer(string player)
    {
        if (GameServerClient.IsConnected)
        {
            Debug.Log("Attempting to challenge " + player);

            GameServerClient.WriteString("challenge", player);
        }
    }

    private void CreateLobbyPlayerItem(string name)
    {
        GameObject playerItem = Instantiate(lobbyPlayerItemPrefab, contentManager);

        playerItem.transform.Find("Player Name").GetComponent<TextMeshProUGUI>().text = name;

        var challengeButton = playerItem.transform.Find("Challenge Button").GetComponent<Button>();
        challengeButton.onClick.AddListener(delegate { ChallengePlayer(name); });
    }

    public void DisconnectFromLobby()
    {
        GameServerClient.TryDisconect();
    }
}
