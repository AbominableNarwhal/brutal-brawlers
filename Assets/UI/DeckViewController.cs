﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckViewController : MonoBehaviour
{
    private Transform contentArea;

    [SerializeField]
    private DeckBuilderData deckBuilderData = null;
    [SerializeField]
    private GameObject cardClickerPrefab = null;
    [SerializeField]
    private DeckBuilderController controller = null;

    // Start is called before the first frame update
    void Start()
    {
        contentArea = transform.GetChild(0).GetChild(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (deckBuilderData.DeckDirty)
        {
            foreach (Transform child in contentArea.transform)
            {
                Destroy(child.gameObject);
            }

            AddCards(deckBuilderData.Deck.fighters);
            AddCards(deckBuilderData.Deck.cards);
        }
    }

    private void AddCards(Card[] cards)
    {
        int i = 0;
        foreach (var card in cards)
        {
            var cardClicker = Instantiate(cardClickerPrefab, contentArea);
            if (card != null)
            {
                var button = cardClicker.transform.GetChild(1).GetComponent<Button>();

                int i2 = i;
                button.onClick.AddListener(delegate
                {
                    controller.RemoveCard(i2, cards);
                });

                CardComponent cardComponent = CardFactory.CreateCardObject(card, cardClicker.transform);
                cardComponent.transform.SetSiblingIndex(1);
            }
            else
            {
                cardClicker.transform.GetChild(0).gameObject.SetActive(true);
            }
            i++;
        }
    }
}
