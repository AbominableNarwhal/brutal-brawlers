﻿Shader "Custom/Card Stencil"
{
    Properties
    {
        [NoScaleOffset] _MainTex("Card Front", 2D) = "white" {}
        [NoScaleOffset]_CardBack("Card Back", 2D) = "white" {}
        [NoScaleOffset]_CardArt("Card Art", 2D) = "white" {}
        [NoScaleOffset]_StatBubble("Stat Bubble", 2D) = "white" {}
        _ShowMagic("Show Magic", Float) = 1
        _ShowTech("Show Tech", Float) = 1
        _ShowStrength("Show Strength", Float) = 1
        _ShowArt("Show Art", Float) = 0
        [NoScaleOffset]_cardBorder("Card Border", 2D) = "white" {}
        _borderColor("Border Color", Color) = (0, 0, 0, 1)
    }
        SubShader
        {
            Tags
            {
                "RenderPipeline" = "UniversalPipeline"
                "RenderType" = "Transparent"
                "Queue" = "Transparent+0"
            }
            
            Stencil {
            Ref 1
            Comp Equal
            Pass keep
            }

            Pass
            {
                Name "Universal Forward"
                Tags
                {
                    "LightMode" = "UniversalForward"
                }

            // Render State
            Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
            Cull Off
            ZTest LEqual
            ZWrite Off
            // ColorMask: <None>


            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            // Pragmas
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0
            #pragma multi_compile_fog
            #pragma multi_compile_instancing

            // Keywords
            #pragma multi_compile _ LIGHTMAP_ON
            #pragma multi_compile _ DIRLIGHTMAP_COMBINED
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            #pragma multi_compile _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS _ADDITIONAL_OFF
            #pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
            #pragma multi_compile _ _SHADOWS_SOFT
            #pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define VARYINGS_NEED_POSITION_WS 
            #define VARYINGS_NEED_NORMAL_WS
            #define VARYINGS_NEED_TANGENT_WS
            #define VARYINGS_NEED_TEXCOORD0
            #define VARYINGS_NEED_VIEWDIRECTION_WS
            #define VARYINGS_NEED_FOG_AND_VERTEX_LIGHT
            #define VARYINGS_NEED_CULLFACE
            #pragma multi_compile_instancing
            #define SHADERPASS_FORWARD


            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
            #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
            float _ShowMagic;
            float _ShowTech;
            float _ShowStrength;
            float _ShowArt;
            float4 _borderColor;
            CBUFFER_END
            TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;
            TEXTURE2D(_CardBack); SAMPLER(sampler_CardBack); float4 _CardBack_TexelSize;
            TEXTURE2D(_CardArt); SAMPLER(sampler_CardArt); float4 _CardArt_TexelSize;
            TEXTURE2D(_StatBubble); SAMPLER(sampler_StatBubble); float4 _StatBubble_TexelSize;
            TEXTURE2D(_cardBorder); SAMPLER(sampler_cardBorder); float4 _cardBorder_TexelSize;
            SAMPLER(_SampleTexture2D_6137F6C8_Sampler_3_Linear_Repeat);
            SAMPLER(SamplerState_Point_Repeat);
            SAMPLER(_SampleTexture2D_46230EA2_Sampler_3_Linear_Repeat);
            SAMPLER(_SampleTexture2D_469B015_Sampler_3_Linear_Repeat);
            SAMPLER(_SampleTexture2D_107D84F0_Sampler_3_Linear_Repeat);
            SAMPLER(_SampleTexture2D_EA89F198_Sampler_3_Linear_Repeat);
            SAMPLER(_SampleTexture2D_A0087B33_Sampler_3_Linear_Repeat);

            // Graph Functions

            void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
            {
                Out = A * B;
            }

            void Unity_Lerp_float4(float4 A, float4 B, float4 T, out float4 Out)
            {
                Out = lerp(A, B, T);
            }

            void Unity_Multiply_float(float2 A, float2 B, out float2 Out)
            {
                Out = A * B;
            }

            void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
            {
                Out = UV * Tiling + Offset;
            }

            void Unity_Rectangle_float(float2 UV, float Width, float Height, out float Out)
            {
                float2 d = abs(UV * 2 - 1) - float2(Width, Height);
                d = 1 - d / fwidth(d);
                Out = saturate(min(d.x, d.y));
            }

            void Unity_Multiply_float(float A, float B, out float Out)
            {
                Out = A * B;
            }

            void Unity_Add_float4(float4 A, float4 B, out float4 Out)
            {
                Out = A + B;
            }

            void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
            {
                Out = Predicate ? True : False;
            }

            void Unity_Multiply_float(float3 A, float3 B, out float3 Out)
            {
                Out = A * B;
            }

            void Unity_Branch_float3(float Predicate, float3 True, float3 False, out float3 Out)
            {
                Out = Predicate ? True : False;
            }

            // Graph Vertex
            // GraphVertex: <None>

            // Graph Pixel
            struct SurfaceDescriptionInputs
            {
                float3 TangentSpaceNormal;
                float4 uv0;
                float FaceSign;
            };

            struct SurfaceDescription
            {
                float3 Albedo;
                float3 Normal;
                float3 Emission;
                float Metallic;
                float Smoothness;
                float Occlusion;
                float Alpha;
                float AlphaClipThreshold;
            };

            SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
            {
                SurfaceDescription surface = (SurfaceDescription)0;
                float _IsFrontFace_17668B3E_Out_0 = max(0, IN.FaceSign);
                float4 _SampleTexture2D_6137F6C8_RGBA_0 = SAMPLE_TEXTURE2D(_cardBorder, sampler_cardBorder, IN.uv0.xy);
                float _SampleTexture2D_6137F6C8_R_4 = _SampleTexture2D_6137F6C8_RGBA_0.r;
                float _SampleTexture2D_6137F6C8_G_5 = _SampleTexture2D_6137F6C8_RGBA_0.g;
                float _SampleTexture2D_6137F6C8_B_6 = _SampleTexture2D_6137F6C8_RGBA_0.b;
                float _SampleTexture2D_6137F6C8_A_7 = _SampleTexture2D_6137F6C8_RGBA_0.a;
                float4 _Property_EA7892E9_Out_0 = _borderColor;
                float4 _Multiply_36449153_Out_2;
                Unity_Multiply_float(_SampleTexture2D_6137F6C8_RGBA_0, _Property_EA7892E9_Out_0, _Multiply_36449153_Out_2);
                float4 _SampleTexture2D_CCBE16EF_RGBA_0 = SAMPLE_TEXTURE2D(_MainTex, SamplerState_Point_Repeat, IN.uv0.xy);
                float _SampleTexture2D_CCBE16EF_R_4 = _SampleTexture2D_CCBE16EF_RGBA_0.r;
                float _SampleTexture2D_CCBE16EF_G_5 = _SampleTexture2D_CCBE16EF_RGBA_0.g;
                float _SampleTexture2D_CCBE16EF_B_6 = _SampleTexture2D_CCBE16EF_RGBA_0.b;
                float _SampleTexture2D_CCBE16EF_A_7 = _SampleTexture2D_CCBE16EF_RGBA_0.a;
                float4 _Lerp_36236AAE_Out_3;
                Unity_Lerp_float4(_Multiply_36449153_Out_2, _SampleTexture2D_CCBE16EF_RGBA_0, (_SampleTexture2D_CCBE16EF_A_7.xxxx), _Lerp_36236AAE_Out_3);
                float2 _Vector2_98B1B01D_Out_0 = float2(1.07, 2.126582);
                float2 _Multiply_F70CE493_Out_2;
                Unity_Multiply_float(_Vector2_98B1B01D_Out_0, float2(0.995, 0.995), _Multiply_F70CE493_Out_2);
                float2 _TilingAndOffset_823AD7C4_Out_3;
                Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_F70CE493_Out_2, float2 (-0.0334, -0.93), _TilingAndOffset_823AD7C4_Out_3);
                float4 _SampleTexture2D_46230EA2_RGBA_0 = SAMPLE_TEXTURE2D(_CardArt, sampler_CardArt, _TilingAndOffset_823AD7C4_Out_3);
                float _SampleTexture2D_46230EA2_R_4 = _SampleTexture2D_46230EA2_RGBA_0.r;
                float _SampleTexture2D_46230EA2_G_5 = _SampleTexture2D_46230EA2_RGBA_0.g;
                float _SampleTexture2D_46230EA2_B_6 = _SampleTexture2D_46230EA2_RGBA_0.b;
                float _SampleTexture2D_46230EA2_A_7 = _SampleTexture2D_46230EA2_RGBA_0.a;
                float _Property_B9987C17_Out_0 = _ShowArt;
                float2 _TilingAndOffset_E86983B6_Out_3;
                Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1.07, 2.126582), float2 (-0.0334, -0.93), _TilingAndOffset_E86983B6_Out_3);
                float _Rectangle_F201F009_Out_3;
                Unity_Rectangle_float(_TilingAndOffset_E86983B6_Out_3, 1, 1, _Rectangle_F201F009_Out_3);
                float _Multiply_FE7047C5_Out_2;
                Unity_Multiply_float(_Property_B9987C17_Out_0, _Rectangle_F201F009_Out_3, _Multiply_FE7047C5_Out_2);
                float4 _Lerp_C0A10AA4_Out_3;
                Unity_Lerp_float4(_Lerp_36236AAE_Out_3, _SampleTexture2D_46230EA2_RGBA_0, (_Multiply_FE7047C5_Out_2.xxxx), _Lerp_C0A10AA4_Out_3);
                float _Property_1EA691CF_Out_0 = _ShowMagic;
                float2 _TilingAndOffset_DF34649D_Out_3;
                Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -7.26), _TilingAndOffset_DF34649D_Out_3);
                float _Rectangle_3CD8FAA4_Out_3;
                Unity_Rectangle_float(_TilingAndOffset_DF34649D_Out_3, 1, 1, _Rectangle_3CD8FAA4_Out_3);
                float4 Color_2CD1002A = IsGammaSpace() ? float4(0.1568628, 0.6313726, 0.9215686, 1) : float4(SRGBToLinear(float3(0.1568628, 0.6313726, 0.9215686)), 1);
                float2 _Vector2_523D8D2_Out_0 = float2(6.67, 6.67);
                float2 _Multiply_2C2AEDC_Out_2;
                Unity_Multiply_float(_Vector2_523D8D2_Out_0, float2(1, 1.4), _Multiply_2C2AEDC_Out_2);
                float2 _TilingAndOffset_90E9E966_Out_3;
                Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -7.26), _TilingAndOffset_90E9E966_Out_3);
                float4 _SampleTexture2D_469B015_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_90E9E966_Out_3);
                float _SampleTexture2D_469B015_R_4 = _SampleTexture2D_469B015_RGBA_0.r;
                float _SampleTexture2D_469B015_G_5 = _SampleTexture2D_469B015_RGBA_0.g;
                float _SampleTexture2D_469B015_B_6 = _SampleTexture2D_469B015_RGBA_0.b;
                float _SampleTexture2D_469B015_A_7 = _SampleTexture2D_469B015_RGBA_0.a;
                float4 _Multiply_B7547173_Out_2;
                Unity_Multiply_float(Color_2CD1002A, _SampleTexture2D_469B015_RGBA_0, _Multiply_B7547173_Out_2);
                float4 _Multiply_33473234_Out_2;
                Unity_Multiply_float((_Rectangle_3CD8FAA4_Out_3.xxxx), _Multiply_B7547173_Out_2, _Multiply_33473234_Out_2);
                float4 _Multiply_E1E340BF_Out_2;
                Unity_Multiply_float((_Property_1EA691CF_Out_0.xxxx), _Multiply_33473234_Out_2, _Multiply_E1E340BF_Out_2);
                float _Property_52156452_Out_0 = _ShowTech;
                float2 _TilingAndOffset_F6346DAD_Out_3;
                Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -5.76), _TilingAndOffset_F6346DAD_Out_3);
                float _Rectangle_D5D764F8_Out_3;
                Unity_Rectangle_float(_TilingAndOffset_F6346DAD_Out_3, 1, 1, _Rectangle_D5D764F8_Out_3);
                float4 Color_551BDC44 = IsGammaSpace() ? float4(0.9215686, 0.5803922, 0.1098039, 1) : float4(SRGBToLinear(float3(0.9215686, 0.5803922, 0.1098039)), 1);
                float2 _TilingAndOffset_EAD3C75B_Out_3;
                Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -5.76), _TilingAndOffset_EAD3C75B_Out_3);
                float4 _SampleTexture2D_107D84F0_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_EAD3C75B_Out_3);
                float _SampleTexture2D_107D84F0_R_4 = _SampleTexture2D_107D84F0_RGBA_0.r;
                float _SampleTexture2D_107D84F0_G_5 = _SampleTexture2D_107D84F0_RGBA_0.g;
                float _SampleTexture2D_107D84F0_B_6 = _SampleTexture2D_107D84F0_RGBA_0.b;
                float _SampleTexture2D_107D84F0_A_7 = _SampleTexture2D_107D84F0_RGBA_0.a;
                float4 _Multiply_EFD52C29_Out_2;
                Unity_Multiply_float(Color_551BDC44, _SampleTexture2D_107D84F0_RGBA_0, _Multiply_EFD52C29_Out_2);
                float4 _Multiply_B1157814_Out_2;
                Unity_Multiply_float((_Rectangle_D5D764F8_Out_3.xxxx), _Multiply_EFD52C29_Out_2, _Multiply_B1157814_Out_2);
                float4 _Multiply_675F22C4_Out_2;
                Unity_Multiply_float((_Property_52156452_Out_0.xxxx), _Multiply_B1157814_Out_2, _Multiply_675F22C4_Out_2);
                float4 _Add_19AD9021_Out_2;
                Unity_Add_float4(_Multiply_E1E340BF_Out_2, _Multiply_675F22C4_Out_2, _Add_19AD9021_Out_2);
                float _Property_AD7AEA03_Out_0 = _ShowStrength;
                float2 _TilingAndOffset_73BA2981_Out_3;
                Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -4.26), _TilingAndOffset_73BA2981_Out_3);
                float _Rectangle_71D17C2A_Out_3;
                Unity_Rectangle_float(_TilingAndOffset_73BA2981_Out_3, 1, 1, _Rectangle_71D17C2A_Out_3);
                float4 Color_A97EC31D = IsGammaSpace() ? float4(1, 0.01960784, 0.2784314, 1) : float4(SRGBToLinear(float3(1, 0.01960784, 0.2784314)), 1);
                float2 _TilingAndOffset_440FAFA0_Out_3;
                Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -4.26), _TilingAndOffset_440FAFA0_Out_3);
                float4 _SampleTexture2D_EA89F198_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_440FAFA0_Out_3);
                float _SampleTexture2D_EA89F198_R_4 = _SampleTexture2D_EA89F198_RGBA_0.r;
                float _SampleTexture2D_EA89F198_G_5 = _SampleTexture2D_EA89F198_RGBA_0.g;
                float _SampleTexture2D_EA89F198_B_6 = _SampleTexture2D_EA89F198_RGBA_0.b;
                float _SampleTexture2D_EA89F198_A_7 = _SampleTexture2D_EA89F198_RGBA_0.a;
                float4 _Multiply_3A6B93F6_Out_2;
                Unity_Multiply_float(Color_A97EC31D, _SampleTexture2D_EA89F198_RGBA_0, _Multiply_3A6B93F6_Out_2);
                float4 _Multiply_FC6E49F_Out_2;
                Unity_Multiply_float((_Rectangle_71D17C2A_Out_3.xxxx), _Multiply_3A6B93F6_Out_2, _Multiply_FC6E49F_Out_2);
                float4 _Multiply_EAA1618_Out_2;
                Unity_Multiply_float((_Property_AD7AEA03_Out_0.xxxx), _Multiply_FC6E49F_Out_2, _Multiply_EAA1618_Out_2);
                float4 _Add_B4B310DE_Out_2;
                Unity_Add_float4(_Add_19AD9021_Out_2, _Multiply_EAA1618_Out_2, _Add_B4B310DE_Out_2);
                float _Split_681EAD45_R_1 = _Add_B4B310DE_Out_2[0];
                float _Split_681EAD45_G_2 = _Add_B4B310DE_Out_2[1];
                float _Split_681EAD45_B_3 = _Add_B4B310DE_Out_2[2];
                float _Split_681EAD45_A_4 = _Add_B4B310DE_Out_2[3];
                float4 _Lerp_2E898E9C_Out_3;
                Unity_Lerp_float4(_Lerp_C0A10AA4_Out_3, _Add_B4B310DE_Out_2, (_Split_681EAD45_A_4.xxxx), _Lerp_2E898E9C_Out_3);
                float4 _SampleTexture2D_A0087B33_RGBA_0 = SAMPLE_TEXTURE2D(_CardBack, sampler_CardBack, IN.uv0.xy);
                float _SampleTexture2D_A0087B33_R_4 = _SampleTexture2D_A0087B33_RGBA_0.r;
                float _SampleTexture2D_A0087B33_G_5 = _SampleTexture2D_A0087B33_RGBA_0.g;
                float _SampleTexture2D_A0087B33_B_6 = _SampleTexture2D_A0087B33_RGBA_0.b;
                float _SampleTexture2D_A0087B33_A_7 = _SampleTexture2D_A0087B33_RGBA_0.a;
                float4 _Branch_64B7BAAC_Out_3;
                Unity_Branch_float4(_IsFrontFace_17668B3E_Out_0, _Lerp_2E898E9C_Out_3, _SampleTexture2D_A0087B33_RGBA_0, _Branch_64B7BAAC_Out_3);
                float3 _Multiply_37A9881D_Out_2;
                Unity_Multiply_float(IN.TangentSpaceNormal, float3(-1, -1, -1), _Multiply_37A9881D_Out_2);
                float3 _Branch_6EFFB622_Out_3;
                Unity_Branch_float3(_IsFrontFace_17668B3E_Out_0, IN.TangentSpaceNormal, _Multiply_37A9881D_Out_2, _Branch_6EFFB622_Out_3);
                float _Split_4A24BEA_R_1 = _Branch_64B7BAAC_Out_3[0];
                float _Split_4A24BEA_G_2 = _Branch_64B7BAAC_Out_3[1];
                float _Split_4A24BEA_B_3 = _Branch_64B7BAAC_Out_3[2];
                float _Split_4A24BEA_A_4 = _Branch_64B7BAAC_Out_3[3];
                surface.Albedo = (_Branch_64B7BAAC_Out_3.xyz);
                surface.Normal = _Branch_6EFFB622_Out_3;
                surface.Emission = IsGammaSpace() ? float3(0, 0, 0) : SRGBToLinear(float3(0, 0, 0));
                surface.Metallic = 0;
                surface.Smoothness = 0.5;
                surface.Occlusion = 1;
                surface.Alpha = _Split_4A24BEA_A_4;
                surface.AlphaClipThreshold = 0;
                return surface;
            }

            // --------------------------------------------------
            // Structs and Packing

            // Generated Type: Attributes
            struct Attributes
            {
                float3 positionOS : POSITION;
                float3 normalOS : NORMAL;
                float4 tangentOS : TANGENT;
                float4 uv0 : TEXCOORD0;
                float4 uv1 : TEXCOORD1;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : INSTANCEID_SEMANTIC;
                #endif
            };

            // Generated Type: Varyings
            struct Varyings
            {
                float4 positionCS : SV_POSITION;
                float3 positionWS;
                float3 normalWS;
                float4 tangentWS;
                float4 texCoord0;
                float3 viewDirectionWS;
                #if defined(LIGHTMAP_ON)
                float2 lightmapUV;
                #endif
                #if !defined(LIGHTMAP_ON)
                float3 sh;
                #endif
                float4 fogFactorAndVertexLight;
                float4 shadowCoord;
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };

            // Generated Type: PackedVaryings
            struct PackedVaryings
            {
                float4 positionCS : SV_POSITION;
                #if defined(LIGHTMAP_ON)
                #endif
                #if !defined(LIGHTMAP_ON)
                #endif
                #if UNITY_ANY_INSTANCING_ENABLED
                uint instanceID : CUSTOM_INSTANCE_ID;
                #endif
                float3 interp00 : TEXCOORD0;
                float3 interp01 : TEXCOORD1;
                float4 interp02 : TEXCOORD2;
                float4 interp03 : TEXCOORD3;
                float3 interp04 : TEXCOORD4;
                float2 interp05 : TEXCOORD5;
                float3 interp06 : TEXCOORD6;
                float4 interp07 : TEXCOORD7;
                float4 interp08 : TEXCOORD8;
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                #endif
            };

            // Packed Type: Varyings
            PackedVaryings PackVaryings(Varyings input)
            {
                PackedVaryings output = (PackedVaryings)0;
                output.positionCS = input.positionCS;
                output.interp00.xyz = input.positionWS;
                output.interp01.xyz = input.normalWS;
                output.interp02.xyzw = input.tangentWS;
                output.interp03.xyzw = input.texCoord0;
                output.interp04.xyz = input.viewDirectionWS;
                #if defined(LIGHTMAP_ON)
                output.interp05.xy = input.lightmapUV;
                #endif
                #if !defined(LIGHTMAP_ON)
                output.interp06.xyz = input.sh;
                #endif
                output.interp07.xyzw = input.fogFactorAndVertexLight;
                output.interp08.xyzw = input.shadowCoord;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }

            // Unpacked Type: Varyings
            Varyings UnpackVaryings(PackedVaryings input)
            {
                Varyings output = (Varyings)0;
                output.positionCS = input.positionCS;
                output.positionWS = input.interp00.xyz;
                output.normalWS = input.interp01.xyz;
                output.tangentWS = input.interp02.xyzw;
                output.texCoord0 = input.interp03.xyzw;
                output.viewDirectionWS = input.interp04.xyz;
                #if defined(LIGHTMAP_ON)
                output.lightmapUV = input.interp05.xy;
                #endif
                #if !defined(LIGHTMAP_ON)
                output.sh = input.interp06.xyz;
                #endif
                output.fogFactorAndVertexLight = input.interp07.xyzw;
                output.shadowCoord = input.interp08.xyzw;
                #if UNITY_ANY_INSTANCING_ENABLED
                output.instanceID = input.instanceID;
                #endif
                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                #endif
                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                #endif
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                output.cullFace = input.cullFace;
                #endif
                return output;
            }

            // --------------------------------------------------
            // Build Graph Inputs

            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
            {
                SurfaceDescriptionInputs output;
                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);



                output.TangentSpaceNormal = float3(0.0f, 0.0f, 1.0f);


                output.uv0 = input.texCoord0;
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
            #else
            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            #endif
                BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
            #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

                return output;
            }


            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBRForwardPass.hlsl"

            ENDHLSL
        }

        Pass
        {
            Name "ShadowCaster"
            Tags
            {
                "LightMode" = "ShadowCaster"
            }

                // Render State
                Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
                Cull Off
                ZTest LEqual
                ZWrite On
                // ColorMask: <None>


                HLSLPROGRAM
                #pragma vertex vert
                #pragma fragment frag

                // Debug
                // <None>

                // --------------------------------------------------
                // Pass

                // Pragmas
                #pragma prefer_hlslcc gles
                #pragma exclude_renderers d3d11_9x
                #pragma target 2.0
                #pragma multi_compile_instancing

                // Keywords
                // PassKeywords: <None>
                // GraphKeywords: <None>

                // Defines
                #define _SURFACE_TYPE_TRANSPARENT 1
                #define _NORMALMAP 1
                #define _NORMAL_DROPOFF_TS 1
                #define ATTRIBUTES_NEED_NORMAL
                #define ATTRIBUTES_NEED_TANGENT
                #define ATTRIBUTES_NEED_TEXCOORD0
                #define VARYINGS_NEED_TEXCOORD0
                #define VARYINGS_NEED_CULLFACE
                #pragma multi_compile_instancing
                #define SHADERPASS_SHADOWCASTER


                // Includes
                #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
                #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

                // --------------------------------------------------
                // Graph

                // Graph Properties
                CBUFFER_START(UnityPerMaterial)
                float _ShowMagic;
                float _ShowTech;
                float _ShowStrength;
                float _ShowArt;
                float4 _borderColor;
                CBUFFER_END
                TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;
                TEXTURE2D(_CardBack); SAMPLER(sampler_CardBack); float4 _CardBack_TexelSize;
                TEXTURE2D(_CardArt); SAMPLER(sampler_CardArt); float4 _CardArt_TexelSize;
                TEXTURE2D(_StatBubble); SAMPLER(sampler_StatBubble); float4 _StatBubble_TexelSize;
                TEXTURE2D(_cardBorder); SAMPLER(sampler_cardBorder); float4 _cardBorder_TexelSize;
                SAMPLER(_SampleTexture2D_6137F6C8_Sampler_3_Linear_Repeat);
                SAMPLER(SamplerState_Point_Repeat);
                SAMPLER(_SampleTexture2D_46230EA2_Sampler_3_Linear_Repeat);
                SAMPLER(_SampleTexture2D_469B015_Sampler_3_Linear_Repeat);
                SAMPLER(_SampleTexture2D_107D84F0_Sampler_3_Linear_Repeat);
                SAMPLER(_SampleTexture2D_EA89F198_Sampler_3_Linear_Repeat);
                SAMPLER(_SampleTexture2D_A0087B33_Sampler_3_Linear_Repeat);

                // Graph Functions

                void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
                {
                    Out = A * B;
                }

                void Unity_Lerp_float4(float4 A, float4 B, float4 T, out float4 Out)
                {
                    Out = lerp(A, B, T);
                }

                void Unity_Multiply_float(float2 A, float2 B, out float2 Out)
                {
                    Out = A * B;
                }

                void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
                {
                    Out = UV * Tiling + Offset;
                }

                void Unity_Rectangle_float(float2 UV, float Width, float Height, out float Out)
                {
                    float2 d = abs(UV * 2 - 1) - float2(Width, Height);
                    d = 1 - d / fwidth(d);
                    Out = saturate(min(d.x, d.y));
                }

                void Unity_Multiply_float(float A, float B, out float Out)
                {
                    Out = A * B;
                }

                void Unity_Add_float4(float4 A, float4 B, out float4 Out)
                {
                    Out = A + B;
                }

                void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
                {
                    Out = Predicate ? True : False;
                }

                // Graph Vertex
                // GraphVertex: <None>

                // Graph Pixel
                struct SurfaceDescriptionInputs
                {
                    float4 uv0;
                    float FaceSign;
                };

                struct SurfaceDescription
                {
                    float Alpha;
                    float AlphaClipThreshold;
                };

                SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                {
                    SurfaceDescription surface = (SurfaceDescription)0;
                    float _IsFrontFace_17668B3E_Out_0 = max(0, IN.FaceSign);
                    float4 _SampleTexture2D_6137F6C8_RGBA_0 = SAMPLE_TEXTURE2D(_cardBorder, sampler_cardBorder, IN.uv0.xy);
                    float _SampleTexture2D_6137F6C8_R_4 = _SampleTexture2D_6137F6C8_RGBA_0.r;
                    float _SampleTexture2D_6137F6C8_G_5 = _SampleTexture2D_6137F6C8_RGBA_0.g;
                    float _SampleTexture2D_6137F6C8_B_6 = _SampleTexture2D_6137F6C8_RGBA_0.b;
                    float _SampleTexture2D_6137F6C8_A_7 = _SampleTexture2D_6137F6C8_RGBA_0.a;
                    float4 _Property_EA7892E9_Out_0 = _borderColor;
                    float4 _Multiply_36449153_Out_2;
                    Unity_Multiply_float(_SampleTexture2D_6137F6C8_RGBA_0, _Property_EA7892E9_Out_0, _Multiply_36449153_Out_2);
                    float4 _SampleTexture2D_CCBE16EF_RGBA_0 = SAMPLE_TEXTURE2D(_MainTex, SamplerState_Point_Repeat, IN.uv0.xy);
                    float _SampleTexture2D_CCBE16EF_R_4 = _SampleTexture2D_CCBE16EF_RGBA_0.r;
                    float _SampleTexture2D_CCBE16EF_G_5 = _SampleTexture2D_CCBE16EF_RGBA_0.g;
                    float _SampleTexture2D_CCBE16EF_B_6 = _SampleTexture2D_CCBE16EF_RGBA_0.b;
                    float _SampleTexture2D_CCBE16EF_A_7 = _SampleTexture2D_CCBE16EF_RGBA_0.a;
                    float4 _Lerp_36236AAE_Out_3;
                    Unity_Lerp_float4(_Multiply_36449153_Out_2, _SampleTexture2D_CCBE16EF_RGBA_0, (_SampleTexture2D_CCBE16EF_A_7.xxxx), _Lerp_36236AAE_Out_3);
                    float2 _Vector2_98B1B01D_Out_0 = float2(1.07, 2.126582);
                    float2 _Multiply_F70CE493_Out_2;
                    Unity_Multiply_float(_Vector2_98B1B01D_Out_0, float2(0.995, 0.995), _Multiply_F70CE493_Out_2);
                    float2 _TilingAndOffset_823AD7C4_Out_3;
                    Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_F70CE493_Out_2, float2 (-0.0334, -0.93), _TilingAndOffset_823AD7C4_Out_3);
                    float4 _SampleTexture2D_46230EA2_RGBA_0 = SAMPLE_TEXTURE2D(_CardArt, sampler_CardArt, _TilingAndOffset_823AD7C4_Out_3);
                    float _SampleTexture2D_46230EA2_R_4 = _SampleTexture2D_46230EA2_RGBA_0.r;
                    float _SampleTexture2D_46230EA2_G_5 = _SampleTexture2D_46230EA2_RGBA_0.g;
                    float _SampleTexture2D_46230EA2_B_6 = _SampleTexture2D_46230EA2_RGBA_0.b;
                    float _SampleTexture2D_46230EA2_A_7 = _SampleTexture2D_46230EA2_RGBA_0.a;
                    float _Property_B9987C17_Out_0 = _ShowArt;
                    float2 _TilingAndOffset_E86983B6_Out_3;
                    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1.07, 2.126582), float2 (-0.0334, -0.93), _TilingAndOffset_E86983B6_Out_3);
                    float _Rectangle_F201F009_Out_3;
                    Unity_Rectangle_float(_TilingAndOffset_E86983B6_Out_3, 1, 1, _Rectangle_F201F009_Out_3);
                    float _Multiply_FE7047C5_Out_2;
                    Unity_Multiply_float(_Property_B9987C17_Out_0, _Rectangle_F201F009_Out_3, _Multiply_FE7047C5_Out_2);
                    float4 _Lerp_C0A10AA4_Out_3;
                    Unity_Lerp_float4(_Lerp_36236AAE_Out_3, _SampleTexture2D_46230EA2_RGBA_0, (_Multiply_FE7047C5_Out_2.xxxx), _Lerp_C0A10AA4_Out_3);
                    float _Property_1EA691CF_Out_0 = _ShowMagic;
                    float2 _TilingAndOffset_DF34649D_Out_3;
                    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -7.26), _TilingAndOffset_DF34649D_Out_3);
                    float _Rectangle_3CD8FAA4_Out_3;
                    Unity_Rectangle_float(_TilingAndOffset_DF34649D_Out_3, 1, 1, _Rectangle_3CD8FAA4_Out_3);
                    float4 Color_2CD1002A = IsGammaSpace() ? float4(0.1568628, 0.6313726, 0.9215686, 1) : float4(SRGBToLinear(float3(0.1568628, 0.6313726, 0.9215686)), 1);
                    float2 _Vector2_523D8D2_Out_0 = float2(6.67, 6.67);
                    float2 _Multiply_2C2AEDC_Out_2;
                    Unity_Multiply_float(_Vector2_523D8D2_Out_0, float2(1, 1.4), _Multiply_2C2AEDC_Out_2);
                    float2 _TilingAndOffset_90E9E966_Out_3;
                    Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -7.26), _TilingAndOffset_90E9E966_Out_3);
                    float4 _SampleTexture2D_469B015_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_90E9E966_Out_3);
                    float _SampleTexture2D_469B015_R_4 = _SampleTexture2D_469B015_RGBA_0.r;
                    float _SampleTexture2D_469B015_G_5 = _SampleTexture2D_469B015_RGBA_0.g;
                    float _SampleTexture2D_469B015_B_6 = _SampleTexture2D_469B015_RGBA_0.b;
                    float _SampleTexture2D_469B015_A_7 = _SampleTexture2D_469B015_RGBA_0.a;
                    float4 _Multiply_B7547173_Out_2;
                    Unity_Multiply_float(Color_2CD1002A, _SampleTexture2D_469B015_RGBA_0, _Multiply_B7547173_Out_2);
                    float4 _Multiply_33473234_Out_2;
                    Unity_Multiply_float((_Rectangle_3CD8FAA4_Out_3.xxxx), _Multiply_B7547173_Out_2, _Multiply_33473234_Out_2);
                    float4 _Multiply_E1E340BF_Out_2;
                    Unity_Multiply_float((_Property_1EA691CF_Out_0.xxxx), _Multiply_33473234_Out_2, _Multiply_E1E340BF_Out_2);
                    float _Property_52156452_Out_0 = _ShowTech;
                    float2 _TilingAndOffset_F6346DAD_Out_3;
                    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -5.76), _TilingAndOffset_F6346DAD_Out_3);
                    float _Rectangle_D5D764F8_Out_3;
                    Unity_Rectangle_float(_TilingAndOffset_F6346DAD_Out_3, 1, 1, _Rectangle_D5D764F8_Out_3);
                    float4 Color_551BDC44 = IsGammaSpace() ? float4(0.9215686, 0.5803922, 0.1098039, 1) : float4(SRGBToLinear(float3(0.9215686, 0.5803922, 0.1098039)), 1);
                    float2 _TilingAndOffset_EAD3C75B_Out_3;
                    Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -5.76), _TilingAndOffset_EAD3C75B_Out_3);
                    float4 _SampleTexture2D_107D84F0_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_EAD3C75B_Out_3);
                    float _SampleTexture2D_107D84F0_R_4 = _SampleTexture2D_107D84F0_RGBA_0.r;
                    float _SampleTexture2D_107D84F0_G_5 = _SampleTexture2D_107D84F0_RGBA_0.g;
                    float _SampleTexture2D_107D84F0_B_6 = _SampleTexture2D_107D84F0_RGBA_0.b;
                    float _SampleTexture2D_107D84F0_A_7 = _SampleTexture2D_107D84F0_RGBA_0.a;
                    float4 _Multiply_EFD52C29_Out_2;
                    Unity_Multiply_float(Color_551BDC44, _SampleTexture2D_107D84F0_RGBA_0, _Multiply_EFD52C29_Out_2);
                    float4 _Multiply_B1157814_Out_2;
                    Unity_Multiply_float((_Rectangle_D5D764F8_Out_3.xxxx), _Multiply_EFD52C29_Out_2, _Multiply_B1157814_Out_2);
                    float4 _Multiply_675F22C4_Out_2;
                    Unity_Multiply_float((_Property_52156452_Out_0.xxxx), _Multiply_B1157814_Out_2, _Multiply_675F22C4_Out_2);
                    float4 _Add_19AD9021_Out_2;
                    Unity_Add_float4(_Multiply_E1E340BF_Out_2, _Multiply_675F22C4_Out_2, _Add_19AD9021_Out_2);
                    float _Property_AD7AEA03_Out_0 = _ShowStrength;
                    float2 _TilingAndOffset_73BA2981_Out_3;
                    Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -4.26), _TilingAndOffset_73BA2981_Out_3);
                    float _Rectangle_71D17C2A_Out_3;
                    Unity_Rectangle_float(_TilingAndOffset_73BA2981_Out_3, 1, 1, _Rectangle_71D17C2A_Out_3);
                    float4 Color_A97EC31D = IsGammaSpace() ? float4(1, 0.01960784, 0.2784314, 1) : float4(SRGBToLinear(float3(1, 0.01960784, 0.2784314)), 1);
                    float2 _TilingAndOffset_440FAFA0_Out_3;
                    Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -4.26), _TilingAndOffset_440FAFA0_Out_3);
                    float4 _SampleTexture2D_EA89F198_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_440FAFA0_Out_3);
                    float _SampleTexture2D_EA89F198_R_4 = _SampleTexture2D_EA89F198_RGBA_0.r;
                    float _SampleTexture2D_EA89F198_G_5 = _SampleTexture2D_EA89F198_RGBA_0.g;
                    float _SampleTexture2D_EA89F198_B_6 = _SampleTexture2D_EA89F198_RGBA_0.b;
                    float _SampleTexture2D_EA89F198_A_7 = _SampleTexture2D_EA89F198_RGBA_0.a;
                    float4 _Multiply_3A6B93F6_Out_2;
                    Unity_Multiply_float(Color_A97EC31D, _SampleTexture2D_EA89F198_RGBA_0, _Multiply_3A6B93F6_Out_2);
                    float4 _Multiply_FC6E49F_Out_2;
                    Unity_Multiply_float((_Rectangle_71D17C2A_Out_3.xxxx), _Multiply_3A6B93F6_Out_2, _Multiply_FC6E49F_Out_2);
                    float4 _Multiply_EAA1618_Out_2;
                    Unity_Multiply_float((_Property_AD7AEA03_Out_0.xxxx), _Multiply_FC6E49F_Out_2, _Multiply_EAA1618_Out_2);
                    float4 _Add_B4B310DE_Out_2;
                    Unity_Add_float4(_Add_19AD9021_Out_2, _Multiply_EAA1618_Out_2, _Add_B4B310DE_Out_2);
                    float _Split_681EAD45_R_1 = _Add_B4B310DE_Out_2[0];
                    float _Split_681EAD45_G_2 = _Add_B4B310DE_Out_2[1];
                    float _Split_681EAD45_B_3 = _Add_B4B310DE_Out_2[2];
                    float _Split_681EAD45_A_4 = _Add_B4B310DE_Out_2[3];
                    float4 _Lerp_2E898E9C_Out_3;
                    Unity_Lerp_float4(_Lerp_C0A10AA4_Out_3, _Add_B4B310DE_Out_2, (_Split_681EAD45_A_4.xxxx), _Lerp_2E898E9C_Out_3);
                    float4 _SampleTexture2D_A0087B33_RGBA_0 = SAMPLE_TEXTURE2D(_CardBack, sampler_CardBack, IN.uv0.xy);
                    float _SampleTexture2D_A0087B33_R_4 = _SampleTexture2D_A0087B33_RGBA_0.r;
                    float _SampleTexture2D_A0087B33_G_5 = _SampleTexture2D_A0087B33_RGBA_0.g;
                    float _SampleTexture2D_A0087B33_B_6 = _SampleTexture2D_A0087B33_RGBA_0.b;
                    float _SampleTexture2D_A0087B33_A_7 = _SampleTexture2D_A0087B33_RGBA_0.a;
                    float4 _Branch_64B7BAAC_Out_3;
                    Unity_Branch_float4(_IsFrontFace_17668B3E_Out_0, _Lerp_2E898E9C_Out_3, _SampleTexture2D_A0087B33_RGBA_0, _Branch_64B7BAAC_Out_3);
                    float _Split_4A24BEA_R_1 = _Branch_64B7BAAC_Out_3[0];
                    float _Split_4A24BEA_G_2 = _Branch_64B7BAAC_Out_3[1];
                    float _Split_4A24BEA_B_3 = _Branch_64B7BAAC_Out_3[2];
                    float _Split_4A24BEA_A_4 = _Branch_64B7BAAC_Out_3[3];
                    surface.Alpha = _Split_4A24BEA_A_4;
                    surface.AlphaClipThreshold = 0;
                    return surface;
                }

                // --------------------------------------------------
                // Structs and Packing

                // Generated Type: Attributes
                struct Attributes
                {
                    float3 positionOS : POSITION;
                    float3 normalOS : NORMAL;
                    float4 tangentOS : TANGENT;
                    float4 uv0 : TEXCOORD0;
                    #if UNITY_ANY_INSTANCING_ENABLED
                    uint instanceID : INSTANCEID_SEMANTIC;
                    #endif
                };

                // Generated Type: Varyings
                struct Varyings
                {
                    float4 positionCS : SV_POSITION;
                    float4 texCoord0;
                    #if UNITY_ANY_INSTANCING_ENABLED
                    uint instanceID : CUSTOM_INSTANCE_ID;
                    #endif
                    #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                    uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                    #endif
                    #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                    uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                    #endif
                    #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                    FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                    #endif
                };

                // Generated Type: PackedVaryings
                struct PackedVaryings
                {
                    float4 positionCS : SV_POSITION;
                    #if UNITY_ANY_INSTANCING_ENABLED
                    uint instanceID : CUSTOM_INSTANCE_ID;
                    #endif
                    float4 interp00 : TEXCOORD0;
                    #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                    uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                    #endif
                    #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                    uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                    #endif
                    #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                    FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                    #endif
                };

                // Packed Type: Varyings
                PackedVaryings PackVaryings(Varyings input)
                {
                    PackedVaryings output = (PackedVaryings)0;
                    output.positionCS = input.positionCS;
                    output.interp00.xyzw = input.texCoord0;
                    #if UNITY_ANY_INSTANCING_ENABLED
                    output.instanceID = input.instanceID;
                    #endif
                    #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                    output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                    #endif
                    #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                    output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                    #endif
                    #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                    output.cullFace = input.cullFace;
                    #endif
                    return output;
                }

                // Unpacked Type: Varyings
                Varyings UnpackVaryings(PackedVaryings input)
                {
                    Varyings output = (Varyings)0;
                    output.positionCS = input.positionCS;
                    output.texCoord0 = input.interp00.xyzw;
                    #if UNITY_ANY_INSTANCING_ENABLED
                    output.instanceID = input.instanceID;
                    #endif
                    #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                    output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                    #endif
                    #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                    output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                    #endif
                    #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                    output.cullFace = input.cullFace;
                    #endif
                    return output;
                }

                // --------------------------------------------------
                // Build Graph Inputs

                SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
                {
                    SurfaceDescriptionInputs output;
                    ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





                    output.uv0 = input.texCoord0;
                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
                #else
                #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
                #endif
                    BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
                #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

                    return output;
                }


                // --------------------------------------------------
                // Main

                #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShadowCasterPass.hlsl"

                ENDHLSL
            }

            Pass
            {
                Name "DepthOnly"
                Tags
                {
                    "LightMode" = "DepthOnly"
                }

                    // Render State
                    Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
                    Cull Off
                    ZTest LEqual
                    ZWrite On
                    ColorMask 0


                    HLSLPROGRAM
                    #pragma vertex vert
                    #pragma fragment frag

                    // Debug
                    // <None>

                    // --------------------------------------------------
                    // Pass

                    // Pragmas
                    #pragma prefer_hlslcc gles
                    #pragma exclude_renderers d3d11_9x
                    #pragma target 2.0
                    #pragma multi_compile_instancing

                    // Keywords
                    // PassKeywords: <None>
                    // GraphKeywords: <None>

                    // Defines
                    #define _SURFACE_TYPE_TRANSPARENT 1
                    #define _NORMALMAP 1
                    #define _NORMAL_DROPOFF_TS 1
                    #define ATTRIBUTES_NEED_NORMAL
                    #define ATTRIBUTES_NEED_TANGENT
                    #define ATTRIBUTES_NEED_TEXCOORD0
                    #define VARYINGS_NEED_TEXCOORD0
                    #define VARYINGS_NEED_CULLFACE
                    #pragma multi_compile_instancing
                    #define SHADERPASS_DEPTHONLY


                    // Includes
                    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
                    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
                    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
                    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
                    #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

                    // --------------------------------------------------
                    // Graph

                    // Graph Properties
                    CBUFFER_START(UnityPerMaterial)
                    float _ShowMagic;
                    float _ShowTech;
                    float _ShowStrength;
                    float _ShowArt;
                    float4 _borderColor;
                    CBUFFER_END
                    TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;
                    TEXTURE2D(_CardBack); SAMPLER(sampler_CardBack); float4 _CardBack_TexelSize;
                    TEXTURE2D(_CardArt); SAMPLER(sampler_CardArt); float4 _CardArt_TexelSize;
                    TEXTURE2D(_StatBubble); SAMPLER(sampler_StatBubble); float4 _StatBubble_TexelSize;
                    TEXTURE2D(_cardBorder); SAMPLER(sampler_cardBorder); float4 _cardBorder_TexelSize;
                    SAMPLER(_SampleTexture2D_6137F6C8_Sampler_3_Linear_Repeat);
                    SAMPLER(SamplerState_Point_Repeat);
                    SAMPLER(_SampleTexture2D_46230EA2_Sampler_3_Linear_Repeat);
                    SAMPLER(_SampleTexture2D_469B015_Sampler_3_Linear_Repeat);
                    SAMPLER(_SampleTexture2D_107D84F0_Sampler_3_Linear_Repeat);
                    SAMPLER(_SampleTexture2D_EA89F198_Sampler_3_Linear_Repeat);
                    SAMPLER(_SampleTexture2D_A0087B33_Sampler_3_Linear_Repeat);

                    // Graph Functions

                    void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
                    {
                        Out = A * B;
                    }

                    void Unity_Lerp_float4(float4 A, float4 B, float4 T, out float4 Out)
                    {
                        Out = lerp(A, B, T);
                    }

                    void Unity_Multiply_float(float2 A, float2 B, out float2 Out)
                    {
                        Out = A * B;
                    }

                    void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
                    {
                        Out = UV * Tiling + Offset;
                    }

                    void Unity_Rectangle_float(float2 UV, float Width, float Height, out float Out)
                    {
                        float2 d = abs(UV * 2 - 1) - float2(Width, Height);
                        d = 1 - d / fwidth(d);
                        Out = saturate(min(d.x, d.y));
                    }

                    void Unity_Multiply_float(float A, float B, out float Out)
                    {
                        Out = A * B;
                    }

                    void Unity_Add_float4(float4 A, float4 B, out float4 Out)
                    {
                        Out = A + B;
                    }

                    void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
                    {
                        Out = Predicate ? True : False;
                    }

                    // Graph Vertex
                    // GraphVertex: <None>

                    // Graph Pixel
                    struct SurfaceDescriptionInputs
                    {
                        float4 uv0;
                        float FaceSign;
                    };

                    struct SurfaceDescription
                    {
                        float Alpha;
                        float AlphaClipThreshold;
                    };

                    SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                    {
                        SurfaceDescription surface = (SurfaceDescription)0;
                        float _IsFrontFace_17668B3E_Out_0 = max(0, IN.FaceSign);
                        float4 _SampleTexture2D_6137F6C8_RGBA_0 = SAMPLE_TEXTURE2D(_cardBorder, sampler_cardBorder, IN.uv0.xy);
                        float _SampleTexture2D_6137F6C8_R_4 = _SampleTexture2D_6137F6C8_RGBA_0.r;
                        float _SampleTexture2D_6137F6C8_G_5 = _SampleTexture2D_6137F6C8_RGBA_0.g;
                        float _SampleTexture2D_6137F6C8_B_6 = _SampleTexture2D_6137F6C8_RGBA_0.b;
                        float _SampleTexture2D_6137F6C8_A_7 = _SampleTexture2D_6137F6C8_RGBA_0.a;
                        float4 _Property_EA7892E9_Out_0 = _borderColor;
                        float4 _Multiply_36449153_Out_2;
                        Unity_Multiply_float(_SampleTexture2D_6137F6C8_RGBA_0, _Property_EA7892E9_Out_0, _Multiply_36449153_Out_2);
                        float4 _SampleTexture2D_CCBE16EF_RGBA_0 = SAMPLE_TEXTURE2D(_MainTex, SamplerState_Point_Repeat, IN.uv0.xy);
                        float _SampleTexture2D_CCBE16EF_R_4 = _SampleTexture2D_CCBE16EF_RGBA_0.r;
                        float _SampleTexture2D_CCBE16EF_G_5 = _SampleTexture2D_CCBE16EF_RGBA_0.g;
                        float _SampleTexture2D_CCBE16EF_B_6 = _SampleTexture2D_CCBE16EF_RGBA_0.b;
                        float _SampleTexture2D_CCBE16EF_A_7 = _SampleTexture2D_CCBE16EF_RGBA_0.a;
                        float4 _Lerp_36236AAE_Out_3;
                        Unity_Lerp_float4(_Multiply_36449153_Out_2, _SampleTexture2D_CCBE16EF_RGBA_0, (_SampleTexture2D_CCBE16EF_A_7.xxxx), _Lerp_36236AAE_Out_3);
                        float2 _Vector2_98B1B01D_Out_0 = float2(1.07, 2.126582);
                        float2 _Multiply_F70CE493_Out_2;
                        Unity_Multiply_float(_Vector2_98B1B01D_Out_0, float2(0.995, 0.995), _Multiply_F70CE493_Out_2);
                        float2 _TilingAndOffset_823AD7C4_Out_3;
                        Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_F70CE493_Out_2, float2 (-0.0334, -0.93), _TilingAndOffset_823AD7C4_Out_3);
                        float4 _SampleTexture2D_46230EA2_RGBA_0 = SAMPLE_TEXTURE2D(_CardArt, sampler_CardArt, _TilingAndOffset_823AD7C4_Out_3);
                        float _SampleTexture2D_46230EA2_R_4 = _SampleTexture2D_46230EA2_RGBA_0.r;
                        float _SampleTexture2D_46230EA2_G_5 = _SampleTexture2D_46230EA2_RGBA_0.g;
                        float _SampleTexture2D_46230EA2_B_6 = _SampleTexture2D_46230EA2_RGBA_0.b;
                        float _SampleTexture2D_46230EA2_A_7 = _SampleTexture2D_46230EA2_RGBA_0.a;
                        float _Property_B9987C17_Out_0 = _ShowArt;
                        float2 _TilingAndOffset_E86983B6_Out_3;
                        Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1.07, 2.126582), float2 (-0.0334, -0.93), _TilingAndOffset_E86983B6_Out_3);
                        float _Rectangle_F201F009_Out_3;
                        Unity_Rectangle_float(_TilingAndOffset_E86983B6_Out_3, 1, 1, _Rectangle_F201F009_Out_3);
                        float _Multiply_FE7047C5_Out_2;
                        Unity_Multiply_float(_Property_B9987C17_Out_0, _Rectangle_F201F009_Out_3, _Multiply_FE7047C5_Out_2);
                        float4 _Lerp_C0A10AA4_Out_3;
                        Unity_Lerp_float4(_Lerp_36236AAE_Out_3, _SampleTexture2D_46230EA2_RGBA_0, (_Multiply_FE7047C5_Out_2.xxxx), _Lerp_C0A10AA4_Out_3);
                        float _Property_1EA691CF_Out_0 = _ShowMagic;
                        float2 _TilingAndOffset_DF34649D_Out_3;
                        Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -7.26), _TilingAndOffset_DF34649D_Out_3);
                        float _Rectangle_3CD8FAA4_Out_3;
                        Unity_Rectangle_float(_TilingAndOffset_DF34649D_Out_3, 1, 1, _Rectangle_3CD8FAA4_Out_3);
                        float4 Color_2CD1002A = IsGammaSpace() ? float4(0.1568628, 0.6313726, 0.9215686, 1) : float4(SRGBToLinear(float3(0.1568628, 0.6313726, 0.9215686)), 1);
                        float2 _Vector2_523D8D2_Out_0 = float2(6.67, 6.67);
                        float2 _Multiply_2C2AEDC_Out_2;
                        Unity_Multiply_float(_Vector2_523D8D2_Out_0, float2(1, 1.4), _Multiply_2C2AEDC_Out_2);
                        float2 _TilingAndOffset_90E9E966_Out_3;
                        Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -7.26), _TilingAndOffset_90E9E966_Out_3);
                        float4 _SampleTexture2D_469B015_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_90E9E966_Out_3);
                        float _SampleTexture2D_469B015_R_4 = _SampleTexture2D_469B015_RGBA_0.r;
                        float _SampleTexture2D_469B015_G_5 = _SampleTexture2D_469B015_RGBA_0.g;
                        float _SampleTexture2D_469B015_B_6 = _SampleTexture2D_469B015_RGBA_0.b;
                        float _SampleTexture2D_469B015_A_7 = _SampleTexture2D_469B015_RGBA_0.a;
                        float4 _Multiply_B7547173_Out_2;
                        Unity_Multiply_float(Color_2CD1002A, _SampleTexture2D_469B015_RGBA_0, _Multiply_B7547173_Out_2);
                        float4 _Multiply_33473234_Out_2;
                        Unity_Multiply_float((_Rectangle_3CD8FAA4_Out_3.xxxx), _Multiply_B7547173_Out_2, _Multiply_33473234_Out_2);
                        float4 _Multiply_E1E340BF_Out_2;
                        Unity_Multiply_float((_Property_1EA691CF_Out_0.xxxx), _Multiply_33473234_Out_2, _Multiply_E1E340BF_Out_2);
                        float _Property_52156452_Out_0 = _ShowTech;
                        float2 _TilingAndOffset_F6346DAD_Out_3;
                        Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -5.76), _TilingAndOffset_F6346DAD_Out_3);
                        float _Rectangle_D5D764F8_Out_3;
                        Unity_Rectangle_float(_TilingAndOffset_F6346DAD_Out_3, 1, 1, _Rectangle_D5D764F8_Out_3);
                        float4 Color_551BDC44 = IsGammaSpace() ? float4(0.9215686, 0.5803922, 0.1098039, 1) : float4(SRGBToLinear(float3(0.9215686, 0.5803922, 0.1098039)), 1);
                        float2 _TilingAndOffset_EAD3C75B_Out_3;
                        Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -5.76), _TilingAndOffset_EAD3C75B_Out_3);
                        float4 _SampleTexture2D_107D84F0_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_EAD3C75B_Out_3);
                        float _SampleTexture2D_107D84F0_R_4 = _SampleTexture2D_107D84F0_RGBA_0.r;
                        float _SampleTexture2D_107D84F0_G_5 = _SampleTexture2D_107D84F0_RGBA_0.g;
                        float _SampleTexture2D_107D84F0_B_6 = _SampleTexture2D_107D84F0_RGBA_0.b;
                        float _SampleTexture2D_107D84F0_A_7 = _SampleTexture2D_107D84F0_RGBA_0.a;
                        float4 _Multiply_EFD52C29_Out_2;
                        Unity_Multiply_float(Color_551BDC44, _SampleTexture2D_107D84F0_RGBA_0, _Multiply_EFD52C29_Out_2);
                        float4 _Multiply_B1157814_Out_2;
                        Unity_Multiply_float((_Rectangle_D5D764F8_Out_3.xxxx), _Multiply_EFD52C29_Out_2, _Multiply_B1157814_Out_2);
                        float4 _Multiply_675F22C4_Out_2;
                        Unity_Multiply_float((_Property_52156452_Out_0.xxxx), _Multiply_B1157814_Out_2, _Multiply_675F22C4_Out_2);
                        float4 _Add_19AD9021_Out_2;
                        Unity_Add_float4(_Multiply_E1E340BF_Out_2, _Multiply_675F22C4_Out_2, _Add_19AD9021_Out_2);
                        float _Property_AD7AEA03_Out_0 = _ShowStrength;
                        float2 _TilingAndOffset_73BA2981_Out_3;
                        Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -4.26), _TilingAndOffset_73BA2981_Out_3);
                        float _Rectangle_71D17C2A_Out_3;
                        Unity_Rectangle_float(_TilingAndOffset_73BA2981_Out_3, 1, 1, _Rectangle_71D17C2A_Out_3);
                        float4 Color_A97EC31D = IsGammaSpace() ? float4(1, 0.01960784, 0.2784314, 1) : float4(SRGBToLinear(float3(1, 0.01960784, 0.2784314)), 1);
                        float2 _TilingAndOffset_440FAFA0_Out_3;
                        Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -4.26), _TilingAndOffset_440FAFA0_Out_3);
                        float4 _SampleTexture2D_EA89F198_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_440FAFA0_Out_3);
                        float _SampleTexture2D_EA89F198_R_4 = _SampleTexture2D_EA89F198_RGBA_0.r;
                        float _SampleTexture2D_EA89F198_G_5 = _SampleTexture2D_EA89F198_RGBA_0.g;
                        float _SampleTexture2D_EA89F198_B_6 = _SampleTexture2D_EA89F198_RGBA_0.b;
                        float _SampleTexture2D_EA89F198_A_7 = _SampleTexture2D_EA89F198_RGBA_0.a;
                        float4 _Multiply_3A6B93F6_Out_2;
                        Unity_Multiply_float(Color_A97EC31D, _SampleTexture2D_EA89F198_RGBA_0, _Multiply_3A6B93F6_Out_2);
                        float4 _Multiply_FC6E49F_Out_2;
                        Unity_Multiply_float((_Rectangle_71D17C2A_Out_3.xxxx), _Multiply_3A6B93F6_Out_2, _Multiply_FC6E49F_Out_2);
                        float4 _Multiply_EAA1618_Out_2;
                        Unity_Multiply_float((_Property_AD7AEA03_Out_0.xxxx), _Multiply_FC6E49F_Out_2, _Multiply_EAA1618_Out_2);
                        float4 _Add_B4B310DE_Out_2;
                        Unity_Add_float4(_Add_19AD9021_Out_2, _Multiply_EAA1618_Out_2, _Add_B4B310DE_Out_2);
                        float _Split_681EAD45_R_1 = _Add_B4B310DE_Out_2[0];
                        float _Split_681EAD45_G_2 = _Add_B4B310DE_Out_2[1];
                        float _Split_681EAD45_B_3 = _Add_B4B310DE_Out_2[2];
                        float _Split_681EAD45_A_4 = _Add_B4B310DE_Out_2[3];
                        float4 _Lerp_2E898E9C_Out_3;
                        Unity_Lerp_float4(_Lerp_C0A10AA4_Out_3, _Add_B4B310DE_Out_2, (_Split_681EAD45_A_4.xxxx), _Lerp_2E898E9C_Out_3);
                        float4 _SampleTexture2D_A0087B33_RGBA_0 = SAMPLE_TEXTURE2D(_CardBack, sampler_CardBack, IN.uv0.xy);
                        float _SampleTexture2D_A0087B33_R_4 = _SampleTexture2D_A0087B33_RGBA_0.r;
                        float _SampleTexture2D_A0087B33_G_5 = _SampleTexture2D_A0087B33_RGBA_0.g;
                        float _SampleTexture2D_A0087B33_B_6 = _SampleTexture2D_A0087B33_RGBA_0.b;
                        float _SampleTexture2D_A0087B33_A_7 = _SampleTexture2D_A0087B33_RGBA_0.a;
                        float4 _Branch_64B7BAAC_Out_3;
                        Unity_Branch_float4(_IsFrontFace_17668B3E_Out_0, _Lerp_2E898E9C_Out_3, _SampleTexture2D_A0087B33_RGBA_0, _Branch_64B7BAAC_Out_3);
                        float _Split_4A24BEA_R_1 = _Branch_64B7BAAC_Out_3[0];
                        float _Split_4A24BEA_G_2 = _Branch_64B7BAAC_Out_3[1];
                        float _Split_4A24BEA_B_3 = _Branch_64B7BAAC_Out_3[2];
                        float _Split_4A24BEA_A_4 = _Branch_64B7BAAC_Out_3[3];
                        surface.Alpha = _Split_4A24BEA_A_4;
                        surface.AlphaClipThreshold = 0;
                        return surface;
                    }

                    // --------------------------------------------------
                    // Structs and Packing

                    // Generated Type: Attributes
                    struct Attributes
                    {
                        float3 positionOS : POSITION;
                        float3 normalOS : NORMAL;
                        float4 tangentOS : TANGENT;
                        float4 uv0 : TEXCOORD0;
                        #if UNITY_ANY_INSTANCING_ENABLED
                        uint instanceID : INSTANCEID_SEMANTIC;
                        #endif
                    };

                    // Generated Type: Varyings
                    struct Varyings
                    {
                        float4 positionCS : SV_POSITION;
                        float4 texCoord0;
                        #if UNITY_ANY_INSTANCING_ENABLED
                        uint instanceID : CUSTOM_INSTANCE_ID;
                        #endif
                        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                        uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                        #endif
                        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                        uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                        #endif
                        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                        FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                        #endif
                    };

                    // Generated Type: PackedVaryings
                    struct PackedVaryings
                    {
                        float4 positionCS : SV_POSITION;
                        #if UNITY_ANY_INSTANCING_ENABLED
                        uint instanceID : CUSTOM_INSTANCE_ID;
                        #endif
                        float4 interp00 : TEXCOORD0;
                        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                        uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                        #endif
                        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                        uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                        #endif
                        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                        FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                        #endif
                    };

                    // Packed Type: Varyings
                    PackedVaryings PackVaryings(Varyings input)
                    {
                        PackedVaryings output = (PackedVaryings)0;
                        output.positionCS = input.positionCS;
                        output.interp00.xyzw = input.texCoord0;
                        #if UNITY_ANY_INSTANCING_ENABLED
                        output.instanceID = input.instanceID;
                        #endif
                        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                        #endif
                        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                        #endif
                        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                        output.cullFace = input.cullFace;
                        #endif
                        return output;
                    }

                    // Unpacked Type: Varyings
                    Varyings UnpackVaryings(PackedVaryings input)
                    {
                        Varyings output = (Varyings)0;
                        output.positionCS = input.positionCS;
                        output.texCoord0 = input.interp00.xyzw;
                        #if UNITY_ANY_INSTANCING_ENABLED
                        output.instanceID = input.instanceID;
                        #endif
                        #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                        output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                        #endif
                        #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                        output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                        #endif
                        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                        output.cullFace = input.cullFace;
                        #endif
                        return output;
                    }

                    // --------------------------------------------------
                    // Build Graph Inputs

                    SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
                    {
                        SurfaceDescriptionInputs output;
                        ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





                        output.uv0 = input.texCoord0;
                    #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                    #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
                    #else
                    #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
                    #endif
                        BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
                    #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

                        return output;
                    }


                    // --------------------------------------------------
                    // Main

                    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
                    #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthOnlyPass.hlsl"

                    ENDHLSL
                }

                Pass
                {
                    Name "Meta"
                    Tags
                    {
                        "LightMode" = "Meta"
                    }

                        // Render State
                        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
                        Cull Off
                        ZTest LEqual
                        ZWrite On
                        // ColorMask: <None>


                        HLSLPROGRAM
                        #pragma vertex vert
                        #pragma fragment frag

                        // Debug
                        // <None>

                        // --------------------------------------------------
                        // Pass

                        // Pragmas
                        #pragma prefer_hlslcc gles
                        #pragma exclude_renderers d3d11_9x
                        #pragma target 2.0

                        // Keywords
                        #pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
                        // GraphKeywords: <None>

                        // Defines
                        #define _SURFACE_TYPE_TRANSPARENT 1
                        #define _NORMALMAP 1
                        #define _NORMAL_DROPOFF_TS 1
                        #define ATTRIBUTES_NEED_NORMAL
                        #define ATTRIBUTES_NEED_TANGENT
                        #define ATTRIBUTES_NEED_TEXCOORD0
                        #define ATTRIBUTES_NEED_TEXCOORD1
                        #define ATTRIBUTES_NEED_TEXCOORD2
                        #define VARYINGS_NEED_TEXCOORD0
                        #define VARYINGS_NEED_CULLFACE
                        #pragma multi_compile_instancing
                        #define SHADERPASS_META


                        // Includes
                        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
                        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
                        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
                        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
                        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"
                        #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

                        // --------------------------------------------------
                        // Graph

                        // Graph Properties
                        CBUFFER_START(UnityPerMaterial)
                        float _ShowMagic;
                        float _ShowTech;
                        float _ShowStrength;
                        float _ShowArt;
                        float4 _borderColor;
                        CBUFFER_END
                        TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;
                        TEXTURE2D(_CardBack); SAMPLER(sampler_CardBack); float4 _CardBack_TexelSize;
                        TEXTURE2D(_CardArt); SAMPLER(sampler_CardArt); float4 _CardArt_TexelSize;
                        TEXTURE2D(_StatBubble); SAMPLER(sampler_StatBubble); float4 _StatBubble_TexelSize;
                        TEXTURE2D(_cardBorder); SAMPLER(sampler_cardBorder); float4 _cardBorder_TexelSize;
                        SAMPLER(_SampleTexture2D_6137F6C8_Sampler_3_Linear_Repeat);
                        SAMPLER(SamplerState_Point_Repeat);
                        SAMPLER(_SampleTexture2D_46230EA2_Sampler_3_Linear_Repeat);
                        SAMPLER(_SampleTexture2D_469B015_Sampler_3_Linear_Repeat);
                        SAMPLER(_SampleTexture2D_107D84F0_Sampler_3_Linear_Repeat);
                        SAMPLER(_SampleTexture2D_EA89F198_Sampler_3_Linear_Repeat);
                        SAMPLER(_SampleTexture2D_A0087B33_Sampler_3_Linear_Repeat);

                        // Graph Functions

                        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
                        {
                            Out = A * B;
                        }

                        void Unity_Lerp_float4(float4 A, float4 B, float4 T, out float4 Out)
                        {
                            Out = lerp(A, B, T);
                        }

                        void Unity_Multiply_float(float2 A, float2 B, out float2 Out)
                        {
                            Out = A * B;
                        }

                        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
                        {
                            Out = UV * Tiling + Offset;
                        }

                        void Unity_Rectangle_float(float2 UV, float Width, float Height, out float Out)
                        {
                            float2 d = abs(UV * 2 - 1) - float2(Width, Height);
                            d = 1 - d / fwidth(d);
                            Out = saturate(min(d.x, d.y));
                        }

                        void Unity_Multiply_float(float A, float B, out float Out)
                        {
                            Out = A * B;
                        }

                        void Unity_Add_float4(float4 A, float4 B, out float4 Out)
                        {
                            Out = A + B;
                        }

                        void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
                        {
                            Out = Predicate ? True : False;
                        }

                        // Graph Vertex
                        // GraphVertex: <None>

                        // Graph Pixel
                        struct SurfaceDescriptionInputs
                        {
                            float4 uv0;
                            float FaceSign;
                        };

                        struct SurfaceDescription
                        {
                            float3 Albedo;
                            float3 Emission;
                            float Alpha;
                            float AlphaClipThreshold;
                        };

                        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                        {
                            SurfaceDescription surface = (SurfaceDescription)0;
                            float _IsFrontFace_17668B3E_Out_0 = max(0, IN.FaceSign);
                            float4 _SampleTexture2D_6137F6C8_RGBA_0 = SAMPLE_TEXTURE2D(_cardBorder, sampler_cardBorder, IN.uv0.xy);
                            float _SampleTexture2D_6137F6C8_R_4 = _SampleTexture2D_6137F6C8_RGBA_0.r;
                            float _SampleTexture2D_6137F6C8_G_5 = _SampleTexture2D_6137F6C8_RGBA_0.g;
                            float _SampleTexture2D_6137F6C8_B_6 = _SampleTexture2D_6137F6C8_RGBA_0.b;
                            float _SampleTexture2D_6137F6C8_A_7 = _SampleTexture2D_6137F6C8_RGBA_0.a;
                            float4 _Property_EA7892E9_Out_0 = _borderColor;
                            float4 _Multiply_36449153_Out_2;
                            Unity_Multiply_float(_SampleTexture2D_6137F6C8_RGBA_0, _Property_EA7892E9_Out_0, _Multiply_36449153_Out_2);
                            float4 _SampleTexture2D_CCBE16EF_RGBA_0 = SAMPLE_TEXTURE2D(_MainTex, SamplerState_Point_Repeat, IN.uv0.xy);
                            float _SampleTexture2D_CCBE16EF_R_4 = _SampleTexture2D_CCBE16EF_RGBA_0.r;
                            float _SampleTexture2D_CCBE16EF_G_5 = _SampleTexture2D_CCBE16EF_RGBA_0.g;
                            float _SampleTexture2D_CCBE16EF_B_6 = _SampleTexture2D_CCBE16EF_RGBA_0.b;
                            float _SampleTexture2D_CCBE16EF_A_7 = _SampleTexture2D_CCBE16EF_RGBA_0.a;
                            float4 _Lerp_36236AAE_Out_3;
                            Unity_Lerp_float4(_Multiply_36449153_Out_2, _SampleTexture2D_CCBE16EF_RGBA_0, (_SampleTexture2D_CCBE16EF_A_7.xxxx), _Lerp_36236AAE_Out_3);
                            float2 _Vector2_98B1B01D_Out_0 = float2(1.07, 2.126582);
                            float2 _Multiply_F70CE493_Out_2;
                            Unity_Multiply_float(_Vector2_98B1B01D_Out_0, float2(0.995, 0.995), _Multiply_F70CE493_Out_2);
                            float2 _TilingAndOffset_823AD7C4_Out_3;
                            Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_F70CE493_Out_2, float2 (-0.0334, -0.93), _TilingAndOffset_823AD7C4_Out_3);
                            float4 _SampleTexture2D_46230EA2_RGBA_0 = SAMPLE_TEXTURE2D(_CardArt, sampler_CardArt, _TilingAndOffset_823AD7C4_Out_3);
                            float _SampleTexture2D_46230EA2_R_4 = _SampleTexture2D_46230EA2_RGBA_0.r;
                            float _SampleTexture2D_46230EA2_G_5 = _SampleTexture2D_46230EA2_RGBA_0.g;
                            float _SampleTexture2D_46230EA2_B_6 = _SampleTexture2D_46230EA2_RGBA_0.b;
                            float _SampleTexture2D_46230EA2_A_7 = _SampleTexture2D_46230EA2_RGBA_0.a;
                            float _Property_B9987C17_Out_0 = _ShowArt;
                            float2 _TilingAndOffset_E86983B6_Out_3;
                            Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1.07, 2.126582), float2 (-0.0334, -0.93), _TilingAndOffset_E86983B6_Out_3);
                            float _Rectangle_F201F009_Out_3;
                            Unity_Rectangle_float(_TilingAndOffset_E86983B6_Out_3, 1, 1, _Rectangle_F201F009_Out_3);
                            float _Multiply_FE7047C5_Out_2;
                            Unity_Multiply_float(_Property_B9987C17_Out_0, _Rectangle_F201F009_Out_3, _Multiply_FE7047C5_Out_2);
                            float4 _Lerp_C0A10AA4_Out_3;
                            Unity_Lerp_float4(_Lerp_36236AAE_Out_3, _SampleTexture2D_46230EA2_RGBA_0, (_Multiply_FE7047C5_Out_2.xxxx), _Lerp_C0A10AA4_Out_3);
                            float _Property_1EA691CF_Out_0 = _ShowMagic;
                            float2 _TilingAndOffset_DF34649D_Out_3;
                            Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -7.26), _TilingAndOffset_DF34649D_Out_3);
                            float _Rectangle_3CD8FAA4_Out_3;
                            Unity_Rectangle_float(_TilingAndOffset_DF34649D_Out_3, 1, 1, _Rectangle_3CD8FAA4_Out_3);
                            float4 Color_2CD1002A = IsGammaSpace() ? float4(0.1568628, 0.6313726, 0.9215686, 1) : float4(SRGBToLinear(float3(0.1568628, 0.6313726, 0.9215686)), 1);
                            float2 _Vector2_523D8D2_Out_0 = float2(6.67, 6.67);
                            float2 _Multiply_2C2AEDC_Out_2;
                            Unity_Multiply_float(_Vector2_523D8D2_Out_0, float2(1, 1.4), _Multiply_2C2AEDC_Out_2);
                            float2 _TilingAndOffset_90E9E966_Out_3;
                            Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -7.26), _TilingAndOffset_90E9E966_Out_3);
                            float4 _SampleTexture2D_469B015_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_90E9E966_Out_3);
                            float _SampleTexture2D_469B015_R_4 = _SampleTexture2D_469B015_RGBA_0.r;
                            float _SampleTexture2D_469B015_G_5 = _SampleTexture2D_469B015_RGBA_0.g;
                            float _SampleTexture2D_469B015_B_6 = _SampleTexture2D_469B015_RGBA_0.b;
                            float _SampleTexture2D_469B015_A_7 = _SampleTexture2D_469B015_RGBA_0.a;
                            float4 _Multiply_B7547173_Out_2;
                            Unity_Multiply_float(Color_2CD1002A, _SampleTexture2D_469B015_RGBA_0, _Multiply_B7547173_Out_2);
                            float4 _Multiply_33473234_Out_2;
                            Unity_Multiply_float((_Rectangle_3CD8FAA4_Out_3.xxxx), _Multiply_B7547173_Out_2, _Multiply_33473234_Out_2);
                            float4 _Multiply_E1E340BF_Out_2;
                            Unity_Multiply_float((_Property_1EA691CF_Out_0.xxxx), _Multiply_33473234_Out_2, _Multiply_E1E340BF_Out_2);
                            float _Property_52156452_Out_0 = _ShowTech;
                            float2 _TilingAndOffset_F6346DAD_Out_3;
                            Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -5.76), _TilingAndOffset_F6346DAD_Out_3);
                            float _Rectangle_D5D764F8_Out_3;
                            Unity_Rectangle_float(_TilingAndOffset_F6346DAD_Out_3, 1, 1, _Rectangle_D5D764F8_Out_3);
                            float4 Color_551BDC44 = IsGammaSpace() ? float4(0.9215686, 0.5803922, 0.1098039, 1) : float4(SRGBToLinear(float3(0.9215686, 0.5803922, 0.1098039)), 1);
                            float2 _TilingAndOffset_EAD3C75B_Out_3;
                            Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -5.76), _TilingAndOffset_EAD3C75B_Out_3);
                            float4 _SampleTexture2D_107D84F0_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_EAD3C75B_Out_3);
                            float _SampleTexture2D_107D84F0_R_4 = _SampleTexture2D_107D84F0_RGBA_0.r;
                            float _SampleTexture2D_107D84F0_G_5 = _SampleTexture2D_107D84F0_RGBA_0.g;
                            float _SampleTexture2D_107D84F0_B_6 = _SampleTexture2D_107D84F0_RGBA_0.b;
                            float _SampleTexture2D_107D84F0_A_7 = _SampleTexture2D_107D84F0_RGBA_0.a;
                            float4 _Multiply_EFD52C29_Out_2;
                            Unity_Multiply_float(Color_551BDC44, _SampleTexture2D_107D84F0_RGBA_0, _Multiply_EFD52C29_Out_2);
                            float4 _Multiply_B1157814_Out_2;
                            Unity_Multiply_float((_Rectangle_D5D764F8_Out_3.xxxx), _Multiply_EFD52C29_Out_2, _Multiply_B1157814_Out_2);
                            float4 _Multiply_675F22C4_Out_2;
                            Unity_Multiply_float((_Property_52156452_Out_0.xxxx), _Multiply_B1157814_Out_2, _Multiply_675F22C4_Out_2);
                            float4 _Add_19AD9021_Out_2;
                            Unity_Add_float4(_Multiply_E1E340BF_Out_2, _Multiply_675F22C4_Out_2, _Add_19AD9021_Out_2);
                            float _Property_AD7AEA03_Out_0 = _ShowStrength;
                            float2 _TilingAndOffset_73BA2981_Out_3;
                            Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -4.26), _TilingAndOffset_73BA2981_Out_3);
                            float _Rectangle_71D17C2A_Out_3;
                            Unity_Rectangle_float(_TilingAndOffset_73BA2981_Out_3, 1, 1, _Rectangle_71D17C2A_Out_3);
                            float4 Color_A97EC31D = IsGammaSpace() ? float4(1, 0.01960784, 0.2784314, 1) : float4(SRGBToLinear(float3(1, 0.01960784, 0.2784314)), 1);
                            float2 _TilingAndOffset_440FAFA0_Out_3;
                            Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -4.26), _TilingAndOffset_440FAFA0_Out_3);
                            float4 _SampleTexture2D_EA89F198_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_440FAFA0_Out_3);
                            float _SampleTexture2D_EA89F198_R_4 = _SampleTexture2D_EA89F198_RGBA_0.r;
                            float _SampleTexture2D_EA89F198_G_5 = _SampleTexture2D_EA89F198_RGBA_0.g;
                            float _SampleTexture2D_EA89F198_B_6 = _SampleTexture2D_EA89F198_RGBA_0.b;
                            float _SampleTexture2D_EA89F198_A_7 = _SampleTexture2D_EA89F198_RGBA_0.a;
                            float4 _Multiply_3A6B93F6_Out_2;
                            Unity_Multiply_float(Color_A97EC31D, _SampleTexture2D_EA89F198_RGBA_0, _Multiply_3A6B93F6_Out_2);
                            float4 _Multiply_FC6E49F_Out_2;
                            Unity_Multiply_float((_Rectangle_71D17C2A_Out_3.xxxx), _Multiply_3A6B93F6_Out_2, _Multiply_FC6E49F_Out_2);
                            float4 _Multiply_EAA1618_Out_2;
                            Unity_Multiply_float((_Property_AD7AEA03_Out_0.xxxx), _Multiply_FC6E49F_Out_2, _Multiply_EAA1618_Out_2);
                            float4 _Add_B4B310DE_Out_2;
                            Unity_Add_float4(_Add_19AD9021_Out_2, _Multiply_EAA1618_Out_2, _Add_B4B310DE_Out_2);
                            float _Split_681EAD45_R_1 = _Add_B4B310DE_Out_2[0];
                            float _Split_681EAD45_G_2 = _Add_B4B310DE_Out_2[1];
                            float _Split_681EAD45_B_3 = _Add_B4B310DE_Out_2[2];
                            float _Split_681EAD45_A_4 = _Add_B4B310DE_Out_2[3];
                            float4 _Lerp_2E898E9C_Out_3;
                            Unity_Lerp_float4(_Lerp_C0A10AA4_Out_3, _Add_B4B310DE_Out_2, (_Split_681EAD45_A_4.xxxx), _Lerp_2E898E9C_Out_3);
                            float4 _SampleTexture2D_A0087B33_RGBA_0 = SAMPLE_TEXTURE2D(_CardBack, sampler_CardBack, IN.uv0.xy);
                            float _SampleTexture2D_A0087B33_R_4 = _SampleTexture2D_A0087B33_RGBA_0.r;
                            float _SampleTexture2D_A0087B33_G_5 = _SampleTexture2D_A0087B33_RGBA_0.g;
                            float _SampleTexture2D_A0087B33_B_6 = _SampleTexture2D_A0087B33_RGBA_0.b;
                            float _SampleTexture2D_A0087B33_A_7 = _SampleTexture2D_A0087B33_RGBA_0.a;
                            float4 _Branch_64B7BAAC_Out_3;
                            Unity_Branch_float4(_IsFrontFace_17668B3E_Out_0, _Lerp_2E898E9C_Out_3, _SampleTexture2D_A0087B33_RGBA_0, _Branch_64B7BAAC_Out_3);
                            float _Split_4A24BEA_R_1 = _Branch_64B7BAAC_Out_3[0];
                            float _Split_4A24BEA_G_2 = _Branch_64B7BAAC_Out_3[1];
                            float _Split_4A24BEA_B_3 = _Branch_64B7BAAC_Out_3[2];
                            float _Split_4A24BEA_A_4 = _Branch_64B7BAAC_Out_3[3];
                            surface.Albedo = (_Branch_64B7BAAC_Out_3.xyz);
                            surface.Emission = IsGammaSpace() ? float3(0, 0, 0) : SRGBToLinear(float3(0, 0, 0));
                            surface.Alpha = _Split_4A24BEA_A_4;
                            surface.AlphaClipThreshold = 0;
                            return surface;
                        }

                        // --------------------------------------------------
                        // Structs and Packing

                        // Generated Type: Attributes
                        struct Attributes
                        {
                            float3 positionOS : POSITION;
                            float3 normalOS : NORMAL;
                            float4 tangentOS : TANGENT;
                            float4 uv0 : TEXCOORD0;
                            float4 uv1 : TEXCOORD1;
                            float4 uv2 : TEXCOORD2;
                            #if UNITY_ANY_INSTANCING_ENABLED
                            uint instanceID : INSTANCEID_SEMANTIC;
                            #endif
                        };

                        // Generated Type: Varyings
                        struct Varyings
                        {
                            float4 positionCS : SV_POSITION;
                            float4 texCoord0;
                            #if UNITY_ANY_INSTANCING_ENABLED
                            uint instanceID : CUSTOM_INSTANCE_ID;
                            #endif
                            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                            #endif
                            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                            #endif
                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                            #endif
                        };

                        // Generated Type: PackedVaryings
                        struct PackedVaryings
                        {
                            float4 positionCS : SV_POSITION;
                            #if UNITY_ANY_INSTANCING_ENABLED
                            uint instanceID : CUSTOM_INSTANCE_ID;
                            #endif
                            float4 interp00 : TEXCOORD0;
                            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                            #endif
                            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                            #endif
                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                            #endif
                        };

                        // Packed Type: Varyings
                        PackedVaryings PackVaryings(Varyings input)
                        {
                            PackedVaryings output = (PackedVaryings)0;
                            output.positionCS = input.positionCS;
                            output.interp00.xyzw = input.texCoord0;
                            #if UNITY_ANY_INSTANCING_ENABLED
                            output.instanceID = input.instanceID;
                            #endif
                            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                            #endif
                            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                            #endif
                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                            output.cullFace = input.cullFace;
                            #endif
                            return output;
                        }

                        // Unpacked Type: Varyings
                        Varyings UnpackVaryings(PackedVaryings input)
                        {
                            Varyings output = (Varyings)0;
                            output.positionCS = input.positionCS;
                            output.texCoord0 = input.interp00.xyzw;
                            #if UNITY_ANY_INSTANCING_ENABLED
                            output.instanceID = input.instanceID;
                            #endif
                            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                            #endif
                            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                            #endif
                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                            output.cullFace = input.cullFace;
                            #endif
                            return output;
                        }

                        // --------------------------------------------------
                        // Build Graph Inputs

                        SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
                        {
                            SurfaceDescriptionInputs output;
                            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





                            output.uv0 = input.texCoord0;
                        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
                        #else
                        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
                        #endif
                            BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
                        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

                            return output;
                        }


                        // --------------------------------------------------
                        // Main

                        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
                        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/LightingMetaPass.hlsl"

                        ENDHLSL
                    }

                    Pass
                    {
                            // Name: <None>
                            Tags
                            {
                                "LightMode" = "Universal2D"
                            }

                            // Render State
                            Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
                            Cull Off
                            ZTest LEqual
                            ZWrite Off
                            // ColorMask: <None>


                            HLSLPROGRAM
                            #pragma vertex vert
                            #pragma fragment frag

                            // Debug
                            // <None>

                            // --------------------------------------------------
                            // Pass

                            // Pragmas
                            #pragma prefer_hlslcc gles
                            #pragma exclude_renderers d3d11_9x
                            #pragma target 2.0
                            #pragma multi_compile_instancing

                            // Keywords
                            // PassKeywords: <None>
                            // GraphKeywords: <None>

                            // Defines
                            #define _SURFACE_TYPE_TRANSPARENT 1
                            #define _NORMALMAP 1
                            #define _NORMAL_DROPOFF_TS 1
                            #define ATTRIBUTES_NEED_NORMAL
                            #define ATTRIBUTES_NEED_TANGENT
                            #define ATTRIBUTES_NEED_TEXCOORD0
                            #define VARYINGS_NEED_TEXCOORD0
                            #define VARYINGS_NEED_CULLFACE
                            #pragma multi_compile_instancing
                            #define SHADERPASS_2D


                            // Includes
                            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
                            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
                            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
                            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
                            #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

                            // --------------------------------------------------
                            // Graph

                            // Graph Properties
                            CBUFFER_START(UnityPerMaterial)
                            float _ShowMagic;
                            float _ShowTech;
                            float _ShowStrength;
                            float _ShowArt;
                            float4 _borderColor;
                            CBUFFER_END
                            TEXTURE2D(_MainTex); SAMPLER(sampler_MainTex); float4 _MainTex_TexelSize;
                            TEXTURE2D(_CardBack); SAMPLER(sampler_CardBack); float4 _CardBack_TexelSize;
                            TEXTURE2D(_CardArt); SAMPLER(sampler_CardArt); float4 _CardArt_TexelSize;
                            TEXTURE2D(_StatBubble); SAMPLER(sampler_StatBubble); float4 _StatBubble_TexelSize;
                            TEXTURE2D(_cardBorder); SAMPLER(sampler_cardBorder); float4 _cardBorder_TexelSize;
                            SAMPLER(_SampleTexture2D_6137F6C8_Sampler_3_Linear_Repeat);
                            SAMPLER(SamplerState_Point_Repeat);
                            SAMPLER(_SampleTexture2D_46230EA2_Sampler_3_Linear_Repeat);
                            SAMPLER(_SampleTexture2D_469B015_Sampler_3_Linear_Repeat);
                            SAMPLER(_SampleTexture2D_107D84F0_Sampler_3_Linear_Repeat);
                            SAMPLER(_SampleTexture2D_EA89F198_Sampler_3_Linear_Repeat);
                            SAMPLER(_SampleTexture2D_A0087B33_Sampler_3_Linear_Repeat);

                            // Graph Functions

                            void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
                            {
                                Out = A * B;
                            }

                            void Unity_Lerp_float4(float4 A, float4 B, float4 T, out float4 Out)
                            {
                                Out = lerp(A, B, T);
                            }

                            void Unity_Multiply_float(float2 A, float2 B, out float2 Out)
                            {
                                Out = A * B;
                            }

                            void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
                            {
                                Out = UV * Tiling + Offset;
                            }

                            void Unity_Rectangle_float(float2 UV, float Width, float Height, out float Out)
                            {
                                float2 d = abs(UV * 2 - 1) - float2(Width, Height);
                                d = 1 - d / fwidth(d);
                                Out = saturate(min(d.x, d.y));
                            }

                            void Unity_Multiply_float(float A, float B, out float Out)
                            {
                                Out = A * B;
                            }

                            void Unity_Add_float4(float4 A, float4 B, out float4 Out)
                            {
                                Out = A + B;
                            }

                            void Unity_Branch_float4(float Predicate, float4 True, float4 False, out float4 Out)
                            {
                                Out = Predicate ? True : False;
                            }

                            // Graph Vertex
                            // GraphVertex: <None>

                            // Graph Pixel
                            struct SurfaceDescriptionInputs
                            {
                                float4 uv0;
                                float FaceSign;
                            };

                            struct SurfaceDescription
                            {
                                float3 Albedo;
                                float Alpha;
                                float AlphaClipThreshold;
                            };

                            SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
                            {
                                SurfaceDescription surface = (SurfaceDescription)0;
                                float _IsFrontFace_17668B3E_Out_0 = max(0, IN.FaceSign);
                                float4 _SampleTexture2D_6137F6C8_RGBA_0 = SAMPLE_TEXTURE2D(_cardBorder, sampler_cardBorder, IN.uv0.xy);
                                float _SampleTexture2D_6137F6C8_R_4 = _SampleTexture2D_6137F6C8_RGBA_0.r;
                                float _SampleTexture2D_6137F6C8_G_5 = _SampleTexture2D_6137F6C8_RGBA_0.g;
                                float _SampleTexture2D_6137F6C8_B_6 = _SampleTexture2D_6137F6C8_RGBA_0.b;
                                float _SampleTexture2D_6137F6C8_A_7 = _SampleTexture2D_6137F6C8_RGBA_0.a;
                                float4 _Property_EA7892E9_Out_0 = _borderColor;
                                float4 _Multiply_36449153_Out_2;
                                Unity_Multiply_float(_SampleTexture2D_6137F6C8_RGBA_0, _Property_EA7892E9_Out_0, _Multiply_36449153_Out_2);
                                float4 _SampleTexture2D_CCBE16EF_RGBA_0 = SAMPLE_TEXTURE2D(_MainTex, SamplerState_Point_Repeat, IN.uv0.xy);
                                float _SampleTexture2D_CCBE16EF_R_4 = _SampleTexture2D_CCBE16EF_RGBA_0.r;
                                float _SampleTexture2D_CCBE16EF_G_5 = _SampleTexture2D_CCBE16EF_RGBA_0.g;
                                float _SampleTexture2D_CCBE16EF_B_6 = _SampleTexture2D_CCBE16EF_RGBA_0.b;
                                float _SampleTexture2D_CCBE16EF_A_7 = _SampleTexture2D_CCBE16EF_RGBA_0.a;
                                float4 _Lerp_36236AAE_Out_3;
                                Unity_Lerp_float4(_Multiply_36449153_Out_2, _SampleTexture2D_CCBE16EF_RGBA_0, (_SampleTexture2D_CCBE16EF_A_7.xxxx), _Lerp_36236AAE_Out_3);
                                float2 _Vector2_98B1B01D_Out_0 = float2(1.07, 2.126582);
                                float2 _Multiply_F70CE493_Out_2;
                                Unity_Multiply_float(_Vector2_98B1B01D_Out_0, float2(0.995, 0.995), _Multiply_F70CE493_Out_2);
                                float2 _TilingAndOffset_823AD7C4_Out_3;
                                Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_F70CE493_Out_2, float2 (-0.0334, -0.93), _TilingAndOffset_823AD7C4_Out_3);
                                float4 _SampleTexture2D_46230EA2_RGBA_0 = SAMPLE_TEXTURE2D(_CardArt, sampler_CardArt, _TilingAndOffset_823AD7C4_Out_3);
                                float _SampleTexture2D_46230EA2_R_4 = _SampleTexture2D_46230EA2_RGBA_0.r;
                                float _SampleTexture2D_46230EA2_G_5 = _SampleTexture2D_46230EA2_RGBA_0.g;
                                float _SampleTexture2D_46230EA2_B_6 = _SampleTexture2D_46230EA2_RGBA_0.b;
                                float _SampleTexture2D_46230EA2_A_7 = _SampleTexture2D_46230EA2_RGBA_0.a;
                                float _Property_B9987C17_Out_0 = _ShowArt;
                                float2 _TilingAndOffset_E86983B6_Out_3;
                                Unity_TilingAndOffset_float(IN.uv0.xy, float2 (1.07, 2.126582), float2 (-0.0334, -0.93), _TilingAndOffset_E86983B6_Out_3);
                                float _Rectangle_F201F009_Out_3;
                                Unity_Rectangle_float(_TilingAndOffset_E86983B6_Out_3, 1, 1, _Rectangle_F201F009_Out_3);
                                float _Multiply_FE7047C5_Out_2;
                                Unity_Multiply_float(_Property_B9987C17_Out_0, _Rectangle_F201F009_Out_3, _Multiply_FE7047C5_Out_2);
                                float4 _Lerp_C0A10AA4_Out_3;
                                Unity_Lerp_float4(_Lerp_36236AAE_Out_3, _SampleTexture2D_46230EA2_RGBA_0, (_Multiply_FE7047C5_Out_2.xxxx), _Lerp_C0A10AA4_Out_3);
                                float _Property_1EA691CF_Out_0 = _ShowMagic;
                                float2 _TilingAndOffset_DF34649D_Out_3;
                                Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -7.26), _TilingAndOffset_DF34649D_Out_3);
                                float _Rectangle_3CD8FAA4_Out_3;
                                Unity_Rectangle_float(_TilingAndOffset_DF34649D_Out_3, 1, 1, _Rectangle_3CD8FAA4_Out_3);
                                float4 Color_2CD1002A = IsGammaSpace() ? float4(0.1568628, 0.6313726, 0.9215686, 1) : float4(SRGBToLinear(float3(0.1568628, 0.6313726, 0.9215686)), 1);
                                float2 _Vector2_523D8D2_Out_0 = float2(6.67, 6.67);
                                float2 _Multiply_2C2AEDC_Out_2;
                                Unity_Multiply_float(_Vector2_523D8D2_Out_0, float2(1, 1.4), _Multiply_2C2AEDC_Out_2);
                                float2 _TilingAndOffset_90E9E966_Out_3;
                                Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -7.26), _TilingAndOffset_90E9E966_Out_3);
                                float4 _SampleTexture2D_469B015_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_90E9E966_Out_3);
                                float _SampleTexture2D_469B015_R_4 = _SampleTexture2D_469B015_RGBA_0.r;
                                float _SampleTexture2D_469B015_G_5 = _SampleTexture2D_469B015_RGBA_0.g;
                                float _SampleTexture2D_469B015_B_6 = _SampleTexture2D_469B015_RGBA_0.b;
                                float _SampleTexture2D_469B015_A_7 = _SampleTexture2D_469B015_RGBA_0.a;
                                float4 _Multiply_B7547173_Out_2;
                                Unity_Multiply_float(Color_2CD1002A, _SampleTexture2D_469B015_RGBA_0, _Multiply_B7547173_Out_2);
                                float4 _Multiply_33473234_Out_2;
                                Unity_Multiply_float((_Rectangle_3CD8FAA4_Out_3.xxxx), _Multiply_B7547173_Out_2, _Multiply_33473234_Out_2);
                                float4 _Multiply_E1E340BF_Out_2;
                                Unity_Multiply_float((_Property_1EA691CF_Out_0.xxxx), _Multiply_33473234_Out_2, _Multiply_E1E340BF_Out_2);
                                float _Property_52156452_Out_0 = _ShowTech;
                                float2 _TilingAndOffset_F6346DAD_Out_3;
                                Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -5.76), _TilingAndOffset_F6346DAD_Out_3);
                                float _Rectangle_D5D764F8_Out_3;
                                Unity_Rectangle_float(_TilingAndOffset_F6346DAD_Out_3, 1, 1, _Rectangle_D5D764F8_Out_3);
                                float4 Color_551BDC44 = IsGammaSpace() ? float4(0.9215686, 0.5803922, 0.1098039, 1) : float4(SRGBToLinear(float3(0.9215686, 0.5803922, 0.1098039)), 1);
                                float2 _TilingAndOffset_EAD3C75B_Out_3;
                                Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -5.76), _TilingAndOffset_EAD3C75B_Out_3);
                                float4 _SampleTexture2D_107D84F0_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_EAD3C75B_Out_3);
                                float _SampleTexture2D_107D84F0_R_4 = _SampleTexture2D_107D84F0_RGBA_0.r;
                                float _SampleTexture2D_107D84F0_G_5 = _SampleTexture2D_107D84F0_RGBA_0.g;
                                float _SampleTexture2D_107D84F0_B_6 = _SampleTexture2D_107D84F0_RGBA_0.b;
                                float _SampleTexture2D_107D84F0_A_7 = _SampleTexture2D_107D84F0_RGBA_0.a;
                                float4 _Multiply_EFD52C29_Out_2;
                                Unity_Multiply_float(Color_551BDC44, _SampleTexture2D_107D84F0_RGBA_0, _Multiply_EFD52C29_Out_2);
                                float4 _Multiply_B1157814_Out_2;
                                Unity_Multiply_float((_Rectangle_D5D764F8_Out_3.xxxx), _Multiply_EFD52C29_Out_2, _Multiply_B1157814_Out_2);
                                float4 _Multiply_675F22C4_Out_2;
                                Unity_Multiply_float((_Property_52156452_Out_0.xxxx), _Multiply_B1157814_Out_2, _Multiply_675F22C4_Out_2);
                                float4 _Add_19AD9021_Out_2;
                                Unity_Add_float4(_Multiply_E1E340BF_Out_2, _Multiply_675F22C4_Out_2, _Add_19AD9021_Out_2);
                                float _Property_AD7AEA03_Out_0 = _ShowStrength;
                                float2 _TilingAndOffset_73BA2981_Out_3;
                                Unity_TilingAndOffset_float(IN.uv0.xy, float2 (6.67, 9.34), float2 (-5.35, -4.26), _TilingAndOffset_73BA2981_Out_3);
                                float _Rectangle_71D17C2A_Out_3;
                                Unity_Rectangle_float(_TilingAndOffset_73BA2981_Out_3, 1, 1, _Rectangle_71D17C2A_Out_3);
                                float4 Color_A97EC31D = IsGammaSpace() ? float4(1, 0.01960784, 0.2784314, 1) : float4(SRGBToLinear(float3(1, 0.01960784, 0.2784314)), 1);
                                float2 _TilingAndOffset_440FAFA0_Out_3;
                                Unity_TilingAndOffset_float(IN.uv0.xy, _Multiply_2C2AEDC_Out_2, float2 (-5.35, -4.26), _TilingAndOffset_440FAFA0_Out_3);
                                float4 _SampleTexture2D_EA89F198_RGBA_0 = SAMPLE_TEXTURE2D(_StatBubble, sampler_StatBubble, _TilingAndOffset_440FAFA0_Out_3);
                                float _SampleTexture2D_EA89F198_R_4 = _SampleTexture2D_EA89F198_RGBA_0.r;
                                float _SampleTexture2D_EA89F198_G_5 = _SampleTexture2D_EA89F198_RGBA_0.g;
                                float _SampleTexture2D_EA89F198_B_6 = _SampleTexture2D_EA89F198_RGBA_0.b;
                                float _SampleTexture2D_EA89F198_A_7 = _SampleTexture2D_EA89F198_RGBA_0.a;
                                float4 _Multiply_3A6B93F6_Out_2;
                                Unity_Multiply_float(Color_A97EC31D, _SampleTexture2D_EA89F198_RGBA_0, _Multiply_3A6B93F6_Out_2);
                                float4 _Multiply_FC6E49F_Out_2;
                                Unity_Multiply_float((_Rectangle_71D17C2A_Out_3.xxxx), _Multiply_3A6B93F6_Out_2, _Multiply_FC6E49F_Out_2);
                                float4 _Multiply_EAA1618_Out_2;
                                Unity_Multiply_float((_Property_AD7AEA03_Out_0.xxxx), _Multiply_FC6E49F_Out_2, _Multiply_EAA1618_Out_2);
                                float4 _Add_B4B310DE_Out_2;
                                Unity_Add_float4(_Add_19AD9021_Out_2, _Multiply_EAA1618_Out_2, _Add_B4B310DE_Out_2);
                                float _Split_681EAD45_R_1 = _Add_B4B310DE_Out_2[0];
                                float _Split_681EAD45_G_2 = _Add_B4B310DE_Out_2[1];
                                float _Split_681EAD45_B_3 = _Add_B4B310DE_Out_2[2];
                                float _Split_681EAD45_A_4 = _Add_B4B310DE_Out_2[3];
                                float4 _Lerp_2E898E9C_Out_3;
                                Unity_Lerp_float4(_Lerp_C0A10AA4_Out_3, _Add_B4B310DE_Out_2, (_Split_681EAD45_A_4.xxxx), _Lerp_2E898E9C_Out_3);
                                float4 _SampleTexture2D_A0087B33_RGBA_0 = SAMPLE_TEXTURE2D(_CardBack, sampler_CardBack, IN.uv0.xy);
                                float _SampleTexture2D_A0087B33_R_4 = _SampleTexture2D_A0087B33_RGBA_0.r;
                                float _SampleTexture2D_A0087B33_G_5 = _SampleTexture2D_A0087B33_RGBA_0.g;
                                float _SampleTexture2D_A0087B33_B_6 = _SampleTexture2D_A0087B33_RGBA_0.b;
                                float _SampleTexture2D_A0087B33_A_7 = _SampleTexture2D_A0087B33_RGBA_0.a;
                                float4 _Branch_64B7BAAC_Out_3;
                                Unity_Branch_float4(_IsFrontFace_17668B3E_Out_0, _Lerp_2E898E9C_Out_3, _SampleTexture2D_A0087B33_RGBA_0, _Branch_64B7BAAC_Out_3);
                                float _Split_4A24BEA_R_1 = _Branch_64B7BAAC_Out_3[0];
                                float _Split_4A24BEA_G_2 = _Branch_64B7BAAC_Out_3[1];
                                float _Split_4A24BEA_B_3 = _Branch_64B7BAAC_Out_3[2];
                                float _Split_4A24BEA_A_4 = _Branch_64B7BAAC_Out_3[3];
                                surface.Albedo = (_Branch_64B7BAAC_Out_3.xyz);
                                surface.Alpha = _Split_4A24BEA_A_4;
                                surface.AlphaClipThreshold = 0;
                                return surface;
                            }

                            // --------------------------------------------------
                            // Structs and Packing

                            // Generated Type: Attributes
                            struct Attributes
                            {
                                float3 positionOS : POSITION;
                                float3 normalOS : NORMAL;
                                float4 tangentOS : TANGENT;
                                float4 uv0 : TEXCOORD0;
                                #if UNITY_ANY_INSTANCING_ENABLED
                                uint instanceID : INSTANCEID_SEMANTIC;
                                #endif
                            };

                            // Generated Type: Varyings
                            struct Varyings
                            {
                                float4 positionCS : SV_POSITION;
                                float4 texCoord0;
                                #if UNITY_ANY_INSTANCING_ENABLED
                                uint instanceID : CUSTOM_INSTANCE_ID;
                                #endif
                                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                                #endif
                                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                                #endif
                                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                                #endif
                            };

                            // Generated Type: PackedVaryings
                            struct PackedVaryings
                            {
                                float4 positionCS : SV_POSITION;
                                #if UNITY_ANY_INSTANCING_ENABLED
                                uint instanceID : CUSTOM_INSTANCE_ID;
                                #endif
                                float4 interp00 : TEXCOORD0;
                                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                                uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
                                #endif
                                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                                uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
                                #endif
                                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
                                #endif
                            };

                            // Packed Type: Varyings
                            PackedVaryings PackVaryings(Varyings input)
                            {
                                PackedVaryings output = (PackedVaryings)0;
                                output.positionCS = input.positionCS;
                                output.interp00.xyzw = input.texCoord0;
                                #if UNITY_ANY_INSTANCING_ENABLED
                                output.instanceID = input.instanceID;
                                #endif
                                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                                #endif
                                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                                #endif
                                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                output.cullFace = input.cullFace;
                                #endif
                                return output;
                            }

                            // Unpacked Type: Varyings
                            Varyings UnpackVaryings(PackedVaryings input)
                            {
                                Varyings output = (Varyings)0;
                                output.positionCS = input.positionCS;
                                output.texCoord0 = input.interp00.xyzw;
                                #if UNITY_ANY_INSTANCING_ENABLED
                                output.instanceID = input.instanceID;
                                #endif
                                #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
                                output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
                                #endif
                                #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
                                output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
                                #endif
                                #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                                output.cullFace = input.cullFace;
                                #endif
                                return output;
                            }

                            // --------------------------------------------------
                            // Build Graph Inputs

                            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
                            {
                                SurfaceDescriptionInputs output;
                                ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





                                output.uv0 = input.texCoord0;
                            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
                            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
                            #else
                            #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
                            #endif
                                BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
                            #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

                                return output;
                            }


                            // --------------------------------------------------
                            // Main

                            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
                            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBR2DPass.hlsl"

                            ENDHLSL
                        }

        }
            CustomEditor "UnityEditor.ShaderGraph.PBRMasterGUI"
                                FallBack "Hidden/Shader Graph/FallbackError"
}
