﻿Shader "Custom/Stencil Writer"
{
    SubShader
    {
        Zwrite off
        ColorMask 0
        Cull off

        Stencil{
            Ref 1
            Comp Equal
            Pass keep
        }
        Pass {
        }
    }
}
