﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Cards/Ability Card")]
public class Ability : Card, IAuraConsumer
{
    public static readonly string InlineAura = "<sprite name=\"aura-icon-2\">";
    public static readonly string InlineTribute = "<sprite name=\"tribute-icon\">";
    public static readonly string InlineSwordStyle = "<sprite name=\"sword-style-icon\">";
    public static readonly string InlineUtilityStyle = "<sprite name=\"utility-style-icon\">";
    public static readonly string InlineMeleeStyle = "<sprite name=\"melee-style-icon\">";
    public static readonly string InlineSpellStyle = "<sprite name=\"spell-style-icon\">";

    public enum AbilityType { Basic, Electric, Water, Fire, Nature }
    public enum Style { Utility, Sword, Spell, Melee, Shield }

    [SerializeField]
    private int auraCost = 0;
    [SerializeField]
    private int tributeCost = 0;
    [SerializeField]
    bool isSupport = false;

    public int damage;
    public int blockDamage;
    [SerializeField, TextArea]
    private string attackDescription = "";
    [SerializeField, TextArea]
    private string defendDescription = "";
    [SerializeField, TextArea]
    private string onHitDescription = "";
    [SerializeField, TextArea]
    private string description = "";
    public int limit = -1;
    public int MagicRequirement;
    public int TechRequirement;
    public int StrengthRequirement;
    public AbilityType abilityType;
    public Style style;
    public bool hasAttack;
    public bool hasDefend;

    public int AuraCost
    {
        get
        {
            return auraCost;
        }
    }
    public int TributeCost
    {
        get
        {
            return tributeCost;
        }
    }

    public bool IsSupport
    {
        get
        {
            return isSupport;
        }
    }

    public string Description
    {
        get
        {
            return description;
        }
    }

    public string AttackDescription
    {
        get
        {
            if (hasAttack && (attackDescription != "" || damage > 0))
            {
                string attack = "<b>Attack:</b> ";
                if (damage > 0)
                {
                    attack += GetDamageString();
                }

                if (attackDescription != "")
                {
                    attack += attackDescription;
                }

                return attack + "\n";
            }

            return "";
        }
    }

    public string DefendDescription
    {
        get
        {
            if (hasDefend && (defendDescription != "" || blockDamage > 0))
            {
                string defend = "<b>Defend:</b> ";
                if (blockDamage > 0)
                {
                    defend += GetBlockString();
                }

                if (defendDescription != "")
                {
                    defend += defendDescription;
                }
                return defend + "\n";
            }
            return "";
        }
    }

    public string OnHitDescription
    {
        get
        {
            if (onHitDescription != "")
            {
                return "<b>On Hit:</b> " + onHitDescription + "\n";
            }
            return "";
        }
    }

    public string FullDescription
    {
        get
        {
            return AttackDescription + DefendDescription + OnHitDescription + Description;
        }
    }

    public Ability()
    {
        type = Card.Type.AbilityCard;
    }

    private string GetDamageString()
    {
        return "Deals " + damage + " attack damage. ";
    }

    private string GetBlockString()
    {
        return "";// "This fighter gains a " + blockDamage + " damage blocker. ";
    }

    public static string StyleToInlineText(Style style)
    {
        switch (style)
        {
            case Style.Melee:
                return InlineMeleeStyle;
            case Style.Spell:
                return InlineSpellStyle;
            case Style.Sword:
                return InlineSwordStyle;
            case Style.Utility:
                return InlineUtilityStyle;
            default:
                return "";
        }
    }

}
