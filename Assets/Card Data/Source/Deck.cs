﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

[CreateAssetMenu(menuName = "Cards/Deck")]
public class Deck : ScriptableObject
{
    public Fighter[] fighters = new Fighter[3];
    public Card[] cards;
}

[System.Serializable]
public class DeckData
{
    public string[] fighters;
    public string[] cards;

    public DeckData(Deck deck)
    {
        List<string> cardList = new List<string>();
        cardList.Capacity = deck.fighters.Length;

        for (int i = 0; i < deck.fighters.Length; i++)
        {
            if (deck.fighters[i] != null)
            {
                cardList.Add(deck.fighters[i].UniqueId);
            }
        }

        fighters = cardList.ToArray();

        cardList.Clear();

        for (int i = 0; i < deck.cards.Length; i++)
        {
            if (deck.cards[i] != null)
            {
                cardList.Add(deck.cards[i].UniqueId);
            }
        }

        cards = cardList.ToArray();
    }

    public async Task<Deck> CreateDeck()
    {
        Deck deck = ScriptableObject.CreateInstance<Deck>();
        deck.cards = new Card[cards.Length];

        for (int i = 0; i < fighters.Length; i++)
        {
            var handle = Addressables.LoadAssetAsync<Fighter>(fighters[i]);
            await handle.Task;
            deck.fighters[i] = handle.Result;
        }

        for (int i = 0; i < cards.Length; i++)
        {
            var handle = Addressables.LoadAssetAsync<Card>(cards[i]);
            await handle.Task;
            deck.cards[i] = handle.Result;
        }

        return deck;
    }
}