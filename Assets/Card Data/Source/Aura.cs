﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Cards/Aura Card")]
public class Aura : Card
{
    public Aura()
    {
        type = Type.AuraCard;
        cardName = "Aura";
    }
}
