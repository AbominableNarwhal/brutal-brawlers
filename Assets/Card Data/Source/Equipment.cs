using UnityEngine;

[CreateAssetMenu(menuName = "Cards/Equipment Card")]
public class Equipment : Card
{
    [SerializeField]
    private Ability.Style fightStyle = Ability.Style.Utility;

    public int magicBonus;
    public int techBonus;
    public int strengthBonus;
    [TextArea]
    public string description;

    public Ability.Style FightStyle
    {
        get
        {
            return fightStyle;
        }
    }

    public Equipment()
    {
        type = Card.Type.EquipmentCard;
    }
}