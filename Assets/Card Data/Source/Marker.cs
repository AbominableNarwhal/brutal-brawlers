﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Cards/Marker")]
public class Marker : ScriptableObject
{
    public string Name;
    public Sprite Icon;
}
