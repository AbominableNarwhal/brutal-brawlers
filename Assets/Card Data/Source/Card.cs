﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class Card : ScriptableObject
{
    public enum Type {AuraCard, FighterCard, AbilityCard, PowerCard, EquipmentCard}
    [UniqueIdentifier, SerializeField]
    private string uniqueId = null;
    public Type type;
    public string cardName;
    public Sprite artwork;
    public GameObject cardPrefab;//TODO put this in Card component instead

    public string UniqueId
    {
        get { return uniqueId; }
    }
}
