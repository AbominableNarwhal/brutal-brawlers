﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Cards/Power Card")]
public class Power : Card, IAuraConsumer
{
    [SerializeField]
    private int auraCost = 0;

    [TextArea]
    public string description;

    public int AuraCost
    {
        get
        {
            return auraCost;
        }
    }

    protected void OnEnable()
    {
        type = Type.PowerCard;
    }
}
