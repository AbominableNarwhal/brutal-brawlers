﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Cards/Fighter Card")]
public class Fighter : Card
{
    #region Public Stats
    [SerializeField]
    private int hp;
    public int magic;
    public int tech;
    public int strength;
    public int abilitySlots;
    public int equipmentSlots;
    public Ability[] abilities;
    public Equipment[] equipment;
    #endregion

    #region Hidden Stats
    [SerializeField]
    private int tagInCost = 4;
    #endregion

    public int Hp
    {
        get { return hp; }
        set
        {
            if (value < 0)
            {
                hp = 0;
            }
            else
            {
                hp = value;
            }
        }
    }

    public int TagInCost
    {
        get
        {
            return tagInCost;
        }
        set
        {
            tagInCost = value;
        }
    }

    public Fighter()
    {
        type = Type.FighterCard;
        abilities = new Ability[0];
    }
}
